(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(factory());
}(this, (function () { 'use strict';

window.jraDebug = true;
window.toggleDebug = function () {
    window.jraDebug = !window.jraDebug;
};

/**
 * Logs an error message, only when window.jraDebug is set to true;
 * @param message
 */
function logError(message) {
    let css = "background:red;color:white; padding:4px;";
    if (window.jraDebug) {
        console.log(`%c ${ message }`, css);
    }
}

/**
 * Checks the context to ensure it has the desired extension enabled
 * @param ctx {WebGLRenderingContext} the webgl context to check
 * @param extension {String} the name of the extension to look for
 */


/**
 * Logs a warning message, only when window.jraDebug is set to true
 * @param message
 */
function logWarn(message) {
    let css = "background:yellow;color:red; padding:4px;";
    if (window.jraDebug) {
        console.log(`%c ${ message }`, css);
    }
}

/**
 * Logs a regular console.log call, only when window.jraDebug is set to true
 * @param message
 */

var RendererFormat = function (options = { width: window.innerWidth, height: window.innerHeight }) {
    this.width = options.width;
    this.height = options.height;
    this.viewportX = 0;
    this.viewportY = 0;
    this.clearColor = [0, 0, 0, 1];
};

RendererFormat.prototype = {
    /**
     * Appends the canvas to the DOM.
     * @param {node} el the element you want to append to. By default will append to body
     */
    attachToScreen: function (el = document.body) {
        el.appendChild(this.canvas);
        return this;
    },

    /**
     * Enables an attribute to become instanced, provided that the GPU supports the extension.
     * TODO re-write when WebGL 2 comes along
     * @param attributeLoc the attribute location of the attribute you want to be instanced.
     * @param divisor The divisor setting for that attribute. It is 1 by default which should essentially turn on instancing.
     */
    enableInstancedAttribute: function (attributeLoc, divisor = 1) {

        if (this.hasOwnProperty("ANGLE_instanced_arrays")) {
            let ext = this.ANGLE_instanced_arrays;
            ext.vertexAttribDivisorANGLE(attributeLoc, divisor);
        } else {
            console.warn("Current GPU does not support the ANGLE_instance_arrays extension");
        }
    },

    /**
     * Disables an attribute to become instanced.
     * TODO re-write when WebGL 2 comes along
     * @param attributeLoc the attribute location of the attribute you want to be instanced.
     */
    disableInstancedAttribute: function (attributeLoc) {
        if (this.hasOwnProperty("ANGLE_instance_arrays")) {
            let ext = this.ANGLE_instanced_arrays;
            ext.vertexAttribDivisorANGLE(attributeLoc, 0);
        } else {
            console.warn("Current GPU does not support the ANGLE_instance_arrays extension");
        }
    },
    /**
     * Runs the drawArraysInstanced command of the context. If the context is
     * webgl 1, it attempts to try and use the extension, if webgl 2, it runs the
     * regular command.
     * @param mode A GLenum specifying the type primitive to render, ie GL_TRIANGLE, etc..:
     * @param first {Number} a number specifying the starting index in the array of vector points.
     * @param count {Number} a number specifying the number of vertices
     * @param primcount {Number} a number specifying the number of instances to draw
     */
    drawInstancedArrays: function (mode, first, count, primcount) {
        if (!this.isWebGL2) {
            if (this.hasOwnProperty("ANGLE_instanced_arrays")) {
                this.ANGLE_instanced_arrays.drawArraysInstancedANGLE(mode, first, count, primcount);
            } else {
                console.error("Unable to draw instanced geometry - extension is not available");
            }
        } else {
            this.drawArraysInstanced(mode, first, count, primcount);
        }
    },

    /**
     * Sets the context to be fullscreen.
     * @param {function} customResizeCallback
     * @returns {RendererFormat}
     */
    setFullscreen: function (customResizeCallback = null) {
        let self = this;
        let gl = this;
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;

        //set the viewport size
        this.setViewport();

        if (customResizeCallback) {
            window.addEventListener("resize", customResizeCallback);
        } else {
            window.addEventListener("resize", () => {
                this.canvas.width = window.innerWidth;
                this.canvas.height = window.innerHeight;
                this.setViewport();
            });
        }
        return this;
    },

    /**
     * Helper function for clearing the screen, clear with a clear color,
     * set the viewport and clear the depth and color buffer bits
     * @param {number} r the value for the red channel of the clear color.
     * @param {number} g the value for the green channel of the clear color.
     * @param {number} b the value for the blue channel of the clear color.
     * @param {number} a the value for the alpha channel
     */
    clearScreen: function (r = 0, g = 0, b = 0, a = 1) {
        let gl = this;
        this.clearColor(r, g, b, a);
        gl.viewport(this.viewportX, this.viewportY, this.canvas.width, this.canvas.height);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        return this;
    },

    /**
     * Enable depth testing
     */
    enableDepth: function () {
        this.gl.enable(this.gl.DEPTH_TEST);
        return this;
    },

    /**
     * Disables Depth testing
     */
    disableDepth: function () {
        this.gl.disable(this.gl.DEPTH_TEST);
    },

    /**
     * Returns the maximum texture size that the current card
     * supports.
     */
    getMaxTextureSize() {
        return this.gl.getParameter(this.gl.MAX_TEXTURE_SIZE);
    },

    /**
     * Sets the viewport for the context
     * @param {number} x the x coordinate for the viewport
     * @param {number} y the y coordinate for the viewport
     * @param {number} width the width for the viewport
     * @param {number} height the height for the viewport
     */
    setViewport: function (x = 0, y = 0, width = window.innerWidth, height = window.innerHeight) {
        let gl = this;
        gl.viewport(x, y, width, height);
    }

};

/**
 * Get list of all the extensions we want for this
 * @param gl a webgl context
 * @returns {{}}
 */
function getExtensions(gl) {
    let exts = {};

    // common extensions we might want
    const extensions = ["OES_texture_float", "OES_vertex_array_object", "ANGLE_instanced_arrays", "OES_texture_half_float", "OES_texture_float_linear", "OES_texture_half_float_linear", "WEBGL_color_buffer_float", "EXT_color_buffer_half_float", "WEBGL_draw_buffers"];

    extensions.forEach(name => {
        // try getting the extension
        let ext = gl.getExtension(name);

        // if debugging is active, show warning message for any missing extensions
        if (ext === null) {
            logWarn(`Unable to get extension ${ name }, things might look weird or just plain fail`);
        }
        exts[name] = ext;
    });

    return exts;
}

/**
 * Creates a WebGLRendering context
 * @param node an optional node to build the context from. If nothing is provided, we generate a canvas
 * @param options any options for the context
 * @returns {*} the resulting WebGLRenderingContext
 */
function createContext(node = null, options = {}) {
    let el = node !== null ? node : document.createElement("canvas");
    let isWebgl2 = false;
    let defaults = {
        alpha: true,
        antialias: true,
        depth: true
    };

    // override any defaults if set
    Object.assign(options, defaults);

    // the possible context flags, try for webgl 2
    let types = ["webgl2", "experimental-webgl2", "webgl", "experimental-webgl"];

    // loop through trying different context settings.
    var ctx = types.map(type => {
        var tCtx = el.getContext(type);
        if (tCtx !== null) {
            if (type === "webgl2" || type === "experimental-webgl2") {
                isWebgl2 = true;
            }
            return tCtx;
        }
    }).filter(val => {
        if (val !== undefined) {
            return val;
        }
    });

    // make sure to note that this is a webgl 2 context
    if (isWebgl2) {
        ctx[0]["isWebGL2"] = true;
    }
    // just return 1 context
    return ctx[0];
}

/**
 * Sets up some WebGL constant values on top of the
 * window object for ease of use
 * @param gl {WebGLRenderingContext} a WebGLRenderingContext
 */
function setupConstants(gl) {
    var constants = {
        "FLOAT": gl.FLOAT,
        "UNSIGNED_BYTE": gl.UNSIGNED_BYTE,
        "UNSIGNED_SHORT": gl.UNSIGNED_SHORT,
        "ARRAY_BUFFER": gl.ARRAY_BUFFER,
        "ELEMENT_BUFFER": gl.ELEMENT_ARRAY_BUFFER,
        "RGBA": gl.RGBA,
        "RGB": gl.RGB,
        "TEXTURE_2D": gl.TEXTURE_2D,
        "STATIC_DRAW": gl.STATIC_DRAW,
        "DYNAMIC_DRAW": gl.DYNAMIC_DRAW,
        "TRIANGLES": gl.TRIANGLES,
        "TRIANGLE_STRIP": gl.TRIANGLE_STRIP,
        "POINTS": gl.POINTS,
        "UNSIGNED_SHORT": gl.UNSIGNED_SHORT,
        "FRAMEBUFFER": gl.FRAMEBUFFER,
        "COLOR_ATTACHMENT0": gl.COLOR_ATTACHMENT0,

        // texture related
        "CLAMP_TO_EDGE": gl.CLAMP_TO_EDGE,
        "LINEAR": gl.LINEAR,
        "MAG_FILTER": gl.TEXTURE_MAG_FILTER,
        "MIN_FILTER": gl.TEXTURE_MIN_FILTER,
        "WRAP_S": gl.TEXTURE_WRAP_S,
        "WRAP_T": gl.TEXTURE_WRAP_T,
        "TEXTURE0": gl.TEXTURE0,
        "TEXTURE1": gl.TEXTURE1,
        "TEXTURE2": gl.TEXTURE2,

        // some math related stuff
        "PI": 3.14149,
        "2_PI": 3.14149 * 3.14149
    };

    if (!window.GL_CONSTANTS_SET) {
        for (var i in constants) {
            window[i] = constants[i];
        }
        window.GL_CONSTANTS_SET = true;
    }
}

/**
 * Builds the WebGLRendering context
 * @param canvas {DomElement} an optional canvas, if you'd rather use one already in the DOM
 * @param ctxOptions {Object} options for the context
 * @param getCommonExtensions {Bool} include the common extensions for doing neat things in WebGL 1
 */
function createRenderer(canvas = null, ctxOptions = {}, getCommonExtensions = true) {
    let gl = createContext(canvas, ctxOptions);
    var format = new RendererFormat();
    let ext = null;

    if (getCommonExtensions) {
        ext = getExtensions(gl);
    }

    //setup constants
    setupConstants(gl);

    // assign some convenience functions onto the gl context
    var newProps = Object.assign(gl.__proto__, format.__proto__);
    gl.__proto__ = newProps;

    // loop through and assign extensions onto the context as well
    for (var i in ext) {
        gl[i] = ext[i];
    }

    return gl;
}

/**
 * Works along the same lines as thi.ng's render function
 * @param gl {WebGLRenderingContext} a WebGLRenderingContext
 * @param spec a model spec to render
 */

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var index$2 = createCommonjsModule(function (module) {
	'use strict';

	function ToObject(val) {
		if (val == null) {
			throw new TypeError('Object.assign cannot be called with null or undefined');
		}

		return Object(val);
	}

	module.exports = Object.assign || function (target, source) {
		var from;
		var keys;
		var to = ToObject(target);

		for (var s = 1; s < arguments.length; s++) {
			from = arguments[s];
			keys = Object.keys(Object(from));

			for (var i = 0; i < keys.length; i++) {
				to[keys[i]] = from[keys[i]];
			}
		}

		return to;
	};
});

var cross = createCommonjsModule(function (module) {
    module.exports = cross;

    /**
     * Computes the cross product of two vec3's
     *
     * @param {vec3} out the receiving vector
     * @param {vec3} a the first operand
     * @param {vec3} b the second operand
     * @returns {vec3} out
     */
    function cross(out, a, b) {
        var ax = a[0],
            ay = a[1],
            az = a[2],
            bx = b[0],
            by = b[1],
            bz = b[2];

        out[0] = ay * bz - az * by;
        out[1] = az * bx - ax * bz;
        out[2] = ax * by - ay * bx;
        return out;
    }
});

var dot = createCommonjsModule(function (module) {
  module.exports = dot;

  /**
   * Calculates the dot product of two vec3's
   *
   * @param {vec3} a the first operand
   * @param {vec3} b the second operand
   * @returns {Number} dot product of a and b
   */
  function dot(a, b) {
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
  }
});

var subtract = createCommonjsModule(function (module) {
  module.exports = subtract;

  /**
   * Subtracts vector b from vector a
   *
   * @param {vec3} out the receiving vector
   * @param {vec3} a the first operand
   * @param {vec3} b the second operand
   * @returns {vec3} out
   */
  function subtract(out, a, b) {
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
    return out;
  }
});

var index$6 = createCommonjsModule(function (module) {
    var cross$$1 = cross;
    var dot$$1 = dot;
    var sub = subtract;

    var EPSILON = 0.000001;
    var edge1 = [0, 0, 0];
    var edge2 = [0, 0, 0];
    var tvec = [0, 0, 0];
    var pvec = [0, 0, 0];
    var qvec = [0, 0, 0];

    module.exports = intersectTriangle;

    function intersectTriangle(out, pt, dir, tri) {
        sub(edge1, tri[1], tri[0]);
        sub(edge2, tri[2], tri[0]);

        cross$$1(pvec, dir, edge2);
        var det = dot$$1(edge1, pvec);

        if (det < EPSILON) return null;
        sub(tvec, pt, tri[0]);
        var u = dot$$1(tvec, pvec);
        if (u < 0 || u > det) return null;
        cross$$1(qvec, tvec, edge1);
        var v = dot$$1(dir, qvec);
        if (v < 0 || u + v > det) return null;

        var t = dot$$1(edge2, qvec) / det;
        out[0] = pt[0] + t * dir[0];
        out[1] = pt[1] + t * dir[1];
        out[2] = pt[2] + t * dir[2];
        return out;
    }
});

var add = createCommonjsModule(function (module) {
  module.exports = add;

  /**
   * Adds two vec3's
   *
   * @param {vec3} out the receiving vector
   * @param {vec3} a the first operand
   * @param {vec3} b the second operand
   * @returns {vec3} out
   */
  function add(out, a, b) {
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    return out;
  }
});

var scale = createCommonjsModule(function (module) {
  module.exports = scale;

  /**
   * Scales a vec3 by a scalar number
   *
   * @param {vec3} out the receiving vector
   * @param {vec3} a the vector to scale
   * @param {Number} b amount to scale the vector by
   * @returns {vec3} out
   */
  function scale(out, a, b) {
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    return out;
  }
});

var copy = createCommonjsModule(function (module) {
  module.exports = copy;

  /**
   * Copy the values from one vec3 to another
   *
   * @param {vec3} out the receiving vector
   * @param {vec3} a the source vector
   * @returns {vec3} out
   */
  function copy(out, a) {
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    return out;
  }
});

var index$8 = createCommonjsModule(function (module) {
  var dot$$1 = dot;
  var add$$1 = add;
  var scale$$1 = scale;
  var copy$$1 = copy;

  module.exports = intersectRayPlane;

  var v0 = [0, 0, 0];

  function intersectRayPlane(out, origin, direction, normal, dist) {
    var denom = dot$$1(direction, normal);
    if (denom !== 0) {
      var t = -(dot$$1(origin, normal) + dist) / denom;
      if (t < 0) {
        return null;
      }
      scale$$1(v0, direction, t);
      return add$$1(out, origin, v0);
    } else if (dot$$1(normal, origin) + dist === 0) {
      return copy$$1(out, origin);
    } else {
      return null;
    }
  }
});

var squaredDistance = createCommonjsModule(function (module) {
    module.exports = squaredDistance;

    /**
     * Calculates the squared euclidian distance between two vec3's
     *
     * @param {vec3} a the first operand
     * @param {vec3} b the second operand
     * @returns {Number} squared distance between a and b
     */
    function squaredDistance(a, b) {
        var x = b[0] - a[0],
            y = b[1] - a[1],
            z = b[2] - a[2];
        return x * x + y * y + z * z;
    }
});

var scaleAndAdd = createCommonjsModule(function (module) {
  module.exports = scaleAndAdd;

  /**
   * Adds two vec3's after scaling the second operand by a scalar value
   *
   * @param {vec3} out the receiving vector
   * @param {vec3} a the first operand
   * @param {vec3} b the second operand
   * @param {Number} scale the amount to scale b by before adding
   * @returns {vec3} out
   */
  function scaleAndAdd(out, a, b, scale) {
    out[0] = a[0] + b[0] * scale;
    out[1] = a[1] + b[1] * scale;
    out[2] = a[2] + b[2] * scale;
    return out;
  }
});

var index$10 = createCommonjsModule(function (module) {
  var squaredDist = squaredDistance;
  var dot$$1 = dot;
  var sub = subtract;
  var scaleAndAdd$$1 = scaleAndAdd;
  var scale$$1 = scale;
  var add$$1 = add;

  var tmp = [0, 0, 0];

  module.exports = intersectRaySphere;
  function intersectRaySphere(out, origin, direction, center, radius) {
    sub(tmp, center, origin);
    var len = dot$$1(direction, tmp);
    if (len < 0) {
      // sphere is behind ray
      return null;
    }

    scaleAndAdd$$1(tmp, origin, direction, len);
    var dSq = squaredDist(center, tmp);
    var rSq = radius * radius;
    if (dSq > rSq) {
      return null;
    }

    scale$$1(out, direction, len - Math.sqrt(rSq - dSq));
    return add$$1(out, out, origin);
  }
});

var index$12 = createCommonjsModule(function (module) {
  module.exports = intersection;
  module.exports.distance = distance;

  function intersection(out, ro, rd, aabb) {
    var d = distance(ro, rd, aabb);
    if (d === Infinity) {
      out = null;
    } else {
      out = out || [];
      for (var i = 0; i < ro.length; i++) {
        out[i] = ro[i] + rd[i] * d;
      }
    }

    return out;
  }

  function distance(ro, rd, aabb) {
    var dims = ro.length;
    var lo = -Infinity;
    var hi = +Infinity;

    for (var i = 0; i < dims; i++) {
      var dimLo = (aabb[0][i] - ro[i]) / rd[i];
      var dimHi = (aabb[1][i] - ro[i]) / rd[i];

      if (dimLo > dimHi) {
        var tmp = dimLo;
        dimLo = dimHi;
        dimHi = tmp;
      }

      if (dimHi < lo || dimLo > hi) {
        return Infinity;
      }

      if (dimLo > lo) lo = dimLo;
      if (dimHi < hi) hi = dimHi;
    }

    return lo > hi ? Infinity : lo;
  }
});

var index$4 = createCommonjsModule(function (module) {
  var intersectRayTriangle = index$6;
  var intersectRayPlane = index$8;
  var intersectRaySphere = index$10;
  var intersectRayBox = index$12;
  var copy3 = copy;

  var tmpTriangle = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];

  var tmp3 = [0, 0, 0];

  module.exports = Ray;
  function Ray(origin, direction) {
    this.origin = origin || [0, 0, 0];
    this.direction = direction || [0, 0, -1];
  }

  Ray.prototype.set = function (origin, direction) {
    this.origin = origin;
    this.direction = direction;
  };

  Ray.prototype.copy = function (other) {
    copy3(this.origin, other.origin);
    copy3(this.direction, other.direction);
  };

  Ray.prototype.clone = function () {
    var other = new Ray();
    other.copy(this);
    return other;
  };

  Ray.prototype.intersectsSphere = function (center, radius) {
    return intersectRaySphere(tmp3, this.origin, this.direction, center, radius);
  };

  Ray.prototype.intersectsPlane = function (normal, distance) {
    return intersectRayPlane(tmp3, this.origin, this.direction, normal, distance);
  };

  Ray.prototype.intersectsTriangle = function (triangle) {
    return intersectRayTriangle(tmp3, this.origin, this.direction, triangle);
  };

  Ray.prototype.intersectsBox = function (aabb) {
    return intersectRayBox(tmp3, this.origin, this.direction, aabb);
  };

  Ray.prototype.intersectsTriangleCell = function (cell, positions) {
    var a = cell[0],
        b = cell[1],
        c = cell[2];
    tmpTriangle[0] = positions[a];
    tmpTriangle[1] = positions[b];
    tmpTriangle[2] = positions[c];
    return this.intersectsTriangle(tmpTriangle);
  };
});

var transformMat4 = createCommonjsModule(function (module) {
  module.exports = transformMat4;

  /**
   * Transforms the vec4 with a mat4.
   *
   * @param {vec4} out the receiving vector
   * @param {vec4} a the vector to transform
   * @param {mat4} m matrix to transform with
   * @returns {vec4} out
   */
  function transformMat4(out, a, m) {
    var x = a[0],
        y = a[1],
        z = a[2],
        w = a[3];
    out[0] = m[0] * x + m[4] * y + m[8] * z + m[12] * w;
    out[1] = m[1] * x + m[5] * y + m[9] * z + m[13] * w;
    out[2] = m[2] * x + m[6] * y + m[10] * z + m[14] * w;
    out[3] = m[3] * x + m[7] * y + m[11] * z + m[15] * w;
    return out;
  }
});

var set = createCommonjsModule(function (module) {
  module.exports = set;

  /**
   * Set the components of a vec4 to the given values
   *
   * @param {vec4} out the receiving vector
   * @param {Number} x X component
   * @param {Number} y Y component
   * @param {Number} z Z component
   * @param {Number} w W component
   * @returns {vec4} out
   */
  function set(out, x, y, z, w) {
    out[0] = x;
    out[1] = y;
    out[2] = z;
    out[3] = w;
    return out;
  }
});

var index$14 = createCommonjsModule(function (module) {
  var transformMat4$$1 = transformMat4;
  var set$$1 = set;

  var NEAR_RANGE = 0;
  var FAR_RANGE = 1;
  var tmp4 = [0, 0, 0, 0];

  module.exports = cameraProject;
  function cameraProject(out, vec, viewport, combinedProjView) {
    var vX = viewport[0],
        vY = viewport[1],
        vWidth = viewport[2],
        vHeight = viewport[3],
        n = NEAR_RANGE,
        f = FAR_RANGE;

    // convert: clip space -> NDC -> window coords
    // implicit 1.0 for w component
    set$$1(tmp4, vec[0], vec[1], vec[2], 1.0);

    // transform into clip space
    transformMat4$$1(tmp4, tmp4, combinedProjView);

    // now transform into NDC
    var w = tmp4[3];
    if (w !== 0) {
      // how to handle infinity here?
      tmp4[0] = tmp4[0] / w;
      tmp4[1] = tmp4[1] / w;
      tmp4[2] = tmp4[2] / w;
    }

    // and finally into window coordinates
    // the foruth component is (1/clip.w)
    // which is the same as gl_FragCoord.w
    out[0] = vX + vWidth / 2 * tmp4[0] + (0 + vWidth / 2);
    out[1] = vY + vHeight / 2 * tmp4[1] + (0 + vHeight / 2);
    out[2] = (f - n) / 2 * tmp4[2] + (f + n) / 2;
    out[3] = w === 0 ? 0 : 1 / w;
    return out;
  }
});

var projectMat4 = createCommonjsModule(function (module) {
  module.exports = project;

  /**
   * Multiplies the input vec by the specified matrix, 
   * applying a W divide, and stores the result in out 
   * vector. This is useful for projection,
   * e.g. unprojecting a 2D point into 3D space.
   *
   * @method  prj
   * @param {vec3} out the output vector
   * @param {vec3} vec the input vector to project
   * @param {mat4} m the 4x4 matrix to multiply with 
   * @return {vec3} the out vector
   */
  function project(out, vec, m) {
    var x = vec[0],
        y = vec[1],
        z = vec[2],
        a00 = m[0],
        a01 = m[1],
        a02 = m[2],
        a03 = m[3],
        a10 = m[4],
        a11 = m[5],
        a12 = m[6],
        a13 = m[7],
        a20 = m[8],
        a21 = m[9],
        a22 = m[10],
        a23 = m[11],
        a30 = m[12],
        a31 = m[13],
        a32 = m[14],
        a33 = m[15];

    var lw = 1 / (x * a03 + y * a13 + z * a23 + a33);

    out[0] = (x * a00 + y * a10 + z * a20 + a30) * lw;
    out[1] = (x * a01 + y * a11 + z * a21 + a31) * lw;
    out[2] = (x * a02 + y * a12 + z * a22 + a32) * lw;
    return out;
  }
});

var index$16 = createCommonjsModule(function (module) {
  var transform = projectMat4;

  module.exports = unproject;

  /**
   * Unproject a point from screen space to 3D space.
   * The point should have its x and y properties set to
   * 2D screen space, and the z either at 0 (near plane)
   * or 1 (far plane). The provided matrix is assumed to already
   * be combined, i.e. projection * view.
   *
   * After this operation, the out vector's [x, y, z] components will
   * represent the unprojected 3D coordinate.
   *
   * @param  {vec3} out               the output vector
   * @param  {vec3} vec               the 2D space vector to unproject
   * @param  {vec4} viewport          screen x, y, width and height in pixels
   * @param  {mat4} invProjectionView combined projection and view matrix
   * @return {vec3}                   the output vector
   */
  function unproject(out, vec, viewport, invProjectionView) {
    var viewX = viewport[0],
        viewY = viewport[1],
        viewWidth = viewport[2],
        viewHeight = viewport[3];

    var x = vec[0],
        y = vec[1],
        z = vec[2];

    x = x - viewX;
    y = viewHeight - y - 1;
    y = y - viewY;

    out[0] = 2 * x / viewWidth - 1;
    out[1] = 2 * y / viewHeight - 1;
    out[2] = 2 * z - 1;
    return transform(out, out, invProjectionView);
  }
});

var normalize = createCommonjsModule(function (module) {
    module.exports = normalize;

    /**
     * Normalize a vec3
     *
     * @param {vec3} out the receiving vector
     * @param {vec3} a vector to normalize
     * @returns {vec3} out
     */
    function normalize(out, a) {
        var x = a[0],
            y = a[1],
            z = a[2];
        var len = x * x + y * y + z * z;
        if (len > 0) {
            //TODO: evaluate use of glm_invsqrt here?
            len = 1 / Math.sqrt(len);
            out[0] = a[0] * len;
            out[1] = a[1] * len;
            out[2] = a[2] * len;
        }
        return out;
    }
});

var cameraLookAt = createCommonjsModule(function (module) {
  // could be modularized...
  var cross$$1 = cross;
  var sub = subtract;
  var normalize$$1 = normalize;
  var copy$$1 = copy;
  var dot$$1 = dot;
  var scale$$1 = scale;

  var tmp = [0, 0, 0];
  var epsilon = 0.000000001;

  // modifies direction & up vectors in place
  module.exports = function (direction, up, position, target) {
    sub(tmp, target, position);
    normalize$$1(tmp, tmp);
    var isZero = tmp[0] === 0 && tmp[1] === 0 && tmp[2] === 0;
    if (!isZero) {
      var d = dot$$1(tmp, up);
      if (Math.abs(d - 1) < epsilon) {
        // collinear
        scale$$1(up, direction, -1);
      } else if (Math.abs(d + 1) < epsilon) {
        // collinear opposite
        copy$$1(up, direction);
      }
      copy$$1(direction, tmp);

      // normalize up vector
      cross$$1(tmp, direction, up);
      normalize$$1(tmp, tmp);

      cross$$1(up, tmp, direction);
      normalize$$1(up, up);
    }
  };
});

var set$2 = createCommonjsModule(function (module) {
  module.exports = set;

  /**
   * Set the components of a vec3 to the given values
   *
   * @param {vec3} out the receiving vector
   * @param {Number} x X component
   * @param {Number} y Y component
   * @param {Number} z Z component
   * @returns {vec3} out
   */
  function set(out, x, y, z) {
    out[0] = x;
    out[1] = y;
    out[2] = z;
    return out;
  }
});

var index$18 = createCommonjsModule(function (module) {
  var unproject = index$16;
  var set = set$2;
  var sub = subtract;
  var normalize$$1 = normalize;

  module.exports = createPickRay;
  function createPickRay(origin, direction, point, viewport, invProjView) {
    set(origin, point[0], point[1], 0);
    set(direction, point[0], point[1], 1);
    unproject(origin, origin, viewport, invProjView);
    unproject(direction, direction, viewport, invProjView);
    sub(direction, direction, origin);
    normalize$$1(direction, direction);
  }
});

var multiply = createCommonjsModule(function (module) {
    module.exports = multiply;

    /**
     * Multiplies two mat4's
     *
     * @param {mat4} out the receiving matrix
     * @param {mat4} a the first operand
     * @param {mat4} b the second operand
     * @returns {mat4} out
     */
    function multiply(out, a, b) {
        var a00 = a[0],
            a01 = a[1],
            a02 = a[2],
            a03 = a[3],
            a10 = a[4],
            a11 = a[5],
            a12 = a[6],
            a13 = a[7],
            a20 = a[8],
            a21 = a[9],
            a22 = a[10],
            a23 = a[11],
            a30 = a[12],
            a31 = a[13],
            a32 = a[14],
            a33 = a[15];

        // Cache only the current line of the second matrix
        var b0 = b[0],
            b1 = b[1],
            b2 = b[2],
            b3 = b[3];
        out[0] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
        out[1] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
        out[2] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
        out[3] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

        b0 = b[4];b1 = b[5];b2 = b[6];b3 = b[7];
        out[4] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
        out[5] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
        out[6] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
        out[7] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

        b0 = b[8];b1 = b[9];b2 = b[10];b3 = b[11];
        out[8] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
        out[9] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
        out[10] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
        out[11] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

        b0 = b[12];b1 = b[13];b2 = b[14];b3 = b[15];
        out[12] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
        out[13] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
        out[14] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
        out[15] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;
        return out;
    }
});

var invert = createCommonjsModule(function (module) {
    module.exports = invert;

    /**
     * Inverts a mat4
     *
     * @param {mat4} out the receiving matrix
     * @param {mat4} a the source matrix
     * @returns {mat4} out
     */
    function invert(out, a) {
        var a00 = a[0],
            a01 = a[1],
            a02 = a[2],
            a03 = a[3],
            a10 = a[4],
            a11 = a[5],
            a12 = a[6],
            a13 = a[7],
            a20 = a[8],
            a21 = a[9],
            a22 = a[10],
            a23 = a[11],
            a30 = a[12],
            a31 = a[13],
            a32 = a[14],
            a33 = a[15],
            b00 = a00 * a11 - a01 * a10,
            b01 = a00 * a12 - a02 * a10,
            b02 = a00 * a13 - a03 * a10,
            b03 = a01 * a12 - a02 * a11,
            b04 = a01 * a13 - a03 * a11,
            b05 = a02 * a13 - a03 * a12,
            b06 = a20 * a31 - a21 * a30,
            b07 = a20 * a32 - a22 * a30,
            b08 = a20 * a33 - a23 * a30,
            b09 = a21 * a32 - a22 * a31,
            b10 = a21 * a33 - a23 * a31,
            b11 = a22 * a33 - a23 * a32,


        // Calculate the determinant
        det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

        if (!det) {
            return null;
        }
        det = 1.0 / det;

        out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
        out[1] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
        out[2] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
        out[3] = (a22 * b04 - a21 * b05 - a23 * b03) * det;
        out[4] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
        out[5] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
        out[6] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
        out[7] = (a20 * b05 - a22 * b02 + a23 * b01) * det;
        out[8] = (a10 * b10 - a11 * b08 + a13 * b06) * det;
        out[9] = (a01 * b08 - a00 * b10 - a03 * b06) * det;
        out[10] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
        out[11] = (a21 * b02 - a20 * b04 - a23 * b00) * det;
        out[12] = (a11 * b07 - a10 * b09 - a12 * b06) * det;
        out[13] = (a00 * b09 - a01 * b07 + a02 * b06) * det;
        out[14] = (a31 * b01 - a30 * b03 - a32 * b00) * det;
        out[15] = (a20 * b03 - a21 * b01 + a22 * b00) * det;

        return out;
    }
});

var identity = createCommonjsModule(function (module) {
    module.exports = identity;

    /**
     * Set a mat4 to the identity matrix
     *
     * @param {mat4} out the receiving matrix
     * @returns {mat4} out
     */
    function identity(out) {
        out[0] = 1;
        out[1] = 0;
        out[2] = 0;
        out[3] = 0;
        out[4] = 0;
        out[5] = 1;
        out[6] = 0;
        out[7] = 0;
        out[8] = 0;
        out[9] = 0;
        out[10] = 1;
        out[11] = 0;
        out[12] = 0;
        out[13] = 0;
        out[14] = 0;
        out[15] = 1;
        return out;
    }
});

var cameraBase = createCommonjsModule(function (module) {
  var assign = index$2;
  var Ray = index$4;

  var cameraProject = index$14;
  var cameraUnproject = index$16;
  var cameraLookAt$$1 = cameraLookAt;
  var cameraPickRay = index$18;

  var add$$1 = add;
  var multiply4x4 = multiply;
  var invert4x4 = invert;
  var identity4x4 = identity;
  var setVec3 = set$2;

  // this could also be useful for a orthographic camera
  module.exports = function cameraBase(opt) {
    opt = opt || {};

    var camera = {
      projection: identity4x4([]),
      view: identity4x4([]),
      position: opt.position || [0, 0, 0],
      direction: opt.direction || [0, 0, -1],
      up: opt.up || [0, 1, 0],
      viewport: opt.viewport || [-1, -1, 1, 1],
      projView: identity4x4([]),
      invProjView: identity4x4([])
    };

    function update() {
      multiply4x4(camera.projView, camera.projection, camera.view);
      var valid = invert4x4(camera.invProjView, camera.projView);
      if (!valid) {
        throw new Error('camera projection * view is non-invertible');
      }
    }

    function lookAt(target) {
      cameraLookAt$$1(camera.direction, camera.up, camera.position, target);
      return camera;
    }

    function identity$$1() {
      setVec3(camera.position, 0, 0, 0);
      setVec3(camera.direction, 0, 0, -1);
      setVec3(camera.up, 0, 1, 0);
      identity4x4(camera.view);
      identity4x4(camera.projection);
      identity4x4(camera.projView);
      identity4x4(camera.invProjView);
      return camera;
    }

    function translate(vec) {
      add$$1(camera.position, camera.position, vec);
      return camera;
    }

    function createPickingRay(mouse) {
      var ray = new Ray();
      cameraPickRay(ray.origin, ray.direction, mouse, camera.viewport, camera.invProjView);
      return ray;
    }

    function project(point) {
      return cameraProject([], point, camera.viewport, camera.projView);
    }

    function unproject(point) {
      return cameraUnproject([], point, camera.viewport, camera.invProjView);
    }

    return assign(camera, {
      translate: translate,
      identity: identity$$1,
      lookAt: lookAt,
      createPickingRay: createPickingRay,
      update: update,
      project: project,
      unproject: unproject
    });
  };
});

var index$20 = createCommonjsModule(function (module) {
    module.exports = function () {
        for (var i = 0; i < arguments.length; i++) {
            if (arguments[i] !== undefined) return arguments[i];
        }
    };
});

var perspective = createCommonjsModule(function (module) {
    module.exports = perspective;

    /**
     * Generates a perspective projection matrix with the given bounds
     *
     * @param {mat4} out mat4 frustum matrix will be written into
     * @param {number} fovy Vertical field of view in radians
     * @param {number} aspect Aspect ratio. typically viewport width/height
     * @param {number} near Near bound of the frustum
     * @param {number} far Far bound of the frustum
     * @returns {mat4} out
     */
    function perspective(out, fovy, aspect, near, far) {
        var f = 1.0 / Math.tan(fovy / 2),
            nf = 1 / (near - far);
        out[0] = f / aspect;
        out[1] = 0;
        out[2] = 0;
        out[3] = 0;
        out[4] = 0;
        out[5] = f;
        out[6] = 0;
        out[7] = 0;
        out[8] = 0;
        out[9] = 0;
        out[10] = (far + near) * nf;
        out[11] = -1;
        out[12] = 0;
        out[13] = 0;
        out[14] = 2 * far * near * nf;
        out[15] = 0;
        return out;
    }
});

var lookAt = createCommonjsModule(function (module) {
    var identity$$1 = identity;

    module.exports = lookAt;

    /**
     * Generates a look-at matrix with the given eye position, focal point, and up axis
     *
     * @param {mat4} out mat4 frustum matrix will be written into
     * @param {vec3} eye Position of the viewer
     * @param {vec3} center Point the viewer is looking at
     * @param {vec3} up vec3 pointing up
     * @returns {mat4} out
     */
    function lookAt(out, eye, center, up) {
        var x0,
            x1,
            x2,
            y0,
            y1,
            y2,
            z0,
            z1,
            z2,
            len,
            eyex = eye[0],
            eyey = eye[1],
            eyez = eye[2],
            upx = up[0],
            upy = up[1],
            upz = up[2],
            centerx = center[0],
            centery = center[1],
            centerz = center[2];

        if (Math.abs(eyex - centerx) < 0.000001 && Math.abs(eyey - centery) < 0.000001 && Math.abs(eyez - centerz) < 0.000001) {
            return identity$$1(out);
        }

        z0 = eyex - centerx;
        z1 = eyey - centery;
        z2 = eyez - centerz;

        len = 1 / Math.sqrt(z0 * z0 + z1 * z1 + z2 * z2);
        z0 *= len;
        z1 *= len;
        z2 *= len;

        x0 = upy * z2 - upz * z1;
        x1 = upz * z0 - upx * z2;
        x2 = upx * z1 - upy * z0;
        len = Math.sqrt(x0 * x0 + x1 * x1 + x2 * x2);
        if (!len) {
            x0 = 0;
            x1 = 0;
            x2 = 0;
        } else {
            len = 1 / len;
            x0 *= len;
            x1 *= len;
            x2 *= len;
        }

        y0 = z1 * x2 - z2 * x1;
        y1 = z2 * x0 - z0 * x2;
        y2 = z0 * x1 - z1 * x0;

        len = Math.sqrt(y0 * y0 + y1 * y1 + y2 * y2);
        if (!len) {
            y0 = 0;
            y1 = 0;
            y2 = 0;
        } else {
            len = 1 / len;
            y0 *= len;
            y1 *= len;
            y2 *= len;
        }

        out[0] = x0;
        out[1] = y0;
        out[2] = z0;
        out[3] = 0;
        out[4] = x1;
        out[5] = y1;
        out[6] = z1;
        out[7] = 0;
        out[8] = x2;
        out[9] = y2;
        out[10] = z2;
        out[11] = 0;
        out[12] = -(x0 * eyex + x1 * eyey + x2 * eyez);
        out[13] = -(y0 * eyex + y1 * eyey + y2 * eyez);
        out[14] = -(z0 * eyex + z1 * eyey + z2 * eyez);
        out[15] = 1;

        return out;
    }
});

var cameraPerspective = createCommonjsModule(function (module) {
  var create = cameraBase;
  var assign = index$2;
  var defined = index$20;

  var perspective$$1 = perspective;
  var lookAt4x4 = lookAt;
  var add$$1 = add;

  module.exports = function cameraPerspective(opt) {
    opt = opt || {};

    var camera = create(opt);
    camera.fov = defined(opt.fov, Math.PI / 4);
    camera.near = defined(opt.near, 1);
    camera.far = defined(opt.far, 100);

    var center = [0, 0, 0];

    var updateCombined = camera.update;

    function update() {
      var aspect = camera.viewport[2] / camera.viewport[3];

      // build projection matrix
      perspective$$1(camera.projection, camera.fov, aspect, Math.abs(camera.near), Math.abs(camera.far));

      // build view matrix
      add$$1(center, camera.position, camera.direction);
      lookAt4x4(camera.view, camera.position, center, camera.up);

      // update projection * view and invert
      updateCombined();
      return camera;
    }

    // set it up initially from constructor options
    update();
    return assign(camera, {
      update: update
    });
  };
});

var index$1 = createCommonjsModule(function (module) {
  module.exports = cameraPerspective;
});

/* Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV.
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE. */

/**
 * @class Common utilities
 * @name glMatrix
 */
var glMatrix = {};

// Configuration Constants
glMatrix.EPSILON = 0.000001;
glMatrix.ARRAY_TYPE = typeof Float32Array !== 'undefined' ? Float32Array : Array;
glMatrix.RANDOM = Math.random;
glMatrix.ENABLE_SIMD = false;

// Capability detection
glMatrix.SIMD_AVAILABLE = glMatrix.ARRAY_TYPE === window.Float32Array && 'SIMD' in window;
glMatrix.USE_SIMD = glMatrix.ENABLE_SIMD && glMatrix.SIMD_AVAILABLE;

/**
 * Sets the type of array used when creating new vectors and matrices
 *
 * @param {Type} type Array type, such as Float32Array or Array
 */
glMatrix.setMatrixArrayType = function (type) {
  glMatrix.ARRAY_TYPE = type;
};

var degree = Math.PI / 180;

/**
 * Convert Degree To Radian
 *
 * @param {Number} a Angle in Degrees
 */
glMatrix.toRadian = function (a) {
  return a * degree;
};

/**
 * Tests whether or not the arguments have approximately the same value, within an absolute
 * or relative tolerance of glMatrix.EPSILON (an absolute tolerance is used for values less
 * than or equal to 1.0, and a relative tolerance is used for larger values)
 *
 * @param {Number} a The first number to test.
 * @param {Number} b The second number to test.
 * @returns {Boolean} True if the numbers are approximately equal, false otherwise.
 */
glMatrix.equals = function (a, b) {
  return Math.abs(a - b) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a), Math.abs(b));
};

/* Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV.
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE. */

/**
 * @class 4x4 Matrix
 * @name mat4
 */
var mat4 = {
    scalar: {},
    SIMD: {}
};

/**
 * Creates a new identity mat4
 *
 * @returns {mat4} a new 4x4 matrix
 */
mat4.create = function () {
    var out = new glMatrix.ARRAY_TYPE(16);
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a new mat4 initialized with values from an existing matrix
 *
 * @param {mat4} a matrix to clone
 * @returns {mat4} a new 4x4 matrix
 */
mat4.clone = function (a) {
    var out = new glMatrix.ARRAY_TYPE(16);
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    out[9] = a[9];
    out[10] = a[10];
    out[11] = a[11];
    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
    return out;
};

/**
 * Copy the values from one mat4 to another
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.copy = function (out, a) {
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    out[9] = a[9];
    out[10] = a[10];
    out[11] = a[11];
    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
    return out;
};

/**
 * Create a new mat4 with the given values
 *
 * @param {Number} m00 Component in column 0, row 0 position (index 0)
 * @param {Number} m01 Component in column 0, row 1 position (index 1)
 * @param {Number} m02 Component in column 0, row 2 position (index 2)
 * @param {Number} m03 Component in column 0, row 3 position (index 3)
 * @param {Number} m10 Component in column 1, row 0 position (index 4)
 * @param {Number} m11 Component in column 1, row 1 position (index 5)
 * @param {Number} m12 Component in column 1, row 2 position (index 6)
 * @param {Number} m13 Component in column 1, row 3 position (index 7)
 * @param {Number} m20 Component in column 2, row 0 position (index 8)
 * @param {Number} m21 Component in column 2, row 1 position (index 9)
 * @param {Number} m22 Component in column 2, row 2 position (index 10)
 * @param {Number} m23 Component in column 2, row 3 position (index 11)
 * @param {Number} m30 Component in column 3, row 0 position (index 12)
 * @param {Number} m31 Component in column 3, row 1 position (index 13)
 * @param {Number} m32 Component in column 3, row 2 position (index 14)
 * @param {Number} m33 Component in column 3, row 3 position (index 15)
 * @returns {mat4} A new mat4
 */
mat4.fromValues = function (m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33) {
    var out = new glMatrix.ARRAY_TYPE(16);
    out[0] = m00;
    out[1] = m01;
    out[2] = m02;
    out[3] = m03;
    out[4] = m10;
    out[5] = m11;
    out[6] = m12;
    out[7] = m13;
    out[8] = m20;
    out[9] = m21;
    out[10] = m22;
    out[11] = m23;
    out[12] = m30;
    out[13] = m31;
    out[14] = m32;
    out[15] = m33;
    return out;
};

/**
 * Set the components of a mat4 to the given values
 *
 * @param {mat4} out the receiving matrix
 * @param {Number} m00 Component in column 0, row 0 position (index 0)
 * @param {Number} m01 Component in column 0, row 1 position (index 1)
 * @param {Number} m02 Component in column 0, row 2 position (index 2)
 * @param {Number} m03 Component in column 0, row 3 position (index 3)
 * @param {Number} m10 Component in column 1, row 0 position (index 4)
 * @param {Number} m11 Component in column 1, row 1 position (index 5)
 * @param {Number} m12 Component in column 1, row 2 position (index 6)
 * @param {Number} m13 Component in column 1, row 3 position (index 7)
 * @param {Number} m20 Component in column 2, row 0 position (index 8)
 * @param {Number} m21 Component in column 2, row 1 position (index 9)
 * @param {Number} m22 Component in column 2, row 2 position (index 10)
 * @param {Number} m23 Component in column 2, row 3 position (index 11)
 * @param {Number} m30 Component in column 3, row 0 position (index 12)
 * @param {Number} m31 Component in column 3, row 1 position (index 13)
 * @param {Number} m32 Component in column 3, row 2 position (index 14)
 * @param {Number} m33 Component in column 3, row 3 position (index 15)
 * @returns {mat4} out
 */
mat4.set = function (out, m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33) {
    out[0] = m00;
    out[1] = m01;
    out[2] = m02;
    out[3] = m03;
    out[4] = m10;
    out[5] = m11;
    out[6] = m12;
    out[7] = m13;
    out[8] = m20;
    out[9] = m21;
    out[10] = m22;
    out[11] = m23;
    out[12] = m30;
    out[13] = m31;
    out[14] = m32;
    out[15] = m33;
    return out;
};

/**
 * Set a mat4 to the identity matrix
 *
 * @param {mat4} out the receiving matrix
 * @returns {mat4} out
 */
mat4.identity = function (out) {
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Transpose the values of a mat4 not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.scalar.transpose = function (out, a) {
    // If we are transposing ourselves we can skip a few steps but have to cache some values
    if (out === a) {
        var a01 = a[1],
            a02 = a[2],
            a03 = a[3],
            a12 = a[6],
            a13 = a[7],
            a23 = a[11];

        out[1] = a[4];
        out[2] = a[8];
        out[3] = a[12];
        out[4] = a01;
        out[6] = a[9];
        out[7] = a[13];
        out[8] = a02;
        out[9] = a12;
        out[11] = a[14];
        out[12] = a03;
        out[13] = a13;
        out[14] = a23;
    } else {
        out[0] = a[0];
        out[1] = a[4];
        out[2] = a[8];
        out[3] = a[12];
        out[4] = a[1];
        out[5] = a[5];
        out[6] = a[9];
        out[7] = a[13];
        out[8] = a[2];
        out[9] = a[6];
        out[10] = a[10];
        out[11] = a[14];
        out[12] = a[3];
        out[13] = a[7];
        out[14] = a[11];
        out[15] = a[15];
    }

    return out;
};

/**
 * Transpose the values of a mat4 using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.SIMD.transpose = function (out, a) {
    var a0, a1, a2, a3, tmp01, tmp23, out0, out1, out2, out3;

    a0 = SIMD.Float32x4.load(a, 0);
    a1 = SIMD.Float32x4.load(a, 4);
    a2 = SIMD.Float32x4.load(a, 8);
    a3 = SIMD.Float32x4.load(a, 12);

    tmp01 = SIMD.Float32x4.shuffle(a0, a1, 0, 1, 4, 5);
    tmp23 = SIMD.Float32x4.shuffle(a2, a3, 0, 1, 4, 5);
    out0 = SIMD.Float32x4.shuffle(tmp01, tmp23, 0, 2, 4, 6);
    out1 = SIMD.Float32x4.shuffle(tmp01, tmp23, 1, 3, 5, 7);
    SIMD.Float32x4.store(out, 0, out0);
    SIMD.Float32x4.store(out, 4, out1);

    tmp01 = SIMD.Float32x4.shuffle(a0, a1, 2, 3, 6, 7);
    tmp23 = SIMD.Float32x4.shuffle(a2, a3, 2, 3, 6, 7);
    out2 = SIMD.Float32x4.shuffle(tmp01, tmp23, 0, 2, 4, 6);
    out3 = SIMD.Float32x4.shuffle(tmp01, tmp23, 1, 3, 5, 7);
    SIMD.Float32x4.store(out, 8, out2);
    SIMD.Float32x4.store(out, 12, out3);

    return out;
};

/**
 * Transpse a mat4 using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.transpose = glMatrix.USE_SIMD ? mat4.SIMD.transpose : mat4.scalar.transpose;

/**
 * Inverts a mat4 not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.scalar.invert = function (out, a) {
    var a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11],
        a30 = a[12],
        a31 = a[13],
        a32 = a[14],
        a33 = a[15],
        b00 = a00 * a11 - a01 * a10,
        b01 = a00 * a12 - a02 * a10,
        b02 = a00 * a13 - a03 * a10,
        b03 = a01 * a12 - a02 * a11,
        b04 = a01 * a13 - a03 * a11,
        b05 = a02 * a13 - a03 * a12,
        b06 = a20 * a31 - a21 * a30,
        b07 = a20 * a32 - a22 * a30,
        b08 = a20 * a33 - a23 * a30,
        b09 = a21 * a32 - a22 * a31,
        b10 = a21 * a33 - a23 * a31,
        b11 = a22 * a33 - a23 * a32,


    // Calculate the determinant
    det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

    if (!det) {
        return null;
    }
    det = 1.0 / det;

    out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
    out[1] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
    out[2] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
    out[3] = (a22 * b04 - a21 * b05 - a23 * b03) * det;
    out[4] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
    out[5] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
    out[6] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
    out[7] = (a20 * b05 - a22 * b02 + a23 * b01) * det;
    out[8] = (a10 * b10 - a11 * b08 + a13 * b06) * det;
    out[9] = (a01 * b08 - a00 * b10 - a03 * b06) * det;
    out[10] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
    out[11] = (a21 * b02 - a20 * b04 - a23 * b00) * det;
    out[12] = (a11 * b07 - a10 * b09 - a12 * b06) * det;
    out[13] = (a00 * b09 - a01 * b07 + a02 * b06) * det;
    out[14] = (a31 * b01 - a30 * b03 - a32 * b00) * det;
    out[15] = (a20 * b03 - a21 * b01 + a22 * b00) * det;

    return out;
};

/**
 * Inverts a mat4 using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.SIMD.invert = function (out, a) {
    var row0,
        row1,
        row2,
        row3,
        tmp1,
        minor0,
        minor1,
        minor2,
        minor3,
        det,
        a0 = SIMD.Float32x4.load(a, 0),
        a1 = SIMD.Float32x4.load(a, 4),
        a2 = SIMD.Float32x4.load(a, 8),
        a3 = SIMD.Float32x4.load(a, 12);

    // Compute matrix adjugate
    tmp1 = SIMD.Float32x4.shuffle(a0, a1, 0, 1, 4, 5);
    row1 = SIMD.Float32x4.shuffle(a2, a3, 0, 1, 4, 5);
    row0 = SIMD.Float32x4.shuffle(tmp1, row1, 0, 2, 4, 6);
    row1 = SIMD.Float32x4.shuffle(row1, tmp1, 1, 3, 5, 7);
    tmp1 = SIMD.Float32x4.shuffle(a0, a1, 2, 3, 6, 7);
    row3 = SIMD.Float32x4.shuffle(a2, a3, 2, 3, 6, 7);
    row2 = SIMD.Float32x4.shuffle(tmp1, row3, 0, 2, 4, 6);
    row3 = SIMD.Float32x4.shuffle(row3, tmp1, 1, 3, 5, 7);

    tmp1 = SIMD.Float32x4.mul(row2, row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor0 = SIMD.Float32x4.mul(row1, tmp1);
    minor1 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row1, tmp1), minor0);
    minor1 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor1);
    minor1 = SIMD.Float32x4.swizzle(minor1, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(row1, row2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor0 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor0);
    minor3 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(minor0, SIMD.Float32x4.mul(row3, tmp1));
    minor3 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor3);
    minor3 = SIMD.Float32x4.swizzle(minor3, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(row1, 2, 3, 0, 1), row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    row2 = SIMD.Float32x4.swizzle(row2, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row2, tmp1), minor0);
    minor2 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(minor0, SIMD.Float32x4.mul(row2, tmp1));
    minor2 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor2);
    minor2 = SIMD.Float32x4.swizzle(minor2, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(row0, row1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor2 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor2);
    minor3 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row2, tmp1), minor3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor2 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row3, tmp1), minor2);
    minor3 = SIMD.Float32x4.sub(minor3, SIMD.Float32x4.mul(row2, tmp1));

    tmp1 = SIMD.Float32x4.mul(row0, row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor1 = SIMD.Float32x4.sub(minor1, SIMD.Float32x4.mul(row2, tmp1));
    minor2 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row1, tmp1), minor2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor1 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row2, tmp1), minor1);
    minor2 = SIMD.Float32x4.sub(minor2, SIMD.Float32x4.mul(row1, tmp1));

    tmp1 = SIMD.Float32x4.mul(row0, row2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor1 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor1);
    minor3 = SIMD.Float32x4.sub(minor3, SIMD.Float32x4.mul(row1, tmp1));
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor1 = SIMD.Float32x4.sub(minor1, SIMD.Float32x4.mul(row3, tmp1));
    minor3 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row1, tmp1), minor3);

    // Compute matrix determinant
    det = SIMD.Float32x4.mul(row0, minor0);
    det = SIMD.Float32x4.add(SIMD.Float32x4.swizzle(det, 2, 3, 0, 1), det);
    det = SIMD.Float32x4.add(SIMD.Float32x4.swizzle(det, 1, 0, 3, 2), det);
    tmp1 = SIMD.Float32x4.reciprocalApproximation(det);
    det = SIMD.Float32x4.sub(SIMD.Float32x4.add(tmp1, tmp1), SIMD.Float32x4.mul(det, SIMD.Float32x4.mul(tmp1, tmp1)));
    det = SIMD.Float32x4.swizzle(det, 0, 0, 0, 0);
    if (!det) {
        return null;
    }

    // Compute matrix inverse
    SIMD.Float32x4.store(out, 0, SIMD.Float32x4.mul(det, minor0));
    SIMD.Float32x4.store(out, 4, SIMD.Float32x4.mul(det, minor1));
    SIMD.Float32x4.store(out, 8, SIMD.Float32x4.mul(det, minor2));
    SIMD.Float32x4.store(out, 12, SIMD.Float32x4.mul(det, minor3));
    return out;
};

/**
 * Inverts a mat4 using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.invert = glMatrix.USE_SIMD ? mat4.SIMD.invert : mat4.scalar.invert;

/**
 * Calculates the adjugate of a mat4 not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.scalar.adjoint = function (out, a) {
    var a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11],
        a30 = a[12],
        a31 = a[13],
        a32 = a[14],
        a33 = a[15];

    out[0] = a11 * (a22 * a33 - a23 * a32) - a21 * (a12 * a33 - a13 * a32) + a31 * (a12 * a23 - a13 * a22);
    out[1] = -(a01 * (a22 * a33 - a23 * a32) - a21 * (a02 * a33 - a03 * a32) + a31 * (a02 * a23 - a03 * a22));
    out[2] = a01 * (a12 * a33 - a13 * a32) - a11 * (a02 * a33 - a03 * a32) + a31 * (a02 * a13 - a03 * a12);
    out[3] = -(a01 * (a12 * a23 - a13 * a22) - a11 * (a02 * a23 - a03 * a22) + a21 * (a02 * a13 - a03 * a12));
    out[4] = -(a10 * (a22 * a33 - a23 * a32) - a20 * (a12 * a33 - a13 * a32) + a30 * (a12 * a23 - a13 * a22));
    out[5] = a00 * (a22 * a33 - a23 * a32) - a20 * (a02 * a33 - a03 * a32) + a30 * (a02 * a23 - a03 * a22);
    out[6] = -(a00 * (a12 * a33 - a13 * a32) - a10 * (a02 * a33 - a03 * a32) + a30 * (a02 * a13 - a03 * a12));
    out[7] = a00 * (a12 * a23 - a13 * a22) - a10 * (a02 * a23 - a03 * a22) + a20 * (a02 * a13 - a03 * a12);
    out[8] = a10 * (a21 * a33 - a23 * a31) - a20 * (a11 * a33 - a13 * a31) + a30 * (a11 * a23 - a13 * a21);
    out[9] = -(a00 * (a21 * a33 - a23 * a31) - a20 * (a01 * a33 - a03 * a31) + a30 * (a01 * a23 - a03 * a21));
    out[10] = a00 * (a11 * a33 - a13 * a31) - a10 * (a01 * a33 - a03 * a31) + a30 * (a01 * a13 - a03 * a11);
    out[11] = -(a00 * (a11 * a23 - a13 * a21) - a10 * (a01 * a23 - a03 * a21) + a20 * (a01 * a13 - a03 * a11));
    out[12] = -(a10 * (a21 * a32 - a22 * a31) - a20 * (a11 * a32 - a12 * a31) + a30 * (a11 * a22 - a12 * a21));
    out[13] = a00 * (a21 * a32 - a22 * a31) - a20 * (a01 * a32 - a02 * a31) + a30 * (a01 * a22 - a02 * a21);
    out[14] = -(a00 * (a11 * a32 - a12 * a31) - a10 * (a01 * a32 - a02 * a31) + a30 * (a01 * a12 - a02 * a11));
    out[15] = a00 * (a11 * a22 - a12 * a21) - a10 * (a01 * a22 - a02 * a21) + a20 * (a01 * a12 - a02 * a11);
    return out;
};

/**
 * Calculates the adjugate of a mat4 using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.SIMD.adjoint = function (out, a) {
    var a0, a1, a2, a3;
    var row0, row1, row2, row3;
    var tmp1;
    var minor0, minor1, minor2, minor3;

    a0 = SIMD.Float32x4.load(a, 0);
    a1 = SIMD.Float32x4.load(a, 4);
    a2 = SIMD.Float32x4.load(a, 8);
    a3 = SIMD.Float32x4.load(a, 12);

    // Transpose the source matrix.  Sort of.  Not a true transpose operation
    tmp1 = SIMD.Float32x4.shuffle(a0, a1, 0, 1, 4, 5);
    row1 = SIMD.Float32x4.shuffle(a2, a3, 0, 1, 4, 5);
    row0 = SIMD.Float32x4.shuffle(tmp1, row1, 0, 2, 4, 6);
    row1 = SIMD.Float32x4.shuffle(row1, tmp1, 1, 3, 5, 7);

    tmp1 = SIMD.Float32x4.shuffle(a0, a1, 2, 3, 6, 7);
    row3 = SIMD.Float32x4.shuffle(a2, a3, 2, 3, 6, 7);
    row2 = SIMD.Float32x4.shuffle(tmp1, row3, 0, 2, 4, 6);
    row3 = SIMD.Float32x4.shuffle(row3, tmp1, 1, 3, 5, 7);

    tmp1 = SIMD.Float32x4.mul(row2, row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor0 = SIMD.Float32x4.mul(row1, tmp1);
    minor1 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row1, tmp1), minor0);
    minor1 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor1);
    minor1 = SIMD.Float32x4.swizzle(minor1, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(row1, row2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor0 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor0);
    minor3 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(minor0, SIMD.Float32x4.mul(row3, tmp1));
    minor3 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor3);
    minor3 = SIMD.Float32x4.swizzle(minor3, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(row1, 2, 3, 0, 1), row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    row2 = SIMD.Float32x4.swizzle(row2, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row2, tmp1), minor0);
    minor2 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(minor0, SIMD.Float32x4.mul(row2, tmp1));
    minor2 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor2);
    minor2 = SIMD.Float32x4.swizzle(minor2, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(row0, row1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor2 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor2);
    minor3 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row2, tmp1), minor3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor2 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row3, tmp1), minor2);
    minor3 = SIMD.Float32x4.sub(minor3, SIMD.Float32x4.mul(row2, tmp1));

    tmp1 = SIMD.Float32x4.mul(row0, row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor1 = SIMD.Float32x4.sub(minor1, SIMD.Float32x4.mul(row2, tmp1));
    minor2 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row1, tmp1), minor2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor1 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row2, tmp1), minor1);
    minor2 = SIMD.Float32x4.sub(minor2, SIMD.Float32x4.mul(row1, tmp1));

    tmp1 = SIMD.Float32x4.mul(row0, row2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor1 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor1);
    minor3 = SIMD.Float32x4.sub(minor3, SIMD.Float32x4.mul(row1, tmp1));
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor1 = SIMD.Float32x4.sub(minor1, SIMD.Float32x4.mul(row3, tmp1));
    minor3 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row1, tmp1), minor3);

    SIMD.Float32x4.store(out, 0, minor0);
    SIMD.Float32x4.store(out, 4, minor1);
    SIMD.Float32x4.store(out, 8, minor2);
    SIMD.Float32x4.store(out, 12, minor3);
    return out;
};

/**
 * Calculates the adjugate of a mat4 using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.adjoint = glMatrix.USE_SIMD ? mat4.SIMD.adjoint : mat4.scalar.adjoint;

/**
 * Calculates the determinant of a mat4
 *
 * @param {mat4} a the source matrix
 * @returns {Number} determinant of a
 */
mat4.determinant = function (a) {
    var a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11],
        a30 = a[12],
        a31 = a[13],
        a32 = a[14],
        a33 = a[15],
        b00 = a00 * a11 - a01 * a10,
        b01 = a00 * a12 - a02 * a10,
        b02 = a00 * a13 - a03 * a10,
        b03 = a01 * a12 - a02 * a11,
        b04 = a01 * a13 - a03 * a11,
        b05 = a02 * a13 - a03 * a12,
        b06 = a20 * a31 - a21 * a30,
        b07 = a20 * a32 - a22 * a30,
        b08 = a20 * a33 - a23 * a30,
        b09 = a21 * a32 - a22 * a31,
        b10 = a21 * a33 - a23 * a31,
        b11 = a22 * a33 - a23 * a32;

    // Calculate the determinant
    return b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;
};

/**
 * Multiplies two mat4's explicitly using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the first operand, must be a Float32Array
 * @param {mat4} b the second operand, must be a Float32Array
 * @returns {mat4} out
 */
mat4.SIMD.multiply = function (out, a, b) {
    var a0 = SIMD.Float32x4.load(a, 0);
    var a1 = SIMD.Float32x4.load(a, 4);
    var a2 = SIMD.Float32x4.load(a, 8);
    var a3 = SIMD.Float32x4.load(a, 12);

    var b0 = SIMD.Float32x4.load(b, 0);
    var out0 = SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b0, 0, 0, 0, 0), a0), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b0, 1, 1, 1, 1), a1), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b0, 2, 2, 2, 2), a2), SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b0, 3, 3, 3, 3), a3))));
    SIMD.Float32x4.store(out, 0, out0);

    var b1 = SIMD.Float32x4.load(b, 4);
    var out1 = SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b1, 0, 0, 0, 0), a0), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b1, 1, 1, 1, 1), a1), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b1, 2, 2, 2, 2), a2), SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b1, 3, 3, 3, 3), a3))));
    SIMD.Float32x4.store(out, 4, out1);

    var b2 = SIMD.Float32x4.load(b, 8);
    var out2 = SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b2, 0, 0, 0, 0), a0), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b2, 1, 1, 1, 1), a1), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b2, 2, 2, 2, 2), a2), SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b2, 3, 3, 3, 3), a3))));
    SIMD.Float32x4.store(out, 8, out2);

    var b3 = SIMD.Float32x4.load(b, 12);
    var out3 = SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b3, 0, 0, 0, 0), a0), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b3, 1, 1, 1, 1), a1), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b3, 2, 2, 2, 2), a2), SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b3, 3, 3, 3, 3), a3))));
    SIMD.Float32x4.store(out, 12, out3);

    return out;
};

/**
 * Multiplies two mat4's explicitly not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the first operand
 * @param {mat4} b the second operand
 * @returns {mat4} out
 */
mat4.scalar.multiply = function (out, a, b) {
    var a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11],
        a30 = a[12],
        a31 = a[13],
        a32 = a[14],
        a33 = a[15];

    // Cache only the current line of the second matrix
    var b0 = b[0],
        b1 = b[1],
        b2 = b[2],
        b3 = b[3];
    out[0] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[1] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[2] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[3] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

    b0 = b[4];b1 = b[5];b2 = b[6];b3 = b[7];
    out[4] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[5] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[6] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[7] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

    b0 = b[8];b1 = b[9];b2 = b[10];b3 = b[11];
    out[8] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[9] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[10] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[11] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

    b0 = b[12];b1 = b[13];b2 = b[14];b3 = b[15];
    out[12] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[13] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[14] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[15] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;
    return out;
};

/**
 * Multiplies two mat4's using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the first operand
 * @param {mat4} b the second operand
 * @returns {mat4} out
 */
mat4.multiply = glMatrix.USE_SIMD ? mat4.SIMD.multiply : mat4.scalar.multiply;

/**
 * Alias for {@link mat4.multiply}
 * @function
 */
mat4.mul = mat4.multiply;

/**
 * Translate a mat4 by the given vector not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to translate
 * @param {vec3} v vector to translate by
 * @returns {mat4} out
 */
mat4.scalar.translate = function (out, a, v) {
    var x = v[0],
        y = v[1],
        z = v[2],
        a00,
        a01,
        a02,
        a03,
        a10,
        a11,
        a12,
        a13,
        a20,
        a21,
        a22,
        a23;

    if (a === out) {
        out[12] = a[0] * x + a[4] * y + a[8] * z + a[12];
        out[13] = a[1] * x + a[5] * y + a[9] * z + a[13];
        out[14] = a[2] * x + a[6] * y + a[10] * z + a[14];
        out[15] = a[3] * x + a[7] * y + a[11] * z + a[15];
    } else {
        a00 = a[0];a01 = a[1];a02 = a[2];a03 = a[3];
        a10 = a[4];a11 = a[5];a12 = a[6];a13 = a[7];
        a20 = a[8];a21 = a[9];a22 = a[10];a23 = a[11];

        out[0] = a00;out[1] = a01;out[2] = a02;out[3] = a03;
        out[4] = a10;out[5] = a11;out[6] = a12;out[7] = a13;
        out[8] = a20;out[9] = a21;out[10] = a22;out[11] = a23;

        out[12] = a00 * x + a10 * y + a20 * z + a[12];
        out[13] = a01 * x + a11 * y + a21 * z + a[13];
        out[14] = a02 * x + a12 * y + a22 * z + a[14];
        out[15] = a03 * x + a13 * y + a23 * z + a[15];
    }

    return out;
};

/**
 * Translates a mat4 by the given vector using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to translate
 * @param {vec3} v vector to translate by
 * @returns {mat4} out
 */
mat4.SIMD.translate = function (out, a, v) {
    var a0 = SIMD.Float32x4.load(a, 0),
        a1 = SIMD.Float32x4.load(a, 4),
        a2 = SIMD.Float32x4.load(a, 8),
        a3 = SIMD.Float32x4.load(a, 12),
        vec = SIMD.Float32x4(v[0], v[1], v[2], 0);

    if (a !== out) {
        out[0] = a[0];out[1] = a[1];out[2] = a[2];out[3] = a[3];
        out[4] = a[4];out[5] = a[5];out[6] = a[6];out[7] = a[7];
        out[8] = a[8];out[9] = a[9];out[10] = a[10];out[11] = a[11];
    }

    a0 = SIMD.Float32x4.mul(a0, SIMD.Float32x4.swizzle(vec, 0, 0, 0, 0));
    a1 = SIMD.Float32x4.mul(a1, SIMD.Float32x4.swizzle(vec, 1, 1, 1, 1));
    a2 = SIMD.Float32x4.mul(a2, SIMD.Float32x4.swizzle(vec, 2, 2, 2, 2));

    var t0 = SIMD.Float32x4.add(a0, SIMD.Float32x4.add(a1, SIMD.Float32x4.add(a2, a3)));
    SIMD.Float32x4.store(out, 12, t0);

    return out;
};

/**
 * Translates a mat4 by the given vector using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to translate
 * @param {vec3} v vector to translate by
 * @returns {mat4} out
 */
mat4.translate = glMatrix.USE_SIMD ? mat4.SIMD.translate : mat4.scalar.translate;

/**
 * Scales the mat4 by the dimensions in the given vec3 not using vectorization
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to scale
 * @param {vec3} v the vec3 to scale the matrix by
 * @returns {mat4} out
 **/
mat4.scalar.scale = function (out, a, v) {
    var x = v[0],
        y = v[1],
        z = v[2];

    out[0] = a[0] * x;
    out[1] = a[1] * x;
    out[2] = a[2] * x;
    out[3] = a[3] * x;
    out[4] = a[4] * y;
    out[5] = a[5] * y;
    out[6] = a[6] * y;
    out[7] = a[7] * y;
    out[8] = a[8] * z;
    out[9] = a[9] * z;
    out[10] = a[10] * z;
    out[11] = a[11] * z;
    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
    return out;
};

/**
 * Scales the mat4 by the dimensions in the given vec3 using vectorization
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to scale
 * @param {vec3} v the vec3 to scale the matrix by
 * @returns {mat4} out
 **/
mat4.SIMD.scale = function (out, a, v) {
    var a0, a1, a2;
    var vec = SIMD.Float32x4(v[0], v[1], v[2], 0);

    a0 = SIMD.Float32x4.load(a, 0);
    SIMD.Float32x4.store(out, 0, SIMD.Float32x4.mul(a0, SIMD.Float32x4.swizzle(vec, 0, 0, 0, 0)));

    a1 = SIMD.Float32x4.load(a, 4);
    SIMD.Float32x4.store(out, 4, SIMD.Float32x4.mul(a1, SIMD.Float32x4.swizzle(vec, 1, 1, 1, 1)));

    a2 = SIMD.Float32x4.load(a, 8);
    SIMD.Float32x4.store(out, 8, SIMD.Float32x4.mul(a2, SIMD.Float32x4.swizzle(vec, 2, 2, 2, 2)));

    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
    return out;
};

/**
 * Scales the mat4 by the dimensions in the given vec3 using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to scale
 * @param {vec3} v the vec3 to scale the matrix by
 * @returns {mat4} out
 */
mat4.scale = glMatrix.USE_SIMD ? mat4.SIMD.scale : mat4.scalar.scale;

/**
 * Rotates a mat4 by the given angle around the given axis
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @param {vec3} axis the axis to rotate around
 * @returns {mat4} out
 */
mat4.rotate = function (out, a, rad, axis) {
    var x = axis[0],
        y = axis[1],
        z = axis[2],
        len = Math.sqrt(x * x + y * y + z * z),
        s,
        c,
        t,
        a00,
        a01,
        a02,
        a03,
        a10,
        a11,
        a12,
        a13,
        a20,
        a21,
        a22,
        a23,
        b00,
        b01,
        b02,
        b10,
        b11,
        b12,
        b20,
        b21,
        b22;

    if (Math.abs(len) < glMatrix.EPSILON) {
        return null;
    }

    len = 1 / len;
    x *= len;
    y *= len;
    z *= len;

    s = Math.sin(rad);
    c = Math.cos(rad);
    t = 1 - c;

    a00 = a[0];a01 = a[1];a02 = a[2];a03 = a[3];
    a10 = a[4];a11 = a[5];a12 = a[6];a13 = a[7];
    a20 = a[8];a21 = a[9];a22 = a[10];a23 = a[11];

    // Construct the elements of the rotation matrix
    b00 = x * x * t + c;b01 = y * x * t + z * s;b02 = z * x * t - y * s;
    b10 = x * y * t - z * s;b11 = y * y * t + c;b12 = z * y * t + x * s;
    b20 = x * z * t + y * s;b21 = y * z * t - x * s;b22 = z * z * t + c;

    // Perform rotation-specific matrix multiplication
    out[0] = a00 * b00 + a10 * b01 + a20 * b02;
    out[1] = a01 * b00 + a11 * b01 + a21 * b02;
    out[2] = a02 * b00 + a12 * b01 + a22 * b02;
    out[3] = a03 * b00 + a13 * b01 + a23 * b02;
    out[4] = a00 * b10 + a10 * b11 + a20 * b12;
    out[5] = a01 * b10 + a11 * b11 + a21 * b12;
    out[6] = a02 * b10 + a12 * b11 + a22 * b12;
    out[7] = a03 * b10 + a13 * b11 + a23 * b12;
    out[8] = a00 * b20 + a10 * b21 + a20 * b22;
    out[9] = a01 * b20 + a11 * b21 + a21 * b22;
    out[10] = a02 * b20 + a12 * b21 + a22 * b22;
    out[11] = a03 * b20 + a13 * b21 + a23 * b22;

    if (a !== out) {
        // If the source and destination differ, copy the unchanged last row
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }
    return out;
};

/**
 * Rotates a matrix by the given angle around the X axis not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.scalar.rotateX = function (out, a, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad),
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11];

    if (a !== out) {
        // If the source and destination differ, copy the unchanged rows
        out[0] = a[0];
        out[1] = a[1];
        out[2] = a[2];
        out[3] = a[3];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    out[4] = a10 * c + a20 * s;
    out[5] = a11 * c + a21 * s;
    out[6] = a12 * c + a22 * s;
    out[7] = a13 * c + a23 * s;
    out[8] = a20 * c - a10 * s;
    out[9] = a21 * c - a11 * s;
    out[10] = a22 * c - a12 * s;
    out[11] = a23 * c - a13 * s;
    return out;
};

/**
 * Rotates a matrix by the given angle around the X axis using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.SIMD.rotateX = function (out, a, rad) {
    var s = SIMD.Float32x4.splat(Math.sin(rad)),
        c = SIMD.Float32x4.splat(Math.cos(rad));

    if (a !== out) {
        // If the source and destination differ, copy the unchanged rows
        out[0] = a[0];
        out[1] = a[1];
        out[2] = a[2];
        out[3] = a[3];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    var a_1 = SIMD.Float32x4.load(a, 4);
    var a_2 = SIMD.Float32x4.load(a, 8);
    SIMD.Float32x4.store(out, 4, SIMD.Float32x4.add(SIMD.Float32x4.mul(a_1, c), SIMD.Float32x4.mul(a_2, s)));
    SIMD.Float32x4.store(out, 8, SIMD.Float32x4.sub(SIMD.Float32x4.mul(a_2, c), SIMD.Float32x4.mul(a_1, s)));
    return out;
};

/**
 * Rotates a matrix by the given angle around the X axis using SIMD if availabe and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.rotateX = glMatrix.USE_SIMD ? mat4.SIMD.rotateX : mat4.scalar.rotateX;

/**
 * Rotates a matrix by the given angle around the Y axis not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.scalar.rotateY = function (out, a, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad),
        a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11];

    if (a !== out) {
        // If the source and destination differ, copy the unchanged rows
        out[4] = a[4];
        out[5] = a[5];
        out[6] = a[6];
        out[7] = a[7];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    out[0] = a00 * c - a20 * s;
    out[1] = a01 * c - a21 * s;
    out[2] = a02 * c - a22 * s;
    out[3] = a03 * c - a23 * s;
    out[8] = a00 * s + a20 * c;
    out[9] = a01 * s + a21 * c;
    out[10] = a02 * s + a22 * c;
    out[11] = a03 * s + a23 * c;
    return out;
};

/**
 * Rotates a matrix by the given angle around the Y axis using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.SIMD.rotateY = function (out, a, rad) {
    var s = SIMD.Float32x4.splat(Math.sin(rad)),
        c = SIMD.Float32x4.splat(Math.cos(rad));

    if (a !== out) {
        // If the source and destination differ, copy the unchanged rows
        out[4] = a[4];
        out[5] = a[5];
        out[6] = a[6];
        out[7] = a[7];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    var a_0 = SIMD.Float32x4.load(a, 0);
    var a_2 = SIMD.Float32x4.load(a, 8);
    SIMD.Float32x4.store(out, 0, SIMD.Float32x4.sub(SIMD.Float32x4.mul(a_0, c), SIMD.Float32x4.mul(a_2, s)));
    SIMD.Float32x4.store(out, 8, SIMD.Float32x4.add(SIMD.Float32x4.mul(a_0, s), SIMD.Float32x4.mul(a_2, c)));
    return out;
};

/**
 * Rotates a matrix by the given angle around the Y axis if SIMD available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.rotateY = glMatrix.USE_SIMD ? mat4.SIMD.rotateY : mat4.scalar.rotateY;

/**
 * Rotates a matrix by the given angle around the Z axis not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.scalar.rotateZ = function (out, a, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad),
        a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7];

    if (a !== out) {
        // If the source and destination differ, copy the unchanged last row
        out[8] = a[8];
        out[9] = a[9];
        out[10] = a[10];
        out[11] = a[11];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    out[0] = a00 * c + a10 * s;
    out[1] = a01 * c + a11 * s;
    out[2] = a02 * c + a12 * s;
    out[3] = a03 * c + a13 * s;
    out[4] = a10 * c - a00 * s;
    out[5] = a11 * c - a01 * s;
    out[6] = a12 * c - a02 * s;
    out[7] = a13 * c - a03 * s;
    return out;
};

/**
 * Rotates a matrix by the given angle around the Z axis using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.SIMD.rotateZ = function (out, a, rad) {
    var s = SIMD.Float32x4.splat(Math.sin(rad)),
        c = SIMD.Float32x4.splat(Math.cos(rad));

    if (a !== out) {
        // If the source and destination differ, copy the unchanged last row
        out[8] = a[8];
        out[9] = a[9];
        out[10] = a[10];
        out[11] = a[11];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    var a_0 = SIMD.Float32x4.load(a, 0);
    var a_1 = SIMD.Float32x4.load(a, 4);
    SIMD.Float32x4.store(out, 0, SIMD.Float32x4.add(SIMD.Float32x4.mul(a_0, c), SIMD.Float32x4.mul(a_1, s)));
    SIMD.Float32x4.store(out, 4, SIMD.Float32x4.sub(SIMD.Float32x4.mul(a_1, c), SIMD.Float32x4.mul(a_0, s)));
    return out;
};

/**
 * Rotates a matrix by the given angle around the Z axis if SIMD available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.rotateZ = glMatrix.USE_SIMD ? mat4.SIMD.rotateZ : mat4.scalar.rotateZ;

/**
 * Creates a matrix from a vector translation
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.translate(dest, dest, vec);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {vec3} v Translation vector
 * @returns {mat4} out
 */
mat4.fromTranslation = function (out, v) {
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = v[0];
    out[13] = v[1];
    out[14] = v[2];
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from a vector scaling
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.scale(dest, dest, vec);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {vec3} v Scaling vector
 * @returns {mat4} out
 */
mat4.fromScaling = function (out, v) {
    out[0] = v[0];
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = v[1];
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = v[2];
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from a given angle around a given axis
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.rotate(dest, dest, rad, axis);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {Number} rad the angle to rotate the matrix by
 * @param {vec3} axis the axis to rotate around
 * @returns {mat4} out
 */
mat4.fromRotation = function (out, rad, axis) {
    var x = axis[0],
        y = axis[1],
        z = axis[2],
        len = Math.sqrt(x * x + y * y + z * z),
        s,
        c,
        t;

    if (Math.abs(len) < glMatrix.EPSILON) {
        return null;
    }

    len = 1 / len;
    x *= len;
    y *= len;
    z *= len;

    s = Math.sin(rad);
    c = Math.cos(rad);
    t = 1 - c;

    // Perform rotation-specific matrix multiplication
    out[0] = x * x * t + c;
    out[1] = y * x * t + z * s;
    out[2] = z * x * t - y * s;
    out[3] = 0;
    out[4] = x * y * t - z * s;
    out[5] = y * y * t + c;
    out[6] = z * y * t + x * s;
    out[7] = 0;
    out[8] = x * z * t + y * s;
    out[9] = y * z * t - x * s;
    out[10] = z * z * t + c;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from the given angle around the X axis
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.rotateX(dest, dest, rad);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.fromXRotation = function (out, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad);

    // Perform axis-specific matrix multiplication
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = c;
    out[6] = s;
    out[7] = 0;
    out[8] = 0;
    out[9] = -s;
    out[10] = c;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from the given angle around the Y axis
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.rotateY(dest, dest, rad);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.fromYRotation = function (out, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad);

    // Perform axis-specific matrix multiplication
    out[0] = c;
    out[1] = 0;
    out[2] = -s;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = s;
    out[9] = 0;
    out[10] = c;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from the given angle around the Z axis
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.rotateZ(dest, dest, rad);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.fromZRotation = function (out, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad);

    // Perform axis-specific matrix multiplication
    out[0] = c;
    out[1] = s;
    out[2] = 0;
    out[3] = 0;
    out[4] = -s;
    out[5] = c;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from a quaternion rotation and vector translation
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.translate(dest, vec);
 *     var quatMat = mat4.create();
 *     quat4.toMat4(quat, quatMat);
 *     mat4.multiply(dest, quatMat);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {quat4} q Rotation quaternion
 * @param {vec3} v Translation vector
 * @returns {mat4} out
 */
mat4.fromRotationTranslation = function (out, q, v) {
    // Quaternion math
    var x = q[0],
        y = q[1],
        z = q[2],
        w = q[3],
        x2 = x + x,
        y2 = y + y,
        z2 = z + z,
        xx = x * x2,
        xy = x * y2,
        xz = x * z2,
        yy = y * y2,
        yz = y * z2,
        zz = z * z2,
        wx = w * x2,
        wy = w * y2,
        wz = w * z2;

    out[0] = 1 - (yy + zz);
    out[1] = xy + wz;
    out[2] = xz - wy;
    out[3] = 0;
    out[4] = xy - wz;
    out[5] = 1 - (xx + zz);
    out[6] = yz + wx;
    out[7] = 0;
    out[8] = xz + wy;
    out[9] = yz - wx;
    out[10] = 1 - (xx + yy);
    out[11] = 0;
    out[12] = v[0];
    out[13] = v[1];
    out[14] = v[2];
    out[15] = 1;

    return out;
};

/**
 * Returns the translation vector component of a transformation
 *  matrix. If a matrix is built with fromRotationTranslation,
 *  the returned vector will be the same as the translation vector
 *  originally supplied.
 * @param  {vec3} out Vector to receive translation component
 * @param  {mat4} mat Matrix to be decomposed (input)
 * @return {vec3} out
 */
mat4.getTranslation = function (out, mat) {
    out[0] = mat[12];
    out[1] = mat[13];
    out[2] = mat[14];

    return out;
};

/**
 * Returns a quaternion representing the rotational component
 *  of a transformation matrix. If a matrix is built with
 *  fromRotationTranslation, the returned quaternion will be the
 *  same as the quaternion originally supplied.
 * @param {quat} out Quaternion to receive the rotation component
 * @param {mat4} mat Matrix to be decomposed (input)
 * @return {quat} out
 */
mat4.getRotation = function (out, mat) {
    // Algorithm taken from http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
    var trace = mat[0] + mat[5] + mat[10];
    var S = 0;

    if (trace > 0) {
        S = Math.sqrt(trace + 1.0) * 2;
        out[3] = 0.25 * S;
        out[0] = (mat[6] - mat[9]) / S;
        out[1] = (mat[8] - mat[2]) / S;
        out[2] = (mat[1] - mat[4]) / S;
    } else if (mat[0] > mat[5] & mat[0] > mat[10]) {
        S = Math.sqrt(1.0 + mat[0] - mat[5] - mat[10]) * 2;
        out[3] = (mat[6] - mat[9]) / S;
        out[0] = 0.25 * S;
        out[1] = (mat[1] + mat[4]) / S;
        out[2] = (mat[8] + mat[2]) / S;
    } else if (mat[5] > mat[10]) {
        S = Math.sqrt(1.0 + mat[5] - mat[0] - mat[10]) * 2;
        out[3] = (mat[8] - mat[2]) / S;
        out[0] = (mat[1] + mat[4]) / S;
        out[1] = 0.25 * S;
        out[2] = (mat[6] + mat[9]) / S;
    } else {
        S = Math.sqrt(1.0 + mat[10] - mat[0] - mat[5]) * 2;
        out[3] = (mat[1] - mat[4]) / S;
        out[0] = (mat[8] + mat[2]) / S;
        out[1] = (mat[6] + mat[9]) / S;
        out[2] = 0.25 * S;
    }

    return out;
};

/**
 * Creates a matrix from a quaternion rotation, vector translation and vector scale
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.translate(dest, vec);
 *     var quatMat = mat4.create();
 *     quat4.toMat4(quat, quatMat);
 *     mat4.multiply(dest, quatMat);
 *     mat4.scale(dest, scale)
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {quat4} q Rotation quaternion
 * @param {vec3} v Translation vector
 * @param {vec3} s Scaling vector
 * @returns {mat4} out
 */
mat4.fromRotationTranslationScale = function (out, q, v, s) {
    // Quaternion math
    var x = q[0],
        y = q[1],
        z = q[2],
        w = q[3],
        x2 = x + x,
        y2 = y + y,
        z2 = z + z,
        xx = x * x2,
        xy = x * y2,
        xz = x * z2,
        yy = y * y2,
        yz = y * z2,
        zz = z * z2,
        wx = w * x2,
        wy = w * y2,
        wz = w * z2,
        sx = s[0],
        sy = s[1],
        sz = s[2];

    out[0] = (1 - (yy + zz)) * sx;
    out[1] = (xy + wz) * sx;
    out[2] = (xz - wy) * sx;
    out[3] = 0;
    out[4] = (xy - wz) * sy;
    out[5] = (1 - (xx + zz)) * sy;
    out[6] = (yz + wx) * sy;
    out[7] = 0;
    out[8] = (xz + wy) * sz;
    out[9] = (yz - wx) * sz;
    out[10] = (1 - (xx + yy)) * sz;
    out[11] = 0;
    out[12] = v[0];
    out[13] = v[1];
    out[14] = v[2];
    out[15] = 1;

    return out;
};

/**
 * Creates a matrix from a quaternion rotation, vector translation and vector scale, rotating and scaling around the given origin
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.translate(dest, vec);
 *     mat4.translate(dest, origin);
 *     var quatMat = mat4.create();
 *     quat4.toMat4(quat, quatMat);
 *     mat4.multiply(dest, quatMat);
 *     mat4.scale(dest, scale)
 *     mat4.translate(dest, negativeOrigin);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {quat4} q Rotation quaternion
 * @param {vec3} v Translation vector
 * @param {vec3} s Scaling vector
 * @param {vec3} o The origin vector around which to scale and rotate
 * @returns {mat4} out
 */
mat4.fromRotationTranslationScaleOrigin = function (out, q, v, s, o) {
    // Quaternion math
    var x = q[0],
        y = q[1],
        z = q[2],
        w = q[3],
        x2 = x + x,
        y2 = y + y,
        z2 = z + z,
        xx = x * x2,
        xy = x * y2,
        xz = x * z2,
        yy = y * y2,
        yz = y * z2,
        zz = z * z2,
        wx = w * x2,
        wy = w * y2,
        wz = w * z2,
        sx = s[0],
        sy = s[1],
        sz = s[2],
        ox = o[0],
        oy = o[1],
        oz = o[2];

    out[0] = (1 - (yy + zz)) * sx;
    out[1] = (xy + wz) * sx;
    out[2] = (xz - wy) * sx;
    out[3] = 0;
    out[4] = (xy - wz) * sy;
    out[5] = (1 - (xx + zz)) * sy;
    out[6] = (yz + wx) * sy;
    out[7] = 0;
    out[8] = (xz + wy) * sz;
    out[9] = (yz - wx) * sz;
    out[10] = (1 - (xx + yy)) * sz;
    out[11] = 0;
    out[12] = v[0] + ox - (out[0] * ox + out[4] * oy + out[8] * oz);
    out[13] = v[1] + oy - (out[1] * ox + out[5] * oy + out[9] * oz);
    out[14] = v[2] + oz - (out[2] * ox + out[6] * oy + out[10] * oz);
    out[15] = 1;

    return out;
};

/**
 * Calculates a 4x4 matrix from the given quaternion
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {quat} q Quaternion to create matrix from
 *
 * @returns {mat4} out
 */
mat4.fromQuat = function (out, q) {
    var x = q[0],
        y = q[1],
        z = q[2],
        w = q[3],
        x2 = x + x,
        y2 = y + y,
        z2 = z + z,
        xx = x * x2,
        yx = y * x2,
        yy = y * y2,
        zx = z * x2,
        zy = z * y2,
        zz = z * z2,
        wx = w * x2,
        wy = w * y2,
        wz = w * z2;

    out[0] = 1 - yy - zz;
    out[1] = yx + wz;
    out[2] = zx - wy;
    out[3] = 0;

    out[4] = yx - wz;
    out[5] = 1 - xx - zz;
    out[6] = zy + wx;
    out[7] = 0;

    out[8] = zx + wy;
    out[9] = zy - wx;
    out[10] = 1 - xx - yy;
    out[11] = 0;

    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;

    return out;
};

/**
 * Generates a frustum matrix with the given bounds
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {Number} left Left bound of the frustum
 * @param {Number} right Right bound of the frustum
 * @param {Number} bottom Bottom bound of the frustum
 * @param {Number} top Top bound of the frustum
 * @param {Number} near Near bound of the frustum
 * @param {Number} far Far bound of the frustum
 * @returns {mat4} out
 */
mat4.frustum = function (out, left, right, bottom, top, near, far) {
    var rl = 1 / (right - left),
        tb = 1 / (top - bottom),
        nf = 1 / (near - far);
    out[0] = near * 2 * rl;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = near * 2 * tb;
    out[6] = 0;
    out[7] = 0;
    out[8] = (right + left) * rl;
    out[9] = (top + bottom) * tb;
    out[10] = (far + near) * nf;
    out[11] = -1;
    out[12] = 0;
    out[13] = 0;
    out[14] = far * near * 2 * nf;
    out[15] = 0;
    return out;
};

/**
 * Generates a perspective projection matrix with the given bounds
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {number} fovy Vertical field of view in radians
 * @param {number} aspect Aspect ratio. typically viewport width/height
 * @param {number} near Near bound of the frustum
 * @param {number} far Far bound of the frustum
 * @returns {mat4} out
 */
mat4.perspective = function (out, fovy, aspect, near, far) {
    var f = 1.0 / Math.tan(fovy / 2),
        nf = 1 / (near - far);
    out[0] = f / aspect;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = f;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = (far + near) * nf;
    out[11] = -1;
    out[12] = 0;
    out[13] = 0;
    out[14] = 2 * far * near * nf;
    out[15] = 0;
    return out;
};

/**
 * Generates a perspective projection matrix with the given field of view.
 * This is primarily useful for generating projection matrices to be used
 * with the still experiemental WebVR API.
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {Object} fov Object containing the following values: upDegrees, downDegrees, leftDegrees, rightDegrees
 * @param {number} near Near bound of the frustum
 * @param {number} far Far bound of the frustum
 * @returns {mat4} out
 */
mat4.perspectiveFromFieldOfView = function (out, fov, near, far) {
    var upTan = Math.tan(fov.upDegrees * Math.PI / 180.0),
        downTan = Math.tan(fov.downDegrees * Math.PI / 180.0),
        leftTan = Math.tan(fov.leftDegrees * Math.PI / 180.0),
        rightTan = Math.tan(fov.rightDegrees * Math.PI / 180.0),
        xScale = 2.0 / (leftTan + rightTan),
        yScale = 2.0 / (upTan + downTan);

    out[0] = xScale;
    out[1] = 0.0;
    out[2] = 0.0;
    out[3] = 0.0;
    out[4] = 0.0;
    out[5] = yScale;
    out[6] = 0.0;
    out[7] = 0.0;
    out[8] = -((leftTan - rightTan) * xScale * 0.5);
    out[9] = (upTan - downTan) * yScale * 0.5;
    out[10] = far / (near - far);
    out[11] = -1.0;
    out[12] = 0.0;
    out[13] = 0.0;
    out[14] = far * near / (near - far);
    out[15] = 0.0;
    return out;
};

/**
 * Generates a orthogonal projection matrix with the given bounds
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {number} left Left bound of the frustum
 * @param {number} right Right bound of the frustum
 * @param {number} bottom Bottom bound of the frustum
 * @param {number} top Top bound of the frustum
 * @param {number} near Near bound of the frustum
 * @param {number} far Far bound of the frustum
 * @returns {mat4} out
 */
mat4.ortho = function (out, left, right, bottom, top, near, far) {
    var lr = 1 / (left - right),
        bt = 1 / (bottom - top),
        nf = 1 / (near - far);
    out[0] = -2 * lr;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = -2 * bt;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 2 * nf;
    out[11] = 0;
    out[12] = (left + right) * lr;
    out[13] = (top + bottom) * bt;
    out[14] = (far + near) * nf;
    out[15] = 1;
    return out;
};

/**
 * Generates a look-at matrix with the given eye position, focal point, and up axis
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {vec3} eye Position of the viewer
 * @param {vec3} center Point the viewer is looking at
 * @param {vec3} up vec3 pointing up
 * @returns {mat4} out
 */
mat4.lookAt = function (out, eye, center, up) {
    var x0,
        x1,
        x2,
        y0,
        y1,
        y2,
        z0,
        z1,
        z2,
        len,
        eyex = eye[0],
        eyey = eye[1],
        eyez = eye[2],
        upx = up[0],
        upy = up[1],
        upz = up[2],
        centerx = center[0],
        centery = center[1],
        centerz = center[2];

    if (Math.abs(eyex - centerx) < glMatrix.EPSILON && Math.abs(eyey - centery) < glMatrix.EPSILON && Math.abs(eyez - centerz) < glMatrix.EPSILON) {
        return mat4.identity(out);
    }

    z0 = eyex - centerx;
    z1 = eyey - centery;
    z2 = eyez - centerz;

    len = 1 / Math.sqrt(z0 * z0 + z1 * z1 + z2 * z2);
    z0 *= len;
    z1 *= len;
    z2 *= len;

    x0 = upy * z2 - upz * z1;
    x1 = upz * z0 - upx * z2;
    x2 = upx * z1 - upy * z0;
    len = Math.sqrt(x0 * x0 + x1 * x1 + x2 * x2);
    if (!len) {
        x0 = 0;
        x1 = 0;
        x2 = 0;
    } else {
        len = 1 / len;
        x0 *= len;
        x1 *= len;
        x2 *= len;
    }

    y0 = z1 * x2 - z2 * x1;
    y1 = z2 * x0 - z0 * x2;
    y2 = z0 * x1 - z1 * x0;

    len = Math.sqrt(y0 * y0 + y1 * y1 + y2 * y2);
    if (!len) {
        y0 = 0;
        y1 = 0;
        y2 = 0;
    } else {
        len = 1 / len;
        y0 *= len;
        y1 *= len;
        y2 *= len;
    }

    out[0] = x0;
    out[1] = y0;
    out[2] = z0;
    out[3] = 0;
    out[4] = x1;
    out[5] = y1;
    out[6] = z1;
    out[7] = 0;
    out[8] = x2;
    out[9] = y2;
    out[10] = z2;
    out[11] = 0;
    out[12] = -(x0 * eyex + x1 * eyey + x2 * eyez);
    out[13] = -(y0 * eyex + y1 * eyey + y2 * eyez);
    out[14] = -(z0 * eyex + z1 * eyey + z2 * eyez);
    out[15] = 1;

    return out;
};

/**
 * Returns a string representation of a mat4
 *
 * @param {mat4} a matrix to represent as a string
 * @returns {String} string representation of the matrix
 */
mat4.str = function (a) {
    return 'mat4(' + a[0] + ', ' + a[1] + ', ' + a[2] + ', ' + a[3] + ', ' + a[4] + ', ' + a[5] + ', ' + a[6] + ', ' + a[7] + ', ' + a[8] + ', ' + a[9] + ', ' + a[10] + ', ' + a[11] + ', ' + a[12] + ', ' + a[13] + ', ' + a[14] + ', ' + a[15] + ')';
};

/**
 * Returns Frobenius norm of a mat4
 *
 * @param {mat4} a the matrix to calculate Frobenius norm of
 * @returns {Number} Frobenius norm
 */
mat4.frob = function (a) {
    return Math.sqrt(Math.pow(a[0], 2) + Math.pow(a[1], 2) + Math.pow(a[2], 2) + Math.pow(a[3], 2) + Math.pow(a[4], 2) + Math.pow(a[5], 2) + Math.pow(a[6], 2) + Math.pow(a[7], 2) + Math.pow(a[8], 2) + Math.pow(a[9], 2) + Math.pow(a[10], 2) + Math.pow(a[11], 2) + Math.pow(a[12], 2) + Math.pow(a[13], 2) + Math.pow(a[14], 2) + Math.pow(a[15], 2));
};

/**
 * Adds two mat4's
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the first operand
 * @param {mat4} b the second operand
 * @returns {mat4} out
 */
mat4.add = function (out, a, b) {
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    out[3] = a[3] + b[3];
    out[4] = a[4] + b[4];
    out[5] = a[5] + b[5];
    out[6] = a[6] + b[6];
    out[7] = a[7] + b[7];
    out[8] = a[8] + b[8];
    out[9] = a[9] + b[9];
    out[10] = a[10] + b[10];
    out[11] = a[11] + b[11];
    out[12] = a[12] + b[12];
    out[13] = a[13] + b[13];
    out[14] = a[14] + b[14];
    out[15] = a[15] + b[15];
    return out;
};

/**
 * Subtracts matrix b from matrix a
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the first operand
 * @param {mat4} b the second operand
 * @returns {mat4} out
 */
mat4.subtract = function (out, a, b) {
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
    out[3] = a[3] - b[3];
    out[4] = a[4] - b[4];
    out[5] = a[5] - b[5];
    out[6] = a[6] - b[6];
    out[7] = a[7] - b[7];
    out[8] = a[8] - b[8];
    out[9] = a[9] - b[9];
    out[10] = a[10] - b[10];
    out[11] = a[11] - b[11];
    out[12] = a[12] - b[12];
    out[13] = a[13] - b[13];
    out[14] = a[14] - b[14];
    out[15] = a[15] - b[15];
    return out;
};

/**
 * Alias for {@link mat4.subtract}
 * @function
 */
mat4.sub = mat4.subtract;

/**
 * Multiply each element of the matrix by a scalar.
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to scale
 * @param {Number} b amount to scale the matrix's elements by
 * @returns {mat4} out
 */
mat4.multiplyScalar = function (out, a, b) {
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    out[3] = a[3] * b;
    out[4] = a[4] * b;
    out[5] = a[5] * b;
    out[6] = a[6] * b;
    out[7] = a[7] * b;
    out[8] = a[8] * b;
    out[9] = a[9] * b;
    out[10] = a[10] * b;
    out[11] = a[11] * b;
    out[12] = a[12] * b;
    out[13] = a[13] * b;
    out[14] = a[14] * b;
    out[15] = a[15] * b;
    return out;
};

/**
 * Adds two mat4's after multiplying each element of the second operand by a scalar value.
 *
 * @param {mat4} out the receiving vector
 * @param {mat4} a the first operand
 * @param {mat4} b the second operand
 * @param {Number} scale the amount to scale b's elements by before adding
 * @returns {mat4} out
 */
mat4.multiplyScalarAndAdd = function (out, a, b, scale) {
    out[0] = a[0] + b[0] * scale;
    out[1] = a[1] + b[1] * scale;
    out[2] = a[2] + b[2] * scale;
    out[3] = a[3] + b[3] * scale;
    out[4] = a[4] + b[4] * scale;
    out[5] = a[5] + b[5] * scale;
    out[6] = a[6] + b[6] * scale;
    out[7] = a[7] + b[7] * scale;
    out[8] = a[8] + b[8] * scale;
    out[9] = a[9] + b[9] * scale;
    out[10] = a[10] + b[10] * scale;
    out[11] = a[11] + b[11] * scale;
    out[12] = a[12] + b[12] * scale;
    out[13] = a[13] + b[13] * scale;
    out[14] = a[14] + b[14] * scale;
    out[15] = a[15] + b[15] * scale;
    return out;
};

/**
 * Returns whether or not the matrices have exactly the same elements in the same position (when compared with ===)
 *
 * @param {mat4} a The first matrix.
 * @param {mat4} b The second matrix.
 * @returns {Boolean} True if the matrices are equal, false otherwise.
 */
mat4.exactEquals = function (a, b) {
    return a[0] === b[0] && a[1] === b[1] && a[2] === b[2] && a[3] === b[3] && a[4] === b[4] && a[5] === b[5] && a[6] === b[6] && a[7] === b[7] && a[8] === b[8] && a[9] === b[9] && a[10] === b[10] && a[11] === b[11] && a[12] === b[12] && a[13] === b[13] && a[14] === b[14] && a[15] === b[15];
};

/**
 * Returns whether or not the matrices have approximately the same elements in the same position.
 *
 * @param {mat4} a The first matrix.
 * @param {mat4} b The second matrix.
 * @returns {Boolean} True if the matrices are equal, false otherwise.
 */
mat4.equals = function (a, b) {
    var a0 = a[0],
        a1 = a[1],
        a2 = a[2],
        a3 = a[3],
        a4 = a[4],
        a5 = a[5],
        a6 = a[6],
        a7 = a[7],
        a8 = a[8],
        a9 = a[9],
        a10 = a[10],
        a11 = a[11],
        a12 = a[12],
        a13 = a[13],
        a14 = a[14],
        a15 = a[15];

    var b0 = b[0],
        b1 = b[1],
        b2 = b[2],
        b3 = b[3],
        b4 = b[4],
        b5 = b[5],
        b6 = b[6],
        b7 = b[7],
        b8 = b[8],
        b9 = b[9],
        b10 = b[10],
        b11 = b[11],
        b12 = b[12],
        b13 = b[13],
        b14 = b[14],
        b15 = b[15];

    return Math.abs(a0 - b0) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a0), Math.abs(b0)) && Math.abs(a1 - b1) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a1), Math.abs(b1)) && Math.abs(a2 - b2) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a2), Math.abs(b2)) && Math.abs(a3 - b3) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a3), Math.abs(b3)) && Math.abs(a4 - b4) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a4), Math.abs(b4)) && Math.abs(a5 - b5) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a5), Math.abs(b5)) && Math.abs(a6 - b6) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a6), Math.abs(b6)) && Math.abs(a7 - b7) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a7), Math.abs(b7)) && Math.abs(a8 - b8) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a8), Math.abs(b8)) && Math.abs(a9 - b9) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a9), Math.abs(b9)) && Math.abs(a10 - b10) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a10), Math.abs(b10)) && Math.abs(a11 - b11) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a11), Math.abs(b11)) && Math.abs(a12 - b12) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a12), Math.abs(b12)) && Math.abs(a13 - b13) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a13), Math.abs(b13)) && Math.abs(a14 - b14) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a14), Math.abs(b14)) && Math.abs(a15 - b15) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a15), Math.abs(b15));
};

/* Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV.
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE. */

/**
 * @class 3 Dimensional Vector
 * @name vec3
 */
var vec3 = {};

/**
 * Creates a new, empty vec3
 *
 * @returns {vec3} a new 3D vector
 */
vec3.create = function () {
    var out = new glMatrix.ARRAY_TYPE(3);
    out[0] = 0;
    out[1] = 0;
    out[2] = 0;
    return out;
};

/**
 * Creates a new vec3 initialized with values from an existing vector
 *
 * @param {vec3} a vector to clone
 * @returns {vec3} a new 3D vector
 */
vec3.clone = function (a) {
    var out = new glMatrix.ARRAY_TYPE(3);
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    return out;
};

/**
 * Creates a new vec3 initialized with the given values
 *
 * @param {Number} x X component
 * @param {Number} y Y component
 * @param {Number} z Z component
 * @returns {vec3} a new 3D vector
 */
vec3.fromValues = function (x, y, z) {
    var out = new glMatrix.ARRAY_TYPE(3);
    out[0] = x;
    out[1] = y;
    out[2] = z;
    return out;
};

/**
 * Copy the values from one vec3 to another
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the source vector
 * @returns {vec3} out
 */
vec3.copy = function (out, a) {
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    return out;
};

/**
 * Set the components of a vec3 to the given values
 *
 * @param {vec3} out the receiving vector
 * @param {Number} x X component
 * @param {Number} y Y component
 * @param {Number} z Z component
 * @returns {vec3} out
 */
vec3.set = function (out, x, y, z) {
    out[0] = x;
    out[1] = y;
    out[2] = z;
    return out;
};

/**
 * Adds two vec3's
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {vec3} out
 */
vec3.add = function (out, a, b) {
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    return out;
};

/**
 * Subtracts vector b from vector a
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {vec3} out
 */
vec3.subtract = function (out, a, b) {
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
    return out;
};

/**
 * Alias for {@link vec3.subtract}
 * @function
 */
vec3.sub = vec3.subtract;

/**
 * Multiplies two vec3's
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {vec3} out
 */
vec3.multiply = function (out, a, b) {
    out[0] = a[0] * b[0];
    out[1] = a[1] * b[1];
    out[2] = a[2] * b[2];
    return out;
};

/**
 * Alias for {@link vec3.multiply}
 * @function
 */
vec3.mul = vec3.multiply;

/**
 * Divides two vec3's
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {vec3} out
 */
vec3.divide = function (out, a, b) {
    out[0] = a[0] / b[0];
    out[1] = a[1] / b[1];
    out[2] = a[2] / b[2];
    return out;
};

/**
 * Alias for {@link vec3.divide}
 * @function
 */
vec3.div = vec3.divide;

/**
 * Math.ceil the components of a vec3
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a vector to ceil
 * @returns {vec3} out
 */
vec3.ceil = function (out, a) {
    out[0] = Math.ceil(a[0]);
    out[1] = Math.ceil(a[1]);
    out[2] = Math.ceil(a[2]);
    return out;
};

/**
 * Math.floor the components of a vec3
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a vector to floor
 * @returns {vec3} out
 */
vec3.floor = function (out, a) {
    out[0] = Math.floor(a[0]);
    out[1] = Math.floor(a[1]);
    out[2] = Math.floor(a[2]);
    return out;
};

/**
 * Returns the minimum of two vec3's
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {vec3} out
 */
vec3.min = function (out, a, b) {
    out[0] = Math.min(a[0], b[0]);
    out[1] = Math.min(a[1], b[1]);
    out[2] = Math.min(a[2], b[2]);
    return out;
};

/**
 * Returns the maximum of two vec3's
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {vec3} out
 */
vec3.max = function (out, a, b) {
    out[0] = Math.max(a[0], b[0]);
    out[1] = Math.max(a[1], b[1]);
    out[2] = Math.max(a[2], b[2]);
    return out;
};

/**
 * Math.round the components of a vec3
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a vector to round
 * @returns {vec3} out
 */
vec3.round = function (out, a) {
    out[0] = Math.round(a[0]);
    out[1] = Math.round(a[1]);
    out[2] = Math.round(a[2]);
    return out;
};

/**
 * Scales a vec3 by a scalar number
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the vector to scale
 * @param {Number} b amount to scale the vector by
 * @returns {vec3} out
 */
vec3.scale = function (out, a, b) {
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    return out;
};

/**
 * Adds two vec3's after scaling the second operand by a scalar value
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @param {Number} scale the amount to scale b by before adding
 * @returns {vec3} out
 */
vec3.scaleAndAdd = function (out, a, b, scale) {
    out[0] = a[0] + b[0] * scale;
    out[1] = a[1] + b[1] * scale;
    out[2] = a[2] + b[2] * scale;
    return out;
};

/**
 * Calculates the euclidian distance between two vec3's
 *
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {Number} distance between a and b
 */
vec3.distance = function (a, b) {
    var x = b[0] - a[0],
        y = b[1] - a[1],
        z = b[2] - a[2];
    return Math.sqrt(x * x + y * y + z * z);
};

/**
 * Alias for {@link vec3.distance}
 * @function
 */
vec3.dist = vec3.distance;

/**
 * Calculates the squared euclidian distance between two vec3's
 *
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {Number} squared distance between a and b
 */
vec3.squaredDistance = function (a, b) {
    var x = b[0] - a[0],
        y = b[1] - a[1],
        z = b[2] - a[2];
    return x * x + y * y + z * z;
};

/**
 * Alias for {@link vec3.squaredDistance}
 * @function
 */
vec3.sqrDist = vec3.squaredDistance;

/**
 * Calculates the length of a vec3
 *
 * @param {vec3} a vector to calculate length of
 * @returns {Number} length of a
 */
vec3.length = function (a) {
    var x = a[0],
        y = a[1],
        z = a[2];
    return Math.sqrt(x * x + y * y + z * z);
};

/**
 * Alias for {@link vec3.length}
 * @function
 */
vec3.len = vec3.length;

/**
 * Calculates the squared length of a vec3
 *
 * @param {vec3} a vector to calculate squared length of
 * @returns {Number} squared length of a
 */
vec3.squaredLength = function (a) {
    var x = a[0],
        y = a[1],
        z = a[2];
    return x * x + y * y + z * z;
};

/**
 * Alias for {@link vec3.squaredLength}
 * @function
 */
vec3.sqrLen = vec3.squaredLength;

/**
 * Negates the components of a vec3
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a vector to negate
 * @returns {vec3} out
 */
vec3.negate = function (out, a) {
    out[0] = -a[0];
    out[1] = -a[1];
    out[2] = -a[2];
    return out;
};

/**
 * Returns the inverse of the components of a vec3
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a vector to invert
 * @returns {vec3} out
 */
vec3.inverse = function (out, a) {
    out[0] = 1.0 / a[0];
    out[1] = 1.0 / a[1];
    out[2] = 1.0 / a[2];
    return out;
};

/**
 * Normalize a vec3
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a vector to normalize
 * @returns {vec3} out
 */
vec3.normalize = function (out, a) {
    var x = a[0],
        y = a[1],
        z = a[2];
    var len = x * x + y * y + z * z;
    if (len > 0) {
        //TODO: evaluate use of glm_invsqrt here?
        len = 1 / Math.sqrt(len);
        out[0] = a[0] * len;
        out[1] = a[1] * len;
        out[2] = a[2] * len;
    }
    return out;
};

/**
 * Calculates the dot product of two vec3's
 *
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {Number} dot product of a and b
 */
vec3.dot = function (a, b) {
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
};

/**
 * Computes the cross product of two vec3's
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {vec3} out
 */
vec3.cross = function (out, a, b) {
    var ax = a[0],
        ay = a[1],
        az = a[2],
        bx = b[0],
        by = b[1],
        bz = b[2];

    out[0] = ay * bz - az * by;
    out[1] = az * bx - ax * bz;
    out[2] = ax * by - ay * bx;
    return out;
};

/**
 * Performs a linear interpolation between two vec3's
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @param {Number} t interpolation amount between the two inputs
 * @returns {vec3} out
 */
vec3.lerp = function (out, a, b, t) {
    var ax = a[0],
        ay = a[1],
        az = a[2];
    out[0] = ax + t * (b[0] - ax);
    out[1] = ay + t * (b[1] - ay);
    out[2] = az + t * (b[2] - az);
    return out;
};

/**
 * Performs a hermite interpolation with two control points
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @param {vec3} c the third operand
 * @param {vec3} d the fourth operand
 * @param {Number} t interpolation amount between the two inputs
 * @returns {vec3} out
 */
vec3.hermite = function (out, a, b, c, d, t) {
    var factorTimes2 = t * t,
        factor1 = factorTimes2 * (2 * t - 3) + 1,
        factor2 = factorTimes2 * (t - 2) + t,
        factor3 = factorTimes2 * (t - 1),
        factor4 = factorTimes2 * (3 - 2 * t);

    out[0] = a[0] * factor1 + b[0] * factor2 + c[0] * factor3 + d[0] * factor4;
    out[1] = a[1] * factor1 + b[1] * factor2 + c[1] * factor3 + d[1] * factor4;
    out[2] = a[2] * factor1 + b[2] * factor2 + c[2] * factor3 + d[2] * factor4;

    return out;
};

/**
 * Performs a bezier interpolation with two control points
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @param {vec3} c the third operand
 * @param {vec3} d the fourth operand
 * @param {Number} t interpolation amount between the two inputs
 * @returns {vec3} out
 */
vec3.bezier = function (out, a, b, c, d, t) {
    var inverseFactor = 1 - t,
        inverseFactorTimesTwo = inverseFactor * inverseFactor,
        factorTimes2 = t * t,
        factor1 = inverseFactorTimesTwo * inverseFactor,
        factor2 = 3 * t * inverseFactorTimesTwo,
        factor3 = 3 * factorTimes2 * inverseFactor,
        factor4 = factorTimes2 * t;

    out[0] = a[0] * factor1 + b[0] * factor2 + c[0] * factor3 + d[0] * factor4;
    out[1] = a[1] * factor1 + b[1] * factor2 + c[1] * factor3 + d[1] * factor4;
    out[2] = a[2] * factor1 + b[2] * factor2 + c[2] * factor3 + d[2] * factor4;

    return out;
};

/**
 * Generates a random vector with the given scale
 *
 * @param {vec3} out the receiving vector
 * @param {Number} [scale] Length of the resulting vector. If ommitted, a unit vector will be returned
 * @returns {vec3} out
 */
vec3.random = function (out, scale) {
    scale = scale || 1.0;

    var r = glMatrix.RANDOM() * 2.0 * Math.PI;
    var z = glMatrix.RANDOM() * 2.0 - 1.0;
    var zScale = Math.sqrt(1.0 - z * z) * scale;

    out[0] = Math.cos(r) * zScale;
    out[1] = Math.sin(r) * zScale;
    out[2] = z * scale;
    return out;
};

/**
 * Transforms the vec3 with a mat4.
 * 4th vector component is implicitly '1'
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the vector to transform
 * @param {mat4} m matrix to transform with
 * @returns {vec3} out
 */
vec3.transformMat4 = function (out, a, m) {
    var x = a[0],
        y = a[1],
        z = a[2],
        w = m[3] * x + m[7] * y + m[11] * z + m[15];
    w = w || 1.0;
    out[0] = (m[0] * x + m[4] * y + m[8] * z + m[12]) / w;
    out[1] = (m[1] * x + m[5] * y + m[9] * z + m[13]) / w;
    out[2] = (m[2] * x + m[6] * y + m[10] * z + m[14]) / w;
    return out;
};

/**
 * Transforms the vec3 with a mat3.
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the vector to transform
 * @param {mat4} m the 3x3 matrix to transform with
 * @returns {vec3} out
 */
vec3.transformMat3 = function (out, a, m) {
    var x = a[0],
        y = a[1],
        z = a[2];
    out[0] = x * m[0] + y * m[3] + z * m[6];
    out[1] = x * m[1] + y * m[4] + z * m[7];
    out[2] = x * m[2] + y * m[5] + z * m[8];
    return out;
};

/**
 * Transforms the vec3 with a quat
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the vector to transform
 * @param {quat} q quaternion to transform with
 * @returns {vec3} out
 */
vec3.transformQuat = function (out, a, q) {
    // benchmarks: http://jsperf.com/quaternion-transform-vec3-implementations

    var x = a[0],
        y = a[1],
        z = a[2],
        qx = q[0],
        qy = q[1],
        qz = q[2],
        qw = q[3],


    // calculate quat * vec
    ix = qw * x + qy * z - qz * y,
        iy = qw * y + qz * x - qx * z,
        iz = qw * z + qx * y - qy * x,
        iw = -qx * x - qy * y - qz * z;

    // calculate result * inverse quat
    out[0] = ix * qw + iw * -qx + iy * -qz - iz * -qy;
    out[1] = iy * qw + iw * -qy + iz * -qx - ix * -qz;
    out[2] = iz * qw + iw * -qz + ix * -qy - iy * -qx;
    return out;
};

/**
 * Rotate a 3D vector around the x-axis
 * @param {vec3} out The receiving vec3
 * @param {vec3} a The vec3 point to rotate
 * @param {vec3} b The origin of the rotation
 * @param {Number} c The angle of rotation
 * @returns {vec3} out
 */
vec3.rotateX = function (out, a, b, c) {
    var p = [],
        r = [];
    //Translate point to the origin
    p[0] = a[0] - b[0];
    p[1] = a[1] - b[1];
    p[2] = a[2] - b[2];

    //perform rotation
    r[0] = p[0];
    r[1] = p[1] * Math.cos(c) - p[2] * Math.sin(c);
    r[2] = p[1] * Math.sin(c) + p[2] * Math.cos(c);

    //translate to correct position
    out[0] = r[0] + b[0];
    out[1] = r[1] + b[1];
    out[2] = r[2] + b[2];

    return out;
};

/**
 * Rotate a 3D vector around the y-axis
 * @param {vec3} out The receiving vec3
 * @param {vec3} a The vec3 point to rotate
 * @param {vec3} b The origin of the rotation
 * @param {Number} c The angle of rotation
 * @returns {vec3} out
 */
vec3.rotateY = function (out, a, b, c) {
    var p = [],
        r = [];
    //Translate point to the origin
    p[0] = a[0] - b[0];
    p[1] = a[1] - b[1];
    p[2] = a[2] - b[2];

    //perform rotation
    r[0] = p[2] * Math.sin(c) + p[0] * Math.cos(c);
    r[1] = p[1];
    r[2] = p[2] * Math.cos(c) - p[0] * Math.sin(c);

    //translate to correct position
    out[0] = r[0] + b[0];
    out[1] = r[1] + b[1];
    out[2] = r[2] + b[2];

    return out;
};

/**
 * Rotate a 3D vector around the z-axis
 * @param {vec3} out The receiving vec3
 * @param {vec3} a The vec3 point to rotate
 * @param {vec3} b The origin of the rotation
 * @param {Number} c The angle of rotation
 * @returns {vec3} out
 */
vec3.rotateZ = function (out, a, b, c) {
    var p = [],
        r = [];
    //Translate point to the origin
    p[0] = a[0] - b[0];
    p[1] = a[1] - b[1];
    p[2] = a[2] - b[2];

    //perform rotation
    r[0] = p[0] * Math.cos(c) - p[1] * Math.sin(c);
    r[1] = p[0] * Math.sin(c) + p[1] * Math.cos(c);
    r[2] = p[2];

    //translate to correct position
    out[0] = r[0] + b[0];
    out[1] = r[1] + b[1];
    out[2] = r[2] + b[2];

    return out;
};

/**
 * Perform some operation over an array of vec3s.
 *
 * @param {Array} a the array of vectors to iterate over
 * @param {Number} stride Number of elements between the start of each vec3. If 0 assumes tightly packed
 * @param {Number} offset Number of elements to skip at the beginning of the array
 * @param {Number} count Number of vec3s to iterate over. If 0 iterates over entire array
 * @param {Function} fn Function to call for each vector in the array
 * @param {Object} [arg] additional argument to pass to fn
 * @returns {Array} a
 * @function
 */
vec3.forEach = function () {
    var vec = vec3.create();

    return function (a, stride, offset, count, fn, arg) {
        var i, l;
        if (!stride) {
            stride = 3;
        }

        if (!offset) {
            offset = 0;
        }

        if (count) {
            l = Math.min(count * stride + offset, a.length);
        } else {
            l = a.length;
        }

        for (i = offset; i < l; i += stride) {
            vec[0] = a[i];vec[1] = a[i + 1];vec[2] = a[i + 2];
            fn(vec, vec, arg);
            a[i] = vec[0];a[i + 1] = vec[1];a[i + 2] = vec[2];
        }

        return a;
    };
}();

/**
 * Get the angle between two 3D vectors
 * @param {vec3} a The first operand
 * @param {vec3} b The second operand
 * @returns {Number} The angle in radians
 */
vec3.angle = function (a, b) {

    var tempA = vec3.fromValues(a[0], a[1], a[2]);
    var tempB = vec3.fromValues(b[0], b[1], b[2]);

    vec3.normalize(tempA, tempA);
    vec3.normalize(tempB, tempB);

    var cosine = vec3.dot(tempA, tempB);

    if (cosine > 1.0) {
        return 0;
    } else {
        return Math.acos(cosine);
    }
};

/**
 * Returns a string representation of a vector
 *
 * @param {vec3} a vector to represent as a string
 * @returns {String} string representation of the vector
 */
vec3.str = function (a) {
    return 'vec3(' + a[0] + ', ' + a[1] + ', ' + a[2] + ')';
};

/**
 * Returns whether or not the vectors have exactly the same elements in the same position (when compared with ===)
 *
 * @param {vec3} a The first vector.
 * @param {vec3} b The second vector.
 * @returns {Boolean} True if the vectors are equal, false otherwise.
 */
vec3.exactEquals = function (a, b) {
    return a[0] === b[0] && a[1] === b[1] && a[2] === b[2];
};

/**
 * Returns whether or not the vectors have approximately the same elements in the same position.
 *
 * @param {vec3} a The first vector.
 * @param {vec3} b The second vector.
 * @returns {Boolean} True if the vectors are equal, false otherwise.
 */
vec3.equals = function (a, b) {
    var a0 = a[0],
        a1 = a[1],
        a2 = a[2];
    var b0 = b[0],
        b1 = b[1],
        b2 = b[2];
    return Math.abs(a0 - b0) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a0), Math.abs(b0)) && Math.abs(a1 - b1) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a1), Math.abs(b1)) && Math.abs(a2 - b2) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a2), Math.abs(b2));
};

/**
 * Creates a VertexAttributeObject aka VAO
 * @param gl a webgl context
 * @param useNative flag for whether or not to use native VAOs (which uses an extension for now)
 */
function createVAO(gl, useNative = true) {
    let vao = null;
    let ext = null;
    // TODO support cards that don't have this extension later
    if (useNative) {
        if (gl.hasOwnProperty('OES_vertex_array_object')) {
            ext = gl['OES_vertex_array_object'];
            vao = ext.createVertexArrayOES();
        } else {
            ext = gl.getExtension('OES_vertex_array_object');
            vao = ext.createVertexArrayOES();
        }
    }

    return {
        gl: gl,
        vao: vao,
        ext: ext,
        attributes: {},

        /**
         * Sets an attribute's location
         * @param shader {WebGLProgram} a WebGl shader program to associate with the attribute location
         * @param name {String} the name of the attribute
         * @param index {Number} an optional index. If null, will utilize the automatically assigned location
         * @returns {number} returns the location for the attribute
         */
        setAttributeLocation: function (shader, name, index = null) {
            let loc = 0;
            let gl = this.gl;

            // if we don't assign an index, get the automatically generated one
            if (index === null || index === undefined) {
                loc = gl.getAttribLocation(shader, name);
            } else {
                loc = gl.bindAttribLocation(shader, index, name);
            }
            return loc;
        },

        /**
         * Enable all of the attributes on a shader onto the VAO.
         * This will automatically set the attribute location to the order in which the
         * attribute was set in the shader settings, but will override that decision if the location index is
         * set in the attribute.
         *
         * @param shader a plane JS object that contains 3 things
         * 1. A WebGLProgram on the key "shader"
         * 2. an array at the key "attributes" that contains the name of all of the attributes we're looking for
         * as well as the size of each attribute.
         */
        enableAttributes: function (shader) {
            let gl = this.gl;
            let attribs = shader.attributes;
            for (let a in attribs) {
                let attrib = attribs[a];
                let attribLoc = this.attributes.length;

                // if the attribute has a location parameter, use that to set the attribute location,
                // otherwise use the next index in the attributes array
                if (attrib.hasOwnProperty('location')) {
                    attribLoc = attrib.location;
                }

                this.addAttribute(shader, attrib.name, attrib.size, attribLoc);
            }
            return this;
        },

        /**
         * Adds an attribute for the VAO to keep track of
         * @param shader {WebGLProgram} the shader that the attribute is a part of. Takes a WebGLProgram but also accepts a plain object created by the
         * {@link createShader} function
         * @param name {String} the name of the attribute to add/enable
         * @param size {Number} optional - the number of items that compose the attribute. For example, for something like, position, you might have xyz components, thus, 3 would be the size
         * @param location {Number} optional - the number to use as the attribute location. If it's not specified, will simply use it's index in the attributes object
         * @param setData {Boolean} optional - flag for whether or not to immediately run setData on the attribute. TODO enable by default
         * @param dataOptions {Object} optional - any options you might want to add when calling setData like an offset or stride value for the data
         * @returns {addAttribute}
         */
        addAttribute: function (shader, name, { size = 3, location, setData = false, dataOptions = {} } = {}) {
            let attribLoc = this.attributes.length;
            let webglProg = null;

            if (shader instanceof WebGLProgram) {
                webglProg = shader;
            } else {
                webglProg = shader.program;
            }

            // if location is undefined, just set attribute location
            // to be the next index in the attribute set.
            if (location === undefined) {
                attribLoc = location;
            }

            let attribLocation = this.setAttributeLocation(webglProg, name, attribLoc);
            this.attributes[name] = {
                loc: attribLocation,
                enabled: true,
                size: size
            };
            //enable the attribute
            this.enableAttribute(name);

            // if we want to just go ahead and set the data , run that
            if (setData) {
                this.setData(name, dataOptions);
            }
            return this;
        },

        /**
         * Returns the location of the specified attribute
         * @param name {String} the name of the attribute.
         * @returns {*|number}
         */
        getAttribute(name) {
            return this.attributes[name].loc;
        },

        /**
         * Enables a vertex attribute
         * @param name {String} the name of the attribute you want to enable
         */
        enableAttribute: function (name) {
            // enable vertex attribute at the location
            this.gl.enableVertexAttribArray(this.attributes[name].loc);
        },

        /**
         * Disables a vertex attribute
         * @param name {String} the name of the vertex attribute to disable
         */
        disableAttribute: function (name) {
            this.gl.disableVertexAttribArray(this.attributes[name].loc);
        },

        /**
         * Shorthand for calling gl.vertexAttribPointer. Essentially sets the data into the vao for the
         * currently bound buffer.
         * @param name {String} the name of the attribute to pass data to in the shader
         * @param options {Object} options for utilizing that information
         */
        setData: function (name, { options } = {}) {
            let loc = this.attributes[name].loc;
            let size = this.attributes[name].size;

            let pointerOptions = {
                type: gl.FLOAT,
                normalized: gl.FALSE,
                stride: 0,
                offset: 0
            };

            if (options !== undefined) {
                Object.assign(pointerOptions, options);
            }
            gl.vertexAttribPointer(loc, size, pointerOptions.type, pointerOptions.normalized, pointerOptions.stride, pointerOptions.offset);
        },
        /**
         * Shorthand for calling gl.vertexAttribPointer
         * @deprecated gonna start using setData instead so it's a little more clear as to what this is doing
         * @param name {String} the name of the attribute to pass data to in the shader
         * @param options {Object} options for utilizing that information
         */
        point: function (name, { options } = {}) {
            let loc = this.attributes[name].loc;
            let size = this.attributes[name].size;

            let pointerOptions = {
                type: gl.FLOAT,
                normalized: gl.FALSE,
                stride: 0,
                offset: 0
            };

            if (options !== undefined) {
                Object.assign(pointerOptions, options);
            }
            gl.vertexAttribPointer(loc, size, pointerOptions.type, pointerOptions.normalized, pointerOptions.stride, pointerOptions.offset);
        },

        /**
         * Binds the vao
         */
        bind: function () {
            ext.bindVertexArrayOES(this.vao);
        },

        /**
         * Unbinds the vao
         */
        unbind: function () {
            ext.bindVertexArrayOES(null);
        }
    };
}

class Mesh {
    constructor(gl) {
        this.gl = gl;
        this.model = mat4.create();
        this.rotation = {
            x: 0,
            y: 0,
            z: 0
        };
        this.rotateAxis = vec3.create();
        vec3.set(this.rotateAxis, this.rotation.x, this.rotation.y, this.rotation.z);
        this.scale = vec3.create();
        this.position = vec3.create();
        this.vao = createVAO(gl);
    }

    translate(x = 1, y = 1, z = 0) {
        vec3.set(this.position, x, y, z);
        mat4.translate(this.model, this.model, this.position);
    }

    scaleModel(x = 1, y = 1, z = 1) {
        vec3.set(this.scale, x, y, z);
        mat4.scale(this.model, this.model, this.scale);
    }

    rotateX(angle) {
        mat4.rotateX(this.model, this.model, angle);
    }

    rotateY(angle) {
        mat4.rotateY(this.model, this.model, angle);
    }

    rotateZ(angle) {
        mat4.rotateZ(this.model, this.model, angle);
    }
}

/**
 * Simple function to create a VBO aka buffer
 * @param gl a WebGLRendering context
 * @param data the information for the buffer. If it's a regular array, it'll be turned into a TypedArray
 * @param bufferType the type of buffer it is. By default, it's an ARRAY_BUFFER
 * @param usage the usage for the buffer. by default it's STATIC_DRAW
 */
function createVBO(gl, { data = null, indexed = false, usage } = {}) {
    let buffer = null;

    // set the buffer type
    let bufferType = "ARRAY_BUFFER";
    if (indexed === true) {
        bufferType = "ELEMENT_ARRAY_BUFFER";
    }
    let name = bufferType;
    bufferType = gl[bufferType];

    // set the usage
    usage = usage || "STATIC_DRAW";
    usage = gl[usage];
    buffer = gl.createBuffer();

    var obj = {
        gl: gl,
        buffer: buffer,
        bufferTypeName: name,
        type: bufferType,
        usage: usage,

        raw: function () {
            return this.buffer;
        },
        /**
         * Updates the buffer with new information
         * @param data a array of some kind containing your new data
         */
        updateBuffer: function (data) {
            if (data instanceof Array) {
                if (this.bufferTypeName === "ARRAY_BUFFER") {
                    data = new Float32Array(data);
                } else {
                    data = new Uint16Array(data);
                }
            }
            this.bind();
            this.gl.bufferSubData(this.type, 0, data);
            this.unbind();
        },
        /**
         * Sets data onto the vbo.
         * @param data the data for the vbo. Can either be a regular array or a typed array.
         * If a regular array is used, will determine buffer type based on the settings.
         */
        bufferData: function (data, updatedData = false) {
            if (data instanceof Array) {
                if (this.bufferTypeName === "ARRAY_BUFFER") {
                    data = new Float32Array(data);
                } else {
                    data = new Uint16Array(data);
                }
            }
            this.gl.bufferData(this.type, data, usage);
        },
        bind: function () {
            this.gl.bindBuffer(this.type, this.buffer);
        },
        unbind: function () {
            this.gl.bindBuffer(this.type, null);
        }
    };

    if (data !== null) {
        obj.bind();
        obj.bufferData(data);
        obj.unbind();
    }

    return obj;
}

/**
 * Gets the correct uniform setter based on the
 * the name you pass in
 * @param gl a WebGLRendering context
 * @param val the name of the type of uniform you're looking to set
 * @returns {*} returns the setter function for that uniform
 */


/**
 * Compiles either a fragment or vertex shader
 * @param gl a webgl context
 * @param type the type of shader. Should be either gl.FRAGMENT_SHADER or gl.VERTEX_SHADER
 * @param source the source (as a string) for the shader
 * @returns {*} returns the compiled shader
 */
function compileShader(gl, type, source) {
    let shader = gl.createShader(type);

    gl.shaderSource(shader, source);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        console.error("Error in shader compilation - " + gl.getShaderInfoLog(shader));
        return false;
    } else {
        return shader;
    }
}

/**
 * The main function for creating a shader. Shader also manages figuring out
 * attribute and uniform location indices.
 *
 * @param gl a webgl context
 * @param vertex the source for the vertex shader
 * @param fragment the source for the fragment shader
 * @returns {*} returns the WebGLProgram compiled from the two shaders
 */
function makeShader(gl, vertex, fragment) {
    let vShader = compileShader(gl, gl.VERTEX_SHADER, vertex);
    let fShader = compileShader(gl, gl.FRAGMENT_SHADER, fragment);

    if (vShader !== false && fShader !== false) {
        let program = gl.createProgram();
        gl.attachShader(program, vShader);
        gl.attachShader(program, fShader);
        gl.linkProgram(program);

        if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
            logError("Could not initialize WebGLProgram");
            throw "Couldn't link shader program - " + gl.getProgramInfoLog(program);
            return false;
        } else {
            return program;
        }
    }
}
/**
 * A function to quickly setup a WebGL shader program.
 * Modeled a bit after thi.ng
 * @param gl the webgl context to use
 * @param spec a object containing the out line of what the shader would look like.
 * @returns {*} and JS object with the shader information along with some helpful functions
 */
function createShader(gl = null, spec) {
    let vs = null;
    let fs = null;
    let uniforms = {};
    let attributes = {};
    let precision = spec.precision !== undefined ? spec.precision : "highp";
    if (gl === null) {
        console.error("");
        return false;
    }

    if (!spec.hasOwnProperty("vertex") || !spec.hasOwnProperty("fragment")) {
        logError("spec does not contain vertex and/or fragment shader");
        return false;
    }

    // if either of the shader sources are arrays, run the compile shader function
    if (spec.vertex instanceof Array) {
        spec.vertex = compileShaderSource(spec.vertex);
    }

    if (spec.fragment instanceof Array) {
        spec.fragment = `precision ${ highp } float;` + compileShaderSource(spec.fragment);
    }

    // build the shader
    let shader = makeShader(gl, spec.vertex, spec.fragment);

    // set uniforms and their locations (plus default values if specified)
    if (spec.hasOwnProperty('uniforms')) {

        let uValues = spec.uniforms.map(value => {
            if (typeof value === 'string') {
                let loc = gl.getUniformLocation(shader, value);
                uniforms[value] = loc;
            } else if (typeof value === 'object') {
                // TODO make sure to set default uniform value if present
                let loc = gl.getUniformLocation(shader, value.name);
                uniforms[value.name] = loc;
            }
        });
    }

    /**
     * Arranges all of the attribute data into neat containers
     * to allow for easy processing by a VAO.
     * Attributes should be specified as arrays
     */
    if (spec.hasOwnProperty('attributes')) {
        let attribs = spec.attributes.map(value => {

            attributes[value[0]] = {
                size: value[1],
                name: value[0]
            };

            // if a desired uniform location is set ,
            // make sure to reflect that in the information
            if (value[2] !== undefined) {
                attributes[value[0]].location = value[2];
            }
        });
    }

    return {
        gl: gl,
        program: shader,
        uniforms: uniforms,
        attributes: attributes,
        /**
         * Binds the shader for use
         */
        bind: function (u) {

            this.gl.useProgram(this.program);
        },
        /**
         * Sets a matrix uniform for a 4x4 matrix
         * @param name the name of the uniform whose value you want to set.
         */
        setMatrixUniform: function (name, value) {

            this.gl.uniformMatrix4fv(this.uniforms[name], false, value);
        },
        /**
         * Sets the uniform value for a texture. Optionally
         * @param value
         */
        setTextureUniform: function (name, value) {
            this.gl.uniform1i(this.uniforms[name], value);
        },

        getUniform(name) {
            return this.uniforms[name];
        },

        /**
         * Sends a uniform to the currently bound shader
         * @param name the name of the uniform
         * @param value the value to send to the uniform
         */
        uniform: function (name, value) {

            // if the value is a 4x4 matrix (assuming from gl-matrix)
            if (value.length !== undefined && value.length === 16) {
                this.setMatrixUniform(name, value);
            } else if (value.length !== undefined && value.length === 3) {
                this.gl.uniform3fv(this.uniforms[name], value);
            } else {
                // send a float based value
                this.gl.uniform1f(this.uniforms[name], value);
            }
        }

    };
}

/**
 * Allows you to compile multiple shader sources into one file.
 * Keep in mind this does not distinguish between vertex and fragment and will not
 * insert things like precision specifiers and/or extensions
 * @param sources
 */
function compileShaderSource(...sources) {
    if (sources[0] instanceof Array) {
        var s = sources[0].map(source => {
            return source + "\n";
        });
        return s.join("");
    } else {
        var s = sources.map(source => {
            return source + "\n";
        });
        return s.join("");
    }
}

/**
 * A stand alone function for creating data based textures with TypedArrays.
 * Usable on it's own, but recommended that you use the {@link createTexture2d}
 * function
 * @param gl {WebGLRenderingContext} a WebGLRenderingContext
 * @param data {TypedArray} a TypedArray of data you want to write onto the texture
 * @param options {Object} a map of options for the texture creation
 * @returns {*}
 */
function createDataTexture(gl, data, options) {
    let texture = gl.createTexture();

    gl.bindTexture(TEXTURE_2D, texture);
    gl.texImage2D(TEXTURE_2D, 0, options.internalFormat, options.width, options.height, 0, options.format, options.type, data);

    // set min and mag filters
    gl.texParameteri(TEXTURE_2D, MAG_FILTER, options.magFilter);
    gl.texParameteri(TEXTURE_2D, MIN_FILTER, options.minFilter);

    //set wrapping
    gl.texParameteri(TEXTURE_2D, WRAP_S, options.wrapS);
    gl.texParameteri(TEXTURE_2D, WRAP_T, options.wrapT);

    // generate mipmaps if necessary
    if (options.generateMipMaps) {
        gl.generateMipmap(TEXTURE_2D);
    }

    gl.bindTexture(TEXTURE_2D, null);

    return texture;
}



/**
 * Create an image based texture. Usable on it's own, but recommended that you use the {@link createTexture2d}
 * function
 * @param gl {WebGLRenderingContext} a WebGLRenderingContext
 * @param image {Image} and image object
 * @param options {Object} a map of options for the texture creation
 * @returns {*}
 */
function createImageTexture(gl, image, options) {
    let texture = gl.createTexture();
    gl.bindTexture(TEXTURE_2D, texture);

    // set the image
    gl.texImage2D(TEXTURE_2D, 0, options.format, options.format, options.type, image);

    // set min and mag filters
    gl.texParameteri(TEXTURE_2D, MAG_FILTER, options.magFilter);
    gl.texParameteri(TEXTURE_2D, MIN_FILTER, options.minFilter);

    //set wrapping
    gl.texParameteri(TEXTURE_2D, WRAP_S, options.wrapS);
    gl.texParameteri(TEXTURE_2D, WRAP_T, options.wrapT);

    // generate mipmaps if necessary
    if (options.generateMipMaps) {
        gl.generateMipmap(TEXTURE_2D);
    }

    gl.bindTexture(TEXTURE_2D, null);

    return texture;
}

/**
 * Simple function for creating a 2D texture
 * @param gl a WebGLRendering context
 */
function createTexture2d(gl, { data, textureOptions, width, height } = {}) {
    let texture = null;

    // NOTES
    // 1. in WebGL 1 , internalFormat and format ought to be the same value.
    // 2. UNSIGNED_BYTE corresponds to a Uint8Array, float corresponds to a Float32Array
    let defaults = {
        format: RGBA,
        internalFormat: RGBA,
        type: UNSIGNED_BYTE,
        wrapS: CLAMP_TO_EDGE,
        wrapT: CLAMP_TO_EDGE,
        minFilter: LINEAR,
        magFilter: LINEAR,
        generateMipMaps: false
    };

    if (textureOptions !== undefined) {
        Object.assign(defaults, textureOptions);
    }

    // if we have data, process it as such, otherwise generate a blank texture of random data
    if (data === undefined) {
        width = width || 128;
        height = height || 128;

        let data = null;

        // if textureOptions isn't undefined, check to see if we've defined the "type" key.
        // if that is set to the floating point constant, make sure to use a Float32Array,
        // otherwise default to Uint8Array.
        // If the parameter isn't defined, default to Uint8Array
        if (textureOptions !== undefined) {
            if (textureOptions.hasOwnProperty('type')) {
                if (textureOptions.type === FLOAT) {
                    data = new Float32Array(width * height * 4);
                } else {
                    data = new Uint8Array(width * height * 4);
                }
            }
        } else {
            data = new Uint8Array(width * height * 4);
        }

        for (var i = 0; i < width * height * 4; i += 4) {
            data[i] = Math.random();
            data[i + 1] = Math.random();
            data[i + 2] = Math.random();
            data[i + 3] = 1.0;
        }

        defaults["width"] = width;
        defaults["height"] = height;
        texture = createDataTexture(gl, data, defaults);

        // if we have data
    } else {
        defaults["width"] = width || 128;
        defaults["height"] = height || 128;

        // if it's an image, build an image texture
        if (data instanceof Image) {
            texture = createImageTexture(gl, data, defaults);
        }

        // if it's a float 32 array we, build a data texture.
        if (data instanceof Float32Array) {
            if (defaults.type !== FLOAT) {
                defaults.type = FLOAT;
            }
            texture = createDataTexture(gl, data, defaults);
        }

        // if it's a float 32 array we, build a data texture.
        if (data instanceof Uint8Array) {
            if (defaults.type !== FLOAT) {
                defaults.type = FLOAT;
            }
            texture = createDataTexture(gl, data, defaults);
        }

        if (data instanceof Array) {
            if (defaults.type !== FLOAT) {
                defaults.type = FLOAT;
            }
            texture = createDataTexture(gl, new Float32Array(data), defaults);
        }
    }

    return {
        gl: gl,
        texture: texture,
        getTexture: function () {
            return this.texture;
        },
        bind: function (index = 0) {
            let gl = this.gl;
            gl.activeTexture(TEXTURE0 + index);
            gl.bindTexture(TEXTURE_2D, this.texture);

            this.isBound = true;
        }
    };
}

function createFBOWithAttachments(gl, num = 1, { textures, width = 512, height = 512, floatingPoint = false, uniformMap } = {}) {
    textures = textures || false;

    let attachments = [];
    let framebuffer = gl.createFramebuffer();

    // get the max number of color attachments.
    // we know 0 will already be occupied.
    let ext = gl.WEBGL_draw_buffers;
    let maxAttachments = ext.MAX_COLOR_ATTACHMENTS_WEBGL;

    if (textures instanceof Array && textures.length > 0) {

        if (textures.length > maxAttachments) {
            console.error("The number of textures passed in appears to exceed the max number of attachments");
        } else {
            gl.bindFramebuffer(FRAMEBUFFER, framebuffer);
            // append textures to framebuffer
            for (var i = 0; i < textures.length; ++i) {

                // attach primary drawing textures
                gl.framebufferTexture2D(FRAMEBUFFER, ext[`COLOR_ATTACHMENT${ i }_WEBGL`], TEXTURE_2D, textures[i].texture, 0);
                attachments.push(ext[`COLOR_ATTACHMENT${ i }_WEBGL`]);
            }
            ext.drawBuffersWEBGL(attachments);
            gl.bindFramebuffer(FRAMEBUFFER, null);
        }
    } else {
        textures = [];
        // if floating point is true, we create a FBO with
        // a floating point texture(meaning, a texture that can store floating point numbers), otherwise, just create a regular fbo
        // with integer based information
        if (floatingPoint) {
            for (var i = 0; i < num; ++i) {
                textures.push(createTexture2d(gl, {
                    width: width,
                    height: height,
                    textureOptions: {
                        type: FLOAT
                    }
                }));
            }
        } else {
            for (var i = 0; i < num; ++i) {
                textures.push(createTexture2d(gl, {
                    width: width,
                    height: height
                }));
            }
        }

        // append textures to framebuffer
        for (var i = 0; i < num; ++i) {
            // attach primary drawing texture.
            // TODO change to WebGL 2 code later on
            gl.bindFramebuffer(FRAMEBUFFER, framebuffer);
            gl.framebufferTexture2D(FRAMEBUFFER, ext[`COLOR_ATTACHMENT${ i }_WEBGL`], TEXTURE_2D, textures[i].texture, 0);
            gl.bindFramebuffer(FRAMEBUFFER, null);
            attachments.push(ext[`COLOR_ATTACHMENT${ i }_WEBGL`]);
        }
    }

    return {
        gl: gl,
        textures: textures,
        fbo: framebuffer,
        attachments: attachments,
        ext: ext,
        maxDrawBuffers: ext.MAX_DRAW_BUFFERS_WEBGL,
        bindBuffers: function () {
            this.bindFbo();
            this.ext.drawBuffersWEBGL(this.attachments);
            this.unbindFbo();
        },
        /**
         * For binding the Fbo to draw onto it.
         */
        bindFbo: function () {
            gl.bindFramebuffer(FRAMEBUFFER, framebuffer);
        },
        /**
         * Unbinds the previously bound FBO, returning drawing commands to
         * the main context Framebuffer.
         */
        unbindFbo: function () {
            gl.bindFramebuffer(FRAMEBUFFER, null);
        },
        /**
         * Binds all the textures on the framebuffer.
         * the order is determined by the order of creation.
         */
        bindTextures: function () {
            let len = this.textures.length;
            for (var i = 0; i < len; ++i) {
                this.textures[i].bind(i);
            }
        },
        /**
         * Binds the texture of the framebuffer
         * @param index the index to bind the texture to
         */
        bindTexture: function (index = 0) {
            this.textures[index].bind(index);
        },
        /**
         * Unbinds the framebuffer's texture
         */
        unbindTexture: function (index = 0) {
            this.textures[index].unbind();
        }
    };
}

/**
 * Creates a WebGL Framebuffer object.
 * @param gl {WebGLRenderingContext} a WebGlRenderingContext
 * @param width {Number} the width for the fbo
 * @param height {Number} the height for the number
 * @param floatingPoint {Bool} whether or not the FBO should store floating point information
 * @returns {{gl: *, drawTexture: *, fbo: *, bindFbo: bindFbo, unbindFbo: unbindFbo, bind: bind, unbind: unbind}}
 */
function createFBO(gl, { width, height, floatingPoint, texture = null } = {}) {
    width = width || 512;
    height = height || 512;
    floatingPoint = floatingPoint || false;

    let framebuffer = gl.createFramebuffer();
    let t = null;

    if (texture !== null) {
        t = texture;
    } else {
        // if floating point is true, we create a FBO with
        // a floating point texture(meaning, a texture that can store floating point numbers), otherwise, just create a regular fbo
        // with integer based information
        if (floatingPoint) {
            t = createTexture2d(gl, {
                width: width,
                height: height,
                textureOptions: {
                    type: FLOAT
                }
            });
        } else {
            t = createTexture2d(gl);
        }
    }
    // attach primary drawing texture.
    gl.bindFramebuffer(FRAMEBUFFER, framebuffer);
    gl.framebufferTexture2D(FRAMEBUFFER, COLOR_ATTACHMENT0, TEXTURE_2D, t.texture, 0);
    gl.bindFramebuffer(FRAMEBUFFER, null);

    return {
        gl: gl,
        drawTexture: t,
        fbo: framebuffer,
        /**
         * For binding the Fbo to draw onto it.
         */
        bindFbo: function () {
            gl.bindFramebuffer(FRAMEBUFFER, framebuffer);
        },

        /**
         * Unbinds the previously bound FBO, returning drawing commands to
         * the main context Framebuffer.
         */
        unbindFbo: function () {
            gl.bindFramebuffer(FRAMEBUFFER, null);
        },

        /**
         * Binds the texture of the framebuffer
         * @param index the index to bind the texture to
         */
        bindTexture: function (index = 0) {
            this.drawTexture.bind(index);
        },

        /**
         * Unbinds the framebuffer's texture
         */
        unbindTexture: function () {
            this.drawTexture.unbind();
        }
    };
}

var quadvert = "\nattribute vec3 position;\nvarying vec2 uv;\nconst vec2 scale = vec2(0.5,0.5);\nvoid main(){\n    uv = position.xy * scale + scale;\n    gl_Position = vec4(position,1.0);\n}";

var quadfrag = "precision highp float;\n#ifdef USE_TEXTURE\n    uniform sampler2D debugTex;\n#endif\nvarying vec2 uv;\nvoid main(){\n  #ifdef USE_TEXTURE\n     vec4 dat = texture2D(debugTex,uv);\n     gl_FragColor = dat;\n  #else\n     gl_FragColor = vec4(1.);\n  #endif\n}";

/**
 * Quickly creates a quad usable in a variety of ways.
 * @param gl {WebGLRenderingContext}
 * @param withTexture {boolean} a boolean value indicating whether or not you're trying to create a rendering quad. If you are, pass in true and instead of a color
 * things will get set up to draw a texture instead.
 * @param fragmentShader {String} optional fragment shader, primarily used for ping-ponging data between textures.
 * @returns {{vao: ({gl, vao, ext, attributes, setAttributeLocation, enableAttributes, addAttribute, getAttribute, enableAttribute, disableAttribute, setData, point, bind, unbind}|*), shader: ({gl, program, uniforms, attributes, bind, setMatrixUniform, setTextureUniform, uniform}|*), buffer: *, hasTexture: boolean, gl: *, draw: draw}}
 */
function createQuad(gl, { withTexture = false, fragmentShader = false, uniformMap = [] } = {}) {
    let frag = quadfrag;
    let uniforms = ['debugTex'];
    let vertices = [1.0, 1.0, 0.0, -1.0, 1.0, 0.0, 1.0, -1.0, 0.0, -1.0, -1.0, 0.0];

    let vao = createVAO(gl);
    vao.bind();

    // if uniform map is not empty, merge with default map
    if (uniformMap.length > 0) {
        uniformMap.forEach(obj => {
            uniforms.push(obj);
        });
    }

    // if we don't have our own fragment shader, make sure to inject
    // the texture define statement if we need to support a texture.
    if (!fragmentShader) {
        if (withTexture) {
            frag = "#define USE_TEXTURE\n" + quadfrag;
        }
    } else {
        frag = fragmentShader;
    }

    let shader = createShader(gl, {
        vertex: quadvert,
        fragment: frag,
        attributes: [['position', 3]],
        uniforms: uniforms
    });
    // buffer data onto the buffer
    let buffer = createVBO(gl);

    // enable attributes
    buffer.bind();
    buffer.bufferData(vertices);
    vao.addAttribute(shader, 'position');
    vao.setData('position');
    buffer.unbind();
    vao.unbind();

    return {
        vao: vao,
        shader: shader,
        buffer: buffer,
        type: "quad",
        hasTexture: withTexture,
        gl: gl,
        draw: function (textureUnit = 0) {

            this.vao.bind();
            this.shader.bind();

            this.vao.enableAttribute('position');

            if (this.hasTexture) {
                this.shader.setTextureUniform('debugTex', textureUnit);
            }

            this.gl.drawArrays(TRIANGLE_STRIP, 0, 4);
            this.vao.disableAttribute('position');
            this.vao.unbind();
        }
    };
}

/**
 * Takes a Quad object made with `createQuad` and draws it
 * @param quad
 */

/**
 * Helper function to generate textures suitable for ping-ponging
 * @param gl {WebGLRenderingContext} a webgl context
 * @param data {Array or TypedArray} data for the texture. can either be a typed array or a regular array
 * @param width {Number} width for the texture. Defaults to 128
 * @param height {Number} height for the texture. Defaults to 128
 * @param type {Number} the datatype to use for the texture. Defaults to Floating point(gl.FLOAT)
 * @returns {*[]} an array of two textures built from the provided data
 */
function generatePingpongTexture(gl, data = null, { width = 128, height = 128, type = 5126 } = {}) {
    if (data === null) {
        console.error("generatePingpongTexture error - Need to provide texture data");
        return false;
    } else {
        return [createTexture2d(gl, {
            width: width,
            height: height,
            data: data,
            textureOptions: {
                type: type
            }
        }), createTexture2d(gl, {
            width: width,
            height: height,
            data: data,
            textureOptions: {
                type: type
            }
        })];
    }
}

/**
 * Creates a Multi attachment ping pong buffer. The difference between this one and a regular one is that you can write to a bunch
 * of textures simultaneously on just two fbos instead of having a pair of fbos for each property you want to manipulate.
 * @param gl {WebGLRenderingContext} a webgl rendering context
 * @param simulation the source GLSL for the ping pong code that you want to run
 * @param textureMap {Array} an optional texture map of initial starting data. If this is undefined, random data will be generated
 * @param uniformMap {Array} an optional map of uniforms to feed to the simulation pass. This lets the shader set values for those uniforms
 * @param numAttachments {Number} the number of attachments to create on each FBO. This is ignored if textureMap has a filled array
 * @param width {Number} The width
 * @param height
 * @returns {*}
 */
function createMultiPingpongBuffer(gl, simulation, { textureMap, uniformMap = [], numAttachments = 1, width = 128, height = 128 }) {
    let rt1, rt2;

    // for the moment, this requires the WEBGL_draw_buffers extension. First check to make sure it's enabled
    if (!gl.hasOwnProperty('WEBGL_draw_buffers') || !gl.getExtension('WEBGL_draw_buffers')) {
        console.error("The current computer does not have the WEBGL_draw_buffers extension available.");
        return false;
    }

    // if we've passed in a texture map
    if (textureMap !== undefined) {
        let texSet1 = [];
        let texSet2 = [];
        if (textureMap instanceof Array) {
            // it's assumed that each set is an array of two textures
            textureMap.forEach(set => {
                texSet1.push(set[0]);
                texSet2.push(set[1]);
            });
        } else {
            console.error("createMultiPingpongBuffer error - textureMap must be an array");
            return false;
        }

        rt1 = createFBOWithAttachments(gl, 1, {
            floatingPoint: true,
            textures: texSet1
        });

        rt2 = createFBOWithAttachments(gl, 1, {
            floatingPoint: true,
            textures: texSet2
        });
    } else {
        rt1 = createFBOWithAttachments(gl, numAttachments, {
            width: 512,
            height: 512,
            floatingPoint: true
        });

        rt2 = createFBOWithAttachments(gl, numAttachments, {
            width: 512,
            height: 512,
            floatingPoint: true
        });
    }

    // append extension to the glsl
    let extension = "#extension GL_EXT_draw_buffers : require \n";

    // build quad for drawing
    let quad = createQuad(gl, {
        fragmentShader: extension + simulation,
        uniformMap: uniformMap
    });

    rt1.bindBuffers();
    rt2.bindBuffers();

    return {
        gl: gl,
        quad: quad,
        rt1: rt1,
        rt2, rt2,
        bindTexture: function (index = 0) {
            this.rt2.bindTexture(index);
        },
        bindTextures: function () {
            this.rt2.bindTextures();
        },
        update: function (cb) {
            let gl = this.gl;
            let rt1 = this.rt1;
            let rt2 = this.rt2;

            // bind the write texture
            rt1.bindFbo();
            // bind previous buffer's texture
            rt2.bindTextures();
            // draw the quad
            cb(this.quad.shader);
            this.quad.draw();

            rt1.unbindFbo();
            // swap
            var tmp = this.rt2;
            this.rt2 = this.rt1;
            this.rt1 = tmp;
        }
    };
}

/**
 * A basic, pingpong setup. Generates it's own textures at the moment.
 * @param gl {WebGLRenderingContext} a webgl context
 * @param simulation {Number} the shader for the manipulating the texture data
 * @param width {Number} width for the textures
 * @param height {Number} height for the textures
 * @returns {{gl: *, flag: number, rt1: ({gl, drawTexture, fbo, bindFbo, unbindFbo, bindTexture, unbindTexture}|{gl: *, drawTexture: *, fbo: *, bindFbo: bindFbo, unbindFbo: unbindFbo, bind: bind, unbind: unbind}), rt2: ({gl, drawTexture, fbo, bindFbo, unbindFbo, bindTexture, unbindTexture}|{gl: *, drawTexture: *, fbo: *, bindFbo: bindFbo, unbindFbo: unbindFbo, bind: bind, unbind: unbind}), quad: ({vao, shader, buffer, type, hasTexture, gl, draw}|{vao: ({gl, vao, ext, attributes, setAttributeLocation, enableAttributes, addAttribute, getAttribute, enableAttribute, disableAttribute, setData, point, bind, unbind}|*), shader: ({gl, program, uniforms, attributes, bind, setMatrixUniform, setTextureUniform, uniform}|*), buffer: *, hasTexture: boolean, gl: *, draw: draw}), update: update, getOutput: getOutput}}
 */

// Ported from Stefan Gustavson's java implementation
// http://staffwww.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf
// Read Stefan's excellent paper for details on how this code works.
//
// Sean McCullough banksean@gmail.com
//
// Added 4D noise
// Joshua Koo zz85nus@gmail.com

/**
 * You can pass in a random number generator object if you like.
 * It is assumed to have a random() method.
 */
var SimplexNoise = function (r) {
    if (r == undefined) r = Math;
    this.grad3 = [[1, 1, 0], [-1, 1, 0], [1, -1, 0], [-1, -1, 0], [1, 0, 1], [-1, 0, 1], [1, 0, -1], [-1, 0, -1], [0, 1, 1], [0, -1, 1], [0, 1, -1], [0, -1, -1]];

    this.grad4 = [[0, 1, 1, 1], [0, 1, 1, -1], [0, 1, -1, 1], [0, 1, -1, -1], [0, -1, 1, 1], [0, -1, 1, -1], [0, -1, -1, 1], [0, -1, -1, -1], [1, 0, 1, 1], [1, 0, 1, -1], [1, 0, -1, 1], [1, 0, -1, -1], [-1, 0, 1, 1], [-1, 0, 1, -1], [-1, 0, -1, 1], [-1, 0, -1, -1], [1, 1, 0, 1], [1, 1, 0, -1], [1, -1, 0, 1], [1, -1, 0, -1], [-1, 1, 0, 1], [-1, 1, 0, -1], [-1, -1, 0, 1], [-1, -1, 0, -1], [1, 1, 1, 0], [1, 1, -1, 0], [1, -1, 1, 0], [1, -1, -1, 0], [-1, 1, 1, 0], [-1, 1, -1, 0], [-1, -1, 1, 0], [-1, -1, -1, 0]];

    this.p = [];
    for (var i = 0; i < 256; i++) {
        this.p[i] = Math.floor(r.random() * 256);
    }
    // To remove the need for index wrapping, double the permutation table length
    this.perm = [];
    for (var i = 0; i < 512; i++) {
        this.perm[i] = this.p[i & 255];
    }

    // A lookup table to traverse the simplex around a given point in 4D.
    // Details can be found where this table is used, in the 4D noise method.
    this.simplex = [[0, 1, 2, 3], [0, 1, 3, 2], [0, 0, 0, 0], [0, 2, 3, 1], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [1, 2, 3, 0], [0, 2, 1, 3], [0, 0, 0, 0], [0, 3, 1, 2], [0, 3, 2, 1], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [1, 3, 2, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [1, 2, 0, 3], [0, 0, 0, 0], [1, 3, 0, 2], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [2, 3, 0, 1], [2, 3, 1, 0], [1, 0, 2, 3], [1, 0, 3, 2], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [2, 0, 3, 1], [0, 0, 0, 0], [2, 1, 3, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [2, 0, 1, 3], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [3, 0, 1, 2], [3, 0, 2, 1], [0, 0, 0, 0], [3, 1, 2, 0], [2, 1, 0, 3], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [3, 1, 0, 2], [0, 0, 0, 0], [3, 2, 0, 1], [3, 2, 1, 0]];
};

SimplexNoise.prototype.dot = function (g, x, y) {
    return g[0] * x + g[1] * y;
};

SimplexNoise.prototype.dot3 = function (g, x, y, z) {
    return g[0] * x + g[1] * y + g[2] * z;
};

SimplexNoise.prototype.dot4 = function (g, x, y, z, w) {
    return g[0] * x + g[1] * y + g[2] * z + g[3] * w;
};

SimplexNoise.prototype.noise = function (xin, yin) {
    var n0, n1, n2; // Noise contributions from the three corners
    // Skew the input space to determine which simplex cell we're in
    var F2 = 0.5 * (Math.sqrt(3.0) - 1.0);
    var s = (xin + yin) * F2; // Hairy factor for 2D
    var i = Math.floor(xin + s);
    var j = Math.floor(yin + s);
    var G2 = (3.0 - Math.sqrt(3.0)) / 6.0;
    var t = (i + j) * G2;
    var X0 = i - t; // Unskew the cell origin back to (x,y) space
    var Y0 = j - t;
    var x0 = xin - X0; // The x,y distances from the cell origin
    var y0 = yin - Y0;
    // For the 2D case, the simplex shape is an equilateral triangle.
    // Determine which simplex we are in.
    var i1, j1; // Offsets for second (middle) corner of simplex in (i,j) coords
    if (x0 > y0) {
        i1 = 1;j1 = 0;
    } // lower triangle, XY order: (0,0)->(1,0)->(1,1)
    else {
            i1 = 0;j1 = 1;
        } // upper triangle, YX order: (0,0)->(0,1)->(1,1)
    // A step of (1,0) in (i,j) means a step of (1-c,-c) in (x,y), and
    // a step of (0,1) in (i,j) means a step of (-c,1-c) in (x,y), where
    // c = (3-sqrt(3))/6
    var x1 = x0 - i1 + G2; // Offsets for middle corner in (x,y) unskewed coords
    var y1 = y0 - j1 + G2;
    var x2 = x0 - 1.0 + 2.0 * G2; // Offsets for last corner in (x,y) unskewed coords
    var y2 = y0 - 1.0 + 2.0 * G2;
    // Work out the hashed gradient indices of the three simplex corners
    var ii = i & 255;
    var jj = j & 255;
    var gi0 = this.perm[ii + this.perm[jj]] % 12;
    var gi1 = this.perm[ii + i1 + this.perm[jj + j1]] % 12;
    var gi2 = this.perm[ii + 1 + this.perm[jj + 1]] % 12;
    // Calculate the contribution from the three corners
    var t0 = 0.5 - x0 * x0 - y0 * y0;
    if (t0 < 0) n0 = 0.0;else {
        t0 *= t0;
        n0 = t0 * t0 * this.dot(this.grad3[gi0], x0, y0); // (x,y) of grad3 used for 2D gradient
    }
    var t1 = 0.5 - x1 * x1 - y1 * y1;
    if (t1 < 0) n1 = 0.0;else {
        t1 *= t1;
        n1 = t1 * t1 * this.dot(this.grad3[gi1], x1, y1);
    }
    var t2 = 0.5 - x2 * x2 - y2 * y2;
    if (t2 < 0) n2 = 0.0;else {
        t2 *= t2;
        n2 = t2 * t2 * this.dot(this.grad3[gi2], x2, y2);
    }
    // Add contributions from each corner to get the final noise value.
    // The result is scaled to return values in the interval [-1,1].
    return 70.0 * (n0 + n1 + n2);
};

// 3D simplex noise
SimplexNoise.prototype.noise3d = function (xin, yin, zin) {
    var n0, n1, n2, n3; // Noise contributions from the four corners
    // Skew the input space to determine which simplex cell we're in
    var F3 = 1.0 / 3.0;
    var s = (xin + yin + zin) * F3; // Very nice and simple skew factor for 3D
    var i = Math.floor(xin + s);
    var j = Math.floor(yin + s);
    var k = Math.floor(zin + s);
    var G3 = 1.0 / 6.0; // Very nice and simple unskew factor, too
    var t = (i + j + k) * G3;
    var X0 = i - t; // Unskew the cell origin back to (x,y,z) space
    var Y0 = j - t;
    var Z0 = k - t;
    var x0 = xin - X0; // The x,y,z distances from the cell origin
    var y0 = yin - Y0;
    var z0 = zin - Z0;
    // For the 3D case, the simplex shape is a slightly irregular tetrahedron.
    // Determine which simplex we are in.
    var i1, j1, k1; // Offsets for second corner of simplex in (i,j,k) coords
    var i2, j2, k2; // Offsets for third corner of simplex in (i,j,k) coords
    if (x0 >= y0) {
        if (y0 >= z0) {
            i1 = 1;j1 = 0;k1 = 0;i2 = 1;j2 = 1;k2 = 0;
        } // X Y Z order
        else if (x0 >= z0) {
                i1 = 1;j1 = 0;k1 = 0;i2 = 1;j2 = 0;k2 = 1;
            } // X Z Y order
            else {
                    i1 = 0;j1 = 0;k1 = 1;i2 = 1;j2 = 0;k2 = 1;
                } // Z X Y order
    } else {
        // x0<y0
        if (y0 < z0) {
            i1 = 0;j1 = 0;k1 = 1;i2 = 0;j2 = 1;k2 = 1;
        } // Z Y X order
        else if (x0 < z0) {
                i1 = 0;j1 = 1;k1 = 0;i2 = 0;j2 = 1;k2 = 1;
            } // Y Z X order
            else {
                    i1 = 0;j1 = 1;k1 = 0;i2 = 1;j2 = 1;k2 = 0;
                } // Y X Z order
    }
    // A step of (1,0,0) in (i,j,k) means a step of (1-c,-c,-c) in (x,y,z),
    // a step of (0,1,0) in (i,j,k) means a step of (-c,1-c,-c) in (x,y,z), and
    // a step of (0,0,1) in (i,j,k) means a step of (-c,-c,1-c) in (x,y,z), where
    // c = 1/6.
    var x1 = x0 - i1 + G3; // Offsets for second corner in (x,y,z) coords
    var y1 = y0 - j1 + G3;
    var z1 = z0 - k1 + G3;
    var x2 = x0 - i2 + 2.0 * G3; // Offsets for third corner in (x,y,z) coords
    var y2 = y0 - j2 + 2.0 * G3;
    var z2 = z0 - k2 + 2.0 * G3;
    var x3 = x0 - 1.0 + 3.0 * G3; // Offsets for last corner in (x,y,z) coords
    var y3 = y0 - 1.0 + 3.0 * G3;
    var z3 = z0 - 1.0 + 3.0 * G3;
    // Work out the hashed gradient indices of the four simplex corners
    var ii = i & 255;
    var jj = j & 255;
    var kk = k & 255;
    var gi0 = this.perm[ii + this.perm[jj + this.perm[kk]]] % 12;
    var gi1 = this.perm[ii + i1 + this.perm[jj + j1 + this.perm[kk + k1]]] % 12;
    var gi2 = this.perm[ii + i2 + this.perm[jj + j2 + this.perm[kk + k2]]] % 12;
    var gi3 = this.perm[ii + 1 + this.perm[jj + 1 + this.perm[kk + 1]]] % 12;
    // Calculate the contribution from the four corners
    var t0 = 0.6 - x0 * x0 - y0 * y0 - z0 * z0;
    if (t0 < 0) n0 = 0.0;else {
        t0 *= t0;
        n0 = t0 * t0 * this.dot3(this.grad3[gi0], x0, y0, z0);
    }
    var t1 = 0.6 - x1 * x1 - y1 * y1 - z1 * z1;
    if (t1 < 0) n1 = 0.0;else {
        t1 *= t1;
        n1 = t1 * t1 * this.dot3(this.grad3[gi1], x1, y1, z1);
    }
    var t2 = 0.6 - x2 * x2 - y2 * y2 - z2 * z2;
    if (t2 < 0) n2 = 0.0;else {
        t2 *= t2;
        n2 = t2 * t2 * this.dot3(this.grad3[gi2], x2, y2, z2);
    }
    var t3 = 0.6 - x3 * x3 - y3 * y3 - z3 * z3;
    if (t3 < 0) n3 = 0.0;else {
        t3 *= t3;
        n3 = t3 * t3 * this.dot3(this.grad3[gi3], x3, y3, z3);
    }
    // Add contributions from each corner to get the final noise value.
    // The result is scaled to stay just inside [-1,1]
    return 32.0 * (n0 + n1 + n2 + n3);
};

// 4D simplex noise
SimplexNoise.prototype.noise4d = function (x, y, z, w) {
    // For faster and easier lookups
    var grad4 = this.grad4;
    var simplex = this.simplex;
    var perm = this.perm;

    // The skewing and unskewing factors are hairy again for the 4D case
    var F4 = (Math.sqrt(5.0) - 1.0) / 4.0;
    var G4 = (5.0 - Math.sqrt(5.0)) / 20.0;
    var n0, n1, n2, n3, n4; // Noise contributions from the five corners
    // Skew the (x,y,z,w) space to determine which cell of 24 simplices we're in
    var s = (x + y + z + w) * F4; // Factor for 4D skewing
    var i = Math.floor(x + s);
    var j = Math.floor(y + s);
    var k = Math.floor(z + s);
    var l = Math.floor(w + s);
    var t = (i + j + k + l) * G4; // Factor for 4D unskewing
    var X0 = i - t; // Unskew the cell origin back to (x,y,z,w) space
    var Y0 = j - t;
    var Z0 = k - t;
    var W0 = l - t;
    var x0 = x - X0; // The x,y,z,w distances from the cell origin
    var y0 = y - Y0;
    var z0 = z - Z0;
    var w0 = w - W0;

    // For the 4D case, the simplex is a 4D shape I won't even try to describe.
    // To find out which of the 24 possible simplices we're in, we need to
    // determine the magnitude ordering of x0, y0, z0 and w0.
    // The method below is a good way of finding the ordering of x,y,z,w and
    // then find the correct traversal order for the simplex weâ€™re in.
    // First, six pair-wise comparisons are performed between each possible pair
    // of the four coordinates, and the results are used to add up binary bits
    // for an integer index.
    var c1 = x0 > y0 ? 32 : 0;
    var c2 = x0 > z0 ? 16 : 0;
    var c3 = y0 > z0 ? 8 : 0;
    var c4 = x0 > w0 ? 4 : 0;
    var c5 = y0 > w0 ? 2 : 0;
    var c6 = z0 > w0 ? 1 : 0;
    var c = c1 + c2 + c3 + c4 + c5 + c6;
    var i1, j1, k1, l1; // The integer offsets for the second simplex corner
    var i2, j2, k2, l2; // The integer offsets for the third simplex corner
    var i3, j3, k3, l3; // The integer offsets for the fourth simplex corner
    // simplex[c] is a 4-vector with the numbers 0, 1, 2 and 3 in some order.
    // Many values of c will never occur, since e.g. x>y>z>w makes x<z, y<w and x<w
    // impossible. Only the 24 indices which have non-zero entries make any sense.
    // We use a thresholding to set the coordinates in turn from the largest magnitude.
    // The number 3 in the "simplex" array is at the position of the largest coordinate.
    i1 = simplex[c][0] >= 3 ? 1 : 0;
    j1 = simplex[c][1] >= 3 ? 1 : 0;
    k1 = simplex[c][2] >= 3 ? 1 : 0;
    l1 = simplex[c][3] >= 3 ? 1 : 0;
    // The number 2 in the "simplex" array is at the second largest coordinate.
    i2 = simplex[c][0] >= 2 ? 1 : 0;
    j2 = simplex[c][1] >= 2 ? 1 : 0;k2 = simplex[c][2] >= 2 ? 1 : 0;
    l2 = simplex[c][3] >= 2 ? 1 : 0;
    // The number 1 in the "simplex" array is at the second smallest coordinate.
    i3 = simplex[c][0] >= 1 ? 1 : 0;
    j3 = simplex[c][1] >= 1 ? 1 : 0;
    k3 = simplex[c][2] >= 1 ? 1 : 0;
    l3 = simplex[c][3] >= 1 ? 1 : 0;
    // The fifth corner has all coordinate offsets = 1, so no need to look that up.
    var x1 = x0 - i1 + G4; // Offsets for second corner in (x,y,z,w) coords
    var y1 = y0 - j1 + G4;
    var z1 = z0 - k1 + G4;
    var w1 = w0 - l1 + G4;
    var x2 = x0 - i2 + 2.0 * G4; // Offsets for third corner in (x,y,z,w) coords
    var y2 = y0 - j2 + 2.0 * G4;
    var z2 = z0 - k2 + 2.0 * G4;
    var w2 = w0 - l2 + 2.0 * G4;
    var x3 = x0 - i3 + 3.0 * G4; // Offsets for fourth corner in (x,y,z,w) coords
    var y3 = y0 - j3 + 3.0 * G4;
    var z3 = z0 - k3 + 3.0 * G4;
    var w3 = w0 - l3 + 3.0 * G4;
    var x4 = x0 - 1.0 + 4.0 * G4; // Offsets for last corner in (x,y,z,w) coords
    var y4 = y0 - 1.0 + 4.0 * G4;
    var z4 = z0 - 1.0 + 4.0 * G4;
    var w4 = w0 - 1.0 + 4.0 * G4;
    // Work out the hashed gradient indices of the five simplex corners
    var ii = i & 255;
    var jj = j & 255;
    var kk = k & 255;
    var ll = l & 255;
    var gi0 = perm[ii + perm[jj + perm[kk + perm[ll]]]] % 32;
    var gi1 = perm[ii + i1 + perm[jj + j1 + perm[kk + k1 + perm[ll + l1]]]] % 32;
    var gi2 = perm[ii + i2 + perm[jj + j2 + perm[kk + k2 + perm[ll + l2]]]] % 32;
    var gi3 = perm[ii + i3 + perm[jj + j3 + perm[kk + k3 + perm[ll + l3]]]] % 32;
    var gi4 = perm[ii + 1 + perm[jj + 1 + perm[kk + 1 + perm[ll + 1]]]] % 32;
    // Calculate the contribution from the five corners
    var t0 = 0.6 - x0 * x0 - y0 * y0 - z0 * z0 - w0 * w0;
    if (t0 < 0) n0 = 0.0;else {
        t0 *= t0;
        n0 = t0 * t0 * this.dot4(grad4[gi0], x0, y0, z0, w0);
    }
    var t1 = 0.6 - x1 * x1 - y1 * y1 - z1 * z1 - w1 * w1;
    if (t1 < 0) n1 = 0.0;else {
        t1 *= t1;
        n1 = t1 * t1 * this.dot4(grad4[gi1], x1, y1, z1, w1);
    }
    var t2 = 0.6 - x2 * x2 - y2 * y2 - z2 * z2 - w2 * w2;
    if (t2 < 0) n2 = 0.0;else {
        t2 *= t2;
        n2 = t2 * t2 * this.dot4(grad4[gi2], x2, y2, z2, w2);
    }var t3 = 0.6 - x3 * x3 - y3 * y3 - z3 * z3 - w3 * w3;
    if (t3 < 0) n3 = 0.0;else {
        t3 *= t3;
        n3 = t3 * t3 * this.dot4(grad4[gi3], x3, y3, z3, w3);
    }
    var t4 = 0.6 - x4 * x4 - y4 * y4 - z4 * z4 - w4 * w4;
    if (t4 < 0) n4 = 0.0;else {
        t4 *= t4;
        n4 = t4 * t4 * this.dot4(grad4[gi4], x4, y4, z4, w4);
    }
    // Sum up and scale the result to cover the range [-1,1]
    return 27.0 * (n0 + n1 + n2 + n3 + n4);
};

var sim = "precision highp float;\nuniform sampler2D pos;\nuniform sampler2D vel;\nuniform sampler2D startTime;\nuniform sampler2D initVelocity;\nuniform float Time;\nconst float H =  1.0 / 60.0;\nconst float ParticleLifetime = 1.0;\nconst vec3 Accel = vec3(0.);\nvarying vec2 uv;\nvoid main (){\n    vec2 sampleSize = gl_FragCoord.xy / vec2(512.0);\n    vec4 Position = texture2D(pos,sampleSize);\n    vec4 Velocity = texture2D(vel,sampleSize);\n    vec4 StartTime = texture2D(startTime,sampleSize);\n    vec4 initVelo = texture2D(initVelocity,sampleSize);\n    float accelerant = 20.0;\n    float age = Time - StartTime.x;\n    vec4 outTest = vec4(0.);\n\tif( Time >= StartTime.x ) {\n\t\tfloat age = Time - StartTime.x;\n\t\tif( age > ParticleLifetime ) {\n\t\t\tPosition = vec4(0.0);\n\t\t\tVelocity = initVelo;\n\t\t\tStartTime.x = Time;\n\t\t}\n\t\telse {\n\t\t\tPosition += Velocity * H * accelerant;\n\t\t\tVelocity += vec4(Accel,1.0) * H * accelerant;\n\t\t}\n\t}\n    gl_FragData[0] = Position;\n    gl_FragData[1] = Velocity;\n    gl_FragData[2] = StartTime;\n}\n";

var rvertex = "uniform mat4 projectionMatrix;\nuniform mat4 viewMatrix;\nuniform mat4 modelMatrix;\nuniform float theta;\nattribute vec3 position;\nattribute vec3 offset;\nuniform sampler2D pos;\nuniform float Time;\nuniform sampler2D startTime;\nvarying float transp;\nvoid main(){\n    vec3 posOffset = position + offset;\n    vec4 calcPos = texture2D(pos,posOffset.xy);\n    vec4 calcTime = texture2D(startTime,posOffset.xy);\n    vec3 finalPos = calcPos.xyz;\n    float age = Time - calcTime.r;\n    if(Time >= calcTime.r){\n        float agePct = age / 3.0;\n        transp = 1.0 - agePct;\n    }\n    mat4 rot = rotationMatrix(vec3(1.0,0.0,1.0),theta);\n    mat4 model = modelMatrix + rot;\n    gl_Position = projectionMatrix * viewMatrix * model * vec4(finalPos,1);\n}";

var rfragment = "precision highp float;\nconst vec3 pink = vec3(1.0, 0.07, 0.57);\nvarying float transp;\nvoid main(){\n    gl_FragColor = vec4(1.0,1.0,0.0,1.0);\n}";

var rotation = "vec3 rotateX(vec3 p, float theta) {\n  float s = sin(theta);\n  float c = cos(theta);\n  return vec3(p.x, p.y * c - p.z * s, p.z * c + p.y * s);\n}\nvec3 rotateY(vec3 p, float theta) {\n  float s = sin(theta);\n  float c = cos(theta);\n  return vec3(p.x * c + p.z * s, p.y, p.z * c - p.x * s);\n}\nvec3 rotateZ(vec3 p, float theta) {\n  float s = sin(theta);\n  float c = cos(theta);\n  return vec3(p.x * c - p.y * s, p.y * c + p.x * s, p.z);\n}\nmat4 rotationMatrix(vec3 axis, float angle)\n{\n    axis = normalize(axis);\n    float s = sin(angle);\n    float c = cos(angle);\n    float oc = 1.0 - c;\n    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,\n                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,\n                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,\n                0.0,                                0.0,                                0.0,                                1.0);\n}";

// import shaders
class SmokeParticleSystem$1 extends Mesh {
    constructor(gl) {
        super(gl);

        this.instanced = gl.ANGLE_instanced_arrays;

        this.shader = createShader(gl, {
            vertex: [rotation, rvertex],
            fragment: rfragment,
            attributes: [['position', 3], ['offset', 3]],
            uniforms: ['projectionMatrix', 'modelMatrix', 'viewMatrix', 'Time', 'pos', 'startTime']
        });

        this.theta = 0.0;
        this.size = 512.0;
        this.s2 = this.size * this.size;
        this.time = 0.0;
        this.rand = new SimplexNoise();
        this._buildAttribs();
        //  this._buildBaseShape();
        //this._buildPositions();
        this._testParticles();
    }

    _buildBaseShape() {
        let gl = this.gl;
        let position = createVBO(gl);
        let vertices = [0.0, 0.5, 0.0, -0.5, -0.5, 0.0, 0.5, -0.5, 0.0];
        this.vao.bind();
        position.bind();
        position.bufferData(vertices);
        this.vao.addAttribute(this.shader, 'position', {
            setData: true
        });
        position.unbind();
        this.vao.unbind();
    }

    _buildPositions() {
        let gl = this.gl;
        let numParticles = 128 * 128;

        let set = new Float32Array(numParticles * 3);
        for (var i = 0; i < set.length; ++i) {
            set[i] = Math.random() * 20;
        }

        let position = createVBO(gl);
        this.vao.bind();
        position.bind();
        position.bufferData(set);
        this.vao.addAttribute(this.shader, 'offset', {
            setData: true
        });
        this.instanced.vertexAttribDivisorANGLE(this.vao.getAttribute('offset'), 1);
        position.unbind();
        this.vao.unbind();
    }

    _buildAttribs() {
        let gl = this.gl;
        let size = this.s2 * 4;
        let baseArray = new Float32Array(size);
        // setup positions
        let positionTex = generatePingpongTexture(gl, baseArray, {
            width: this.size,
            height: this.size
        });

        //setup normals, we'll reuse the positions array
        let normals = new Float32Array(size);
        let len = normals.length;
        let radius = 20;
        for (var i = 0; i < len; ++i) {
            let n = this.rand.noise(0.0, 1.5) * radius;
            normals[i] = (Math.random() - 0.5) * n;
            normals[i + 1] = (Math.random() - 0.5) * n;
            normals[i + 2] = (Math.random() - 0.5) * n;
            normals[i + 3] = (Math.random() - 0.5) * n;
        }
        let velocityTex = generatePingpongTexture(gl, normals, {
            width: this.size,
            height: this.size
        });

        // a texture that can be used to reset things when a particle is dead
        let initVelocity = createTexture2d(gl, {
            width: this.size,
            height: this.size,
            data: normals,
            textureOptions: {
                type: FLOAT
            }
        });

        let timeData = new Float32Array(size);
        let time = 0;
        let rate = 0.001;
        // it's only a float value, so we only need to store something in
        // the red channel
        for (var i = 0; i < size; i += 4) {
            timeData[i] = time;
            time += rate;
        }
        let timeTex = generatePingpongTexture(gl, timeData, {
            width: this.size,
            height: this.size
        });

        // generate ping pong-ing buffer
        this.buffer = createMultiPingpongBuffer(gl, sim, {
            width: this.size,
            height: this.size,
            textureMap: [positionTex, velocityTex, timeTex],
            uniformMap: ['pos', 'vel', 'startTime', 'Time', 'initVelocity']
        });

        this.initVelocity = initVelocity;
    }

    _testParticles() {
        let gl = this.gl;
        let position = createVBO(gl);
        let vertices = new Float32Array(this.s2 * 4);
        let len = vertices.length;
        for (var i = 0; i < len; ++i) {
            vertices[i] = Math.random();
        }
        this.vao.bind();
        position.bind();
        position.bufferData(vertices);
        this.vao.addAttribute(this.shader, 'position', {
            setData: true
        });
        position.unbind();
        this.vao.unbind();
        this.num = vertices.length / 3;
    }
    update() {
        let gl = this.gl;
        this.time += 1.0;
        this.theta += 200.1;
        gl.disable(gl.BLEND);
        this.buffer.update(shader => {
            shader.bind();
            this.initVelocity.bind(3);
            shader.uniform("Time", this.time / 60.0);
            shader.setTextureUniform('pos', 0);
            shader.setTextureUniform('vel', 1);
            shader.setTextureUniform('startTime', 2);
            shader.setTextureUniform('initVelocity', 3);
        });
    }

    draw(camera) {
        let gl = this.gl;
        this.gl.bindTexture(TEXTURE_2D, null);

        gl.disable(gl.DEPTH_TEST);
        gl.enable(gl.BLEND);
        gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);

        this.shader.bind();

        // remember - this occupies texture slots 0-2
        this.buffer.bindTextures();
        this.shader.uniform('projectionMatrix', camera.projection);
        this.shader.uniform('viewMatrix', camera.view);
        this.shader.uniform('modelMatrix', this.model);
        this.shader.uniform('theta', this.theta);
        this.shader.uniform('Time', this.time);
        this.shader.setTextureUniform('pos', 0);
        this.shader.setTextureUniform('startTime', 2);

        this.vao.bind();
        //this.gl.drawInstancedArrays(TRIANGLES,0,3,128 * 128);
        this.gl.drawArrays(POINTS, 0, this.num);
        this.vao.unbind();
    }
}

/**
 * Flattens an nested array that is assumed to be nested with child arrays used in place of
 * an actual vector object. Note, this does not check for completeness and will automatically
 * only take the first 3 values of the child arrays
 * @param array the parent array
 * @returns {Array}
 */
function flattenArray(array) {

    let fin = [];
    let len = array.length;
    for (var i = 0; i < len; ++i) {
        let arr = array[i];
        fin.push(arr[0], arr[1], arr[2]);
    }
    return fin;
}

/**
 * Does subtraction between two arrays. Assumes both arrays have 3 values each inside
 * @param array1 {Array} the array to subtract from
 * @param array2 {Array} the array to subtract
 * @returns {*[]}
 */






/**
 * Creates an array with a range of values
 * @param from {Number} the value to start from
 * @param to {Number} the value end at.
 * @returns {Array}
 */


/**
 * Returns a random vec3(in the form of an array)
 * @returns {*[]}
 */


/**
 * Very simple array cloning util.
 * Note - only works with arrays who have 3 elements
 * @param arrayToClone the array to clone
 * @returns {*[]} the new array
 */

// port of the Three.js explode modifier.
function explode(geo) {
    var vertices = [];

    for (var i = 0, il = geo.cells.length; i < il; i++) {

        var n = vertices.length;
        var face = geo.cells[i];
        var a = face[0];
        var b = face[1];
        var c = face[2];

        var va = geo.positions[a];
        var vb = geo.positions[b];
        var vc = geo.positions[c];

        vertices.push([va[0], va[1], va[2]]);
        vertices.push([vb[0], vb[1], vb[2]]);
        vertices.push([vc[0], vc[1], vc[2]]);

        face[0] = n;
        face[1] = n + 1;
        face[2] = n + 2;
    }

    geo.positions = vertices;
    return geo;
}

var vert = "\nuniform mat4 viewMatrix;\nuniform mat4 projectionMatrix;\nuniform mat4 modelMatrix;\nuniform sampler2D amplitude;\nattribute vec3 position;\nattribute vec3 displacement;\nvarying vec3 vDisplacement;\nvarying vec3 vPosition;\nvoid main(){\n    vDisplacement = displacement;\n    vec3 pos = position + vec3(0.2)  * displacement;   vPosition = pos;\n    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(pos,1.);\n}";

var frag = "precision highp float;\nuniform vec2 iResolution;\nuniform sampler2D amplitude;\nvarying vec3 vDisplacement;\nvarying vec3 vPosition;\nuniform float time;\nvoid main(){\n    vec4 amp = vec4(1.);\n    vec2 u = gl_FragCoord.xy;\n    u /= iResolution.xy;\n    vec3 color = vec3(0.);\n    color.xy = .5 - u;\n    float t = time;\n    float z = atan(color.y,color.x) + 3.;\n    float v = cos(z + sin(t * .1)) + 0.5 * cos(u.x * 10. * 1.3) * .4;\n    color.x = 1.2 + sin(z + t * .2) + sin(u.y*10.+t*1.5)*.5;\n \tcolor.yz = vec2( sin(v*4.)*.25, sin(v*2.)*.3 ) + color.x*.5;\n    color.xy * amp.xy;\n \tcolor *= vPosition * vDisplacement;\n    gl_FragColor = vec4(color,1.);\n}";

var normalizeNd = createCommonjsModule(function (module) {
  module.exports = normalize;

  function normalize(vec) {
    var mag = 0;
    for (var n = 0; n < vec.length; n++) {
      mag += vec[n] * vec[n];
    }
    mag = Math.sqrt(mag);

    // avoid dividing by zero
    if (mag === 0) {
      return Array.apply(null, new Array(vec.length)).map(Number.prototype.valueOf, 0);
    }

    for (var n = 0; n < vec.length; n++) {
      vec[n] /= mag;
    }

    return vec;
  }
});

var index$22 = createCommonjsModule(function (module) {
  var normalize = normalizeNd;

  module.exports = icosphere;

  function icosphere(subdivisions) {
    subdivisions = +subdivisions | 0;

    var positions = [];
    var faces = [];
    var t = 0.5 + Math.sqrt(5) / 2;

    positions.push([-1, +t, 0]);
    positions.push([+1, +t, 0]);
    positions.push([-1, -t, 0]);
    positions.push([+1, -t, 0]);

    positions.push([0, -1, +t]);
    positions.push([0, +1, +t]);
    positions.push([0, -1, -t]);
    positions.push([0, +1, -t]);

    positions.push([+t, 0, -1]);
    positions.push([+t, 0, +1]);
    positions.push([-t, 0, -1]);
    positions.push([-t, 0, +1]);

    faces.push([0, 11, 5]);
    faces.push([0, 5, 1]);
    faces.push([0, 1, 7]);
    faces.push([0, 7, 10]);
    faces.push([0, 10, 11]);

    faces.push([1, 5, 9]);
    faces.push([5, 11, 4]);
    faces.push([11, 10, 2]);
    faces.push([10, 7, 6]);
    faces.push([7, 1, 8]);

    faces.push([3, 9, 4]);
    faces.push([3, 4, 2]);
    faces.push([3, 2, 6]);
    faces.push([3, 6, 8]);
    faces.push([3, 8, 9]);

    faces.push([4, 9, 5]);
    faces.push([2, 4, 11]);
    faces.push([6, 2, 10]);
    faces.push([8, 6, 7]);
    faces.push([9, 8, 1]);

    var complex = {
      cells: faces,
      positions: positions
    };

    while (subdivisions-- > 0) {
      complex = subdivide(complex);
    }

    positions = complex.positions;
    for (var i = 0; i < positions.length; i++) {
      normalize(positions[i]);
    }

    return complex;
  }

  // TODO: work out the second half of loop subdivision
  // and extract this into its own module.
  function subdivide(complex) {
    var positions = complex.positions;
    var cells = complex.cells;

    var newCells = [];
    var newPositions = [];
    var midpoints = {};
    var f = [0, 1, 2];
    var l = 0;

    for (var i = 0; i < cells.length; i++) {
      var cell = cells[i];
      var c0 = cell[0];
      var c1 = cell[1];
      var c2 = cell[2];
      var v0 = positions[c0];
      var v1 = positions[c1];
      var v2 = positions[c2];

      var a = getMidpoint(v0, v1);
      var b = getMidpoint(v1, v2);
      var c = getMidpoint(v2, v0);

      var ai = newPositions.indexOf(a);
      if (ai === -1) ai = l++, newPositions.push(a);
      var bi = newPositions.indexOf(b);
      if (bi === -1) bi = l++, newPositions.push(b);
      var ci = newPositions.indexOf(c);
      if (ci === -1) ci = l++, newPositions.push(c);

      var v0i = newPositions.indexOf(v0);
      if (v0i === -1) v0i = l++, newPositions.push(v0);
      var v1i = newPositions.indexOf(v1);
      if (v1i === -1) v1i = l++, newPositions.push(v1);
      var v2i = newPositions.indexOf(v2);
      if (v2i === -1) v2i = l++, newPositions.push(v2);

      newCells.push([v0i, ai, ci]);
      newCells.push([v1i, bi, ai]);
      newCells.push([v2i, ci, bi]);
      newCells.push([ai, bi, ci]);
    }

    return {
      cells: newCells,
      positions: newPositions
    };

    // reuse midpoint vertices between iterations.
    // Otherwise, there'll be duplicate vertices in the final
    // mesh, resulting in sharp edges.
    function getMidpoint(a, b) {
      var point = midpoint(a, b);
      var pointKey = pointToKey(point);
      var cachedPoint = midpoints[pointKey];
      if (cachedPoint) {
        return cachedPoint;
      } else {
        return midpoints[pointKey] = point;
      }
    }

    function pointToKey(point) {
      return point[0].toPrecision(6) + ',' + point[1].toPrecision(6) + ',' + point[2].toPrecision(6);
    }

    function midpoint(a, b) {
      return [(a[0] + b[0]) / 2, (a[1] + b[1]) / 2, (a[2] + b[2]) / 2];
    }
  }
});

//import {createIco} from "./ico"
class Speaker extends Mesh {
    constructor(gl) {
        super(gl);

        this.shader = createShader(gl, {
            vertex: vert,
            fragment: frag,
            attributes: [['position', 3], ['displacement', 3]],
            uniforms: ['projectionMatrix', 'viewMatrix', 'modelMatrix', 'amplitude', 'iResolution', 'time']
        });
        this.time = 0.0;
        this.resolution = new Float32Array([window.innerWidth, window.innerHeight]);

        this._build();
    }

    _build() {
        let gl = this.gl;
        let data = index$22(2);

        data = explode(data);

        let displacement = createVBO(gl);
        let positions = createVBO(gl);
        let idx = createVBO(gl, {
            indexed: true
        });

        this.vao.bind();

        // buffer positions
        positions.bind();
        positions.bufferData(flattenArray(data.positions));
        this.vao.addAttribute(this.shader, 'position', {
            setData: true
        });

        // buffer indices
        idx.bind();
        idx.bufferData(flattenArray(data.cells));

        displacement.bind();
        var displaceData = [];
        var numFaces = data.cells.length;
        for (var f = 0; f < numFaces; ++f) {
            var index = 9 * f;
            var d = 10 * (0.5 - Math.random());

            for (var i = 0; i < 3; ++i) {
                displaceData[index + 3 * i] = d;
                displaceData[index + 3 * i + 1] = d;
                displaceData[index + 3 * i + 2] = d;
            }
        }
        displacement.bufferData(displaceData);
        this.vao.addAttribute(this.shader, 'displacement', {
            setData: true
        });

        this.vao.unbind();
        displacement.unbind();
        positions.unbind();
        idx.unbind();
        this.num = data.cells.length * 3;

        // scale the base shape so we can see it.
        this.scaleModel(25, 25, 25);
    }

    draw(camera) {
        var dt = Date.now() * 0.001;
        this.time += 0.1;
        let gl = this.gl;

        this.shader.bind();
        //analyser.bindFrequencies(0);
        this.shader.setMatrixUniform('projectionMatrix', camera.projection);
        this.shader.setMatrixUniform('viewMatrix', camera.view);
        this.shader.setMatrixUniform('modelMatrix', this.model);
        // this.shader.uniform('amplitude',1 + Math.sin(dt * 0.5));
        this.shader.uniform('time', this.time);
        this.gl.uniform2fv(this.shader.getUniform('iResolution'), this.resolution);
        this.rotateY(0.02);
        this.rotateZ(0.02);

        this.vao.bind();
        this.gl.drawElements(this.gl.TRIANGLES, this.num, UNSIGNED_SHORT, 0);
        this.vao.unbind();
    }

}

// background for everything
class Background {
    constructor(gl, fragment) {
        this.gl = gl;
        this.quad = createQuad(gl, {
            fragmentShader: fragment,
            uniformMap: ['time']
        });

        this.time = 0.0;
    }

    draw() {
        this.time += 1.0;
        this.quad.draw(shader => {
            shader.uniform('time', this.time);
        });
    }
}

/**
 * Load audio file for playback
 * @param path path to file
 * @param cb callback for when it's ok to start playing the audio
 */

var background = "precision highp float;\nvoid main(){\n    gl_FragColor = vec4(1.0,1.0,0.0,1.0);\n}";

var Node = createCommonjsModule(function (module) {
    function Node() {
        this._element = null;

        switch (arguments.length) {
            case 1:
                var arg = arguments[0];
                if (arg != Node.INPUT_TEXT && arg != Node.INPUT_BUTTON && arg != Node.INPUT_SELECT && arg != Node.INPUT_CHECKBOX) {
                    this._element = document.createElement(arg);
                } else {
                    this._element = document.createElement('input');
                    this._element.type = arg;
                }
                break;
            case 0:
                this._element = document.createElement('div');
                break;
        }
    }

    Node.DIV = 'div';
    Node.INPUT_TEXT = 'text';
    Node.INPUT_BUTTON = 'button';
    Node.INPUT_SELECT = 'select';
    Node.INPUT_CHECKBOX = 'checkbox';
    Node.OPTION = 'option';
    Node.LIST = 'ul';
    Node.LIST_ITEM = 'li';
    Node.SPAN = 'span';
    Node.TEXTAREA = 'textarea';

    Node.prototype = {
        addChild: function (node) {
            this._element.appendChild(node.getElement());
            return node;
        },
        addChildren: function () {
            var i = -1,
                l = arguments.length,
                e = this._element;
            while (++i < l) {
                e.appendChild(arguments[i].getElement());
            }
            return this;
        },
        addChildAt: function (node, index) {
            this._element.insertBefore(node.getElement(), this._element.children[index]);
            return node;
        },
        removeChild: function (node) {
            if (!this.contains(node)) return null;
            this._element.removeChild(node.getElement());
            return node;
        },
        removeChildren: function () {
            var i = -1,
                l = arguments.length,
                e = this._element;
            while (++i < l) {
                e.removeChild(arguments[i].getElement());
            }
            return this;
        },
        removeChildAt: function (node, index) {
            if (!this.contains(node)) return null;
            this._element.removeChild(node.getElement());
            return node;
        },
        removeAllChildren: function () {
            var element = this._element;
            while (element.hasChildNodes()) element.removeChild(element.lastChild);
            return this;
        },
        setWidth: function (value) {
            this._element.style.width = value + 'px';
            return this;
        },
        getWidth: function () {
            return this._element.offsetWidth;
        },
        setHeight: function (value) {
            this._element.style.height = value + 'px';
            return this;
        },
        getHeight: function () {
            return this._element.offsetHeight;
        },
        setPosition: function (x, y) {
            return this.setPosition(x).setPosition(y);
        },
        setPositionX: function (x) {
            this._element.style.marginLeft = x + 'px';
            return this;
        },
        setPositionY: function (y) {
            this._element.style.marginTop = y + 'px';
            return this;
        },
        setPositionGlobal: function (x, y) {
            return this.setPositionGlobalX(x).setPositionGlobalY(y);
        },
        setPositionGlobalX: function (x) {
            this._element.style.left = x + 'px';
            return this;
        },
        setPositionGlobalY: function (y) {
            this._element.style.top = y + 'px';
            return this;
        },
        getPosition: function () {
            return [this.getPositionX(), this.getPositionY()];
        },
        getPositionX: function () {
            return this._element.offsetLeft;
        },
        getPositionY: function () {
            return this._element.offsetTop;
        },
        getPositionGlobal: function () {
            var offset = [0, 0],
                element = this._element;

            while (element) {
                offset[0] += element.offsetLeft;
                offset[1] += element.offsetTop;
                element = element.offsetParent;
            }

            return offset;
        },
        getPositionGlobalX: function () {
            var offset = 0,
                element = this._element;

            while (element) {
                offset += element.offsetLeft;
                element = element.offsetParent;
            }

            return offset;
        },
        getPositionGlobalY: function () {
            var offset = 0,
                element = this._element;

            while (element) {
                offset += element.offsetTop;
                element = element.offsetParent;
            }

            return offset;
        },
        addEventListener: function (type, listener, useCapture) {
            this._element.addEventListener(type, listener, useCapture);
            return this;
        },
        removeEventListener: function (type, listener, useCapture) {
            this._element.removeEventListener(type, listener, useCapture);
            return this;
        },
        dispatchEvent: function (event) {
            this._element.dispatchEvent(event);
            return this;
        },
        setStyleClass: function (style) {
            this._element.className = style;
            return this;
        },
        setStyleProperty: function (property, value) {
            this._element.style[property] = value;
            return this;
        },
        getStyleProperty: function (property) {
            return this._element.style[property];
        },
        setStyleProperties: function (properties) {
            for (var p in properties) {
                this._element.style[p] = properties[p];
            }
            return this;
        },
        deleteStyleClass: function () {
            this._element.className = '';
            return this;
        },
        deleteStyleProperty: function (property) {
            this._element.style[property] = '';
            return this;
        },
        deleteStyleProperties: function (properties) {
            for (var p in properties) {
                this._element.style[p] = '';
            }
            return this;
        },
        getChildAt: function (index) {
            return new Node().setElement(this._element.children[index]);
        },
        getChildIndex: function (node) {
            return this._indexOf(this._element, node.getElement());
        },
        getNumChildren: function () {
            return this._element.children.length;
        },
        getFirstChild: function () {
            return new Node().setElement(this._element.firstChild);
        },
        getLastChild: function () {
            return new Node().setElement(this._element.lastChild);
        },
        hasChildren: function () {
            return this._element.children.length != 0;
        },
        contains: function (node) {
            return this._indexOf(this._element, node.getElement()) != -1;
        },
        _indexOf: function (parentElement, element) {
            return Array.prototype.indexOf.call(parentElement.children, element);
        },
        setProperty: function (property, value) {
            this._element[property] = value;
            return this;
        },
        setProperties: function (properties) {
            for (var p in properties) {
                this._element[p] = properties[p];
            }
            return this;
        },
        getProperty: function (property) {
            return this._element[property];
        },
        setElement: function (element) {
            this._element = element;
            return this;
        },
        getElement: function () {
            return this._element;
        },
        getStyle: function () {
            return this._element.style;
        },
        getParent: function () {
            return new Node().setElement(this._element.parentNode);
        }
    };

    Node.getNodeByElement = function (element) {
        return new Node().setElement(element);
    };
    Node.getNodeById = function (id) {
        return new Node().setElement(document.getElementById(id));
    };

    module.exports = Node;
});

var EventDispatcher = createCommonjsModule(function (module) {
    function EventDispatcher() {
        this._listeners = [];
    }

    EventDispatcher.prototype = {
        addEventListener: function (eventType, listener, callbackMethod) {
            this._listeners[eventType] = this._listeners[eventType] || [];
            this._listeners[eventType].push({ obj: listener, method: callbackMethod });
        },

        dispatchEvent: function (event) {
            var type = event.type;

            if (!this.hasEventListener(type)) {
                return;
            }

            var listeners = this._listeners[type];
            var i = -1,
                l = listeners.length;

            var obj, method;

            while (++i < l) {
                obj = listeners[i].obj;
                method = listeners[i].method;

                if (!obj[method]) {
                    throw obj + ' has no method ' + method;
                }

                obj[method](event);
            }
        },

        removeEventListener: function (type, obj, method) {
            if (!this.hasEventListener(type)) {
                return;
            }

            var listeners = this._listeners[type];

            var i = listeners.length;
            while (--i > -1) {
                if (listeners[i].obj == obj && listeners[i].method == method) {
                    listeners.splice(i, 1);
                    if (listeners.length == 0) {
                        delete this._listeners[type];
                    }
                    break;
                }
            }
        },

        removeAllEventListeners: function () {
            this._listeners = [];
        },

        hasEventListener: function (type) {
            return this._listeners[type] != undefined && this._listeners[type] != null;
        }
    };

    module.exports = EventDispatcher;
});

var Metric = createCommonjsModule(function (module) {
	var Metric = {
		COMPONENT_MIN_HEIGHT: 25,
		STROKE_SIZE: 1,
		PADDING_WRAPPER: 12,
		PADDING_OPTIONS: 2,
		PADDING_PRESET: 20,

		SCROLLBAR_TRACK_PADDING: 2,
		FUNCTION_PLOTTER_LABEL_TICK_SIZE: 6
	};

	module.exports = Metric;
});

var CSS = createCommonjsModule(function (module) {
    var CSS = {
        ControlKit: 'controlKit',

        Panel: 'panel',
        Head: 'head',
        Label: 'label',
        Menu: 'menu',
        Wrap: 'wrap',

        ButtonMenuClose: 'button-menu-close',
        ButtonMenuHide: 'button-menu-hide',
        ButtonMenuShow: 'button-menu-show',
        ButtonMenuUndo: 'button-menu-undo',
        ButtonMenuLoad: 'button-menu-load',
        ButtonMenuSave: 'button-menu-save',
        MenuActive: 'menu-active',

        Button: 'button',
        ButtonPreset: 'button-preset',
        ButtonPresetActive: 'button-preset-active',

        WrapInputWPreset: 'input-with-preset-wrap',
        WrapColorWPreset: 'color-with-preset-wrap',

        HeadInactive: 'head-inactive',
        PanelHeadInactive: 'panel-head-inactive',

        GroupList: 'group-list',
        Group: 'group',
        SubGroupList: 'sub-group-list',
        SubGroup: 'sub-group',

        TextAreaWrap: 'textarea-wrap',

        WrapSlider: 'wrap-slider',
        SliderWrap: 'slider-wrap',
        SliderSlot: 'slider-slot',
        SliderHandle: 'slider-handle',

        ArrowBMin: 'arrow-b-min',
        ArrowBMax: 'arrow-b-max',
        ArrowBSubMin: 'arrow-b-sub-min',
        ArrowBSubMax: 'arrow-b-sub-max',
        ArrowSMin: 'arrow-s-min',
        ArrowSMax: 'arrow-s-max',

        Select: 'select',
        SelectActive: 'select-active',

        Options: 'options',
        OptionsSelected: 'li-selected',

        CanvasListItem: 'canvas-list-item',
        CanvasWrap: 'canvas-wrap',

        SVGListItem: 'svg-list-item',
        SVGWrap: 'svg-wrap',

        GraphSliderXWrap: 'graph-slider-x-wrap',
        GraphSliderYWrap: 'graph-slider-y-wrap',
        GraphSliderX: 'graph-slider-x',
        GraphSliderY: 'graph-slider-y',
        GraphSliderXHandle: 'graph-slider-x-handle',
        GraphSliderYHandle: 'graph-slider-y-handle',

        Picker: 'picker',
        PickerFieldWrap: 'field-wrap',
        PickerInputWrap: 'input-wrap',
        PickerInputField: 'input-field',
        PickerControlsWrap: 'controls-wrap',
        PickerColorContrast: 'color-contrast',
        PickerHandleField: 'indicator',
        PickerHandleSlider: 'indicator',

        Color: 'color',

        ScrollBar: 'scrollBar',
        ScrollWrap: 'scroll-wrap',
        ScrollBarBtnUp: 'btnUp',
        ScrollBarBtnDown: 'btnDown',
        ScrollBarTrack: 'track',
        ScrollBarThumb: 'thumb',
        ScrollBuffer: 'scroll-buffer'
    };

    module.exports = CSS;
});

var DocumentEvent = createCommonjsModule(function (module) {
    var DocumentEvent = {
        MOUSE_MOVE: 'mousemove',
        MOUSE_UP: 'mouseup',
        MOUSE_DOWN: 'mousedown',
        MOUSE_WHEEL: 'mousewheel',
        WINDOW_RESIZE: 'resize'
    };

    module.exports = DocumentEvent;
});

var NodeEvent = createCommonjsModule(function (module) {
    var NodeEvent = {
        MOUSE_DOWN: 'mousedown',
        MOUSE_UP: 'mouseup',
        MOUSE_OVER: 'mouseover',
        MOUSE_MOVE: 'mousemove',
        MOUSE_OUT: 'mouseout',
        KEY_DOWN: 'keydown',
        KEY_UP: 'keyup',
        CHANGE: 'change',
        FINISH: 'finish',
        DBL_CLICK: 'dblclick',
        ON_CLICK: 'click',
        SELECT_START: 'selectstart',
        DRAG_START: 'dragstart',
        DRAG: 'drag',
        DRAG_END: 'dragend',

        DRAG_ENTER: 'dragenter',
        DRAG_OVER: 'dragover',
        DRAG_LEAVE: 'dragleave',

        RESIZE: 'resize'
    };

    module.exports = NodeEvent;
});

var Event$1 = createCommonjsModule(function (module) {
    function Event_(sender, type, data) {
        this.sender = sender;
        this.type = type;
        this.data = data;
    }
    module.exports = Event_;
});

var Mouse = createCommonjsModule(function (module) {
    var EventDispatcher$$1 = EventDispatcher,
        Event_ = Event$1,
        DocumentEvent$$1 = DocumentEvent;
    var instance = null;

    function Mouse() {
        EventDispatcher$$1.apply(this);
        this._pos = [0, 0];
        this._wheelDirection = 0;
        this._hoverElement = null;

        var self = this;
        this._onDocumentMouseMove = function (e) {
            var dx = 0,
                dy = 0;

            if (!e) e = window.event;
            if (e.pageX) {
                dx = e.pageX;
                dy = e.pageY;
            } else if (e.clientX) {
                dx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                dy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
            }
            self._pos[0] = dx;
            self._pos[1] = dy;

            self._hoverElement = document.elementFromPoint(dx, dy);
        };

        this._onDocumentMouseWheel = function (event) {
            self._wheelDirection = event.detail < 0 ? 1 : event.wheelDelta > 0 ? 1 : -1;
            self.dispatchEvent(new Event_(self, DocumentEvent$$1.MOUSE_WHEEL, event));
        };

        document.addEventListener(DocumentEvent$$1.MOUSE_MOVE, this._onDocumentMouseMove);
        document.addEventListener(DocumentEvent$$1.MOUSE_WHEEL, this._onDocumentMouseWheel);
    }
    Mouse.prototype = Object.create(EventDispatcher$$1.prototype);
    Mouse.prototype.constructor = Mouse;

    Mouse.prototype._removeDocumentListener = function () {
        document.removeEventListener(DocumentEvent$$1.MOUSE_MOVE, this._onDocumentMouseMove);
        document.removeEventListener(DocumentEvent$$1.MOUSE_WHEEL, this._onDocumentMouseWheel);
    };

    Mouse.prototype.getPosition = function () {
        return this._pos;
    };

    Mouse.prototype.getX = function () {
        return this._pos[0];
    };

    Mouse.prototype.getY = function () {
        return this._pos[1];
    };

    Mouse.prototype.getWheelDirection = function () {
        return this._wheelDirection;
    };

    Mouse.prototype.getHoverElement = function () {
        return this._hoverElement;
    };

    Mouse.setup = function () {
        instance = instance || new Mouse();
        return instance;
    };

    Mouse.get = function () {
        return instance;
    };

    Mouse.destroy = function () {
        instance._removeDocumentListener();
        instance = null;
    };

    module.exports = Mouse;
});

var ScrollBar = createCommonjsModule(function (module) {
    var Node$$1 = Node;
    var Metric$$1 = Metric;
    var CSS$$1 = CSS;
    var DocumentEvent$$1 = DocumentEvent,
        NodeEvent$$1 = NodeEvent;
    var Mouse$$1 = Mouse;

    function ScrollBar(parentNode, targetNode, wrapHeight) {
        this._parentNode = parentNode;
        this._targetNode = targetNode;
        this._wrapHeight = wrapHeight;

        var wrap = this._wrapNode = new Node$$1().setStyleClass(CSS$$1.ScrollWrap),
            node = this._node = new Node$$1().setStyleClass(CSS$$1.ScrollBar),
            track = this._trackNode = new Node$$1().setStyleClass(CSS$$1.ScrollBarTrack),
            thumb = this._thumbNode = new Node$$1().setStyleClass(CSS$$1.ScrollBarThumb);

        parentNode.removeChild(targetNode);
        parentNode.addChild(wrap);
        parentNode.addChildAt(node, 0);

        wrap.addChild(targetNode);
        node.addChild(track);
        track.addChild(thumb);

        this._mouseThumbOffset = 0;
        this._scrollHeight = 0;
        this._scrollUnit = 0;
        this._scrollMin = 0;
        this._scrollMax = 0;

        thumb.setPositionY(Metric$$1.SCROLLBAR_TRACK_PADDING);
        thumb.addEventListener(DocumentEvent$$1.MOUSE_DOWN, this._onThumbDragStart.bind(this));

        this._isValid = false;
        this._enabled = false;

        var nodeElement = node.getElement(),
            thumbElement = thumb.getElement();
        var self = this;
        this._onMouseWheel = function (e) {
            var sender = e.sender,
                hoverElement = sender.getHoverElement();
            if (hoverElement != nodeElement && hoverElement != thumbElement) {
                return;
            }
            var scrollStep = self._scrollHeight * 0.0125;
            self._scroll(thumb.getPositionY() + sender.getWheelDirection() * scrollStep * -1);
            e.data.preventDefault();
        };

        this.addMouseListener();
    }

    ScrollBar.prototype.update = function () {
        var target = this._targetNode,
            thumb = this._thumbNode;

        var padding = Metric$$1.SCROLLBAR_TRACK_PADDING;

        var targetWrapHeight = this._wrapHeight,
            targetHeight = target.getHeight(),
            trackHeight = targetWrapHeight - padding * 2;

        thumb.setHeight(trackHeight);

        var ratio = targetWrapHeight / targetHeight;

        this._isValid = false;

        if (ratio > 1.0) {
            return;
        }
        var thumbHeight = trackHeight * ratio;

        this._scrollHeight = trackHeight;
        this._scrollUnit = targetHeight - this._scrollHeight - padding * 2;
        this._scrollMin = padding;
        this._scrollMax = padding + trackHeight - thumbHeight;

        thumb.setHeight(thumbHeight);

        this._isValid = true;
    };

    ScrollBar.prototype._scroll = function (y) {
        var min = this._scrollMin,
            max = this._scrollMax,
            pos = Math.max(min, Math.min(y, max)),
            pos_ = (pos - min) / (max - min);

        this._thumbNode.setPositionY(pos);
        this._targetNode.setPositionY(pos_ * this._scrollUnit * -1);
    };

    ScrollBar.prototype._onThumbDragStart = function () {
        if (!this._isValid || this._enabled) {
            return;
        }
        var eventMove = DocumentEvent$$1.MOUSE_MOVE,
            eventUp = DocumentEvent$$1.MOUSE_UP;

        var mouse = Mouse$$1.get();
        var trackOffset = this._trackNode.getPositionGlobalY();

        this._mouseThumbOffset = mouse.getY() - this._thumbNode.getPositionGlobalY();

        var self = this;
        var onDrag = function () {
            self._scroll(mouse.getY() - trackOffset - self._mouseThumbOffset);
        },
            onDragEnd = function () {
            document.removeEventListener(eventMove, onDrag, false);
            document.removeEventListener(eventUp, onDragEnd, false);
        };

        document.addEventListener(eventMove, onDrag, false);
        document.addEventListener(eventUp, onDragEnd, false);
        this._scroll(mouse.getY() - trackOffset - self._mouseThumbOffset);
    };

    ScrollBar.prototype.enable = function () {
        this._enabled = false;
        this._updateAppearance();
    };

    ScrollBar.prototype.disable = function () {
        this._enabled = true;
        this._updateAppearance();
    };
    ScrollBar.prototype.reset = function () {
        this._scroll(0);
    };

    ScrollBar.prototype._updateAppearance = function () {
        if (this._enabled) {
            this._node.setStyleProperty('display', 'none');
            this._targetNode.setPositionY(0);
            this._thumbNode.setPositionY(Metric$$1.SCROLLBAR_TRACK_PADDING);
        } else {
            this._node.setStyleProperty('display', 'block');
        }
    };

    ScrollBar.prototype.isValid = function () {
        return this._isValid;
    };

    ScrollBar.prototype.setWrapHeight = function (height) {
        this._wrapHeight = height;
        this.update();
    };

    ScrollBar.prototype.removeTargetNode = function () {
        return this._wrapNode.removeChild(this._targetNode);
    };

    ScrollBar.prototype.removeMouseListener = function () {
        Mouse$$1.get().removeEventListener(DocumentEvent$$1.MOUSE_WHEEL, this, '_onMouseWheel');
    };

    ScrollBar.prototype.addMouseListener = function () {
        Mouse$$1.get().addEventListener(DocumentEvent$$1.MOUSE_WHEEL, this, '_onMouseWheel');
    };

    ScrollBar.prototype.removeFromParent = function () {
        var parentNode = this._parentNode,
            rootNode = this._node,
            targetNode = this._targetNode;

        rootNode.removeChild(targetNode);
        parentNode.removeChild(this._wrapNode);
        parentNode.removeChild(rootNode);

        return targetNode;
    };

    ScrollBar.prototype.getWrapNode = function () {
        return this._wrapNode;
    };

    ScrollBar.prototype.getNode = function () {
        return this._node;
    };

    ScrollBar.prototype.getTargetNode = function () {
        return this._targetNode;
    };

    module.exports = ScrollBar;
});

var AbstractGroup = createCommonjsModule(function (module) {
    var EventDispatcher$$1 = EventDispatcher;
    var Node$$1 = Node;
    var ScrollBar$$1 = ScrollBar;

    function AbstractGroup(parent, params) {
        EventDispatcher$$1.apply(this, arguments);

        params = params || {};
        params.height = params.height || null;
        params.enable = params.enable === undefined ? true : params.enable;

        this._parent = parent;
        this._height = params.height;
        this._enabled = params.enable;
        this._scrollBar = null;

        this._node = new Node$$1(Node$$1.LIST_ITEM);
        this._wrapNode = new Node$$1();
        this._listNode = new Node$$1(Node$$1.LIST);

        this._parent.getList().addChild(this._node);
    }
    AbstractGroup.prototype = Object.create(EventDispatcher$$1.prototype);
    AbstractGroup.prototype.constructor = AbstractGroup;

    AbstractGroup.prototype.addScrollWrap = function () {
        var wrapNode = this._wrapNode,
            maxHeight = this.getMaxHeight();

        this._scrollBar = new ScrollBar$$1(wrapNode, this._listNode, maxHeight);
        if (this.isEnabled()) {
            wrapNode.setHeight(maxHeight);
        }
    };

    AbstractGroup.prototype.preventSelectDrag = function () {
        this._parent.preventSelectDrag();

        if (!this.hasScrollWrap()) {
            return;
        }
        this._wrapNode.getElement().scrollTop = 0;
    };

    AbstractGroup.prototype.hasMaxHeight = function () {
        return this._height != null;
    };

    AbstractGroup.prototype.getMaxHeight = function () {
        return this._height;
    };

    AbstractGroup.prototype.hasScrollWrap = function () {
        return this._scrollBar != null;
    };

    AbstractGroup.prototype.hasLabel = function () {
        return this._lablNode != null;
    };

    AbstractGroup.prototype.disable = function () {
        this._enabled = false;
        this._updateAppearance();
    };

    AbstractGroup.prototype.enable = function () {
        this._enabled = true;
        this._updateAppearance();
    };

    AbstractGroup.prototype.isDisabled = function () {
        return !this._enabled;
    };

    AbstractGroup.prototype.isEnabled = function () {
        return this._enabled;
    };

    AbstractGroup.prototype.getList = function () {
        return this._listNode;
    };

    module.exports = AbstractGroup;
});

var PanelEvent = createCommonjsModule(function (module) {
	var PanelEvent = {
		PANEL_MOVE_BEGIN: 'panelMoveBegin',
		PANEL_MOVE: 'panelMove',
		PANEL_MOVE_END: 'panelMoveEnd',

		PANEL_SHOW: 'panelShow',
		PANEL_HIDE: 'panelHide',

		PANEL_SCROLL_WRAP_ADDED: 'panelScrollWrapAdded',
		PANEL_SCROLL_WRAP_REMOVED: 'panelScrollWrapRemoved',

		PANEL_SIZE_CHANGE: 'panelSizeChange'
	};
	module.exports = PanelEvent;
});

var GroupEvent = createCommonjsModule(function (module) {
	var GroupEvent = {
		GROUP_SIZE_CHANGE: 'groupSizeChange',
		GROUP_LIST_SIZE_CHANGE: 'groupListSizeChange',
		GROUP_SIZE_UPDATE: 'groupSizeUpdate',
		SUBGROUP_TRIGGER: 'subGroupTrigger',

		SUBGROUP_ENABLE: 'enableSubGroup',
		SUBGROUP_DISABLE: 'disableSubGroup'
	};

	module.exports = GroupEvent;
});

var ComponentEvent = createCommonjsModule(function (module) {
	var ComponentEvent = {
		VALUE_UPDATED: 'valueUpdated',
		UPDATE_VALUE: 'updateValue',

		INPUT_SELECT_DRAG: 'inputSelectDrag',

		ENABLE: 'enable',
		DISABLE: 'disable'
	};

	module.exports = ComponentEvent;
});

var SubGroup = createCommonjsModule(function (module) {
    var AbstractGroup$$1 = AbstractGroup;
    var Node$$1 = Node;
    var CSS$$1 = CSS;

    var Event_ = Event$1,
        DocumentEvent$$1 = DocumentEvent,
        PanelEvent$$1 = PanelEvent,
        GroupEvent$$1 = GroupEvent,
        ComponentEvent$$1 = ComponentEvent;

    function SubGroup(parent, params) {
        params = params || {};
        params.label = params.label || null;
        params.useLabels = params.useLabels === undefined ? true : params.useLabels;

        AbstractGroup$$1.apply(this, arguments);

        var rootNode = this._node,
            wrapNode = this._wrapNode,
            listNode = this._listNode;

        rootNode.setStyleClass(CSS$$1.SubGroup);
        wrapNode.setStyleClass(CSS$$1.Wrap);

        wrapNode.addChild(listNode);
        rootNode.addChild(wrapNode);

        this._useLabels = params.useLabels;

        var label = params.label;

        if (label && label.length != 0 && label != 'none') {
            var headNode = this._headNode = new Node$$1(),
                lablWrap = new Node$$1(),
                lablNode = new Node$$1(Node$$1.SPAN);

            headNode.setStyleClass(CSS$$1.Head);
            lablWrap.setStyleClass(CSS$$1.Wrap);
            lablNode.setStyleClass(CSS$$1.Label);

            lablNode.setProperty('innerHTML', label);

            lablWrap.addChild(lablNode);
            headNode.addChild(lablWrap);

            var indiNode = this._indiNode = new Node$$1();
            indiNode.setStyleClass(CSS$$1.ArrowBSubMax);
            headNode.addChildAt(indiNode, 0);

            rootNode.addChildAt(headNode, 0);

            this.addEventListener(GroupEvent$$1.SUBGROUP_TRIGGER, this._parent, 'onSubGroupTrigger');
            headNode.addEventListener(DocumentEvent$$1.MOUSE_DOWN, this._onHeadMouseDown.bind(this));

            this._updateAppearance();
        }

        if (this.hasMaxHeight()) {
            this.addScrollWrap();
        }

        this._parent.addEventListener(GroupEvent$$1.SUBGROUP_ENABLE, this, 'onEnable');
        this._parent.addEventListener(GroupEvent$$1.SUBGROUP_DISABLE, this, 'onDisable');
        this._parent.addEventListener(PanelEvent$$1.PANEL_MOVE_END, this, 'onPanelMoveEnd');
        this._parent.addEventListener(GroupEvent$$1.GROUP_SIZE_CHANGE, this, 'onGroupSizeChange');
        this._parent.addEventListener(PanelEvent$$1.PANEL_SIZE_CHANGE, this, 'onPanelSizeChange');
        this._parent.addEventListener(DocumentEvent$$1.WINDOW_RESIZE, this, 'onWindowResize');

        this.addEventListener(GroupEvent$$1.GROUP_SIZE_UPDATE, this._parent, 'onGroupSizeUpdate');
    }
    SubGroup.prototype = Object.create(AbstractGroup$$1.prototype);
    SubGroup.prototype.constructor = SubGroup;

    //FIXME
    SubGroup.prototype._onHeadMouseDown = function () {
        this._enabled = !this._enabled;
        this._onTrigger();

        var event = DocumentEvent$$1.MOUSE_UP,
            self = this;
        var onDocumentMouseUp = function () {
            self._onTrigger();
            document.removeEventListener(event, onDocumentMouseUp);
        };

        document.addEventListener(event, onDocumentMouseUp);
    };

    SubGroup.prototype._onTrigger = function () {
        this._updateAppearance();
        this.dispatchEvent(new Event_(this, GroupEvent$$1.SUBGROUP_TRIGGER, null));
    };

    SubGroup.prototype._updateAppearance = function () {
        if (this.isDisabled()) {
            this._wrapNode.setHeight(0);
            if (this.hasLabel()) {
                this._headNode.setStyleClass(CSS$$1.HeadInactive);
                this._indiNode.setStyleClass(CSS$$1.ArrowBSubMin);
            }
        } else {
            if (this.hasMaxHeight()) {
                this._wrapNode.setHeight(this.getMaxHeight());
            } else {
                this._wrapNode.deleteStyleProperty('height');
            }
            if (this.hasLabel()) {
                this._headNode.setStyleClass(CSS$$1.Head);
                this._indiNode.setStyleClass(CSS$$1.ArrowBSubMax);
            }
        }
    };

    SubGroup.prototype.update = function () {
        if (this.hasMaxHeight()) {
            this._scrollBar.update();
        }
    };

    SubGroup.prototype.onComponentSelectDrag = function () {
        this.preventSelectDrag();
    };

    SubGroup.prototype.onEnable = function () {
        if (this.isDisabled()) {
            return;
        }
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.ENABLE, null));
    };
    SubGroup.prototype.onDisable = function () {
        if (this.isDisabled()) {
            return;
        }
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.DISABLE, null));
    };

    //bubble
    SubGroup.prototype.onGroupSizeChange = function () {
        this.dispatchEvent(new Event_(this, GroupEvent$$1.GROUP_SIZE_CHANGE, null));
    };
    SubGroup.prototype.onGroupSizeUpdate = function () {
        this.dispatchEvent(new Event_(this, GroupEvent$$1.GROUP_SIZE_UPDATE, null));
    };
    SubGroup.prototype.onPanelMoveEnd = function () {
        this.dispatchEvent(new Event_(this, PanelEvent$$1.PANEL_MOVE_END, null));
    };
    SubGroup.prototype.onPanelSizeChange = function () {
        this._updateAppearance();
    };
    SubGroup.prototype.onWindowResize = function (e) {
        this.dispatchEvent(e);
    };

    SubGroup.prototype.hasLabel = function () {
        return this._headNode != null;
    };
    SubGroup.prototype.addComponentNode = function (node) {
        this._listNode.addChild(node);
    };
    SubGroup.prototype.usesLabels = function () {
        return this._useLabels;
    };

    module.exports = SubGroup;
});

var HistoryEvent = createCommonjsModule(function (module) {
	var HistoryEvent = {
		STATE_PUSH: 'historyStatePush',
		STATE_POP: 'historyStatePop'
	};

	module.exports = HistoryEvent;
});

var History = createCommonjsModule(function (module) {
    var EventDispatcher$$1 = EventDispatcher,
        Event_ = Event$1,
        HistoryEvent$$1 = HistoryEvent;

    var MAX_STATES = 30;

    function History() {
        EventDispatcher$$1.apply(this, arguments);
        this._states = [];
        this._enabled = false;
    }
    History.prototype = Object.create(EventDispatcher$$1.prototype);
    History.prototype.constructor = History;

    History.prototype.pushState = function (object, key, value) {
        if (this._enabled) {
            return;
        }

        var states = this._states;
        if (states.length >= MAX_STATES) {
            states.shift();
        }
        states.push({ object: object, key: key, value: value });
        this.dispatchEvent(new Event_(this, HistoryEvent$$1.STATE_PUSH, null));
    };

    History.prototype.getState = function (object, key) {
        var states = this._states,
            statesLen = states.length;

        if (statesLen == 0) {
            return null;
        }

        var state, value;
        var i = -1;
        while (++i < statesLen) {
            state = states[i];
            if (state.object === object) {
                if (state.key === key) {
                    value = state.value;
                    break;
                }
            }
        }
        return value;
    };

    History.prototype.popState = function () {
        if (this._enabled) {
            return;
        }

        var states = this._states;
        if (states.length < 1) {
            return;
        }

        var lastState = states.pop();
        lastState.object[lastState.key] = lastState.value;

        this.dispatchEvent(new Event_(this, HistoryEvent$$1.STATE_POP, null));
    };

    History.prototype.getNumStates = function () {
        return this._states.length;
    };

    History._instance = null;

    History.setup = function () {
        return History._instance = new History();
    };

    History.get = function () {
        return History._instance;
    };

    History.prototype.enable = function () {
        this._enabled = false;
    };
    History.prototype.disable = function () {
        this._enabled = true;
    };

    module.exports = History;
});

var Component = createCommonjsModule(function (module) {
    var Node$$1 = Node,
        CSS$$1 = CSS;
    var EventDispatcher$$1 = EventDispatcher,
        ComponentEvent$$1 = ComponentEvent;

    function Component(parent, label) {
        EventDispatcher$$1.apply(this, arguments);

        label = parent.usesLabels() ? label : 'none';

        this._parent = parent;
        this._enabled = true;

        var root = this._node = new Node$$1(Node$$1.LIST_ITEM),
            wrap = this._wrapNode = new Node$$1();
        wrap.setStyleClass(CSS$$1.Wrap);
        root.addChild(wrap);

        if (label !== undefined) {
            if (label.length != 0 && label != 'none') {
                var label_ = this._lablNode = new Node$$1(Node$$1.SPAN);
                label_.setStyleClass(CSS$$1.Label);
                label_.setProperty('innerHTML', label);
                root.addChild(label_);
            }

            if (label == 'none') {
                wrap.setStyleProperty('marginLeft', '0');
                wrap.setStyleProperty('width', '100%');
            }
        }

        this._parent.addEventListener(ComponentEvent$$1.ENABLE, this, 'onEnable');
        this._parent.addEventListener(ComponentEvent$$1.DISABLE, this, 'onDisable');
        this._parent.addComponentNode(root);
    }
    Component.prototype = Object.create(EventDispatcher$$1.prototype);
    Component.prototype.constructor = Component;

    Component.prototype.enable = function () {
        this._enabled = true;
    };

    Component.prototype.disable = function () {
        this._enabled = false;
    };

    Component.prototype.isEnabled = function () {
        return this._enabled;
    };
    Component.prototype.isDisabled = function () {
        return !this._enabled;
    };

    Component.prototype.onEnable = function () {
        this.enable();
    };

    Component.prototype.onDisable = function () {
        this.disable();
    };

    module.exports = Component;
});

var OptionEvent = createCommonjsModule(function (module) {
	var OptionEvent = {
		TRIGGERED: 'selectTrigger',
		TRIGGER: 'triggerSelect'
	};
	module.exports = OptionEvent;
});

var ObjectComponentNotifier = createCommonjsModule(function (module) {
	var EventDispatcher$$1 = EventDispatcher,
	    Event_ = Event$1;
	var ComponentEvent$$1 = ComponentEvent,
	    OptionEvent$$1 = OptionEvent;

	function ObjectComponentNotifier() {
		EventDispatcher$$1.apply(this);
	}
	ObjectComponentNotifier.prototype = Object.create(EventDispatcher$$1.prototype);
	ObjectComponentNotifier.prototype.constructor = ObjectComponentNotifier;

	ObjectComponentNotifier.prototype.onValueUpdated = function (e) {
		this.dispatchEvent(new Event_(this, ComponentEvent$$1.UPDATE_VALUE, { origin: e.sender }));
	};

	ObjectComponentNotifier.prototype.onOptionTriggered = function (e) {
		this.dispatchEvent(new Event_(this, OptionEvent$$1.TRIGGER, { origin: e.sender }));
	};

	var instance = null;

	ObjectComponentNotifier.get = function () {
		if (!instance) {
			instance = new ObjectComponentNotifier();
		}
		return instance;
	};

	ObjectComponentNotifier.destroy = function () {
		instance = null;
	};

	module.exports = ObjectComponentNotifier;
});

var ComponentObjectError = createCommonjsModule(function (module) {
	function ComponentObjectError(object, key) {
		Error.apply(this);
		Error.captureStackTrace(this, ComponentObjectError);
		this.name = 'ComponentObjectError';
		this.message = 'Object of type ' + object.constructor.name + ' has no member ' + key + '.';
	}
	ComponentObjectError.prototype = Object.create(Error.prototype);
	ComponentObjectError.prototype.constructor = ComponentObjectError;

	module.exports = ComponentObjectError;
});

var ObjectComponent = createCommonjsModule(function (module) {
    var History$$1 = History;
    var Component$$1 = Component,
        ComponentEvent$$1 = ComponentEvent,
        ObjectComponentNotifier$$1 = ObjectComponentNotifier,
        ComponentObjectError$$1 = ComponentObjectError;
    var Event_ = Event$1;

    function ObjectComponent(parent, obj, key, params) {
        if (obj[key] === undefined) {
            throw new ComponentObjectError$$1(obj, key);
        }
        params = params || {};
        params.label = params.label || key;

        Component$$1.apply(this, [parent, params.label]);

        this._obj = obj;
        this._key = key;
        this._onChange = function () {};

        ObjectComponentNotifier$$1.get().addEventListener(ComponentEvent$$1.UPDATE_VALUE, this, 'onValueUpdate');
        this.addEventListener(ComponentEvent$$1.VALUE_UPDATED, ObjectComponentNotifier$$1.get(), 'onValueUpdated');
    }
    ObjectComponent.prototype = Object.create(Component$$1.prototype);
    ObjectComponent.prototype.constructor = ObjectComponent;

    //Override in Subclass
    ObjectComponent.prototype.applyValue = function () {};
    ObjectComponent.prototype.onValueUpdate = function (e) {};

    ObjectComponent.prototype.pushHistoryState = function () {
        var obj = this._obj,
            key = this._key;
        History$$1.get().pushState(obj, key, obj[key]);
    };

    ObjectComponent.prototype.setValue = function (value) {
        this._obj[this._key] = value;
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.VALUE_UPDATED, null));
    };

    ObjectComponent.prototype.getData = function () {
        var obj = {};
        obj[this._key] = this._obj[this._key];
        return obj;
    };

    module.exports = ObjectComponent;
});

var SVGComponent = createCommonjsModule(function (module) {
    var ObjectComponent$$1 = ObjectComponent;
    var CSS$$1 = CSS;
    var GroupEvent$$1 = GroupEvent;
    var Metric$$1 = Metric;

    function SVGComponent(parent, object, value, params) {
        ObjectComponent$$1.apply(this, arguments);

        var wrap = this._wrapNode;
        wrap.setStyleClass(CSS$$1.SVGWrap);
        var wrapSize = wrap.getWidth();

        var svg = this._svg = this._createSVGObject('svg');
        svg.setAttribute('version', '1.2');
        svg.setAttribute('baseProfile', 'tiny');

        wrap.getElement().appendChild(svg);

        var svgRoot = this._svgRoot = svg.appendChild(this._createSVGObject('g'));
        svgRoot.setAttribute('transform', 'translate(0.5 0.5)');

        this._svgSetSize(wrapSize, wrapSize);
        this._updateHeight();

        this._node.setStyleClass(CSS$$1.SVGListItem);

        this._parent.addEventListener(GroupEvent$$1.GROUP_SIZE_CHANGE, this, 'onGroupSizeChange');
        this.addEventListener(GroupEvent$$1.GROUP_SIZE_UPDATE, this._parent, 'onGroupSizeUpdate');
    }
    SVGComponent.prototype = Object.create(ObjectComponent$$1.prototype);
    SVGComponent.prototype.constructor = SVGComponent;

    SVGComponent.prototype._updateHeight = function () {
        var svgHeight = Number(this._svg.getAttribute('height'));

        this._wrapNode.setHeight(svgHeight);
        this._node.setHeight(svgHeight + Metric$$1.PADDING_WRAPPER);
    };

    SVGComponent.prototype._redraw = function () {};

    SVGComponent.prototype.onGroupSizeChange = function () {
        var width = this._wrapNode.getWidth();

        this._svgSetSize(width, width);
        this._updateHeight();
        this._redraw();
    };

    SVGComponent.prototype._createSVGObject = function (type) {
        return document.createElementNS("http://www.w3.org/2000/svg", type);
    };

    SVGComponent.prototype._svgSetSize = function (width, height) {
        var svg = this._svg;
        svg.setAttribute('width', width);
        svg.setAttribute('height', height);
        svg.setAttribute('viewbox', '0 0 ' + width + ' ' + height);
    };

    SVGComponent.prototype._pathCmdMoveTo = function (x, y) {
        return 'M ' + x + ' ' + y + ' ';
    };

    SVGComponent.prototype._pathCmdLineTo = function (x, y) {
        return 'L ' + x + ' ' + y + ' ';
    };

    SVGComponent.prototype._pathCmdClose = function () {
        return 'Z';
    };

    SVGComponent.prototype._pathCmdLine = function (x0, y0, x1, y1) {
        return 'M ' + x0 + ' ' + y0 + ' L ' + x1 + ' ' + y1;
    };

    SVGComponent.prototype._pathCmdBezierCubic = function (cmd, x0, y0, cx0, cy0, cx1, cy1, x1, y1) {
        return 'M ' + x0 + ' ' + y0 + ' C ' + cx0 + ' ' + cy0 + ', ' + cx1 + ' ' + cy1 + ', ' + x1 + ' ' + y1;
    };

    SVGComponent.prototype._pathCmdBezierQuadratic = function (cmd, x0, y0, cx, cy, x1, y1) {
        return 'M ' + x0 + ' ' + y0 + ' Q ' + cx + ' ' + cy + ', ' + x1 + ' ' + y1;
    };

    module.exports = SVGComponent;
});

var Plotter = createCommonjsModule(function (module) {
    var SVGComponent$$1 = SVGComponent;

    function Plotter(parent, object, value, params) {
        params = params || {};
        params.lineWidth = params.lineWidth || 2;
        params.lineColor = params.lineColor || [255, 255, 255];

        SVGComponent$$1.apply(this, arguments);

        var lineWidth = this._lineWidth = params.lineWidth;
        var lineColor = params.lineColor;

        var grid = this._grid = this._svgRoot.appendChild(this._createSVGObject('path'));
        grid.style.stroke = 'rgb(26,29,31)';

        var path = this._path = this._svgRoot.appendChild(this._createSVGObject('path'));
        path.style.stroke = 'rgb(' + lineColor[0] + ',' + lineColor[1] + ',' + lineColor[2] + ')';
        path.style.strokeWidth = lineWidth;
        path.style.fill = 'none';
    }
    Plotter.prototype = Object.create(SVGComponent$$1.prototype);
    Plotter.prototype.constructor = Plotter;

    module.exports = Plotter;
});

var ValuePlotter = createCommonjsModule(function (module) {
    var Plotter$$1 = Plotter;
    var Metric$$1 = Metric;

    var DEFAULT_RESOLUTION = 1;

    function ValuePlotter(parent, object, value, params) {
        Plotter$$1.apply(this, arguments);

        var svg = this._svg,
            svgWidth = Number(svg.getAttribute('width')),
            svgHeight = Number(svg.getAttribute('height'));

        params = params || {};
        params.height = params.height || svgHeight;
        params.resolution = params.resolution || DEFAULT_RESOLUTION;

        var resolution = params.resolution,
            length = Math.floor(svgWidth / resolution);

        var points = this._points = new Array(length * 2),
            buffer0 = this._buffer0 = new Array(length),
            buffer1 = this._buffer1 = new Array(length);

        var min = this._lineWidth * 0.5;

        var i = -1;
        while (++i < length) {
            buffer0[i] = buffer1[i] = points[i * 2] = points[i * 2 + 1] = min;
        }

        this._height = params.height = params.height < Metric$$1.COMPONENT_MIN_HEIGHT ? Metric$$1.COMPONENT_MIN_HEIGHT : params.height;

        this._svgSetSize(svgHeight, Math.floor(params.height));
        this._grid.style.stroke = 'rgb(39,44,46)';

        this._updateHeight();
        this._drawValue();
    }
    ValuePlotter.prototype = Object.create(Plotter$$1.prototype);
    ValuePlotter.prototype.constructor = ValuePlotter;

    ValuePlotter.prototype._redraw = function () {
        var points = this._points,
            bufferLen = this._buffer0.length;

        var width = Number(this._svg.getAttribute('width')),
            ratio = width / (bufferLen - 1);

        var i = -1;
        while (++i < bufferLen) {
            points[i * 2] = width - i * ratio;
        }

        this._drawValue();
    };

    ValuePlotter.prototype.onGroupSizeChange = function () {
        var width = this._wrapNode.getWidth(),
            height = this._height;

        this._svgSetSize(width, height);
        this._updateHeight();
        this._drawGrid();
        this._redraw();
    };

    ValuePlotter.prototype._drawValue = function () {
        this._drawCurve();
    };

    ValuePlotter.prototype._drawGrid = function () {
        var svg = this._svg;

        var svgWidth = Number(svg.getAttribute('width')),
            svgHeightHalf = Math.floor(Number(svg.getAttribute('height')) * 0.5);

        var pathCmd = '';
        pathCmd += this._pathCmdMoveTo(0, svgHeightHalf);
        pathCmd += this._pathCmdLineTo(svgWidth, svgHeightHalf);

        this._grid.setAttribute('d', pathCmd);
    };

    //TODO: merge update + pathcmd
    ValuePlotter.prototype._drawCurve = function () {
        var svg = this._svg;

        var value = this._obj[this._key];

        var buffer0 = this._buffer0,
            buffer1 = this._buffer1,
            points = this._points;

        var bufferLength = buffer0.length;

        var pathCmd = '';

        var heightHalf = Number(svg.getAttribute('height')) * 0.5,
            unit = heightHalf - this._lineWidth * 0.5;

        points[1] = buffer0[0];
        buffer0[bufferLength - 1] = value * unit * -1 + Math.floor(heightHalf);

        pathCmd += this._pathCmdMoveTo(points[0], points[1]);

        var i = 0,
            index;

        while (++i < bufferLength) {
            index = i * 2;

            buffer1[i - 1] = buffer0[i];
            points[index + 1] = buffer0[i - 1] = buffer1[i - 1];

            pathCmd += this._pathCmdLineTo(points[index], points[index + 1]);
        }

        this._path.setAttribute('d', pathCmd);
    };

    ValuePlotter.prototype.update = function () {
        if (this._parent.isDisabled()) return;
        this._drawValue();
    };

    module.exports = ValuePlotter;
});

var FunctionPlotType = createCommonjsModule(function (module) {
    var FunctionPlotType = {
        IMPLICIT: 'implicit',
        NON_IMPLICIT: 'nonImplicit'
    };

    module.exports = FunctionPlotType;
});

var FunctionPlotterObjectError = createCommonjsModule(function (module) {
	function FunctionPlotterObjectError(object, key) {
		Error.apply(this);
		Error.captureStackTrace(this, FunctionPlotterObjectError);
		this.name = 'ComponentObjectError';
		this.message = 'Object ' + object.constructor.name + ' ' + key + 'should be of type Function.';
	}
	FunctionPlotterObjectError.prototype = Object.create(Error.prototype);
	FunctionPlotterObjectError.prototype.constructor = FunctionPlotterObjectError;

	module.exports = FunctionPlotterObjectError;
});

var FunctionPlotterFunctionArgsError = createCommonjsModule(function (module) {
	function FunctionPlotterFunctionArgsError() {
		Error.apply(this);
		Error.captureStackTrace(this, FunctionPlotterFunctionArgsError);
		this.name = 'FunctionPlotterFunctionArgsError';
		this.message = 'Function should be of form f(x) or f(x,y).';
	}
	FunctionPlotterFunctionArgsError.prototype = Object.create(Error.prototype);
	FunctionPlotterFunctionArgsError.prototype.constructor = FunctionPlotterFunctionArgsError;

	module.exports = FunctionPlotterFunctionArgsError;
});

var FunctionPlotter = createCommonjsModule(function (module) {
    var Plotter$$1 = Plotter;

    var Node$$1 = Node;
    var CSS$$1 = CSS;
    var FunctionPlotType$$1 = FunctionPlotType;

    var Mouse$$1 = Mouse;
    var Metric$$1 = Metric;

    var DocumentEvent$$1 = DocumentEvent,
        ComponentEvent$$1 = ComponentEvent,
        NodeEvent$$1 = NodeEvent;

    var FunctionPlotterObjectError$$1 = FunctionPlotterObjectError,
        FunctionPlotterFunctionArgsError$$1 = FunctionPlotterFunctionArgsError;

    var ObjectComponentNotifier$$1 = ObjectComponentNotifier;

    var DEFAULT_SHOW_MIN_MAX_LABELS = true;

    var DEFAULT_FUNCTION_PLOTTER_NON_IMPLICIT_UNIT_X = 1,
        DEFAULT_FUNCTION_PLOTTER_NON_IMPLICIT_UNIT_Y = 1,
        DEFAULT_FUNCTION_PLOTTER_IMPLICIT_UNIT_X = 0.25,
        DEFAULT_FUNCTION_PLOTTER_IMPLICIT_UNIT_Y = 0.25,
        DEFAULT_FUNCTION_PLOTTER_UNIT_MIN = 0.15,
        DEFAULT_FUNCTION_PLOTTER_UNIT_MAX = 4,
        DEFAULT_FUNCTION_PLOTTER_NON_IMPLICIT_SCALE = 10.0,
        DEFAULT_FUNCTION_PLOTTER_IMPLICIT_SCALE = 1.0,
        DEFAULT_FUNCTION_PLOTTER_SCALE_MIN = 0.02,
        DEFAULT_FUNCTION_PLOTTER_SCALE_MAX = 25,
        DEFAULT_FUNCTION_PLOTTER_IMPLICIT_AXES_COLOR = 'rgba(255,255,255,0.75)',
        DEFAULT_FUNCTION_PLOTTER_IMPLICIT_GRID_COLOR = 'rgba(25,25,25,0.75)',
        DEFAULT_FUNCTION_PLOTTER_NON_IMPLICIT_AXES_COLOR = 'rgb(54,60,64)',
        DEFAULT_FUNCTION_PLOTTER_NON_IMPLICIT_GRID_COLOR = 'rgb(25,25,25)',
        DEFAULT_FUNCTION_PLOTTER_CIRCLE_LABEL_RADIUS = 3,
        DEFAULT_FUNCTION_PLOTTER_CIRCLE_LABEL_FILL = 'rgb(255,255,255)',
        DEFAULT_FUNCTION_PLOTTER_CIRCLE_STROKE = '#b12334';

    function FunctionPlotter(parent, object, value, params) {
        params = params || {};
        params.showMinMaxLabels = params.showMinMaxLabels === undefined ? DEFAULT_SHOW_MIN_MAX_LABELS : params.showMinMaxLabels;

        Plotter$$1.apply(this, arguments);

        if (typeof object[value] !== 'function') {
            throw new FunctionPlotterObjectError$$1(object, value);
        }

        var funcArgLength = object[value].length;

        if (funcArgLength > 2 || funcArgLength == 0) {
            throw new FunctionPlotterFunctionArgsError$$1();
        }

        var svgRoot = this._svgRoot,
            path = this._path;

        var axes = this._axes = svgRoot.insertBefore(this._createSVGObject('path'), path);
        axes.style.strokeWidth = 1;

        var axesLabels = this._axesLabels = svgRoot.insertBefore(this._createSVGObject('path'), path);
        axesLabels.style.stroke = 'rgb(43,48,51)';
        axesLabels.style.strokeWidth = 1;

        var grid = this._grid;

        var svg = this._svg,
            size = Number(svg.getAttribute('width'));

        var sliderXWrap = new Node$$1();
        sliderXWrap.setStyleClass(CSS$$1.GraphSliderXWrap);

        var sliderYWrap = new Node$$1();
        sliderYWrap.setStyleClass(CSS$$1.GraphSliderYWrap);

        var sliderXTrack = this._sliderXTrack = new Node$$1();
        sliderXTrack.setStyleClass(CSS$$1.GraphSliderX);

        var sliderYTrack = this._sliderYTrack = new Node$$1();
        sliderYTrack.setStyleClass(CSS$$1.GraphSliderY);

        var sliderXHandle = this._sliderXHandle = new Node$$1();
        sliderXHandle.setStyleClass(CSS$$1.GraphSliderXHandle);

        var sliderYHandle = this._sliderYHandle = new Node$$1();
        sliderYHandle.setStyleClass(CSS$$1.GraphSliderYHandle);

        sliderXTrack.addChild(sliderXHandle);
        sliderYTrack.addChild(sliderYHandle);
        sliderXWrap.addChild(sliderXTrack);
        sliderYWrap.addChild(sliderYTrack);

        var wrapNode = this._wrapNode;

        var plotMode = this._plotMode = funcArgLength == 1 ? FunctionPlotType$$1.NON_IMPLICIT : FunctionPlotType$$1.IMPLICIT;

        if (plotMode == FunctionPlotType$$1.IMPLICIT) {
            var canvas = this._canvas = document.createElement('canvas');
            canvas.style.width = canvas.style.height = size + 'px';
            canvas.width = canvas.height = size;

            wrapNode.getElement().insertBefore(canvas, svg);

            this._canvasContext = canvas.getContext('2d');
            this._canvasImageData = this._canvasContext.getImageData(0, 0, size, size);

            axes.style.stroke = DEFAULT_FUNCTION_PLOTTER_IMPLICIT_AXES_COLOR;
            grid.style.stroke = DEFAULT_FUNCTION_PLOTTER_IMPLICIT_GRID_COLOR;
        } else {
            axes.style.stroke = DEFAULT_FUNCTION_PLOTTER_NON_IMPLICIT_AXES_COLOR;
            grid.style.stroke = DEFAULT_FUNCTION_PLOTTER_NON_IMPLICIT_GRID_COLOR;
        }

        wrapNode.addChild(sliderXWrap);
        wrapNode.addChild(sliderYWrap);

        sliderXHandle.addEventListener(NodeEvent$$1.MOUSE_DOWN, this._onSliderXHandleDown.bind(this));
        sliderYHandle.addEventListener(NodeEvent$$1.MOUSE_DOWN, this._onSliderYHandleDown.bind(this));

        var units = this._units = [null, null];
        this._scale = null;

        if (plotMode == FunctionPlotType$$1.NON_IMPLICIT) {
            units[0] = DEFAULT_FUNCTION_PLOTTER_NON_IMPLICIT_UNIT_X;
            units[1] = DEFAULT_FUNCTION_PLOTTER_NON_IMPLICIT_UNIT_Y;

            this._scale = DEFAULT_FUNCTION_PLOTTER_NON_IMPLICIT_SCALE;
        } else if (plotMode == FunctionPlotType$$1.IMPLICIT) {
            units[0] = DEFAULT_FUNCTION_PLOTTER_IMPLICIT_UNIT_X;
            units[1] = DEFAULT_FUNCTION_PLOTTER_IMPLICIT_UNIT_Y;

            this._scale = DEFAULT_FUNCTION_PLOTTER_IMPLICIT_SCALE;
        }

        this._unitsMinMax = [DEFAULT_FUNCTION_PLOTTER_UNIT_MIN, DEFAULT_FUNCTION_PLOTTER_UNIT_MAX]; //1/8->4

        this._scaleMinMax = [DEFAULT_FUNCTION_PLOTTER_SCALE_MIN, DEFAULT_FUNCTION_PLOTTER_SCALE_MAX]; //1/50 -> 25

        this._center = [Math.round(size * 0.5), Math.round(size * 0.5)];
        this._svgPos = [0, 0];

        this._func = null;
        this.setFunction(this._obj[this._key]);

        this._sliderXHandleUpdate();
        this._sliderYHandleUpdate();

        svg.addEventListener(DocumentEvent$$1.MOUSE_DOWN, this._onDragStart.bind(this), false);
        this._wrapNode.getElement().addEventListener("mousewheel", this._onScale.bind(this, false));

        ObjectComponentNotifier$$1.get().addEventListener(ComponentEvent$$1.UPDATE_VALUE, this, 'onValueUpdate');
    }
    FunctionPlotter.prototype = Object.create(Plotter$$1.prototype);
    FunctionPlotter.prototype.constructor = FunctionPlotter;

    FunctionPlotter.prototype._updateCenter = function () {
        var svg = this._svg,
            width = Number(svg.getAttribute('width')),
            height = Number(svg.getAttribute('height'));

        var mousePos = Mouse$$1.get().getPosition(),
            svgPos = this._svgPos,
            center = this._center;

        center[0] = Math.max(0, Math.min(mousePos[0] - svgPos[0], width));
        center[1] = Math.max(0, Math.min(mousePos[1] - svgPos[1], height));

        this._plotGraph();
    };

    FunctionPlotter.prototype._onDragStart = function (e) {
        var svgPos = this._svgPos;
        svgPos[0] = 0;
        svgPos[1] = 0;

        //skip to container
        var element = this._svg.parentNode;

        while (element) {
            svgPos[0] += element.offsetLeft;
            svgPos[1] += element.offsetTop;
            element = element.offsetParent;
        }

        var eventMove = DocumentEvent$$1.MOUSE_MOVE,
            eventUp = DocumentEvent$$1.MOUSE_UP;

        var onDrag = this._updateCenter.bind(this),
            onDragEnd = function () {
            this._updateCenter.bind(this);
            document.removeEventListener(eventMove, onDrag, false);
            document.removeEventListener(eventUp, onDragEnd, false);
        }.bind(this);

        document.addEventListener(eventMove, onDrag, false);
        document.addEventListener(eventUp, onDragEnd, false);

        this._updateCenter();
    };

    FunctionPlotter.prototype._onScale = function (e) {
        e = window.event || e;
        this._scale += Math.max(-1, Math.min(1, e.wheelDelta || -e.detail)) * -1;

        var scaleMinMax = this._scaleMinMax;
        this._scale = Math.max(scaleMinMax[0], Math.min(this._scale, scaleMinMax[1]));

        this._plotGraph();

        e.preventDefault();
    };

    FunctionPlotter.prototype.onValueUpdate = function () {
        this.setFunction(this._obj[this._key]);
    };

    FunctionPlotter.prototype._redraw = function () {
        if (this._plotMode == FunctionPlotType$$1.IMPLICIT) {
            var size = this._wrapNode.getWidth(),
                canvas = this._canvas;

            canvas.style.width = canvas.style.height = size + 'px';
            canvas.width = canvas.height = size;

            this._canvasImageData = this._canvasContext.getImageData(0, 0, size, size);
        }

        this._sliderXHandleUpdate();
        this._sliderYHandleUpdate();

        this.setFunction(this._obj[this._key]);
    };

    FunctionPlotter.prototype.setFunction = function (func) {
        this._func = func.bind(this._obj);
        this._plotGraph();
    };

    FunctionPlotter.prototype._plotGraph = function () {
        this._drawGrid();
        this._drawAxes();
        this._drawPlot();
    };

    FunctionPlotter.prototype._drawAxes = function () {
        var svg = this._svg,
            svgWidth = Number(svg.getAttribute('width')),
            svgHeight = Number(svg.getAttribute('height'));

        var center = this._center,
            centerX = center[0],
            centerY = center[1];

        var pathCmd = '';
        pathCmd += this._pathCmdLine(0, centerY, svgWidth, centerY);
        pathCmd += this._pathCmdLine(centerX, 0, centerX, svgHeight);

        this._axes.setAttribute('d', pathCmd);
    };

    FunctionPlotter.prototype._drawPlot = function () {
        var width, height;

        var center = this._center,
            centerX = center[0],
            centerY = center[1];

        var units = this._units,
            unitX,
            unitY;

        var scale = this._scale;
        var normval, scaledVal, value, index;
        var offsetX, offsetY;

        var i;

        if (this._plotMode == FunctionPlotType$$1.NON_IMPLICIT) {
            var svg = this._svg;

            width = Number(svg.getAttribute('width'));
            height = Number(svg.getAttribute('height'));
            unitX = units[0] * scale;
            unitY = height / (units[1] * scale);
            offsetX = centerX / width;

            var len = Math.floor(width),
                points = new Array(len * 2);

            i = -1;
            while (++i < len) {
                normval = -offsetX + i / len;
                scaledVal = normval * unitX;
                value = centerY - this._func(scaledVal) * unitY;

                index = i * 2;

                points[index] = i;
                points[index + 1] = value;
            }

            var pathCmd = '';
            pathCmd += this._pathCmdMoveTo(points[0], points[1]);

            i = 2;
            while (i < points.length) {
                pathCmd += this._pathCmdLineTo(points[i], points[i + 1]);
                i += 2;
            }

            this._path.setAttribute('d', pathCmd);
        } else {
            var canvas = this._canvas,
                context = this._canvasContext,
                imgData = this._canvasImageData;

            width = canvas.width;
            height = canvas.height;

            unitX = units[0] * scale;
            unitY = units[1] * scale;

            offsetX = centerX / width;
            offsetY = centerY / height;

            var invWidth = 1 / width,
                invHeight = 1 / height;
            var rgb = [0, 0, 0];

            var col0 = [30, 34, 36],
                col1 = [255, 255, 255];

            i = -1;
            var j;
            while (++i < height) {
                j = -1;

                while (++j < width) {
                    value = this._func((-offsetX + j * invWidth) * unitX, (-offsetY + i * invHeight) * unitY);

                    rgb[0] = Math.floor((col1[0] - col0[0]) * value + col0[0]);
                    rgb[1] = Math.floor((col1[1] - col0[1]) * value + col0[1]);
                    rgb[2] = Math.floor((col1[2] - col0[2]) * value + col0[2]);

                    index = (i * width + j) * 4;

                    imgData.data[index] = rgb[0];
                    imgData.data[index + 1] = rgb[1];
                    imgData.data[index + 2] = rgb[2];
                    imgData.data[index + 3] = 255;
                }
            }

            context.clearRect(0, 0, width, height);
            context.putImageData(imgData, 0, 0);
        }
    };

    FunctionPlotter.prototype._drawGrid = function () {
        var svg = this._svg,
            width = Number(svg.getAttribute('width')),
            height = Number(svg.getAttribute('height'));

        var scale = this._scale;

        var gridRes = this._units,
            gridSpacingX = width / (gridRes[0] * scale),
            gridSpacingY = height / (gridRes[1] * scale);

        var center = this._center,
            centerX = center[0],
            centerY = center[1];

        var gridNumTop = Math.round(centerY / gridSpacingY) + 1,
            gridNumBottom = Math.round((height - centerY) / gridSpacingY) + 1,
            gridNumLeft = Math.round(centerX / gridSpacingX) + 1,
            gridNumRight = Math.round((width - centerX) / gridSpacingX) + 1;

        var pathCmdGrid = '',
            pathCmdAxesLabels = '';

        var i, temp;

        var strokeSize = Metric$$1.STROKE_SIZE;

        var labelTickSize = Metric$$1.FUNCTION_PLOTTER_LABEL_TICK_SIZE,
            labelTickPaddingRight = width - labelTickSize - strokeSize,
            labelTickPaddingBottom = height - labelTickSize - strokeSize,
            labelTickPaddingRightOffset = labelTickPaddingRight - labelTickSize,
            labelTickPaddingBottomOffset = labelTickPaddingBottom - labelTickSize,
            labelTickOffsetRight = labelTickPaddingRight - (labelTickSize + strokeSize) * 2,
            labelTickOffsetBottom = labelTickPaddingBottom - (labelTickSize + strokeSize) * 2;

        i = -1;
        while (++i < gridNumTop) {
            temp = Math.round(centerY - gridSpacingY * i);
            pathCmdGrid += this._pathCmdLine(0, temp, width, temp);

            if (temp > labelTickSize) {
                pathCmdAxesLabels += this._pathCmdLine(labelTickPaddingRight, temp, labelTickPaddingRightOffset, temp);
            }
        }

        i = -1;
        while (++i < gridNumBottom) {
            temp = Math.round(centerY + gridSpacingY * i);
            pathCmdGrid += this._pathCmdLine(0, temp, width, temp);

            if (temp < labelTickOffsetBottom) {
                pathCmdAxesLabels += this._pathCmdLine(labelTickPaddingRight, temp, labelTickPaddingRightOffset, temp);
            }
        }

        i = -1;
        while (++i < gridNumLeft) {
            temp = Math.round(centerX - gridSpacingX * i);
            pathCmdGrid += this._pathCmdLine(temp, 0, temp, height);

            if (temp > labelTickSize) {
                pathCmdAxesLabels += this._pathCmdLine(temp, labelTickPaddingBottom, temp, labelTickPaddingBottomOffset);
            }
        }

        i = -1;
        while (++i < gridNumRight) {
            temp = Math.round(centerX + gridSpacingX * i);
            pathCmdGrid += this._pathCmdLine(temp, 0, temp, height);

            if (temp < labelTickOffsetRight) {
                pathCmdAxesLabels += this._pathCmdLine(temp, labelTickPaddingBottom, temp, labelTickPaddingBottomOffset);
            }
        }

        this._grid.setAttribute('d', pathCmdGrid);
        this._axesLabels.setAttribute('d', pathCmdAxesLabels);
    };

    FunctionPlotter.prototype._sliderXStep = function (mousePos) {
        var mouseX = mousePos[0];

        var handle = this._sliderXHandle,
            handleWidth = handle.getWidth(),
            handleWidthHalf = handleWidth * 0.5;

        var track = this._sliderXTrack,
            trackWidth = track.getWidth(),
            trackLeft = track.getPositionGlobalX();

        var strokeSize = Metric$$1.STROKE_SIZE;

        var max = trackWidth - handleWidthHalf - strokeSize * 2;

        var pos = Math.max(handleWidthHalf, Math.min(mouseX - trackLeft, max)),
            handlePos = pos - handleWidthHalf;

        handle.setPositionX(handlePos);

        var unitsMin = this._unitsMinMax[0],
            unitsMax = this._unitsMinMax[1];

        var normVal = (pos - handleWidthHalf) / (max - handleWidthHalf),
            mappedVal = unitsMin + (unitsMax - unitsMin) * normVal;

        this._units[0] = mappedVal;

        this._plotGraph();
    };

    FunctionPlotter.prototype._sliderYStep = function (mousePos) {
        var mouseY = mousePos[1];

        var handle = this._sliderYHandle,
            handleHeight = handle.getHeight(),
            handleHeightHalf = handleHeight * 0.5;

        var track = this._sliderYTrack,
            trackHeight = track.getHeight(),
            trackTop = track.getPositionGlobalY();

        var max = trackHeight - handleHeightHalf - 2;

        var pos = Math.max(handleHeightHalf, Math.min(mouseY - trackTop, max)),
            handlePos = pos - handleHeightHalf;

        handle.setPositionY(handlePos);

        var unitsMax = this._unitsMinMax[0],
            unitsMin = this._unitsMinMax[1];

        var normVal = (pos - handleHeightHalf) / (max - handleHeightHalf),
            mappedVal = unitsMin + (unitsMax - unitsMin) * normVal;

        this._units[1] = mappedVal;

        this._plotGraph();
    };

    FunctionPlotter.prototype._onSliderXHandleDown = function () {
        this._onSliderHandleDown(this._sliderXStep.bind(this));
    };

    FunctionPlotter.prototype._onSliderYHandleDown = function () {
        this._onSliderHandleDown(this._sliderYStep.bind(this));
    };

    FunctionPlotter.prototype._onSliderHandleDown = function (sliderStepFunc) {
        var eventMouseMove = DocumentEvent$$1.MOUSE_MOVE,
            eventMouseUp = DocumentEvent$$1.MOUSE_UP;

        var mouse = Mouse$$1.get();

        var onDrag = function () {
            sliderStepFunc(mouse.getPosition());
        },
            onDragEnd = function () {
            document.removeEventListener(eventMouseMove, onDrag, false);
            document.removeEventListener(eventMouseUp, onDragEnd, false);
        };

        sliderStepFunc(mouse.getPosition());
        document.addEventListener(eventMouseMove, onDrag, false);
        document.addEventListener(eventMouseUp, onDragEnd, false);
    };

    FunctionPlotter.prototype._sliderXHandleUpdate = function () {
        var unitMin = this._unitsMinMax[0],
            unitMax = this._unitsMinMax[1],
            unitX = this._units[0];

        var handleX = this._sliderXHandle,
            handleXWidth = handleX.getWidth(),
            handleXWidthHalf = handleXWidth * 0.5,
            trackXWidth = this._sliderXTrack.getWidth();

        var strokeSize = Metric$$1.STROKE_SIZE;

        var handleXMin = handleXWidthHalf,
            handleXMax = trackXWidth - handleXWidthHalf - strokeSize * 2;

        handleX.setPositionX(handleXMin + (handleXMax - handleXMin) * ((unitX - unitMin) / (unitMax - unitMin)) - handleXWidthHalf);
    };

    FunctionPlotter.prototype._sliderYHandleUpdate = function () {
        var unitMin = this._unitsMinMax[0],
            unitMax = this._unitsMinMax[1],
            unitY = this._units[1];

        var handleY = this._sliderYHandle,
            handleYHeight = handleY.getHeight(),
            handleYHeightHalf = handleYHeight * 0.5,
            trackYHeight = this._sliderYTrack.getHeight();

        var strokeSize = Metric$$1.STROKE_SIZE;

        var handleYMin = trackYHeight - handleYHeightHalf - strokeSize * 2,
            handleYMax = handleYHeightHalf;

        handleY.setPositionY(handleYMin + (handleYMax - handleYMin) * ((unitY - unitMin) / (unitMax - unitMin)) - handleYHeightHalf);
    };

    module.exports = FunctionPlotter;
});

var Group = createCommonjsModule(function (module) {
    var AbstractGroup$$1 = AbstractGroup;
    var CSS$$1 = CSS;
    var Node$$1 = Node;

    var SubGroup$$1 = SubGroup;

    var Event_ = Event$1,
        DocumentEvent$$1 = DocumentEvent,
        NodeEvent$$1 = NodeEvent,
        PanelEvent$$1 = PanelEvent,
        GroupEvent$$1 = GroupEvent;

    var ObjectComponent$$1 = ObjectComponent,
        ValuePlotter$$1 = ValuePlotter,
        FunctionPlotter$$1 = FunctionPlotter;

    function Group(parent, params) {
        params = params || {};
        params.label = params.label || null;
        params.useLabels = params.useLabels || true;
        params.enable = params.enable === undefined ? true : params.enable;

        AbstractGroup$$1.apply(this, arguments);

        this._components = [];
        this._subGroups = [];

        var root = this._node,
            wrap = this._wrapNode,
            list = this._listNode;

        root.setStyleClass(CSS$$1.Group);
        wrap.setStyleClass(CSS$$1.Wrap);
        list.setStyleClass(CSS$$1.SubGroupList);

        wrap.addChild(list);

        var label = params.label;

        if (label) {
            var head = new Node$$1(),
                wrap_ = new Node$$1(),
                label_ = new Node$$1(Node$$1.SPAN),
                indicator = this._indiNode = new Node$$1();

            head.setStyleClass(CSS$$1.Head);
            wrap_.setStyleClass(CSS$$1.Wrap);
            label_.setStyleClass(CSS$$1.Label);
            indicator.setStyleClass(CSS$$1.ArrowBMax);
            label_.setProperty('innerHTML', label);

            head.addChild(indicator);
            wrap_.addChild(label_);
            head.addChild(wrap_);
            root.addChild(head);

            head.addEventListener(NodeEvent$$1.MOUSE_DOWN, this._onHeadTrigger.bind(this));
            this.addEventListener(GroupEvent$$1.GROUP_LIST_SIZE_CHANGE, parent, 'onGroupListSizeChange');

            this._updateAppearance();
        }

        if (this.hasMaxHeight()) {
            this.addScrollWrap();
        }

        root.addChild(wrap);

        if (this.hasMaxHeight()) {
            if (!label) {
                var bufferTop = this._scrollBufferTop = new Node$$1();
                bufferTop.setStyleClass(CSS$$1.ScrollBuffer);

                root.addChildAt(bufferTop, 0);
            }
            var bufferBottom = this._scrollBufferBottom = new Node$$1();
            bufferBottom.setStyleClass(CSS$$1.ScrollBuffer);

            root.addChild(bufferBottom);
        }

        parent = this._parent;

        parent.addEventListener(PanelEvent$$1.PANEL_MOVE_BEGIN, this, 'onPanelMoveBegin');
        parent.addEventListener(PanelEvent$$1.PANEL_MOVE, this, 'onPanelMove');
        parent.addEventListener(PanelEvent$$1.PANEL_MOVE_END, this, 'onPanelMoveEnd');
        parent.addEventListener(PanelEvent$$1.PANEL_HIDE, this, 'onPanelHide');
        parent.addEventListener(PanelEvent$$1.PANEL_SHOW, this, 'onPanelShow');
        parent.addEventListener(PanelEvent$$1.PANEL_SCROLL_WRAP_ADDED, this, 'onPanelScrollWrapAdded');
        parent.addEventListener(PanelEvent$$1.PANEL_SCROLL_WRAP_REMOVED, this, 'onPanelScrollWrapRemoved');
        parent.addEventListener(PanelEvent$$1.PANEL_SIZE_CHANGE, this, 'onPanelSizeChange');
        parent.addEventListener(DocumentEvent$$1.WINDOW_RESIZE, this, 'onWindowResize');

        this.addEventListener(GroupEvent$$1.GROUP_SIZE_CHANGE, parent, 'onGroupListSizeChange');
    }
    Group.prototype = Object.create(AbstractGroup$$1.prototype);
    Group.prototype.constructor = Group;

    Group.prototype.onPanelMoveBegin = function () {
        this.dispatchEvent(new Event_(this, PanelEvent$$1.PANEL_MOVE_BEGIN, null));
    };

    Group.prototype.onPanelMove = function () {
        this.dispatchEvent(new Event_(this, PanelEvent$$1.PANEL_MOVE, null));
    };

    Group.prototype.onPanelMoveEnd = function () {
        this.dispatchEvent(new Event_(this, PanelEvent$$1.PANEL_MOVE_END, null));
    };

    Group.prototype.onPanelScrollWrapAdded = function () {
        this.dispatchEvent(new Event_(this, GroupEvent$$1.GROUP_SIZE_CHANGE, null));
    };

    Group.prototype.onPanelScrollWrapRemoved = function () {
        this.dispatchEvent(new Event_(this, GroupEvent$$1.GROUP_SIZE_CHANGE, null));
    };

    Group.prototype.onPanelHide = function () {
        this.dispatchEvent(new Event_(this, GroupEvent$$1.SUBGROUP_DISABLE, null));
    };

    Group.prototype.onPanelShow = function () {
        this.dispatchEvent(new Event_(this, GroupEvent$$1.SUBGROUP_ENABLE, null));
    };

    Group.prototype.onPanelSizeChange = function () {
        this.dispatchEvent(new Event_(this, GroupEvent$$1.GROUP_SIZE_CHANGE, null));
    };

    Group.prototype.onWindowResize = function (e) {
        this.dispatchEvent(e);
    };

    Group.prototype.onSubGroupTrigger = function () {
        this._updateHeight();

        if (!this.hasMaxHeight()) {
            return;
        }
        var scrollBar = this._scrollBar,
            wrap = this._wrapNode;
        var bufferTop = this._scrollBufferTop,
            bufferBottom = this._scrollBufferBottom;

        scrollBar.update();

        if (!scrollBar.isValid()) {
            scrollBar.disable();
            wrap.setHeight(wrap.getChildAt(1).getHeight());
            if (bufferTop) {
                bufferTop.setStyleProperty('display', 'none');
            }
            if (bufferBottom) {
                bufferBottom.setStyleProperty('display', 'none');
            }
        } else {
            scrollBar.enable();
            wrap.setHeight(this.getMaxHeight());

            if (bufferTop) {
                bufferTop.setStyleProperty('display', 'block');
            }
            if (bufferBottom) {
                bufferBottom.setStyleProperty('display', 'block');
            }
        }
        this.dispatchEvent(new Event_(this, GroupEvent$$1.GROUP_SIZE_CHANGE, null));
    };

    Group.prototype._onHeadTrigger = function () {
        this._enabled = !this._enabled;
        this._updateAppearance();
        this.dispatchEvent(new Event_(this, GroupEvent$$1.GROUP_LIST_SIZE_CHANGE, null));
    };

    Group.prototype.addComponent = function () {
        var Class_ = arguments[0];
        var args = Array.prototype.slice.call(arguments);
        args.shift();
        args.unshift(this._getSubGroup());

        var instance = Object.create(Class_.prototype);
        Class_.apply(instance, args);

        this._components.push(instance);
        this._updateHeight();
    };

    Group.prototype._updateHeight = function () {
        this._getSubGroup().update();
        this.dispatchEvent(new Event_(this, GroupEvent$$1.GROUP_SIZE_CHANGE, null));
        if (this.hasMaxHeight()) {
            this._scrollBar.update();
        }
    };

    Group.prototype._updateAppearance = function () {
        var wrap = this._wrapNode,
            indicator = this._indiNode;

        var scrollBar = this._scrollBar;

        var bufferTop = this._scrollBufferTop,
            bufferBottom = this._scrollBufferBottom;

        if (this.isDisabled()) {
            wrap.setHeight(0);
            if (indicator) {
                indicator.setStyleClass(CSS$$1.ArrowBMin);
            }

            if (scrollBar) {
                if (bufferTop) {
                    bufferTop.setStyleProperty('display', 'none');
                }
                if (bufferBottom) {
                    bufferBottom.setStyleProperty('display', 'none');
                }
            }
            return;
        }

        if (this.hasMaxHeight()) {
            var maxHeight = this.getMaxHeight(),
                listHeight = wrap.getChildAt(1).getHeight();

            wrap.setHeight(listHeight < maxHeight ? listHeight : maxHeight);

            if (scrollBar.isValid()) {
                if (bufferTop) {
                    bufferTop.setStyleProperty('display', 'block');
                }
                if (bufferBottom) {
                    bufferBottom.setStyleProperty('display', 'block');
                }
            }
        } else {
            wrap.deleteStyleProperty('height');
        }
        if (indicator) {
            indicator.setStyleClass(CSS$$1.ArrowBMax);
        }
    };

    Group.prototype.onGroupSizeUpdate = function () {
        this._updateAppearance();
        if (this.hasMaxHeight()) {
            this._scrollBar.update();
        }
    };

    Group.prototype.addSubGroup = function (params) {
        this._subGroups.push(new SubGroup$$1(this, params));
        this._updateHeight();
        return this;
    };

    Group.prototype._getSubGroup = function () {
        var subGroups = this._subGroups;
        if (subGroups.length == 0) {
            subGroups.push(new SubGroup$$1(this));
        }
        return subGroups[subGroups.length - 1];
    };

    Group.prototype.getComponents = function () {
        return this._components;
    };

    function isDataComp(comp) {
        return comp instanceof ObjectComponent$$1 && !(comp instanceof ValuePlotter$$1) && !(comp instanceof FunctionPlotter$$1);
    }

    Group.prototype.setData = function (data) {
        var comps = this._components,
            comp,
            data_;
        var i = -1,
            j = 0,
            l = comps.length;
        while (++i < l) {
            comp = comps[i];
            if (!isDataComp(comp)) {
                continue;
            }
            data_ = data[j++];
            comp.setValue(data_[Object.keys(data_)[0]]);
        }
    };

    Group.prototype.getData = function () {
        var comps = this._components,
            i = -1,
            l = comps.length;
        var values = [];
        var comp;
        while (++i < l) {
            comp = comps[i];
            if (!isDataComp(comp)) {
                continue;
            }
            values.push(comp.getData());
        }
        return values;
    };

    module.exports = Group;
});

var LayoutMode = createCommonjsModule(function (module) {
    var LayoutMode = {
        LEFT: 'left',
        RIGHT: 'right',
        TOP: 'top',
        BOTTOM: 'bottom',
        NONE: 'none'
    };

    module.exports = LayoutMode;
});

var MenuEvent = createCommonjsModule(function (module) {
	var MenuEvent = {
		UPDATE_MENU: 'updateMenu'
	};
	module.exports = MenuEvent;
});

var ColorMode = createCommonjsModule(function (module) {
	var ColorMode = {
		RGB: 'rgb',
		HSV: 'hsv',
		HEX: 'hex',
		RGBfv: 'rgbfv'
	};

	module.exports = ColorMode;
});

var ColorUtil = createCommonjsModule(function (module) {
	var ColorUtil = {
		HSV2RGB: function (hue, sat, val) {
			var max_hue = 360.0,
			    max_sat = 100.0,
			    max_val = 100.0;

			var min_hue = 0.0,
			    min_sat = 0,
			    min_val = 0;

			hue = hue % max_hue;
			val = Math.max(min_val, Math.min(val, max_val)) / max_val * 255.0;

			if (sat <= min_sat) {
				val = Math.round(val);
				return [val, val, val];
			} else if (sat > max_sat) sat = max_sat;

			sat = sat / max_sat;

			//http://d.hatena.ne.jp/ja9/20100903/128350434

			var hi = Math.floor(hue / 60.0) % 6,
			    f = hue / 60.0 - hi,
			    p = val * (1 - sat),
			    q = val * (1 - f * sat),
			    t = val * (1 - (1 - f) * sat);

			var r = 0,
			    g = 0,
			    b = 0;

			switch (hi) {
				case 0:
					r = val;
					g = t;
					b = p;
					break;
				case 1:
					r = q;
					g = val;
					b = p;
					break;
				case 2:
					r = p;
					g = val;
					b = t;
					break;
				case 3:
					r = p;
					g = q;
					b = val;
					break;
				case 4:
					r = t;
					g = p;
					b = val;
					break;
				case 5:
					r = val;
					g = p;
					b = q;
					break;
				default:
					break;
			}

			r = Math.round(r);
			g = Math.round(g);
			b = Math.round(b);

			return [r, g, b];
		},

		RGB2HSV: function (r, g, b) {
			var h = 0,
			    s = 0,
			    v = 0;

			r = r / 255.0;
			g = g / 255.0;
			b = b / 255.0;

			var minRGB = Math.min(r, Math.min(g, b)),
			    maxRGB = Math.max(r, Math.max(g, b));

			if (minRGB == maxRGB) {
				v = minRGB;
				return [0, 0, Math.round(v)];
			}

			var dd = r == minRGB ? g - b : b == minRGB ? r - g : b - r,
			    hh = r == minRGB ? 3 : b == minRGB ? 1 : 5;

			h = Math.round(60 * (hh - dd / (maxRGB - minRGB)));
			s = Math.round((maxRGB - minRGB) / maxRGB * 100.0);
			v = Math.round(maxRGB * 100.0);

			return [h, s, v];
		},

		RGB2HEX: function (r, g, b) {
			return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
		},

		RGBfv2HEX: function (r, g, b) {
			return ColorUtil.RGB2HEX(Math.floor(r * 255.0), Math.floor(g * 255.0), Math.floor(b * 255.0));
		},

		HSV2HEX: function (h, s, v) {
			var rgb = ControlKit.ColorUtil.HSV2RGB(h, s, v);
			return ControlKit.ColorUtil.RGB2HEX(rgb[0], rgb[1], rgb[2]);
		},

		HEX2RGB: function (hex) {
			var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
			hex = hex.replace(shorthandRegex, function (m, r, g, b) {
				return r + r + g + g + b + b;
			});

			var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
			return result ? [parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16)] : null;
		},

		isValidHEX: function (hex) {
			return (/^#[0-9A-F]{6}$/i.test(hex)
			);
		},

		isValidRGB: function (r, g, b) {
			return r >= 0 && r <= 255 && g >= 0 && g <= 255 && b >= 0 && b <= 255;
		},

		isValidRGBfv: function (r, g, b) {
			return r >= 0 && r <= 1.0 && g >= 0 && g <= 1.0 && b >= 0 && b <= 1.0;
		}
	};

	module.exports = ColorUtil;
});

var Options = createCommonjsModule(function (module) {
    var Node$$1 = Node;
    var DocumentEvent$$1 = DocumentEvent,
        NodeEvent$$1 = NodeEvent;
    var CSS$$1 = CSS;
    var ColorMode$$1 = ColorMode;
    var ColorUtil$$1 = ColorUtil;
    var Metric$$1 = Metric;

    function Options(parentNode) {
        this._parenNode = parentNode;

        var node = this._node = new Node$$1();
        var listNode = this._listNode = new Node$$1(Node$$1.LIST);

        node.setStyleClass(CSS$$1.Options);
        node.addChild(listNode);

        this._selectedIndex = null;
        this._callbackOut = function () {};

        this._unfocusable = false;

        document.addEventListener(DocumentEvent$$1.MOUSE_DOWN, this._onDocumentMouseDown.bind(this));
        document.addEventListener(DocumentEvent$$1.MOUSE_UP, this._onDocumentMouseUp.bind(this));

        this.clear();
    }

    Options.prototype = {
        _onDocumentMouseDown: function () {
            if (!this._unfocusable) return;
            this._callbackOut();
        },

        _onDocumentMouseUp: function () {
            this._unfocusable = true;
        },

        build: function (entries, selected, element, callbackSelect, callbackOut, paddingRight, areColors, colorMode) {
            this._clearList();

            this._parenNode.addChild(this.getNode());

            var rootNode = this._node,
                listNode = this._listNode;

            paddingRight = paddingRight || 0;

            var self = this;

            // build list
            var itemNode, entry;
            var i = -1;

            if (areColors) {
                colorMode = colorMode || ColorMode$$1.HEX;

                listNode.setStyleClass(CSS$$1.Color);

                var color, nodeColor;

                while (++i < entries.length) {
                    entry = entries[i];
                    itemNode = listNode.addChild(new Node$$1(Node$$1.LIST_ITEM));
                    color = itemNode.addChild(new Node$$1());

                    switch (colorMode) {
                        case ColorMode$$1.HEX:
                            nodeColor = entry;
                            break;
                        case ColorMode$$1.RGB:
                            nodeColor = ColorUtil$$1.RGB2HEX(entry[0], entry[1], entry[2]);
                            break;
                        case ColorMode$$1.RGBfv:
                            nodeColor = ColorUtil$$1.RGBfv2HEX(entry[0], entry[1], entry[2]);
                            break;
                        case ColorMode$$1.HSV:
                            nodeColor = ColorUtil$$1.HSV2RGB(entry[0], entry[1], entry[2]);
                            break;
                    }

                    color.getStyle().backgroundColor = nodeColor;
                    color.getStyle().backgroundImage = 'linear-gradient( rgba(0,0,0,0) 0%, rgba(0,0,0,0.1) 100%)';
                    color.setProperty('innerHTML', entry);

                    if (entry == selected) itemNode.setStyleClass(CSS$$1.OptionsSelected);

                    itemNode.addEventListener(NodeEvent$$1.MOUSE_DOWN, function () {
                        self._selectedIndex = Array.prototype.indexOf.call(this.parentNode.children, this);
                        callbackSelect();
                    });
                }
            } else {
                listNode.deleteStyleClass();

                while (++i < entries.length) {
                    entry = entries[i];

                    itemNode = listNode.addChild(new Node$$1(Node$$1.LIST_ITEM));
                    itemNode.setProperty('innerHTML', entry);
                    if (entry == selected) itemNode.setStyleClass(CSS$$1.OptionsSelected);

                    itemNode.addEventListener(NodeEvent$$1.MOUSE_DOWN, function () {
                        self._selectedIndex = Array.prototype.indexOf.call(this.parentNode.children, this);
                        callbackSelect();
                    });
                }
            }

            //position, set width and enable

            var elementPos = element.getPositionGlobal(),
                elementWidth = element.getWidth() - paddingRight,
                elementHeight = element.getHeight();

            var listWidth = listNode.getWidth(),
                listHeight = listNode.getHeight(),
                strokeOffset = Metric$$1.STROKE_SIZE * 2;

            var paddingOptions = Metric$$1.PADDING_OPTIONS;

            var width = (listWidth < elementWidth ? elementWidth : listWidth) - strokeOffset,
                posX = elementPos[0],
                posY = elementPos[1] + elementHeight - paddingOptions;

            var windowWidth = window.innerWidth,
                windowHeight = window.innerHeight;

            var rootPosX = posX + width > windowWidth ? posX - width + elementWidth - strokeOffset : posX,
                rootPosY = posY + listHeight > windowHeight ? posY - listHeight * 0.5 - elementHeight * 0.5 : posY;

            listNode.setWidth(width);
            rootNode.setPositionGlobal(rootPosX, rootPosY);

            this._callbackOut = callbackOut;
            this._unfocusable = false;
        },

        _clearList: function () {
            this._listNode.removeAllChildren();
            this._listNode.deleteStyleProperty('width');
            this._selectedIndex = null;
            this._build = false;
        },

        clear: function () {
            this._clearList();
            this._callbackOut = function () {};
            this._parenNode.removeChild(this.getNode());
        },

        isBuild: function () {
            return this._build;
        },
        getNode: function () {
            return this._node;
        },
        getSelectedIndex: function () {
            return this._selectedIndex;
        }
    };

    Options.setup = function (parentNode) {
        return Options._instance = new Options(parentNode);
    };
    Options.get = function () {
        return Options._instance;
    };
    Options.destroy = function () {
        Options._instance = null;
    };

    module.exports = Options;
});

var ButtonPreset = createCommonjsModule(function (module) {
    var EventDispatcher$$1 = EventDispatcher;
    var ObjectComponentNotifier$$1 = ObjectComponentNotifier;

    var Event_ = Event$1,
        OptionEvent$$1 = OptionEvent,
        NodeEvent$$1 = NodeEvent;

    var Node$$1 = Node;
    var CSS$$1 = CSS;

    function ButtonPreset(parentNode) {
        EventDispatcher$$1.apply(this);
        var node = this._btnNode = new Node$$1(Node$$1.INPUT_BUTTON),
            imgNode = this._indiNode = new Node$$1();

        this._onActive = function () {};
        this._onDeactive = function () {};
        this._isActive = false;

        node.setStyleClass(CSS$$1.ButtonPreset);
        node.addEventListener(NodeEvent$$1.MOUSE_DOWN, this._onMouseDown.bind(this));

        node.addChild(imgNode);
        parentNode.addChildAt(node, 0);

        ObjectComponentNotifier$$1.get().addEventListener(OptionEvent$$1.TRIGGER, this, 'onOptionTrigger');
        this.addEventListener(OptionEvent$$1.TRIGGERED, ObjectComponentNotifier$$1.get(), 'onOptionTriggered');
    }
    ButtonPreset.prototype = Object.create(EventDispatcher$$1.prototype);
    ButtonPreset.prototype.constructor = ButtonPreset;

    ButtonPreset.prototype.onOptionTrigger = function (e) {
        if (e.data.origin == this) {
            if (!this._isActive) {
                this._onActive();
                this._btnNode.setStyleClass(CSS$$1.ButtonPresetActive);
                this._isActive = true;
            } else {
                this._onDeactive();
            }
            return;
        }

        if (this._isActive) {
            this.deactivate();
        }
    };

    ButtonPreset.prototype._onMouseDown = function () {
        this.dispatchEvent(new Event_(this, OptionEvent$$1.TRIGGERED, null));
    };

    ButtonPreset.prototype.setOnActive = function (func) {
        this._onActive = func;
    };

    ButtonPreset.prototype.setOnDeactive = function (func) {
        this._onDeactive = func;
    };

    ButtonPreset.prototype.deactivate = function () {
        this._isActive = false;
        this._btnNode.setStyleClass(CSS$$1.ButtonPreset);
    };

    module.exports = ButtonPreset;
});

var StringInput = createCommonjsModule(function (module) {
    var ObjectComponent$$1 = ObjectComponent;
    var Node$$1 = Node;
    var CSS$$1 = CSS;
    var Options$$1 = Options;
    var ButtonPreset$$1 = ButtonPreset;
    var Metric$$1 = Metric;

    var Event_ = Event$1,
        DocumentEvent$$1 = DocumentEvent,
        NodeEvent$$1 = NodeEvent,
        ComponentEvent$$1 = ComponentEvent;

    var DEFAULT_PRESET = null;

    function StringInput(parent, object, value, params) {
        ObjectComponent$$1.apply(this, arguments);

        params = params || {};
        params.onChange = params.onChange || this._onChange;
        params.presets = params.presets || DEFAULT_PRESET;

        this._onChange = params.onChange;

        var input = this._input = new Node$$1(Node$$1.INPUT_TEXT);

        var wrap = this._wrapNode;

        var presets = params.presets;
        if (!presets) {
            wrap.addChild(input);
        } else {
            var wrap_ = new Node$$1();
            wrap_.setStyleClass(CSS$$1.WrapInputWPreset);

            wrap.addChild(wrap_);
            wrap_.addChild(input);

            var options = Options$$1.get(),
                btnPreset = new ButtonPreset$$1(this._wrapNode);

            var onPresetDeactivate = function () {
                options.clear();
                btnPreset.deactivate();
            };

            var self = this;
            var onPresetActivate = function () {
                options.build(presets, input.getProperty('value'), input, function () {
                    input.setProperty('value', presets[options.getSelectedIndex()]);
                    self.pushHistoryState();
                    self.applyValue();
                }, onPresetDeactivate, Metric$$1.PADDING_PRESET, false);
            };

            btnPreset.setOnActive(onPresetActivate);
            btnPreset.setOnDeactive(onPresetDeactivate);
        }

        input.setProperty('value', this._obj[this._key]);

        input.addEventListener(NodeEvent$$1.KEY_UP, this._onInputKeyUp.bind(this));
        input.addEventListener(NodeEvent$$1.CHANGE, this._onInputChange.bind(this));

        input.addEventListener(NodeEvent$$1.MOUSE_DOWN, this._onInputDragStart.bind(this));
        this.addEventListener(ComponentEvent$$1.INPUT_SELECT_DRAG, this._parent, 'onComponentSelectDrag');
    }
    StringInput.prototype = Object.create(ObjectComponent$$1.prototype);
    StringInput.prototype.constructor = StringInput;

    StringInput.prototype._onInputKeyUp = function (e) {
        if (this._keyIsChar(e.keyCode)) {
            this.pushHistoryState();
        }
        this.applyValue();
        this._onChange();
    };

    StringInput.prototype._onInputChange = function (e) {
        if (this._keyIsChar(e.keyCode)) {
            this.pushHistoryState();
        }
        this.applyValue();
    };

    //TODO: Finish check
    StringInput.prototype._keyIsChar = function (keyCode) {
        return keyCode != 17 && keyCode != 18 && keyCode != 20 && keyCode != 37 && keyCode != 38 && keyCode != 39 && keyCode != 40 && keyCode != 16;
    };

    StringInput.prototype.applyValue = function () {
        this._obj[this._key] = this._input.getProperty('value');
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.VALUE_UPDATED, null));
    };

    StringInput.prototype.onValueUpdate = function (e) {
        if (e.data.origin == this) return;
        this._input.setProperty('value', this._obj[this._key]);
    };

    //Prevent chrome select drag
    StringInput.prototype._onInputDragStart = function () {
        var eventMove = DocumentEvent$$1.MOUSE_MOVE,
            eventUp = DocumentEvent$$1.MOUSE_UP;

        var event = ComponentEvent$$1.INPUT_SELECT_DRAG;
        var self = this;
        var onDrag = function () {
            self.dispatchEvent(new Event_(this, event, null));
        },
            onDragFinish = function () {
            self.dispatchEvent(new Event_(this, event, null));

            document.removeEventListener(eventMove, onDrag, false);
            document.removeEventListener(eventMove, onDragFinish, false);
        };

        this.dispatchEvent(new Event_(this, event, null));

        document.addEventListener(eventMove, onDrag, false);
        document.addEventListener(eventUp, onDragFinish, false);
    };

    module.exports = StringInput;
});

var NumberInput_Internal = createCommonjsModule(function (module) {
    var EventDispatcher$$1 = EventDispatcher,
        NodeEvent$$1 = NodeEvent;
    var Node$$1 = Node;

    var PRESET_SHIFT_MULTIPLIER = 10;
    var NUM_REGEX = /^-?\d*\.?\d*$/;

    var setCaretPos = null,
        selectAll = null;

    function inputSetValue(input, value) {
        input.setProperty('value', value);
        input.dispatchEvent(new Event('input'));
    }

    NumberInput_Internal = function (stepValue, dp, onBegin, onChange, onFinish, onError) {
        EventDispatcher$$1.apply(this, null);

        this._value = 0;
        this._valueStep = stepValue;
        this._valueDp = dp;

        this._onBegin = onBegin || function () {};
        this._onChange = onChange || function () {};
        this._onFinish = onFinish || function () {};
        this._onError = onError || function () {};

        this._keyCode = null;
        this._caretOffset = 0;

        var input = this._input = new Node$$1('text');
        input.setProperty('value', this._value);

        input.addEventListener('input', this._onInput.bind(this));
        input.addEventListener('keydown', this._onKeydown.bind(this));

        if (!setCaretPos) {
            if (input.getElement().setSelectionRange) {
                setCaretPos = function (input, pos) {
                    input.getElement().setSelectionRange(pos, pos);
                };
                selectAll = function (input) {
                    input.getElement().setSelectionRange(0, input.getProperty('value').length);
                };
            } else {
                setCaretPos = function (input, pos) {
                    var range = input.getElement().createTextRange();
                    range.collapse(true);
                    range.moveEnd('character', pos);
                    range.moveStart('character', pos);
                    range.select();
                };
                selectAll = function (input) {
                    var range = input.getElement().createTextRange();
                    range.collapse(true);
                    range.moveStart('character', 0);
                    range.moveEnd('character', input.getProperty('value').length);
                    range.select();
                };
            }
        }
    };
    NumberInput_Internal.prototype = Object.create(EventDispatcher$$1.prototype);
    NumberInput_Internal.prototype.constructor = NumberInput_Internal;

    NumberInput_Internal.prototype._setValue = function (value) {
        var prefix = ((value = +value) || 1 / value) < 0 && value == 0 ? '-' : ''; //-0
        value = Number(value).toFixed(this._valueDp);
        this._input.setProperty('value', prefix + value);
        this._value = Number(value);
    };

    NumberInput_Internal.prototype._onInput = function () {
        var input = this._input,
            value = input.getProperty('value'),
            start = input.getProperty('selectionStart'),
            dp = this._valueDp;

        var first = value[0];

        if (value == '') {
            value = 0;
        } else if (first === '.') {
            value = '0' + value;
        }

        if (!NUM_REGEX.test(value) || value == '-') {
            input.setProperty('value', this._value.toFixed(dp));
            setCaretPos(input, Math.max(--start, 0));
            this._onError(this._keyCode);
            return;
        }
        this._onBegin(this._value);
        this._setValue(value);
        setCaretPos(input, start - this._caretOffset);
        this._onChange();
    };

    NumberInput_Internal.prototype._onKeydown = function (e) {
        var keyCode = this._keyCode = e.keyCode;

        if (keyCode == 13) {
            this._onFinish();
            e.preventDefault();
            return;
        }

        var input = this._input,
            value = input.getProperty('value');
        var start = input.getProperty('selectionStart'),
            end = input.getProperty('selectionEnd');
        var length = value.length;

        var isBackspaceDelete = keyCode == 8 || keyCode == 45,
            isMetaKey = e.metaKey,
            isCtrlKey = e.ctrlKey,
            isLeft = keyCode == 37,
            isRight = keyCode == 39,
            isLeftRight = isLeft || isRight,
            isShift = e.shiftKey,
            isUpDown = keyCode == 38 || keyCode == 40,
            isSelectAll = (isMetaKey || isCtrlKey) && keyCode == 65,
            isRangeSelected = start != end,
            isAllSelected = start == 0 && end == length,
            isMinus = keyCode == 189;

        var indexDecimalMark = value.indexOf('.');

        this._caretOffset = 0;

        //prevent cmd-z || ctrl-z
        if ((isMetaKey || isCtrlKey) && keyCode == 90) {
            e.preventDefault();
            return;
        }
        //select all cmd+a || ctrl+a
        if (isSelectAll) {
            selectAll(input);
            e.preventDefault();
            return;
        }
        //everything is selected
        if (isAllSelected) {
            if (isMinus) {
                //set negative zero, as starting point for negative number
                inputSetValue(input, '-0');
                //set caret after  '-'
                setCaretPos(input, 1);
            } else {
                //delete number / replace / ignore
                inputSetValue(input, isBackspaceDelete ? 0 : String.fromCharCode(keyCode));
                //jump to start <--> end
                setCaretPos(input, isLeft ? start : end);
            }
            e.preventDefault();
            return;
        }
        //jump over decimal mark
        if (isBackspaceDelete && start - 1 == indexDecimalMark) {
            setCaretPos(input, start - 1);
            return;
        }
        // 0|. enter first dp without jumping over decimal mark
        if (!isLeftRight && value[0] == '0' && start == 1) {
            setCaretPos(input, 1);
            this._caretOffset = 1;
            return;
        }
        //increase / decrease number by (step up / down) * multiplier on shift down
        if (isUpDown) {
            var step = (isShift ? PRESET_SHIFT_MULTIPLIER : 1) * this._valueStep,
                mult = keyCode == 38 ? 1.0 : -1.0;
            inputSetValue(input, Number(value) + step * mult);
            setCaretPos(input, start);
            e.preventDefault();
            return;
        }
        //range selected, not in selection process
        if (isRangeSelected && !(isShift && isLeftRight)) {
            //jump to start <--> end
            if (isLeftRight) {
                setCaretPos(input, isLeft ? start : end);
            } else {
                //replace complete range, not just parts
                value = value.substr(0, start) + String.fromCharCode(keyCode) + value.substr(end, length - end);
                inputSetValue(input, value);
                setCaretPos(input, end);
            }
            e.preventDefault();
            return;
        }
        //caret within fractional part, not moving caret, selecting, deleting
        if (!isShift && !isLeftRight && !isBackspaceDelete && start > indexDecimalMark && start < length) {
            value = value.substr(0, start) + String.fromCharCode(keyCode) + value.substr(start + 1, length - 1);
            inputSetValue(input, value);
            setCaretPos(input, Math.min(start + 1, length - 1));
            e.preventDefault();
            return;
        }
        //caret at end of number, do nothing
        if (!isBackspaceDelete && !isLeftRight && !isUpDown && start >= length) {
            e.preventDefault();
        }
    };

    NumberInput_Internal.prototype.getValue = function () {
        return this._value;
    };

    NumberInput_Internal.prototype.setValue = function (n) {
        this._setValue(n);
    };

    NumberInput_Internal.prototype.getNode = function () {
        return this._input;
    };

    module.exports = NumberInput_Internal;
});

var require$$5$4 = NumberInput_Internal;

var NumberInput = createCommonjsModule(function (module) {
    var ObjectComponent$$1 = ObjectComponent;
    var NumberInput_Internal = require$$5$4;

    var Node$$1 = Node;

    var Options$$1 = Options;
    var ButtonPreset$$1 = ButtonPreset;
    var CSS$$1 = CSS,
        Metric$$1 = Metric;

    var Event_ = Event$1,
        DocumentEvent$$1 = DocumentEvent,
        NodeEvent$$1 = NodeEvent,
        ComponentEvent$$1 = ComponentEvent;

    var DEFAULT_INPUT_DP = 2,
        DEFAULT_INPUT_STEP = 1,
        DEFAULT_INPUT_PRESET = null;

    function NumberInput(parent, object, value, params) {
        ObjectComponent$$1.apply(this, arguments);

        params = params || {};
        params.onBegin = params.onBegin || null;
        params.onChange = params.onChange || this._onChange;
        params.onFinish = params.onFinish || null;
        params.onError = params.onError || null;
        params.dp = params.dp === undefined || params.dp == null ? DEFAULT_INPUT_DP : params.dp;
        params.step = params.step || DEFAULT_INPUT_STEP;
        params.presets = params.presets || DEFAULT_INPUT_PRESET;

        this._onBegin = params.onBegin;
        this._onChange = params.onChange;
        this._presetsKey = params.presets;

        var input = this._input = new NumberInput_Internal(params.step, params.dp, params.onBegin, this._onInputChange.bind(this), params.onFinish, params.onError);

        var wrap = this._wrapNode;

        var presets = params.presets;
        if (!presets) {
            wrap.addChild(input.getNode());
        } else {
            var wrap_ = new Node$$1();
            wrap_.setStyleClass(CSS$$1.WrapInputWPreset);

            wrap.addChild(wrap_);
            wrap_.addChild(input.getNode());

            var options = Options$$1.get();
            var presetBtn = this._btnPreset = new ButtonPreset$$1(this._wrapNode);

            var onPresetDeactivate = function () {
                options.clear();
                presetBtn.deactivate();
            };

            var self = this;
            var onPresetActivate = function () {
                options.build(presets, input.getValue(), input.getNode(), function () {
                    input.setValue(presets[options.getSelectedIndex()]);
                    self.applyValue();
                    self._onChange(self._obj[self._key]);
                }, onPresetDeactivate, Metric$$1.PADDING_PRESET, false);
            };
            presetBtn.setOnActive(onPresetActivate);
            presetBtn.setOnDeactive(onPresetDeactivate);
        }

        input.getNode().addEventListener(NodeEvent$$1.MOUSE_DOWN, this._onInputDragStart.bind(this));
        this.addEventListener(ComponentEvent$$1.INPUT_SELECT_DRAG, this._parent, 'onComponentSelectDrag');

        input.setValue(this._obj[this._key]);
    }
    NumberInput.prototype = Object.create(ObjectComponent$$1.prototype);
    NumberInput.prototype.constructor = NumberInput;

    NumberInput.prototype._onInputChange = function () {
        this.applyValue();
        this._onChange(this._obj[this._key]);
    };

    NumberInput.prototype.applyValue = function () {
        this.pushHistoryState();
        this._obj[this._key] = this._input.getValue();
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.VALUE_UPDATED, null));
    };

    NumberInput.prototype.onValueUpdate = function (e) {
        if (e.data.origin == this) {
            return;
        }
        this._input.setValue(this._obj[this._key]);
    };

    //Prevent chrome select drag
    NumberInput.prototype._onInputDragStart = function () {
        var eventMove = DocumentEvent$$1.MOUSE_MOVE,
            eventUp = DocumentEvent$$1.MOUSE_UP;

        var event = ComponentEvent$$1.INPUT_SELECT_DRAG;

        var self = this;

        var onDrag = function () {
            self.dispatchEvent(new Event_(this, event, null));
        },
            onDragFinish = function () {
            self.dispatchEvent(new Event_(this, event, null));
            document.removeEventListener(eventMove, onDrag, false);
            document.removeEventListener(eventMove, onDragFinish, false);
        };

        this.dispatchEvent(new Event_(this, event, null));

        document.addEventListener(eventMove, onDrag, false);
        document.addEventListener(eventUp, onDragFinish, false);
    };

    module.exports = NumberInput;
});

var Range = createCommonjsModule(function (module) {
    var ObjectComponent$$1 = ObjectComponent;
    var Node$$1 = Node;
    var NumberInput_Internal = require$$5$4;
    var CSS$$1 = CSS;

    var Event_ = Event$1,
        ComponentEvent$$1 = ComponentEvent;

    var DEFAULT_STEP = 1.0,
        DEFAULT_DP = 2;

    function Range(parent, object, value, params) {
        ObjectComponent$$1.apply(this, arguments);

        params = params || {};
        params.onChange = params.onChange || this._onChange;
        params.step = params.step || DEFAULT_STEP;
        params.dp = params.dp != null ? params.dp : DEFAULT_DP;

        this._onChange = params.onChange;

        var step = this._step = params.step,
            dp = this._dp = params.dp;

        //FIXME: history push pop

        var labelMin = new Node$$1();
        var inputMin = this._inputMin = new NumberInput_Internal(step, dp, this.pushHistoryState.bind(this), this._onInputMinChange.bind(this));

        var labelMax = new Node$$1();
        var inputMax = this._inputMax = new NumberInput_Internal(step, dp, this.pushHistoryState.bind(this), this._onInputMaxChange.bind(this));

        var labelMinWrap = new Node$$1().setStyleClass(CSS$$1.Wrap),
            inputMinWrap = new Node$$1().setStyleClass(CSS$$1.Wrap),
            labelMaxWrap = new Node$$1().setStyleClass(CSS$$1.Wrap),
            inputMaxWrap = new Node$$1().setStyleClass(CSS$$1.Wrap);

        labelMin.setStyleClass(CSS$$1.Label).setProperty('innerHTML', 'MIN');
        labelMax.setStyleClass(CSS$$1.Label).setProperty('innerHTML', 'MAX');

        var values = this._obj[this._key];

        inputMin.setValue(values[0]);
        inputMax.setValue(values[1]);

        var wrap = this._wrapNode;

        labelMinWrap.addChild(labelMin);
        inputMinWrap.addChild(inputMin.getNode());
        labelMaxWrap.addChild(labelMax);
        inputMaxWrap.addChild(inputMax.getNode());

        wrap.addChild(labelMinWrap);
        wrap.addChild(inputMinWrap);
        wrap.addChild(labelMaxWrap);
        wrap.addChild(inputMaxWrap);
    }
    Range.prototype = Object.create(ObjectComponent$$1.prototype);
    Range.prototype.constructor = Range;

    Range.prototype._onInputChange = function () {
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.VALUE_UPDATED, null));
        this._onChange();
    };

    Range.prototype._updateValueMin = function () {
        var values = this._obj[this._key];

        var inputMin = this._inputMin,
            inputValue = inputMin.getValue();

        if (inputValue >= this._inputMax.getValue()) {
            inputMin.setValue(values[0]);
            return;
        }
        values[0] = inputValue;
    };

    Range.prototype._updateValueMax = function () {
        var values = this._obj[this._key];

        var inputMax = this._inputMax,
            inputValue = inputMax.getValue();

        if (inputValue <= this._inputMin.getValue()) {
            inputMax.setValue(values[1]);
            return;
        }
        values[1] = inputValue;
    };

    Range.prototype.onValueUpdate = function (e) {
        if (e.data.origin == this) {
            return;
        }
        if (e.data.origin == null) {}
        var o = this._obj,
            k = this._key;
        this._inputMin.setValue(o[k][0]);
        this._inputMax.setValue(o[k][1]);
    };

    Range.prototype.setValue = function (value) {
        var o = this._obj,
            k = this._key;
        o[k][0] = value[0];
        o[k][1] = value[1];
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.VALUE_UPDATED, null));
    };

    Range.prototype._onInputMinChange = function () {
        this._updateValueMin();
        this._onInputChange();
    };

    Range.prototype._onInputMaxChange = function () {
        this._updateValueMax();
        this._onInputChange();
    };

    module.exports = Range;
});

var Checkbox = createCommonjsModule(function (module) {
    var ObjectComponent$$1 = ObjectComponent,
        Node$$1 = Node;

    var Event_ = Event$1,
        NodeEvent$$1 = NodeEvent,
        ComponentEvent$$1 = ComponentEvent;

    function Checkbox(parent, object, value, params) {
        ObjectComponent$$1.apply(this, arguments);

        params = params || {};
        params.onChange = params.onChange || this._onChange;
        this._onChange = params.onChange;

        var node = this._input = new Node$$1(Node$$1.INPUT_CHECKBOX);
        node.setProperty('checked', this._obj[this._key]);
        node.addEventListener(NodeEvent$$1.CHANGE, this._onInputChange.bind(this));

        this._wrapNode.addChild(this._input);
    }
    Checkbox.prototype = Object.create(ObjectComponent$$1.prototype);
    Checkbox.prototype.constructor = Checkbox;

    Checkbox.prototype.applyValue = function () {
        this.pushHistoryState();

        var obj = this._obj,
            key = this._key;
        obj[key] = !obj[key];

        this.dispatchEvent(new Event_(this, ComponentEvent$$1.VALUE_UPDATED, null));
    };

    Checkbox.prototype._onInputChange = function () {
        this.applyValue();
        this._onChange();
    };

    Checkbox.prototype.onValueUpdate = function (e) {
        if (e.data.origin == this) {
            return;
        }
        this._input.setProperty('checked', this._obj[this._key]);
    };

    module.exports = Checkbox;
});

var Picker = createCommonjsModule(function (module) {
    var Node$$1 = Node;

    var CSS$$1 = CSS;
    var NumberInput_Internal = require$$5$4;
    var Mouse$$1 = Mouse;
    var ColorUtil$$1 = ColorUtil;
    var DocumentEvent$$1 = DocumentEvent,
        NodeEvent$$1 = NodeEvent;

    var DEFAULT_VALUE_HUE = 200.0,
        DEFAULT_VALUE_SAT = 50.0,
        DEFAULT_VALUE_VAL = 50.0;

    function Picker(parentNode) {
        var root = this._node = new Node$$1().setStyleClass(CSS$$1.Picker),
            head = this._headNode = new Node$$1().setStyleClass(CSS$$1.Head),
            labelWrap = new Node$$1().setStyleClass(CSS$$1.Wrap),
            label = new Node$$1().setStyleClass(CSS$$1.Label),
            menu = new Node$$1().setStyleClass(CSS$$1.Menu),
            menuWrap = new Node$$1().setStyleClass(CSS$$1.Wrap);

        var menuClose = new Node$$1(Node$$1.INPUT_BUTTON);
        menuClose.setStyleClass(CSS$$1.ButtonMenuClose);

        var fieldWrap = new Node$$1().setStyleClass(CSS$$1.PickerFieldWrap),
            sliderWrap = new Node$$1().setStyleClass(CSS$$1.SliderWrap),
            inputWrap = new Node$$1().setStyleClass(CSS$$1.PickerInputWrap);

        var canvasField = this._canvasField = document.createElement('canvas'),
            canvasSlider = this._canvasSlider = document.createElement('Canvas');

        fieldWrap.getElement().appendChild(canvasField);
        sliderWrap.getElement().appendChild(canvasSlider);

        this._setSizeCanvasField(154, 154);
        this._setSizeCanvasSlider(14, 154);

        var contextCanvasField = this._contextCanvasField = canvasField.getContext('2d'),
            contextCanvasSlider = this._contextCanvasSlider = canvasSlider.getContext('2d');

        var handleField = this._handleField = new Node$$1();
        handleField.setStyleClass(CSS$$1.PickerHandleField);

        var handleSlider = this._handleSlider = new Node$$1();
        handleSlider.setStyleClass(CSS$$1.PickerHandleSlider);

        var step = 1.0,
            dp = 0;

        var callbackHue = this._onInputHueChange.bind(this),
            callbackSat = this._onInputSatChange.bind(this),
            callbackVal = this._onInputValChange.bind(this),
            callbackR = this._onInputRChange.bind(this),
            callbackG = this._onInputGChange.bind(this),
            callbackB = this._onInputBChange.bind(this);

        var inputHue = this._inputHue = new NumberInput_Internal(step, dp, null, callbackHue),
            inputSat = this._inputSat = new NumberInput_Internal(step, dp, null, callbackSat),
            inputVal = this._inputVal = new NumberInput_Internal(step, dp, null, callbackVal),
            inputR = this._inputR = new NumberInput_Internal(step, dp, null, callbackR),
            inputG = this._inputG = new NumberInput_Internal(step, dp, null, callbackG),
            inputB = this._inputB = new NumberInput_Internal(step, dp, null, callbackB);

        var controlsWrap = new Node$$1().setStyleClass(CSS$$1.PickerControlsWrap);

        var buttonPick = new Node$$1(Node$$1.INPUT_BUTTON).setStyleClass(CSS$$1.Button).setProperty('value', 'pick'),
            buttonCancel = new Node$$1(Node$$1.INPUT_BUTTON).setStyleClass(CSS$$1.Button).setProperty('value', 'cancel');

        var colorContrast = new Node$$1().setStyleClass(CSS$$1.PickerColorContrast);

        var color0 = this._colorCurrNode = new Node$$1(),
            color1 = this._colorPrevNode = new Node$$1();

        colorContrast.addChild(color0);
        colorContrast.addChild(color1);

        controlsWrap.addChild(buttonCancel);
        controlsWrap.addChild(buttonPick);
        controlsWrap.addChild(colorContrast);

        this._setContrasPrevColor(0, 0, 0);

        var inputFieldWrapHue = new Node$$1().setStyleClass(CSS$$1.PickerInputField),
            inputFieldWrapSat = new Node$$1().setStyleClass(CSS$$1.PickerInputField),
            inputFieldWrapVal = new Node$$1().setStyleClass(CSS$$1.PickerInputField);

        var inputFieldWrapHueLabel = new Node$$1(Node$$1.SPAN).setStyleClass(CSS$$1.Label).setProperty('innerHTML', 'H'),
            inputFieldWrapSatLabel = new Node$$1(Node$$1.SPAN).setStyleClass(CSS$$1.Label).setProperty('innerHTML', 'S'),
            inputFieldWrapValLabel = new Node$$1(Node$$1.SPAN).setStyleClass(CSS$$1.Label).setProperty('innerHTML', 'V');

        inputFieldWrapHue.addChildren(inputFieldWrapHueLabel, inputHue.getNode());
        inputFieldWrapSat.addChildren(inputFieldWrapSatLabel, inputSat.getNode());
        inputFieldWrapVal.addChildren(inputFieldWrapValLabel, inputVal.getNode());

        var inputFieldWrapR = new Node$$1().setStyleClass(CSS$$1.PickerInputField),
            inputFieldWrapG = new Node$$1().setStyleClass(CSS$$1.PickerInputField),
            inputFieldWrapB = new Node$$1().setStyleClass(CSS$$1.PickerInputField);

        var inputFieldWrapRLabel = new Node$$1(Node$$1.SPAN).setStyleClass(CSS$$1.Label).setProperty('innerHTML', 'R'),
            inputFieldWrapGLabel = new Node$$1(Node$$1.SPAN).setStyleClass(CSS$$1.Label).setProperty('innerHTML', 'G'),
            inputFieldWrapBLabel = new Node$$1(Node$$1.SPAN).setStyleClass(CSS$$1.Label).setProperty('innerHTML', 'B');

        inputFieldWrapR.addChildren(inputFieldWrapRLabel, inputR.getNode());
        inputFieldWrapG.addChildren(inputFieldWrapGLabel, inputG.getNode());
        inputFieldWrapB.addChildren(inputFieldWrapBLabel, inputB.getNode());

        inputWrap.addChildren(inputFieldWrapR, inputFieldWrapHue, inputFieldWrapG, inputFieldWrapSat, inputFieldWrapB, inputFieldWrapVal, colorContrast);

        var hexInputWrap = new Node$$1();
        hexInputWrap.setStyleClass(CSS$$1.PickerInputWrap);

        var inputHEX = this._inputHEX = new Node$$1(Node$$1.INPUT_TEXT),
            inputFieldWrapHEX = new Node$$1().setStyleClass(CSS$$1.PickerInputField),
            inputFieldWrapHEXLabel = new Node$$1(Node$$1.SPAN).setStyleClass(CSS$$1.Label);

        inputFieldWrapHEXLabel.setProperty('innerHTML', '#');
        inputFieldWrapHEX.addChildren(inputFieldWrapHEXLabel, inputHEX);

        hexInputWrap.addChild(inputFieldWrapHEX);

        inputHEX.addEventListener(NodeEvent$$1.CHANGE, this._onInputHEXFinish.bind(this));

        label.setProperty('innerHTML', 'Color Picker');

        menu.addChild(menuClose);
        head.addChild(menu);
        labelWrap.addChild(label);
        head.addChild(labelWrap);
        root.addChild(head);
        root.addChild(menuWrap);

        //wrapNode.addChild(paletteWrap);

        menuWrap.addChild(fieldWrap);
        menuWrap.addChild(sliderWrap);
        menuWrap.addChild(inputWrap);
        menuWrap.addChild(hexInputWrap);
        menuWrap.addChild(controlsWrap);

        fieldWrap.addChild(handleField);
        sliderWrap.addChild(handleSlider);

        var eventMouseDown = NodeEvent$$1.MOUSE_DOWN,
            callback = this._onCanvasFieldMouseDown.bind(this);

        fieldWrap.addEventListener(eventMouseDown, callback);
        handleField.addEventListener(eventMouseDown, callback);

        callback = this._onCanvasSliderMouseDown.bind(this);

        sliderWrap.addEventListener(eventMouseDown, callback);
        handleSlider.addEventListener(eventMouseDown, callback);

        menuClose.addEventListener(eventMouseDown, this._onClose.bind(this));
        buttonPick.addEventListener(eventMouseDown, this._onPick.bind(this));
        buttonCancel.addEventListener(eventMouseDown, this._onClose.bind(this));

        head.addEventListener(NodeEvent$$1.MOUSE_DOWN, this._onHeadDragStart.bind(this));

        this._parentNode = parentNode;

        this._mouseOffset = [0, 0];
        this._position = [null, null];

        this._canvasSliderPos = [0, 0];
        this._canvasFieldPos = [0, 0];
        this._handleFieldSize = 12;
        this._handleSliderHeight = 7;

        this._imageDataSlider = contextCanvasSlider.createImageData(canvasSlider.width, canvasSlider.height);
        this._imageDataField = contextCanvasField.createImageData(canvasField.width, canvasField.height);

        this._valueHueMinMax = [0, 360];
        this._valueSatMinMax = this._valueValMinMax = [0, 100];
        this._valueRGBMinMax = [0, 255];

        this._valueHue = DEFAULT_VALUE_HUE;
        this._valueSat = DEFAULT_VALUE_SAT;
        this._valueVal = DEFAULT_VALUE_VAL;
        this._valueR = 0;
        this._valueG = 0;
        this._valueB = 0;

        this._valueHEX = '#000000';
        this._valueHEXValid = this._valueHEX;

        this._callbackPick = function () {};

        //this._canvasFieldImageDataFunc = function(i,j){return this._HSV2RGB(this._valueHue,j)}

        this._drawCanvasField();
        this._drawCanvasSlider();

        this._setColorHSV(this._valueHue, this._valueSat, this._valueVal);

        this._updateColorRGBFromHSV();
        this._updateColorHEXFromRGB();

        this._updateHandles();
    }

    Picker.prototype = {
        _drawHandleField: function () {
            var canvas = this._canvasField,
                nodePos = this._canvasFieldPos,
                mousePos = Mouse$$1.get().getPosition();

            var posX = Math.max(0, Math.min(mousePos[0] - nodePos[0], canvas.width)),
                posY = Math.max(0, Math.min(mousePos[1] - nodePos[1], canvas.height)),
                posXNorm = posX / canvas.width,
                posYNorm = posY / canvas.height;

            var sat = Math.round(posXNorm * this._valueSatMinMax[1]),
                val = Math.round((1.0 - posYNorm) * this._valueValMinMax[1]);

            this._setColorHSV(this._valueHue, sat, val);

            this._updateColorRGBFromHSV();
            this._updateColorHEXFromRGB();

            this._updateHandleField();
        },

        _updateHandleField: function () {
            var width = this._canvasField.width,
                height = this._canvasField.height,
                offsetHandle = this._handleFieldSize * 0.25;

            var satNorm = this._valueSat / this._valueSatMinMax[1],
                valNorm = this._valueVal / this._valueValMinMax[1];

            this._handleField.setPositionGlobal(satNorm * width - offsetHandle, (1.0 - valNorm) * height - offsetHandle);
        },

        _drawHandleSlider: function () {
            var canvas = this._canvasSlider,
                canvasPosY = this._canvasSliderPos[1],
                mousePosY = Mouse$$1.get().getY();

            var posY = Math.max(0, Math.min(mousePosY - canvasPosY, canvas.height)),
                posYNorm = posY / canvas.height;

            var hue = Math.floor((1.0 - posYNorm) * this._valueHueMinMax[1]);

            this._setColorHSV(hue, this._valueSat, this._valueVal);

            this._updateColorRGBFromHSV();
            this._updateColorHEXFromRGB();

            this._updateHandleSlider();
        },

        _updateHandleSlider: function () {
            var height = this._canvasSlider.height,
                offsetHandle = this._handleSliderHeight * 0.25;

            var hueNorm = this._valueHue / this._valueHueMinMax[1];

            this._handleSlider.setPositionGlobalY((height - offsetHandle) * (1.0 - hueNorm));
        },

        _updateHandles: function () {
            this._updateHandleField();
            this._updateHandleSlider();
        },

        /*---------------------------------------------------------------------------------*/

        _setHue: function (value) {
            var minMax = this._valueHueMinMax;

            this._valueHue = value == minMax[1] ? minMax[0] : value;
            this._updateColorHSV();
            this._drawCanvasField();
        },

        _setSat: function (value) {
            this._valueSat = Math.round(value);
            this._updateColorHSV();
        },

        _setVal: function (value) {
            this._valueVal = Math.round(value);
            this._updateColorHSV();
        },

        _setR: function (value) {
            this._valueR = Math.round(value);
            this._updateColorRGB();
        },

        _setG: function (value) {
            this._valueG = Math.round(value);
            this._updateColorRGB();
        },

        _setB: function (value) {
            this._valueB = Math.round(value);
            this._updateColorRGB();
        },

        /*---------------------------------------------------------------------------------*/

        _onInputHueChange: function () {
            var input = this._inputHue,
                inputVal = this._getValueContrained(input, this._valueHueMinMax);

            var minMax = this._valueHueMinMax;

            if (inputVal == minMax[1]) {
                inputVal = minMax[0];
                input.setValue(inputVal);
            }

            this._setHue(inputVal);
            this._updateColorRGBFromHSV();
            this._updateColorHEXFromRGB();
            this._updateHandleSlider();

            this._drawCanvasField();
        },

        _onInputSatChange: function () {
            this._setSat(this._getValueContrained(this._inputSat, this._valueSatMinMax));
            this._onInputSVChange();
        },

        _onInputValChange: function () {
            this._setVal(this._getValueContrained(this._inputVal, this._valueValMinMax));
            this._onInputSVChange();
        },

        _onInputRChange: function () {
            this._setR(this._getValueContrained(this._inputR, this._valueRGBMinMax));
            this._onInputRGBChange();
        },

        _onInputGChange: function () {
            this._setG(this._getValueContrained(this._inputG, this._valueRGBMinMax));
            this._onInputRGBChange();
        },

        _onInputBChange: function () {
            this._setB(this._getValueContrained(this._inputB, this._valueRGBMinMax));
            this._onInputRGBChange();
        },

        _onInputHEXFinish: function () {
            var input = this._inputHEX,
                value = input.getProperty('value');

            if (!ColorUtil$$1.isValidHEX(value)) {
                input.setProperty('value', this._valueHEXValid);
                return;
            }

            this._valueHEX = this._valueHEXValid = value;
            this._updateColorFromHEX();
        },

        _onInputSVChange: function () {
            this._updateColorRGBFromHSV();
            this._updateColorHEXFromRGB();
            this._updateHandleField();
        },

        _onInputRGBChange: function () {
            this._updateColorHSVFromRGB();
            this._updateColorHEXFromRGB();
            this._updateHandles();
        },

        _getValueContrained: function (input, minMax) {
            var inputVal = Math.round(input.getValue()),
                min = minMax[0],
                max = minMax[1];

            if (inputVal <= min) {
                inputVal = min;
                input.setValue(inputVal);
            }
            if (inputVal >= max) {
                inputVal = max;
                input.setValue(inputVal);
            }

            return inputVal;
        },

        _updateInputHue: function () {
            this._inputHue.setValue(this._valueHue);
        },
        _updateInputSat: function () {
            this._inputSat.setValue(this._valueSat);
        },
        _updateInputVal: function () {
            this._inputVal.setValue(this._valueVal);
        },
        _updateInputR: function () {
            this._inputR.setValue(this._valueR);
        },
        _updateInputG: function () {
            this._inputG.setValue(this._valueG);
        },
        _updateInputB: function () {
            this._inputB.setValue(this._valueB);
        },
        _updateInputHEX: function () {
            this._inputHEX.setProperty('value', this._valueHEX);
        },

        _setColorHSV: function (hue, sat, val) {
            this._valueHue = hue;
            this._valueSat = sat;
            this._valueVal = val;

            this._updateInputHue();
            this._updateInputSat();
            this._updateInputVal();

            this._updateContrastCurrColor();
        },

        _setColorRGB: function (r, g, b) {
            this._valueR = r;
            this._valueG = g;
            this._valueB = b;

            this._updateInputR();
            this._updateInputG();
            this._updateInputB();

            this._updateContrastCurrColor();
        },

        _setColorHEX: function (hex) {
            this._valueHEX = hex;
            this._updateInputHEX();
        },

        _updateColorHSV: function () {
            this._setColorHSV(this._valueHue, this._valueSat, this._valueVal);
            this._updateContrastCurrColor();
        },

        _updateColorRGB: function () {
            this._setColorRGB(this._valueR, this._valueG, this._valueB);
            this._updateContrastCurrColor();
        },

        _updateColorHSVFromRGB: function () {
            var hsv = ColorUtil$$1.RGB2HSV(this._valueR, this._valueG, this._valueB);
            this._setColorHSV(hsv[0], hsv[1], hsv[2]);
        },

        _updateColorRGBFromHSV: function () {
            var rgb = ColorUtil$$1.HSV2RGB(this._valueHue, this._valueSat, this._valueVal);
            this._setColorRGB(rgb[0], rgb[1], rgb[2]);
        },

        _updateColorHEXFromRGB: function () {
            var hex = ColorUtil$$1.RGB2HEX(this._valueR, this._valueG, this._valueB);
            this._setColorHEX(hex);
        },

        _updateColorFromHEX: function () {
            var rgb = ColorUtil$$1.HEX2RGB(this._valueHEX);

            this._setColorRGB(rgb[0], rgb[1], rgb[2]);
            this._updateColorHSVFromRGB();
            this._updateHandles();
        },

        _updateContrastCurrColor: function () {
            this._setContrastCurrColor(this._valueR, this._valueG, this._valueB);
        },
        _updateContrastPrevColor: function () {
            this._setContrasPrevColor(this._valueR, this._valueG, this._valueB);
        },

        _setContrastCurrColor: function (r, g, b) {
            this._colorCurrNode.setStyleProperty('background', 'rgb(' + r + ',' + g + ',' + b + ')');
        },
        _setContrasPrevColor: function (r, g, b) {
            this._colorPrevNode.setStyleProperty('background', 'rgb(' + r + ',' + g + ',' + b + ')');
        },

        _onHeadDragStart: function () {
            var node = this._node,
                parentNode = this._parentNode;

            var nodePos = node.getPositionGlobal(),
                mousePos = Mouse$$1.get().getPosition(),
                offsetPos = this._mouseOffset;

            offsetPos[0] = mousePos[0] - nodePos[0];
            offsetPos[1] = mousePos[1] - nodePos[1];

            var eventMouseMove = DocumentEvent$$1.MOUSE_MOVE,
                eventMouseUp = DocumentEvent$$1.MOUSE_UP;

            var self = this;

            var onDrag = function () {
                self._updatePosition();
                self._updateCanvasNodePositions();
            },
                onDragEnd = function () {
                self._updateCanvasNodePositions();
                document.removeEventListener(eventMouseMove, onDrag, false);
                document.removeEventListener(eventMouseUp, onDragEnd, false);
            };

            parentNode.removeChild(node);
            parentNode.addChild(node);

            document.addEventListener(eventMouseMove, onDrag, false);
            document.addEventListener(eventMouseUp, onDragEnd, false);

            this._updateCanvasNodePositions();
        },

        _updatePosition: function () {
            var mousePos = Mouse$$1.get().getPosition(),
                offsetPos = this._mouseOffset;

            var currPositionX = mousePos[0] - offsetPos[0],
                currPositionY = mousePos[1] - offsetPos[1];

            var node = this._node,
                head = this._headNode,
                position = this._position;

            var maxX = window.innerWidth - node.getWidth(),
                maxY = window.innerHeight - head.getHeight();

            position[0] = Math.max(0, Math.min(currPositionX, maxX));
            position[1] = Math.max(0, Math.min(currPositionY, maxY));

            node.setPositionGlobal(position[0], position[1]);
        },

        _drawCanvasField: function () {
            var canvas = this._canvasField,
                context = this._contextCanvasField;

            var width = canvas.width,
                height = canvas.height,
                invWidth = 1 / width,
                invHeight = 1 / height;

            var imageData = this._imageDataField,
                rgb = [],
                index = 0;

            var valueHue = this._valueHue;

            var i = -1,
                j;
            while (++i < height) {
                j = -1;

                while (++j < width) {
                    rgb = ColorUtil$$1.HSV2RGB(valueHue, j * invWidth * 100.0, (1.0 - i * invHeight) * 100.0);
                    index = (i * width + j) * 4;

                    imageData.data[index] = rgb[0];
                    imageData.data[index + 1] = rgb[1];
                    imageData.data[index + 2] = rgb[2];
                    imageData.data[index + 3] = 255;
                }
            }

            context.putImageData(imageData, 0, 0);
        },

        _drawCanvasSlider: function () {
            var canvas = this._canvasSlider,
                context = this._contextCanvasSlider;

            var width = canvas.width,
                height = canvas.height,
                invHeight = 1 / height;

            var imageData = this._imageDataSlider,
                rgb = [],
                index = 0;

            var i = -1,
                j;
            while (++i < height) {
                j = -1;

                while (++j < width) {
                    rgb = ColorUtil$$1.HSV2RGB((1.0 - i * invHeight) * 360.0, 100.0, 100.0);
                    index = (i * width + j) * 4;

                    imageData.data[index] = rgb[0];
                    imageData.data[index + 1] = rgb[1];
                    imageData.data[index + 2] = rgb[2];
                    imageData.data[index + 3] = 255;
                }
            }

            context.putImageData(imageData, 0, 0);
        },

        _onCanvasFieldMouseDown: function () {
            var eventMouseMove = DocumentEvent$$1.MOUSE_MOVE,
                eventMouseUp = DocumentEvent$$1.MOUSE_UP;

            var self = this;

            var onDrag = function () {
                self._drawHandleField();
            },
                onDragEnd = function () {
                document.removeEventListener(eventMouseMove, onDrag, false);
                document.removeEventListener(eventMouseUp, onDragEnd, false);
            };

            document.addEventListener(eventMouseMove, onDrag, false);
            document.addEventListener(eventMouseUp, onDragEnd, false);

            self._drawHandleField();
        },

        _onCanvasSliderMouseDown: function () {
            var eventMouseMove = DocumentEvent$$1.MOUSE_MOVE,
                eventMouseUp = DocumentEvent$$1.MOUSE_UP;

            var self = this;

            var onDrag = function () {
                self._drawHandleSlider();
                self._drawCanvasField();
            },
                onDragEnd = function () {
                document.removeEventListener(eventMouseMove, onDrag, false);
                document.removeEventListener(eventMouseUp, onDragEnd, false);
                self._drawCanvasField();
            };

            document.addEventListener(eventMouseMove, onDrag, false);
            document.addEventListener(eventMouseUp, onDragEnd, false);

            self._drawHandleSlider();
            self._drawCanvasField();
        },

        _setSizeCanvasField: function (width, height) {
            var canvas = this._canvasField;
            canvas.style.width = width + 'px';
            canvas.style.height = height + 'px';
            canvas.width = width;
            canvas.height = height;
        },

        _setSizeCanvasSlider: function (width, height) {
            var canvas = this._canvasSlider;
            canvas.style.width = width + 'px';
            canvas.style.height = height + 'px';
            canvas.width = width;
            canvas.height = height;
        },

        open: function () {
            var node = this._node;

            this._parentNode.addChild(node);

            var position = this._position;
            if (position[0] === null || position[1] === null) {
                position[0] = window.innerWidth * 0.5 - node.getWidth() * 0.5;
                position[1] = window.innerHeight * 0.5 - node.getHeight() * 0.5;
            } else {
                position[0] = Math.max(0, Math.min(position[0], window.innerWidth - node.getWidth()));
                position[1] = Math.max(0, Math.min(position[1], window.innerHeight - node.getHeight()));
            }

            node.setPositionGlobal(position[0], position[1]);
            this._updateCanvasNodePositions();
        },

        close: function () {
            this._parentNode.removeChild(this._node);
        },

        _onClose: function (e) {
            e.cancelBubble = true;
            this.close();
        },
        _onPick: function () {
            this._callbackPick();
            this.close();
        },

        _updateCanvasNodePositions: function () {
            var canvasSliderPos = this._canvasSliderPos,
                canvasFieldPos = this._canvasFieldPos;

            canvasSliderPos[0] = canvasSliderPos[1] = 0;
            canvasFieldPos[0] = canvasFieldPos[1] = 0;

            var element = this._canvasSlider;

            while (element) {
                canvasSliderPos[0] += element.offsetLeft;
                canvasSliderPos[1] += element.offsetTop;
                element = element.offsetParent;
            }

            element = this._canvasField;

            while (element) {
                canvasFieldPos[0] += element.offsetLeft;
                canvasFieldPos[1] += element.offsetTop;
                element = element.offsetParent;
            }
        },

        setCallbackPick: function (func) {
            this._callbackPick = func;
        },

        setColorHEX: function (hex) {
            this._setColorHEX(hex);
            this._updateColorFromHEX();
            this._setColor();
        },

        setColorRGB: function (r, g, b) {
            this._setColorRGB(r, g, b);
            this._updateColorHEXFromRGB();
            this._updateColorHSVFromRGB();
            this._setColor();
        },

        setColorRGBfv: function (r, g, b) {
            this.setColorRGB(Math.floor(r * 255.0), Math.floor(g * 255.0), Math.floor(b * 255.0));
        },

        setColorHSV: function (h, s, v) {
            this._setColorHSV(h, s, v);
            this._updateColorRGBFromHSV();
            this._updateColorHEXFromRGB();
            this._setColor();
        },

        _setColor: function () {
            this._drawCanvasField();
            this._drawCanvasSlider();
            this._updateHandles();
            this._setContrasPrevColor(this._valueR, this._valueG, this._valueB);
        },

        getR: function () {
            return this._valueR;
        },
        getG: function () {
            return this._valueG;
        },
        getB: function () {
            return this._valueB;
        },
        getRGB: function () {
            return [this._valueR, this._valueG, this._valueB];
        },
        getHue: function () {
            return this._valueHue;
        },
        getSat: function () {
            return this._valueSat;
        },
        getVal: function () {
            return this._valueVal;
        },
        getHSV: function () {
            return [this._valueHue, this._valueSat, this._valueVal];
        },
        getHEX: function () {
            return this._valueHEX;
        },
        getRGBfv: function () {
            return [this._valueR / 255.0, this._valueG / 255.0, this._valueB / 255.0];
        },

        getNode: function () {
            return this._node;
        }
    };

    Picker.setup = function (parentNode) {
        return Picker._instance = new Picker(parentNode);
    };
    Picker.get = function () {
        return Picker._instance;
    };
    Picker.destroy = function () {
        Picker._instance = null;
    };

    module.exports = Picker;
});

var ColorFormatError = createCommonjsModule(function (module) {
	function ColorFormatError(msg) {
		Error.apply(this);
		Error.captureStackTrace(this, ColorFormatError);
		this.name = 'ColorFormatError';
		this.message = msg;
	}
	ColorFormatError.prototype = Object.create(Error.prototype);
	ColorFormatError.prototype.constructor = ColorFormatError;

	module.exports = ColorFormatError;
});

var Color = createCommonjsModule(function (module) {
    var ObjectComponent$$1 = ObjectComponent;

    var Node$$1 = Node;
    var ColorMode$$1 = ColorMode;
    var Picker$$1 = Picker;
    var ColorUtil$$1 = ColorUtil;
    var Options$$1 = Options;
    var ButtonPreset$$1 = ButtonPreset;
    var Metric$$1 = Metric,
        CSS$$1 = CSS;

    var Event_ = Event$1,
        NodeEvent$$1 = NodeEvent,
        ComponentEvent$$1 = ComponentEvent;

    var ColorFormatError$$1 = ColorFormatError;

    var DEFAULT_COLOR_MODE = ColorMode$$1.HEX,
        DEFAULT_PRESETS = null;

    var MSG_COLOR_FORMAT_HEX = 'Color format should be hex. Set colorMode to rgb, rgbfv or hsv.',
        MSG_COLOR_FORMAT_RGB_RGBFV_HSV = 'Color format should be rgb, rgbfv or hsv. Set colorMode to hex.',
        MSG_COLOR_PRESET_FORMAT_HEX = 'Preset color format should be hex.',
        MSG_COLOR_PRESET_FORMAT_RGB_RGBFV_HSV = 'Preset color format should be rgb, rgbfv or hsv.';

    function Color(parent, object, value, params) {
        ObjectComponent$$1.apply(this, arguments);

        params = params || {};
        params.presets = params.presets || DEFAULT_PRESETS;
        params.colorMode = params.colorMode || DEFAULT_COLOR_MODE;
        params.onChange = params.onChange || this._onChange;

        this._presetsKey = params.presets;
        this._onChange = params.onChange;

        var color = this._color = new Node$$1();
        value = this._value = this._obj[this._key];

        var colorMode = this._colorMode = params.colorMode;

        this._validateColorFormat(value, MSG_COLOR_FORMAT_HEX, MSG_COLOR_FORMAT_RGB_RGBFV_HSV);

        var wrap = this._wrapNode;

        if (!this._presetsKey) {
            color.setStyleClass(CSS$$1.Color);
            wrap.addChild(color);
        } else {
            color.setStyleClass(CSS$$1.Color);

            var wrap_ = new Node$$1();
            wrap_.setStyleClass(CSS$$1.WrapColorWPreset);

            wrap.addChild(wrap_);
            wrap_.addChild(color);

            var presets = this._obj[this._presetsKey];

            var i = -1;
            while (++i < presets.length) {
                this._validateColorFormat(presets[i], MSG_COLOR_PRESET_FORMAT_HEX, MSG_COLOR_PRESET_FORMAT_RGB_RGBFV_HSV);
            }

            var options = Options$$1.get(),
                presetBtn = new ButtonPreset$$1(wrap);

            var onPresetDeactivate = function () {
                options.clear();
                presetBtn.deactivate();
            };

            var self = this;
            var onPresetActivate = function () {
                options.build(presets, self._value, color, function () {
                    self.pushHistoryState();
                    self._value = presets[options.getSelectedIndex()];
                    self.applyValue();
                    self._onChange(self._obj[self._key]);
                }, onPresetDeactivate, Metric$$1.PADDING_PRESET, true, colorMode);
            };
            presetBtn.setOnActive(onPresetActivate);
            presetBtn.setOnDeactive(onPresetDeactivate);
        }

        color.addEventListener(NodeEvent$$1.MOUSE_DOWN, this._onColorTrigger.bind(this));
        this._updateColor();
    }
    Color.prototype = Object.create(ObjectComponent$$1.prototype);
    Color.prototype.constructor = Color;

    Color.prototype._onColorTrigger = function () {
        var colorMode = this._colorMode,
            colorModeHEX = ColorMode$$1.HEX,
            colorModeRGB = ColorMode$$1.RGB,
            colorModeRGBfv = ColorMode$$1.RGBfv,
            colorModeHSV = ColorMode$$1.HSV;

        var value = this._value,
            temp;

        var onPickerPick = function () {
            this.pushHistoryState();

            switch (colorMode) {
                case colorModeHEX:
                    this._value = Picker$$1.get().getHEX();
                    break;
                case colorModeRGB:
                    //if val = Float32array or so
                    temp = Picker$$1.get().getRGB();
                    value[0] = temp[0];
                    value[1] = temp[1];
                    value[2] = temp[2];
                    break;

                case colorModeRGBfv:
                    temp = Picker$$1.get().getRGBfv();
                    value[0] = temp[0];
                    value[1] = temp[1];
                    value[2] = temp[2];
                    break;

                case colorModeHSV:
                    this._value = Picker$$1.get().getHSV();
                    break;
            }

            this.applyValue();
        }.bind(this);

        var picker = Picker$$1.get();

        switch (colorMode) {
            case colorModeHEX:
                picker.setColorHEX(value);
                break;
            case colorModeRGB:
                picker.setColorRGB(value[0], value[1], value[2]);
                break;
            case colorModeRGBfv:
                picker.setColorRGBfv(value[0], value[1], value[2]);
                break;
            case colorModeHSV:
                picker.setColorHSV(value[0], value[1], value[2]);
                break;
        }

        picker.setCallbackPick(onPickerPick);
        picker.open();
    };

    Color.prototype.applyValue = function () {
        this._obj[this._key] = this._value;
        this._updateColor();
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.VALUE_UPDATED, null));
        this._onChange(this._obj[this._key]);
    };

    Color.prototype.onValueUpdate = function (e) {
        if (e.data.origin == this) return;
        this._value = this._obj[this._key];
        this._updateColor();
    };

    Color.prototype._updateColor = function () {
        var color = this._value,
            colorNode = this._color,
            nodeColor;

        colorNode.setProperty('innerHTML', color);

        switch (this._colorMode) {
            case ColorMode$$1.HEX:
                nodeColor = color;
                break;

            case ColorMode$$1.RGB:
                nodeColor = ColorUtil$$1.RGB2HEX(color[0], color[1], color[2]);
                break;

            case ColorMode$$1.RGBfv:
                nodeColor = ColorUtil$$1.RGBfv2HEX(color[0], color[1], color[2]);
                break;

            case ColorMode$$1.HSV:
                nodeColor = ColorUtil$$1.HSV2RGB(color[0], color[1], color[2]);
                break;
        }

        colorNode.getStyle().backgroundColor = nodeColor;
    };

    Color.prototype._validateColorFormat = function (value, msgHex, msgArr) {
        var colorMode = this._colorMode;

        if (colorMode == ColorMode$$1.HEX && Object.prototype.toString.call(value) === '[object Array]' || colorMode == ColorMode$$1.HEX && Object.prototype.toString.call(value) === '[object Float32Array]') {
            throw new ColorFormatError$$1(msgHex);
        }
        if ((colorMode == ColorMode$$1.RGB || colorMode == ColorMode$$1.RGBfv || colorMode == ColorMode$$1.HSV) && Object.prototype.toString.call(value) !== '[object Array]' || colorMode == ColorMode$$1.HSV && Object.prototype.toString.call(value) !== '[object Float32Array]') {
            throw new ColorFormatError$$1(msgArr);
        }
    };

    module.exports = Color;
});

var Button = createCommonjsModule(function (module) {
    var Event_ = Event$1,
        NodeEvent$$1 = NodeEvent,
        ComponentEvent$$1 = ComponentEvent;

    var Node$$1 = Node,
        Component$$1 = Component;

    var CSS$$1 = CSS;

    var DEFAULT_LABEL = '';

    function Button(parent, label, onPress, params) {
        onPress = onPress || function () {};
        params = params || {};
        params.label = params.label || DEFAULT_LABEL;

        Component$$1.apply(this, [parent, params.label]);

        var node = this._inputNode = new Node$$1(Node$$1.INPUT_BUTTON);

        node.setStyleClass(CSS$$1.Button);
        node.setProperty('value', label);

        var self = this;
        node.addEventListener(NodeEvent$$1.ON_CLICK, function () {
            onPress.bind(self)();
            self.dispatchEvent(new Event_(self, ComponentEvent$$1.VALUE_UPDATED));
        });

        this._wrapNode.addChild(node);
    }
    Button.prototype = Object.create(Component$$1.prototype);
    Button.prototype.constructor = Button;

    Button.prototype.getButtonLabel = function () {
        return this._inputNode.getProperty('value');
    };

    Button.prototype.setButtonLabel = function (label) {
        this._inputNode.setProperty('value', label);
    };

    module.exports = Button;
});

var Select = createCommonjsModule(function (module) {
    var ObjectComponent$$1 = ObjectComponent;
    var Node$$1 = Node;
    var CSS$$1 = CSS;

    var Options$$1 = Options;

    var History$$1 = History;

    var Event_ = Event$1,
        NodeEvent$$1 = NodeEvent,
        ComponentEvent$$1 = ComponentEvent,
        OptionEvent$$1 = OptionEvent;

    var ObjectComponentNotifier$$1 = ObjectComponentNotifier;

    var STR_CHOOSE = 'Choose ...';

    function Select(parent, object, value, params) {
        ObjectComponent$$1.apply(this, arguments);

        params = params || {};
        params.onChange = params.onChange || this._onChange;
        this._onChange = params.onChange;

        var obj = this._obj,
            key = this._key;

        var targetKey = this._targetKey = params.target,
            values = this._values = obj[key];

        this._selectedIndex = -1;
        this._selected = null;

        var select = this._select = new Node$$1(Node$$1.INPUT_BUTTON);
        select.setStyleClass(CSS$$1.Select);
        select.addEventListener(NodeEvent$$1.MOUSE_DOWN, this._onOptionTrigger.bind(this));

        if (this._hasTarget()) {
            var targetObj = obj[targetKey] || '';
            var i = -1;
            while (++i < values.length) {
                if (targetObj == values[i]) {
                    this._selected = values[i];
                }
            }
            select.setProperty('value', targetObj.toString().length > 0 ? targetObj : values[0]);
        } else {
            select.setProperty('value', params.selected ? values[params.selected] : STR_CHOOSE);
        }

        this._wrapNode.addChild(select);

        ObjectComponentNotifier$$1.get().addEventListener(OptionEvent$$1.TRIGGER, this, 'onOptionTrigger');
        this.addEventListener(OptionEvent$$1.TRIGGERED, ObjectComponentNotifier$$1.get(), 'onOptionTriggered');
    }
    Select.prototype = Object.create(ObjectComponent$$1.prototype);
    Select.prototype.constructor = Select;

    Select.prototype.onOptionTrigger = function (e) {
        if (e.data.origin == this) {
            this._active = !this._active;
            this._updateAppearance();

            if (this._active) {
                this._buildOptions();
            } else {
                Options$$1.get().clear();
            }
            return;
        }
        this._active = false;
        this._updateAppearance();
    };

    Select.prototype._buildOptions = function () {
        var options = Options$$1.get();
        var self = this;

        options.build(this._values, this._selected, this._select, function () {
            self.applyValue();
            self._active = false;
            self._updateAppearance();
            self._selectedIndex = options.getSelectedIndex();
            self._onChange(self._selectedIndex);
            options.clear();
        }, function () {
            self._active = false;
            self._updateAppearance();
            options.clear();
        }, false);
    };

    Select.prototype._applySelected = function (selected) {
        this._select.setProperty('value', selected);
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.VALUE_UPDATED), null);
    };

    Select.prototype.applyValue = function () {
        var index = Options$$1.get().getSelectedIndex(),
            selected = this._selected = this._values[index];

        if (this._hasTarget()) {
            this.pushHistoryState();
            this._obj[this._targetKey] = selected;
        }

        this._applySelected(selected);
    };

    Select.prototype.pushHistoryState = function () {
        var obj = this._obj,
            key = this._targetKey;
        History$$1.get().pushState(obj, key, obj[key]);
    };

    Select.prototype._onOptionTrigger = function () {
        this.dispatchEvent(new Event_(this, OptionEvent$$1.TRIGGERED, null));
    };

    Select.prototype._updateAppearance = function () {
        this._select.setStyleClass(this._active ? CSS$$1.SelectActive : CSS$$1.Select);
    };

    Select.prototype.onValueUpdate = function (e) {
        if (!this._hasTarget()) {
            return;
        }
        this._selected = this._obj[this._targetKey];
        this._select.setProperty('value', this._selected.toString());
    };

    Select.prototype._hasTarget = function () {
        return this._targetKey != null;
    };

    Select.prototype.setValue = function (value) {
        this._selectedIndex = value;
        if (value == -1) {
            this._selected = null;
            this._select.setProperty('value', STR_CHOOSE);
            return;
        }
        this._selected = this._values[this._selectedIndex];
        this._applySelected(this._selected);
    };

    Select.prototype.getData = function () {
        var obj = {};
        obj['selectedIndex'] = this._selectedIndex;
        return obj;
    };

    module.exports = Select;
});

var Slider_Internal = createCommonjsModule(function (module) {
    var Node$$1 = Node;

    var DocumentEvent$$1 = DocumentEvent,
        NodeEvent$$1 = NodeEvent;

    var CSS$$1 = CSS;
    var Mouse$$1 = Mouse;

    function Slider_Internal(parentNode, onBegin, onChange, onFinish) {
        this._bounds = [0, 1];
        this._value = 0;
        this._intrpl = 0;
        this._focus = false;

        this._onBegin = onBegin || function () {};
        this._onChange = onChange || function () {};
        this._onFinish = onFinish || function () {};

        var wrap = new Node$$1().setStyleClass(CSS$$1.SliderWrap);
        parentNode.addChild(wrap);

        var slot = this._slot = { node: new Node$$1().setStyleClass(CSS$$1.SliderSlot),
            offsetX: 0,
            width: 0,
            padding: 3 };

        var handle = this._handle = { node: new Node$$1().setStyleClass(CSS$$1.SliderHandle),
            width: 0,
            dragging: false };

        wrap.addChild(slot.node);
        slot.node.addChild(handle.node);

        slot.offsetX = slot.node.getPositionGlobalX();
        slot.width = Math.floor(slot.node.getWidth() - slot.padding * 2);

        handle.node.setWidth(handle.width);

        slot.node.addEventListener(NodeEvent$$1.MOUSE_DOWN, this._onSlotMouseDown.bind(this));
        slot.node.addEventListener(NodeEvent$$1.MOUSE_UP, this._onSlotMouseUp.bind(this));

        document.addEventListener(DocumentEvent$$1.MOUSE_MOVE, this._onDocumentMouseMove.bind(this));
        document.addEventListener(DocumentEvent$$1.MOUSE_UP, this._onDocumentMouseUp.bind(this));
    }

    Slider_Internal.prototype._onDocumentMouseMove = function () {
        if (!this._handle.dragging) {
            return;
        }
        this._update();
        this._onChange();
    };

    Slider_Internal.prototype._onDocumentMouseUp = function () {
        if (this._handle.dragging) {
            this._onFinish();
        }
        this._handle.dragging = false;
    };

    Slider_Internal.prototype._onSlotMouseDown = function () {
        this._onBegin();
        this._focus = true;
        this._handle.dragging = true;
        this._handle.node.getElement().focus();
        this._update();
    };

    Slider_Internal.prototype._onSlotMouseUp = function () {
        if (this._focus) {
            var handle = this._handle;
            if (handle.dragging) {
                this._onFinish();
            }
            handle.dragging = false;
        }
        this._focus = false;
    };

    Slider_Internal.prototype._update = function () {
        var mx = Mouse$$1.get().getX(),
            sx = this._slot.offsetX,
            sw = this._slot.width,
            px = mx < sx ? 0 : mx > sx + sw ? sw : mx - sx;

        this._handle.node.setWidth(Math.round(px));
        this._intrpl = px / sw;
        this._interpolateValue();
    };

    Slider_Internal.prototype._updateHandle = function () {
        var slotWidth = this._slot.width,
            handleWidth = Math.round(this._intrpl * slotWidth);
        this._handle.node.setWidth(Math.min(handleWidth, slotWidth));
    };

    Slider_Internal.prototype._interpolateValue = function () {
        var intrpl = this._intrpl,
            bounds = this._bounds;
        this._value = bounds[0] * (1.0 - intrpl) + bounds[1] * intrpl;
    };

    Slider_Internal.prototype.resetOffset = function () {
        var slot = this._slot;
        slot.offsetX = slot.node.getPositionGlobalX();
        slot.width = Math.floor(slot.node.getWidth() - slot.padding * 2);
    };

    Slider_Internal.prototype.setBoundMin = function (value) {
        var bounds = this._bounds;
        if (value >= bounds[1]) {
            return;
        }
        bounds[0] = value;
        this._updateFromBounds();
    };

    Slider_Internal.prototype.setBoundMax = function (value) {
        var bounds = this._bounds;
        if (value <= bounds[0]) {
            return;
        }
        bounds[1] = value;
        this._updateFromBounds();
    };

    Slider_Internal.prototype._updateFromBounds = function () {
        var boundsMin = this._bounds[0],
            boundsMax = this._bounds[1];
        this._value = Math.max(boundsMin, Math.min(this._value, boundsMax));
        this._intrpl = Math.abs((this._value - boundsMin) / (boundsMin - boundsMax));
        this._updateHandle();
    };

    Slider_Internal.prototype.setValue = function (value) {
        var boundsMin = this._bounds[0],
            boundsMax = this._bounds[1];

        if (value < boundsMin || value > boundsMax) {
            return;
        }
        this._intrpl = Math.abs((value - boundsMin) / (boundsMin - boundsMax));
        this._updateHandle();
        this._value = value;
    };

    Slider_Internal.prototype.getValue = function () {
        return this._value;
    };

    module.exports = Slider_Internal;
});

var Slider = createCommonjsModule(function (module) {
    var ObjectComponent$$1 = ObjectComponent;
    var CSS$$1 = CSS;
    var Slider_Internal$$1 = Slider_Internal;

    var History$$1 = History;
    var Range$$1 = Range;
    var NumberInput_Internal = require$$5$4;

    var Event_ = Event$1,
        DocumentEvent$$1 = DocumentEvent,
        PanelEvent$$1 = PanelEvent,
        GroupEvent$$1 = GroupEvent,
        ComponentEvent$$1 = ComponentEvent;

    var DEFAULT_STEP = 1.0,
        DEFAULT_DP = 2;

    function Slider(parent, object, value, range, params) {
        params = params || {};
        params.label = params.label || value;

        ObjectComponent$$1.apply(this, [parent, object, range, params]);

        this._values = this._obj[this._key];
        this._targetKey = value;

        params.step = params.step || DEFAULT_STEP;
        params.dp = params.dp === undefined || params.dp == null ? DEFAULT_DP : params.dp;
        params.onChange = params.onChange || this._onChange;
        params.onFinish = params.onFinish || function () {};

        this._dp = params.dp;
        this._onChange = params.onChange;
        this._onFinish = params.onFinish;

        var values = this._values,
            obj = this._obj,
            targetKey = this._targetKey;

        var wrap = this._wrapNode;
        wrap.setStyleClass(CSS$$1.WrapSlider);

        var slider = this._slider = new Slider_Internal$$1(wrap, this._onSliderBegin.bind(this), this._onSliderMove.bind(this), this._onSliderEnd.bind(this));

        slider.setBoundMax(values[1]);
        slider.setBoundMin(values[0]);
        slider.setValue(obj[targetKey]);

        var input = this._input = new NumberInput_Internal(params.step, params.dp, null, this._onInputChange.bind(this));

        input.setValue(obj[targetKey]);

        wrap.addChild(input.getNode());

        this._parent.addEventListener(PanelEvent$$1.PANEL_MOVE_END, this, 'onPanelMoveEnd');
        this._parent.addEventListener(GroupEvent$$1.GROUP_SIZE_CHANGE, this, 'onGroupWidthChange');
        this._parent.addEventListener(DocumentEvent$$1.WINDOW_RESIZE, this, 'onWindowResize');
    }
    Slider.prototype = Object.create(ObjectComponent$$1.prototype);
    Slider.prototype.constructor = Slider;

    Slider.prototype.pushHistoryState = function () {
        var obj = this._obj,
            key = this._targetKey;
        History$$1.get().pushState(obj, key, obj[key]);
    };

    Slider.prototype._onSliderBegin = function () {
        this.pushHistoryState();
    };

    Slider.prototype._onSliderMove = function () {
        this.applyValue();
        this._updateValueField();
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.VALUE_UPDATED, null));
        this._onChange();
    };

    Slider.prototype._onSliderEnd = function () {
        this.applyValue();
        this._updateValueField();
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.VALUE_UPDATED, null));
        this._onFinish();
    };

    Slider.prototype._onInputChange = function () {
        var input = this._input,
            valueMin = this._values[0],
            valueMax = this._values[1];

        if (input.getValue() >= valueMax) {
            input.setValue(valueMax);
        }
        if (input.getValue() <= valueMin) {
            input.setValue(valueMin);
        }

        var value = input.getValue();

        this._slider.setValue(value);
        this._obj[this._targetKey] = value;
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.VALUE_UPDATED, null));
        this._onFinish();
    };

    Slider.prototype.applyValue = function () {
        var value = this._slider.getValue();
        this._obj[this._targetKey] = parseFloat(value.toFixed(this._dp));
        this._input.setValue(value);
    };

    Slider.prototype.onValueUpdate = function (e) {
        var origin = e.data.origin;
        if (origin == this) {
            return;
        }
        var slider = this._slider;
        if (!(origin instanceof Slider)) {
            var values = this._values;
            slider.setBoundMin(values[0]);
            slider.setBoundMax(values[1]);
            if (!(origin instanceof Range$$1)) {
                slider.setValue(this._obj[this._targetKey]);
            }
        } else {
            slider.setValue(this._obj[this._targetKey]);
        }
        this.applyValue();
    };

    Slider.prototype._updateValueField = function () {
        this._input.setValue(this._slider.getValue());
    };

    Slider.prototype.onPanelMoveEnd = Slider.prototype.onGroupWidthChange = Slider.prototype.onWindowResize = function () {
        this._slider.resetOffset();
    };

    Slider.prototype.setValue = function (value) {
        if (value == -1) {
            return;
        }
        this._obj[this._targetKey] = value;
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.VALUE_UPDATED, null));
    };

    Slider.prototype.getData = function () {
        var obj = {};
        obj[this._targetKey] = this._obj[this._targetKey];
        return obj;
    };

    module.exports = Slider;
});

var Pad = createCommonjsModule(function (module) {
    var Plotter$$1 = Plotter;
    var Mouse$$1 = Mouse;

    var Event_ = Event$1,
        DocumentEvent$$1 = DocumentEvent,
        ComponentEvent$$1 = ComponentEvent;

    var DEFAULT_BOUNDS_X = [-1, 1],
        DEFAULT_BOUNDS_Y = [-1, 1],
        DEFAULT_LABEL_X = '',
        DEFAULT_LABEL_Y = '';

    function Pad(parent, object, value, params) {
        Plotter$$1.apply(this, arguments);

        params = params || {};
        params.boundsX = params.boundsX || DEFAULT_BOUNDS_X;
        params.boundsY = params.boundsY || DEFAULT_BOUNDS_Y;
        params.labelX = params.labelX || DEFAULT_LABEL_X;
        params.labelY = params.labelY || DEFAULT_LABEL_Y;

        params.showCross = params.showCross || true;

        this._onChange = params.onChange || this._onChange;
        this._onFinish = params.onFinish || function () {};

        this._boundsX = params.boundsX;
        this._boundsY = params.boundsY;
        this._labelAxisX = params.labelX != '' && params.labelX != 'none' ? params.labelX : null;
        this._labelAxisY = params.labelY != '' && params.labelY != 'none' ? params.labelY : null;

        var path = this._path;
        path.style.strokeWidth = 1;
        path.style.stroke = '#363c40';

        this._grid.style.stroke = 'rgb(25,25,25)';

        this._svgPos = [0, 0];

        var handle = this._handle = this._svgRoot.appendChild(this._createSVGObject('g'));
        var handleCircle0 = handle.appendChild(this._createSVGObject('circle'));
        handleCircle0.setAttribute('r', String(11));
        handleCircle0.setAttribute('fill', 'rgba(0,0,0,0.05)');
        var handleCircle1 = handle.appendChild(this._createSVGObject('circle'));
        handleCircle1.setAttribute('r', String(10));
        handleCircle1.setAttribute('fill', 'rgb(83,93,98)');

        var handleCircle2 = handle.appendChild(this._createSVGObject('circle'));
        handleCircle2.setAttribute('r', String(9));
        handleCircle2.setAttribute('fill', 'rgb(57,69,76)');
        handleCircle2.setAttribute('cy', String(0.75));

        var handleCircle3 = handle.appendChild(this._createSVGObject('circle'));
        handleCircle3.setAttribute('r', String(10));
        handleCircle3.setAttribute('stroke', 'rgb(17,19,20)');
        handleCircle3.setAttribute('stroke-width', String(1));
        handleCircle3.setAttribute('fill', 'none');

        var handleCircle4 = handle.appendChild(this._createSVGObject('circle'));
        handleCircle4.setAttribute('r', String(6));
        handleCircle4.setAttribute('fill', 'rgb(30,34,36)');
        var handleCircle5 = handle.appendChild(this._createSVGObject('circle'));
        handleCircle5.setAttribute('r', String(3));
        handleCircle5.setAttribute('fill', 'rgb(255,255,255)');

        handle.setAttribute('tranform', 'translate(0 0)');

        this._svg.addEventListener(DocumentEvent$$1.MOUSE_DOWN, this._onDragStart.bind(this), false);
        this._drawValue(this._obj[this._key]);
    }
    Pad.prototype = Object.create(Plotter$$1.prototype);
    Pad.prototype.constructor = Pad;

    Pad.prototype._onDragStart = function () {
        var svgPos = this._svgPos;
        svgPos[0] = 0;
        svgPos[1] = 0;

        //skip to container
        var element = this._svg.parentNode;

        while (element) {
            svgPos[0] += element.offsetLeft;
            svgPos[1] += element.offsetTop;
            element = element.offsetParent;
        }

        var eventMove = DocumentEvent$$1.MOUSE_MOVE,
            eventUp = DocumentEvent$$1.MOUSE_UP;

        var onDrag = function () {
            this._drawValueInput();
            this.applyValue();
            this._onChange();
        }.bind(this);

        var onDragEnd = function () {
            this.pushHistoryState();
            this._drawValueInput();
            this.applyValue();
            this._onFinish();

            document.removeEventListener(eventMove, onDrag, false);
            document.removeEventListener(eventUp, onDragEnd, false);
        }.bind(this);

        document.addEventListener(eventMove, onDrag, false);
        document.addEventListener(eventUp, onDragEnd, false);

        this._drawValueInput();
        this.applyValue();
        this._onChange();
    };

    Pad.prototype._redraw = function () {
        this._drawValue(this._obj[this._key]);
    };

    Pad.prototype._drawValueInput = function () {
        this._drawValue(this._getMouseNormalized());
    };

    Pad.prototype._drawValue = function (value) {
        this._obj[this._key] = value;
        this._drawGrid();
        this._drawPoint();
    };

    Pad.prototype._drawGrid = function () {
        var svgSize = Number(this._svg.getAttribute('width')),
            svgMidX = Math.floor(svgSize * 0.5),
            svgMidY = Math.floor(svgSize * 0.5);

        var pathCmd = '';
        pathCmd += this._pathCmdLine(0, svgMidY, svgSize, svgMidY);
        pathCmd += this._pathCmdLine(svgMidX, 0, svgMidX, svgSize);

        this._grid.setAttribute('d', pathCmd);
    };

    Pad.prototype._drawPoint = function () {
        var svgSize = Number(this._svg.getAttribute('width'));

        var value = this._obj[this._key];

        var localX = (0.5 + value[0] * 0.5) * svgSize,
            localY = (0.5 + -value[1] * 0.5) * svgSize;

        var pathCmd = '';
        pathCmd += this._pathCmdLine(0, localY, svgSize, localY);
        pathCmd += this._pathCmdLine(localX, 0, localX, svgSize);

        this._path.setAttribute('d', pathCmd);
        this._handle.setAttribute('transform', 'translate(' + localX + ' ' + localY + ')');
    };

    Pad.prototype._getMouseNormalized = function () {
        var offset = this._svgPos,
            mouse = Mouse$$1.get().getPosition(),
            svgSize = Number(this._svg.getAttribute('width'));

        return [-1 + Math.max(0, Math.min(mouse[0] - offset[0], svgSize)) / svgSize * 2, 1 - Math.max(0, Math.min(mouse[1] - offset[1], svgSize)) / svgSize * 2];
    };

    Pad.prototype.applyValue = function () {
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.VALUE_UPDATED, null));
    };

    Pad.prototype.onValueUpdate = function (e) {
        if (e.data.origin == this) return;
        this._drawValue(this._obj[this._key]);
    };

    module.exports = Pad;
});

var Output = createCommonjsModule(function (module) {
    var ObjectComponent$$1 = ObjectComponent;
    var Node$$1 = Node;

    var CSS$$1 = CSS;
    var Metric$$1 = Metric;
    var ScrollBar$$1 = ScrollBar;

    var Event_ = Event$1,
        DocumentEvent$$1 = DocumentEvent,
        NodeEvent$$1 = NodeEvent,
        ComponentEvent$$1 = ComponentEvent;

    var DEFAULT_HEIGHT = null,
        DEFAULT_WRAP = false,
        DEFAULT_UPDATE = true;

    function Output(parent, object, value, params) {
        ObjectComponent$$1.apply(this, arguments);

        params = params || {};
        params.height = params.height || DEFAULT_HEIGHT;
        params.wrap = params.wrap === undefined ? DEFAULT_WRAP : params.wrap;
        params.update = params.update === undefined ? DEFAULT_UPDATE : params.update;

        this._wrap = params.wrap;
        this._update = params.update;

        var textArea = this._textArea = new Node$$1(Node$$1.TEXTAREA),
            wrap = this._wrapNode,
            root = this._node;

        textArea.setProperty('readOnly', true);
        wrap.addChild(textArea);

        textArea.addEventListener(NodeEvent$$1.MOUSE_DOWN, this._onInputDragStart.bind(this));
        this.addEventListener(ComponentEvent$$1.INPUT_SELECT_DRAG, this._parent, 'onComponentSelectDrag');

        if (params.height) {
            var textAreaWrap = new Node$$1();
            textAreaWrap.setStyleClass(CSS$$1.TextAreaWrap);
            textAreaWrap.addChild(textArea);
            wrap.addChild(textAreaWrap);

            //FIXME
            var height = this._height = params.height,
                padding = 4;

            textArea.setHeight(Math.max(height + padding, Metric$$1.COMPONENT_MIN_HEIGHT));
            wrap.setHeight(textArea.getHeight());
            root.setHeight(wrap.getHeight() + padding);

            this._scrollBar = new ScrollBar$$1(textAreaWrap, textArea, height - padding);
        }

        if (params.wrap) {
            textArea.setStyleProperty('white-space', 'pre-wrap');
        }

        this._prevString = '';
        this._prevScrollHeight = -1;
        this._setValue();
    }
    Output.prototype = Object.create(ObjectComponent$$1.prototype);
    Output.prototype.constructor = Output;

    //Override in subclass
    Output.prototype._setValue = function () {};

    Output.prototype.onValueUpdate = function () {
        this._setValue();
    };

    Output.prototype.update = function () {
        if (!this._update) {
            return;
        }
        this._setValue();
    };

    //Prevent chrome select drag

    Output.prototype._onDrag = function () {
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.INPUT_SELECT_DRAG, null));
    };

    Output.prototype._onDragFinish = function () {
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.INPUT_SELECT_DRAG, null));

        document.removeEventListener(DocumentEvent$$1.MOUSE_MOVE, this._onDrag, false);
        document.removeEventListener(DocumentEvent$$1.MOUSE_MOVE, this._onDragFinish, false);
    };

    Output.prototype._onInputDragStart = function () {
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.INPUT_SELECT_DRAG, null));
        document.addEventListener(DocumentEvent$$1.MOUSE_MOVE, this._onDrag.bind(this), false);
        document.addEventListener(DocumentEvent$$1.MOUSE_UP, this._onDragFinish.bind(this), false);
    };

    module.exports = Output;
});

var NumberOutput = createCommonjsModule(function (module) {
	var Output$$1 = Output;

	var DEFAULT_OUTPUT_DP = 2;

	function NumberOutput(parent, object, value, params) {
		params = params || {};
		params.dp = params.dp || DEFAULT_OUTPUT_DP;

		Output$$1.apply(this, arguments);
		this._valueDp = params.dp + 1;
	}
	NumberOutput.prototype = Object.create(Output$$1.prototype);
	NumberOutput.prototype.constructor = NumberOutput;

	//FIXME
	NumberOutput.prototype._setValue = function () {
		if (this._parent.isDisabled()) {
			return;
		}

		var value = this._obj[this._key],
		    textArea = this._textArea,
		    dp = this._valueDp;

		var index, out;

		if (typeof value === 'object' && typeof value.length === 'number' && typeof value.splice === 'function' && !value.propertyIsEnumerable('length')) {

			out = value.slice();

			var i = -1;
			var temp;
			var wrap = this._wrap;

			while (++i < out.length) {
				temp = out[i] = out[i].toString();
				index = temp.indexOf('.');
				if (index > 0) {
					out[i] = temp.slice(0, index + dp);
				}
			}

			if (wrap) {
				textArea.setStyleProperty('white-space', 'nowrap');
				out = out.join('\n');
			}

			textArea.setProperty('value', out);
		} else {
			out = value.toString();
			index = out.indexOf('.');
			textArea.setProperty('value', index > 0 ? out.slice(0, index + dp) : out);
		}
	};

	module.exports = NumberOutput;
});

var StringOutput = createCommonjsModule(function (module) {
    var Output$$1 = Output;

    StringOutput = function (parent, object, value, params) {
        Output$$1.apply(this, arguments);
    };
    StringOutput.prototype = Object.create(Output$$1.prototype);
    StringOutput.prototype.constructor = StringOutput;

    StringOutput.prototype._setValue = function () {
        if (this._parent.isDisabled()) {
            return;
        }
        var textAreaString = this._obj[this._key];

        if (textAreaString == this._prevString) {
            return;
        }
        var textArea = this._textArea,
            textAreaElement = textArea.getElement(),
            textAreaScrollHeight;

        textArea.setProperty('value', textAreaString);
        textAreaScrollHeight = textAreaElement.scrollHeight;
        textArea.setHeight(textAreaScrollHeight);

        var scrollBar = this._scrollBar;

        if (scrollBar) {
            if (textAreaScrollHeight <= this._wrapNode.getHeight()) {
                scrollBar.disable();
            } else {
                scrollBar.enable();
                scrollBar.update();
                scrollBar.reset();
            }
        }
        this._prevString = textAreaString;
    };

    module.exports = StringOutput;
});

var Canvas = createCommonjsModule(function (module) {
    var Component$$1 = Component;
    var CSS$$1 = CSS,
        Metric$$1 = Metric;

    var Event_ = Event$1,
        GroupEvent$$1 = GroupEvent;

    function Canvas(parent, params) {
        Component$$1.apply(this, arguments);

        var wrap = this._wrapNode;
        wrap.setStyleClass(CSS$$1.CanvasWrap);
        var canvas = this._canvas = document.createElement('canvas');
        wrap.getElement().appendChild(canvas);

        var width = wrap.getWidth();
        this._canvasWidth = this._canvasHeight = 0;
        this._setCanvasSize(width, width);
        this._updateHeight();

        this._node.setStyleClass(CSS$$1.CanvasListItem);
        this._parent.addEventListener(GroupEvent$$1.GROUP_SIZE_CHANGE, this, 'onGroupSizeChange');
        this.addEventListener(GroupEvent$$1.GROUP_SIZE_UPDATE, this._parent, 'onGroupSizeUpdate');
    }
    Canvas.prototype = Object.create(Component$$1.prototype);
    Canvas.prototype.constructor = Canvas;

    Canvas.prototype._updateHeight = function () {
        var canvasHeight = this._canvas.height;

        this._wrapNode.setHeight(canvasHeight);
        this._node.setHeight(canvasHeight + Metric$$1.PADDING_WRAPPER);
    };

    Canvas.prototype.onGroupSizeChange = function () {
        var width = this._wrapNode.getWidth();

        this._setCanvasSize(width, width);
        this._updateHeight();
        this._redraw();

        this.dispatchEvent(new Event_(this, GroupEvent$$1.GROUP_SIZE_UPDATE, null));
    };

    Canvas.prototype._setCanvasSize = function (width, height) {
        var canvasWidth = this._canvasWidth = width,
            canvasHeight = this._canvasHeight = height;

        var canvas = this._canvas;
        canvas.style.width = canvasWidth + 'px';
        canvas.style.height = canvasHeight + 'px';
        canvas.width = canvasWidth;
        canvas.height = canvasHeight;
    };

    Canvas.prototype.getCanvas = function () {
        return this._canvas;
    };

    Canvas.prototype.getContext = function () {
        return this._canvas.getContext('2d');
    };

    module.exports = Canvas;
});

var SVG = createCommonjsModule(function (module) {
    var Component$$1 = Component;
    var CSS$$1 = CSS;
    var Metric$$1 = Metric;
    var GroupEvent$$1 = GroupEvent;

    function SVG(parent, params) {
        Component$$1.apply(this, arguments);

        var wrap = this._wrapNode;
        wrap.setStyleClass(CSS$$1.CanvasWrap);
        var wrapSize = wrap.getWidth();

        var svg = this._svg = this._createSVGObject('svg');
        svg.setAttribute('version', '1.2');
        svg.setAttribute('baseProfile', 'tiny');
        svg.setAttribute('preserveAspectRatio', 'true');

        wrap.getElement().appendChild(svg);

        this._svgSetSize(wrapSize, wrapSize);
        this._updateHeight();

        this._node.setStyleClass(CSS$$1.CanvasListItem);

        this._parent.addEventListener(GroupEvent$$1.GROUP_SIZE_CHANGE, this, 'onGroupSizeChange');
        this.addEventListener(GroupEvent$$1.GROUP_SIZE_UPDATE, this._parent, 'onGroupSizeUpdate');
    }
    SVG.prototype = Object.create(Component$$1.prototype);
    SVG.prototype.constructor = SVG;

    SVG.prototype._updateHeight = function () {
        var svgHeight = Number(this._svg.getAttribute('height'));
        this._wrapNode.setHeight(svgHeight);
        this._node.setHeight(svgHeight + Metric$$1.PADDING_WRAPPER);
    };

    SVG.prototype.onGroupSizeChange = function () {
        var width = this._wrapNode.getWidth();
        this._svgSetSize(width, width);
        this._updateHeight();
    };

    SVG.prototype._svgSetSize = function (width, height) {
        var svg = this._svg;
        svg.setAttribute('width', width);
        svg.setAttribute('height', height);
        svg.setAttribute('viewbox', '0 0 ' + width + ' ' + height);
    };

    SVG.prototype.getSVG = function () {
        return this._svg;
    };

    module.exports = SVG;
});

var require$$2$12 = StringOutput;

var Panel = createCommonjsModule(function (module) {
    var Node$$1 = Node,
        Group$$1 = Group,
        ScrollBar$$1 = ScrollBar;

    var CSS$$1 = CSS;
    var LayoutMode$$1 = LayoutMode;
    var History$$1 = History;

    var EventDispatcher$$1 = EventDispatcher,
        Event_ = Event$1,
        DocumentEvent$$1 = DocumentEvent,
        NodeEvent$$1 = NodeEvent,
        PanelEvent$$1 = PanelEvent,
        MenuEvent$$1 = MenuEvent;

    var Mouse$$1 = Mouse;

    var StringInput$$1 = StringInput,
        NumberInput$$1 = NumberInput,
        Range$$1 = Range,
        Checkbox$$1 = Checkbox,
        Color$$1 = Color,
        Button$$1 = Button,
        Select$$1 = Select,
        Slider$$1 = Slider,
        FunctionPlotter$$1 = FunctionPlotter,
        Pad$$1 = Pad,
        ValuePlotter$$1 = ValuePlotter,
        NumberOutput$$1 = NumberOutput,
        StringOutput = require$$2$12,
        Canvas_ = Canvas,
        SVG_ = SVG;

    var DEFAULT_PANEL_POSITION = null,
        DEFAULT_PANEL_WIDTH = 200,
        DEFAULT_PANEL_HEIGHT = null,
        DEFAULT_PANEL_WIDTH_MIN = 100,
        DEFAULT_PANEL_WIDTH_MAX = 600,
        DEFAULT_PANEL_RATIO = 40,
        DEFAULT_PANEL_LABEL = 'Control Panel',
        DEFAULT_PANEL_VALIGN = LayoutMode$$1.TOP,
        DEFAULT_PANEL_ALIGN = LayoutMode$$1.RIGHT,
        DEFAULT_PANEL_DOCK = { align: LayoutMode$$1.RIGHT, resizable: true },
        DEFAULT_PANEL_ENABLE = true,
        DEFAULT_PANEL_OPACITY = 1.0,
        DEFAULT_PANEL_FIXED = true,
        DEFAULT_PANEL_VCONSTRAIN = true;

    function Panel(controlKit, params) {
        EventDispatcher$$1.apply(this, arguments);
        this._parent = controlKit;

        params = params || {};
        params.valign = params.valign || DEFAULT_PANEL_VALIGN;
        params.align = params.align || DEFAULT_PANEL_ALIGN;
        params.position = params.position || DEFAULT_PANEL_POSITION;
        params.width = params.width || DEFAULT_PANEL_WIDTH;
        params.height = params.height || DEFAULT_PANEL_HEIGHT;
        params.ratio = params.ratio || DEFAULT_PANEL_RATIO;
        params.label = params.label || DEFAULT_PANEL_LABEL;
        params.opacity = params.opacity || DEFAULT_PANEL_OPACITY;
        params.fixed = params.fixed === undefined ? DEFAULT_PANEL_FIXED : params.fixed;
        params.enable = params.enable === undefined ? DEFAULT_PANEL_ENABLE : params.enable;
        params.vconstrain = params.vconstrain === undefined ? DEFAULT_PANEL_VCONSTRAIN : params.vconstrain;

        if (params.dock) {
            params.dock.align = params.dock.align || DEFAULT_PANEL_DOCK.align;
            params.dock.resizable = params.dock.resizable || DEFAULT_PANEL_DOCK.resizable;
        }

        this._width = Math.max(DEFAULT_PANEL_WIDTH_MIN, Math.min(params.width, DEFAULT_PANEL_WIDTH_MAX));
        this._height = params.height ? Math.max(0, Math.min(params.height, window.innerHeight)) : null;
        this._fixed = params.fixed;
        this._dock = params.dock;
        this._position = params.position;
        this._vConstrain = params.vconstrain;
        this._label = params.label;
        this._enabled = params.enable;
        this._groups = [];

        var width = this._width,
            isFixed = this._fixed,
            dock = this._dock,
            position = this._position,
            label = this._label,
            align = params.align,
            opacity = params.opacity;

        var root = this._node = new Node$$1().setStyleClass(CSS$$1.Panel),
            head = this._headNode = new Node$$1().setStyleClass(CSS$$1.Head),
            menu = new Node$$1().setStyleClass(CSS$$1.Menu),
            labelWrap = new Node$$1().setStyleClass(CSS$$1.Wrap),
            label_ = new Node$$1(Node$$1.SPAN).setStyleClass(CSS$$1.Label),
            wrap = this._wrapNode = new Node$$1(Node$$1.DIV).setStyleClass(CSS$$1.Wrap),
            list = this._listNode = new Node$$1(Node$$1.LIST).setStyleClass(CSS$$1.GroupList);

        root.setWidth(width);
        label_.setProperty('innerHTML', label);

        labelWrap.addChild(label_);
        head.addChild(menu);
        head.addChild(labelWrap);
        wrap.addChild(list);
        root.addChild(head);
        root.addChild(wrap);

        controlKit.getNode().addChild(root);

        if (!dock) {
            var menuHide = this._menuHide = new Node$$1(Node$$1.INPUT_BUTTON);
            menuHide.setStyleClass(CSS$$1.ButtonMenuHide);
            menuHide.addEventListener(NodeEvent$$1.MOUSE_DOWN, this._onMenuHideMouseDown.bind(this));

            menu.addChild(menuHide);

            if (this._parent.panelsAreClosable()) {
                var menuClose = new Node$$1(Node$$1.INPUT_BUTTON);
                menuClose.setStyleClass(CSS$$1.ButtonMenuClose);
                menuClose.addEventListener(NodeEvent$$1.MOUSE_DOWN, this.disable.bind(this));

                menu.addChild(menuClose);
            }

            if (this.hasMaxHeight()) {
                this._addScrollWrap();
            }

            if (!isFixed) {
                if (position) {
                    if (align == LayoutMode$$1.LEFT || align == LayoutMode$$1.TOP || align == LayoutMode$$1.BOTTOM) {
                        root.setPositionGlobal(position[0], position[1]);
                    } else {
                        root.setPositionGlobal(window.innerWidth - width - position[0], position[1]);
                        this._position = root.getPosition();
                    }
                } else this._position = root.getPosition();

                this._mouseOffset = [0, 0];

                root.setStyleProperty('position', 'absolute');
                head.addEventListener(NodeEvent$$1.MOUSE_DOWN, this._onHeadDragStart.bind(this));
            } else {
                if (position) {
                    var positionX = position[0],
                        positionY = position[1];

                    if (positionY != 0) root.setPositionY(positionY);
                    if (positionX != 0) if (align == LayoutMode$$1.RIGHT) root.getElement().marginRight = positionX;else root.setPositionX(positionX);
                }

                root.setStyleProperty('float', align);
            }
        } else {
            var dockAlignment = dock.align;

            if (dockAlignment == LayoutMode$$1.LEFT || dockAlignment == LayoutMode$$1.RIGHT) {
                align = dockAlignment;
                this._height = window.innerHeight;
            }

            if (dockAlignment == LayoutMode$$1.TOP || dockAlignment == LayoutMode$$1.BOTTOM) {}

            /*
             if(dock.resizable)
             {
             var sizeHandle = new ControlKit.Node(ControlKit.NodeType.DIV);
             sizeHandle.setStyleClass(ControlKit.CSS.SizeHandle);
             rootNode.addChild(sizeHandle);
             }
             */

            root.setStyleProperty('float', align);
        }

        var parent = this._parent;
        var historyIsEnabled = parent.historyIsEnabled(),
            statesAreEnabled = parent.statesAreEnabled();

        if (historyIsEnabled || statesAreEnabled) {
            menu.addChildAt(new Node$$1(), 0).setStyleClass(CSS$$1.Wrap); //.setStyleProperty('display','none');
        }

        if (historyIsEnabled) {
            this._menuUndo = menu.getChildAt(0).addChild(new Node$$1(Node$$1.INPUT_BUTTON)).setStyleClass(CSS$$1.ButtonMenuUndo).setProperty('value', History$$1.get().getNumStates()).addEventListener(NodeEvent$$1.MOUSE_DOWN, function () {
                History$$1.get().popState();
            });
            parent.addEventListener(MenuEvent$$1.UPDATE_MENU, this, 'onUpdateMenu');
        }
        if (statesAreEnabled) {
            menu.getChildAt(0).addChild(new Node$$1(Node$$1.INPUT_BUTTON)).setStyleClass(CSS$$1.ButtonMenuLoad).setProperty('value', 'Load').addEventListener(NodeEvent$$1.MOUSE_DOWN, function () {
                controlKit._loadState();
            });
            menu.getChildAt(0).addChild(new Node$$1(Node$$1.INPUT_BUTTON)).setStyleClass(CSS$$1.ButtonMenuSave).setProperty('value', 'Save').addEventListener(NodeEvent$$1.MOUSE_DOWN, function () {
                controlKit._saveState();
            });
        }
        if (historyIsEnabled || statesAreEnabled) {
            head.addEventListener(NodeEvent$$1.MOUSE_OVER, function () {
                menu.setStyleClass(CSS$$1.MenuActive);
            });
            head.addEventListener(NodeEvent$$1.MOUSE_OUT, function () {
                menu.setStyleClass(CSS$$1.Menu);
            });
        }
        if (opacity != 1.0 && opacity != 0.0) {
            root.setStyleProperty('opacity', opacity);
        }
        window.addEventListener(DocumentEvent$$1.WINDOW_RESIZE, this._onWindowResize.bind(this));
        this._updateAppearance();
    }
    Panel.prototype = Object.create(EventDispatcher$$1.prototype);
    Panel.prototype.constructor = Panel;

    Panel.prototype._onMenuHideMouseDown = function () {
        this._enabled = !this._enabled;
        this._updateAppearance();
    };

    Panel.prototype.onUpdateMenu = function () {
        this._menuUndo.setProperty('value', History$$1.get().getNumStates());
    };

    Panel.prototype._onMenuUndoTrigger = function () {
        History$$1.get().popState();
    };

    Panel.prototype._updateAppearance = function () {
        var rootNode = this._node,
            headNode = this._headNode,
            menuHide = this._menuHide;

        if (!this._enabled) {
            headNode.getStyle().borderBottom = 'none';
            rootNode.setHeight(headNode.getHeight());
            menuHide.setStyleClass(CSS$$1.ButtonMenuShow);
            this.dispatchEvent(new Event_(this, PanelEvent$$1.PANEL_HIDE, null));
        } else {
            rootNode.setHeight(headNode.getHeight() + this._wrapNode.getHeight());
            rootNode.deleteStyleProperty('height');
            menuHide.setStyleClass(CSS$$1.ButtonMenuHide);
            headNode.setStyleClass(CSS$$1.Head);
            this.dispatchEvent(new Event_(this, PanelEvent$$1.PANEL_SHOW, null));
        }
    };

    Panel.prototype._onHeadDragStart = function () {
        var parentNode = this._parent.getNode(),
            node = this._node;

        var nodePos = node.getPositionGlobal(),
            mousePos = Mouse$$1.get().getPosition(),
            offsetPos = this._mouseOffset;

        offsetPos[0] = mousePos[0] - nodePos[0];
        offsetPos[1] = mousePos[1] - nodePos[1];

        var eventMouseMove = DocumentEvent$$1.MOUSE_MOVE,
            eventMouseUp = DocumentEvent$$1.MOUSE_UP;

        var self = this;

        var onDrag = function () {
            self._updatePosition();
        },
            onDragEnd = function () {
            document.removeEventListener(eventMouseMove, onDrag, false);
            document.removeEventListener(eventMouseUp, onDragEnd, false);
            self.dispatchEvent(new Event_(this, PanelEvent$$1.PANEL_MOVE_END, null));
        };

        parentNode.removeChild(node);
        parentNode.addChild(node);

        document.addEventListener(eventMouseMove, onDrag, false);
        document.addEventListener(eventMouseUp, onDragEnd, false);

        this.dispatchEvent(new Event_(this, PanelEvent$$1.PANEL_MOVE_BEGIN, null));
    };

    Panel.prototype._updatePosition = function () {
        var mousePos = Mouse$$1.get().getPosition(),
            offsetPos = this._mouseOffset;

        var position = this._position;
        position[0] = mousePos[0] - offsetPos[0];
        position[1] = mousePos[1] - offsetPos[1];

        this._constrainHeight();
        this._constrainPosition();

        this.dispatchEvent(new Event_(this, PanelEvent$$1.PANEL_MOVE, null));
    };

    Panel.prototype._onWindowResize = function () {
        if (this.isDocked()) {
            var dock = this._dock;

            if (dock.align == LayoutMode$$1.RIGHT || dock.align == LayoutMode$$1.LEFT) {
                var windowHeight = window.innerHeight,
                    listHeight = this._listNode.getHeight(),
                    headHeight = this._headNode.getHeight();

                this._height = windowHeight;

                if (windowHeight - headHeight > listHeight) {
                    this._scrollBar.disable();
                } else {
                    this._scrollBar.enable();
                }

                this.dispatchEvent(new Event_(this, PanelEvent$$1.PANEL_SIZE_CHANGE));
            }
        } else {
            if (!this.isFixed()) {
                this._constrainPosition();
            }
        }
        this._constrainHeight();
        this.dispatchEvent(new Event_(this, DocumentEvent$$1.WINDOW_RESIZE));
    };

    Panel.prototype._constrainPosition = function () {
        var node = this._node;

        var maxX = window.innerWidth - node.getWidth(),
            maxY = window.innerHeight - node.getHeight();

        var position = this._position;
        position[0] = Math.max(0, Math.min(position[0], maxX));
        position[1] = Math.max(0, Math.min(position[1], maxY));

        node.setPositionGlobal(position[0], position[1]);
    };

    Panel.prototype._constrainHeight = function () {
        if (!this._vConstrain) return;

        var hasMaxHeight = this.hasMaxHeight(),
            hasScrollWrap = this.hasScrollWrap();

        var head = this._headNode,
            wrap = this._wrapNode;

        var scrollBar = this._scrollBar;

        var panelTop = this.isDocked() ? 0 : this.isFixed() ? 0 : this._position[1];

        var panelHeight = hasMaxHeight ? this.getMaxHeight() : hasScrollWrap ? scrollBar.getTargetNode().getHeight() : wrap.getHeight();

        var panelBottom = panelTop + panelHeight;
        var headHeight = head.getHeight();

        var windowHeight = window.innerHeight,
            heightDiff = windowHeight - panelBottom - headHeight,
            heightSum;

        if (heightDiff < 0.0) {
            heightSum = panelHeight + heightDiff;

            if (!hasScrollWrap) {
                this._addScrollWrap(heightSum);
                this.dispatchEvent(new Event_(this, PanelEvent$$1.PANEL_SCROLL_WRAP_ADDED, null));
                return;
            }

            scrollBar.setWrapHeight(heightSum);
            wrap.setHeight(heightSum);
        } else {
            if (!hasMaxHeight && hasScrollWrap) {
                scrollBar.removeFromParent();
                wrap.addChild(this._listNode);
                wrap.deleteStyleProperty('height');
                this._scrollBar.removeMouseListener();
                this._scrollBar = null;

                this.dispatchEvent(new Event_(this, PanelEvent$$1.PANEL_SCROLL_WRAP_REMOVED, null));
            }
        }
    };

    Panel.prototype.onGroupListSizeChange = function () {
        if (this.hasScrollWrap()) {
            this._updateScrollWrap();
        }
        this._constrainHeight();
    };

    Panel.prototype._updateScrollWrap = function () {
        var wrap = this._wrapNode,
            scrollBar = this._scrollBar,
            height = this.hasMaxHeight() ? this.getMaxHeight() : 100,
            listHeight = this._listNode.getHeight();

        wrap.setHeight(listHeight < height ? listHeight : height);

        scrollBar.update();

        if (!scrollBar.isValid()) {
            scrollBar.disable();
            wrap.setHeight(wrap.getChildAt(1).getHeight());
        } else {
            scrollBar.enable();
            wrap.setHeight(height);
        }
    };

    Panel.prototype._addScrollWrap = function () {
        var wrapNode = this._wrapNode,
            listNode = this._listNode,
            height = arguments.length == 0 ? this.getMaxHeight() : arguments[0];

        this._scrollBar = new ScrollBar$$1(wrapNode, listNode, height);
        if (this.isEnabled()) {
            wrapNode.setHeight(height);
        }
    };

    Panel.prototype.hasScrollWrap = function () {
        return this._scrollBar != null;
    };

    Panel.prototype.preventSelectDrag = function () {
        if (!this.hasScrollWrap()) {
            return;
        }
        this._wrapNode.getElement().scrollTop = 0;
    };

    Panel.prototype.enable = function () {
        this._node.setStyleProperty('display', 'block');
        this._enabled = true;
        this._updateAppearance();
    };

    Panel.prototype.disable = function () {
        this._node.setStyleProperty('display', 'none');
        this._enabled = false;
        this._updateAppearance();
    };

    Panel.prototype.isEnabled = function () {
        return this._enabled;
    };

    Panel.prototype.isDisabled = function () {
        return !this._enabled;
    };

    Panel.prototype.hasMaxHeight = function () {
        return this._height != null;
    };

    Panel.prototype.getMaxHeight = function () {
        return this._height;
    };

    Panel.prototype.isDocked = function () {
        return this._dock;
    };

    Panel.prototype.isFixed = function () {
        return this._fixed;
    };

    Panel.prototype.getGroups = function () {
        return this._groups;
    };

    Panel.prototype.getNode = function () {
        return this._node;
    };

    Panel.prototype.getList = function () {
        return this._listNode;
    };

    Panel.prototype.getWidth = function () {
        return this._width;
    };

    Panel.prototype.getPosition = function () {
        return this._position;
    };

    Panel.prototype.getParent = function () {
        return this._parent;
    };

    /**
     * Adds a new Group to the Panel.
     * @param {Object} [params] - Group options
     * @param {String} [params.label=''] - The Group label string
     * @param {Boolean} [params.useLabel=true] - Trigger whether all contained SubGroups and Components should use labels
     * @param {Boolean} [params.enable=true] - Defines initial state open / closed
     * @param {Number} [params.height=null] - Defines if the height of the Group should be constrained to certain height
     * @returns {Panel}
     */

    Panel.prototype.addGroup = function (params) {
        var group = new Group$$1(this, params);
        this._groups.push(group);
        if (this.isDocked()) {
            this.dispatchEvent(new Event_(this, PanelEvent$$1.PANEL_SIZE_CHANGE));
        }
        return this;
    };

    /**
     * Adds a new SubGroup to the last added Group.
     * @param {Object} [params] - SubGroup options
     * @param {String} [params.label=''] - The SubGroup label string
     * @param {Boolean} [params.useLabel=true] - Trigger whether all Components should use labels
     * @param {Boolean} [params.enable=true] - Defines initial state open / closed
     * @param {Number} [params.height=null] - Defines if the height of the SubGroup should be constrained to certain height
     * @returns {Panel}
     */

    Panel.prototype.addSubGroup = function (params) {
        var groups = this._groups;
        if (groups.length == 0) {
            this.addGroup();
        }
        groups[groups.length - 1].addSubGroup(params);
        return this;
    };

    Panel.prototype._addComponent = function () {
        var groups = this._groups,
            group;
        if (groups.length == 0) {
            groups.push(new Group$$1(this));
        }
        group = groups[groups.length - 1];

        group.addComponent.apply(group, arguments);
        return this;
    };

    /**
     * Adds a new StringInput to last added SubGroup.
     * @param {Object} object - The object
     * @param {String} value - The property key
     * @param {Object} [params] - StringInput options
     * @param {String} [params.label=value] - StringInput label
     * @param {Function} [params.onChange] - Callback on change
     * @param {Array} [params.presets] - A set of presets
     * @returns {Panel}
     */

    Panel.prototype.addStringInput = function (object, value, params) {
        return this._addComponent(StringInput$$1, object, value, params);
    };

    /**
     * Adds a new NumberInput to last added SubGroup.
     * @param {Object} object - The object.
     * @param {String} value - The property key.
     * @param {Object} [params] - Component options.
     * @param {String} [params.label=value] - NumberInput label
     * @param {Function} [params.onChange] - Callback on change
     * @param {Number} [params.step] - Amount subbed/added on arrowDown/arrowUp press
     * @param {Number} [params.dp] - Decimal places displayed
     * @param {Array} [params.presets] - A set of presets
     * @returns {Panel}
     */

    Panel.prototype.addNumberInput = function (object, value, params) {
        return this._addComponent(NumberInput$$1, object, value, params);
    };

    /**
     * Adds a new Range input to last added SubGroup.
     * @param {Object} object - The object
     * @param {String} value - The property key
     * @param {Object} [params] - Component options
     * @param {String} [params.label=value] - Range label
     * @param {Function} [params.onChange] - Callback on change
     * @param {Number} [params.step] - Amount subbed/added on arrowDown/arrowUp press
     * @param {Number} [params.dp] - Decimal places displayed
     * @returns {Panel}
     */

    Panel.prototype.addRange = function (object, value, params) {
        return this._addComponent(Range$$1, object, value, params);
    };

    /**
     * Adds a new Checkbox to last added SubGroup.
     * @param {Object} object - The object
     * @param {String} value - The property key
     * @param {Object} [params] - Component options
     * @param {String} [params.label=value] - Checkbox label
     * @param {Function} [params.onChange] - Callback on change
     * @returns {Panel}
     */

    Panel.prototype.addCheckbox = function (object, value, params) {
        return this._addComponent(Checkbox$$1, object, value, params);
    };

    /**
     * Adds a new Color modifier to last added SubGroup.
     * @param {Object} object - The object
     * @param {String} value - The property key
     * @param {Object} [params] - Component options
     * @param {String} [params.label=value] - Color label
     * @param {Function} [params.onChange] - Callback on change
     * @param {String} [params.colorMode='rgb'] - The colorMode to be used: 'hex' #ff00ff, 'rgb' [255,0,255], 'rgbfv' [1,0,1]
     * @param {Array} [params.presets] - A set of preset colors matching params.colorMode
     * @returns {Panel}
     */

    Panel.prototype.addColor = function (object, value, params) {
        return this._addComponent(Color$$1, object, value, params);
    };

    /**
     * Adds a new Button to last added SubGroup.
     * @param {String} label - The object
     * @param {Function} onPress - Callback
     * @param {Object} [params] - Component options
     * @param {String} [params.label=value] - Button label
     * @returns {Panel}
     */

    Panel.prototype.addButton = function (label, onPress, params) {
        return this._addComponent(Button$$1, label, onPress, params);
    };

    /**
     * Adds a new Select to last added SubGroup.
     * @param {Object} object - The object
     * @param {String} value - The property key
     * @param {Object} [params] - Component options
     * @param {String} [params.label=value] - Button label
     * @param {Function} [params.onChange] - Callback on change - function(index){}
     * @param {String} [params.target] - The property to be set on select
     * @returns {Panel}
     */

    Panel.prototype.addSelect = function (object, value, params) {
        return this._addComponent(Select$$1, object, value, params);
    };

    /**
     * Adds a new Slider to last added SubGroup.
     * @param {Object} object - The object
     * @param {String} value - The property key
     * @param {String} range - The min/max array key to be used
     * @param {Object} [params] - Component options
     * @param {String} [params.label=value] - Slider label
     * @param {Function} [params.onChange] - Callback on change
     * @param {Function} [params.onFinish] - Callback on finish
     * @param {Number} [params.step] - Amount subbed/added on arrowDown/arrowUp press inside the input
     * @param {Number} [params.dp] - Decimal places displayed
     * @returns {Panel}
     */

    Panel.prototype.addSlider = function (object, value, range, params) {
        return this._addComponent(Slider$$1, object, value, range, params);
    };

    /**
     * Adds a new FunctionPlotter to last added SubGroup.
     * @param {Object} object - The object
     * @param {String} value - The property key - f(x), f(x,y)
     * @param {Object} [params] - Component options
     * @param {String} [params.label=value] - FunctionPlotter label
     * @returns {Panel}
     */

    Panel.prototype.addFunctionPlotter = function (object, value, params) {
        return this._addComponent(FunctionPlotter$$1, object, value, params);
    };

    /**
     * Adds a new XY-Pad to last added SubGroup.
     * @param {Object} object - The object
     * @param {String} value - The property key
     * @param {Object} [params] - Component options
     * @param {String} [params.label=value] - Pad label
     * @returns {Panel}
     */

    Panel.prototype.addPad = function (object, value, params) {
        return this._addComponent(Pad$$1, object, value, params);
    };

    /**
     * Adds a new ValuePlotter to last added SubGroup.
     * @param {Object} object - The object
     * @param {String} value - The property key
     * @param {Object} [params] - Component options
     * @param {String} [params.label=value] - Plotter label
     * @param {Number} [params.height] - Plotter height
     * @param {Number} [params.resolution] - Graph resolution
     * @returns {Panel}
     */

    Panel.prototype.addValuePlotter = function (object, value, params) {
        return this._addComponent(ValuePlotter$$1, object, value, params);
    };

    /**
     * Adds a new NumberOutput to last added SubGroup.
     * @param {Object} object - The object
     * @param {String} value - The property key
     * @param {Object} [params] - Component options
     * @param {String} [params.label=value] - Output label
     * @param {Number} [params.dp] - Decimal places displayed
     * @returns {Panel}
     */

    Panel.prototype.addNumberOutput = function (object, value, params) {
        return this._addComponent(NumberOutput$$1, object, value, params);
    };

    /**
     * Adds a new StringOutput to last added SubGroup.
     * @param {Object} object - The object
     * @param {String} value - The property key
     * @param {Object} [params] - Component options
     * @param {String} [params.label=value] - Output label
     * @returns {Panel}
     */

    Panel.prototype.addStringOutput = function (object, value, params) {
        return this._addComponent(StringOutput, object, value, params);
    };

    Panel.prototype.addCanvas = function (params) {
        return this._addComponent(Canvas_, params);
    };

    Panel.prototype.addSVG = function (params) {
        return this._addComponent(SVG_, params);
    };

    Panel.prototype.setData = function (data) {
        var groups = this._groups,
            i = -1,
            l = groups.length;
        while (++i < l) {
            groups[i].setData(data[i]);
        }
    };

    Panel.prototype.getData = function () {
        var groups = this._groups,
            i = -1,
            l = groups.length;
        var data = [];
        while (++i < l) {
            data.push(groups[i].getData());
        }
        return data;
    };

    module.exports = Panel;
});

var State = createCommonjsModule(function (module) {
    var DialogTemplate = '<head>\n' + '   <title>ControlKit State</title>\n' + '   <style type="text/css">\n' + '      body{\n' + '          box-sizing: border-box;\n' + '          padding: 20px;\n' + '          margin: 0;\n' + '          font-family: Arial, sans-serif;\n' + '          width: 100%;\n' + '      }\n' + '      textarea{\n' + '          margin-bottom:10px;\n' + '          box-sizing: border-box;\n' + '          padding: 0;\n' + '          border: 0;\n' + '          border: 1px solid #dedede;\n' + '          outline: none;\n' + '          font-family: Monaco, monospace;\n' + '          font-size: 11px;\n' + '          resize: none;\n' + '          word-wrap: break-word;\n' + '          display: block;\n' + '          width: 100%;\n' + '          overflow-y: scroll;\n' + '          height: 125px;\n' + '      }\n' + '      button{\n' + '          margin: 0;\n' + '          padding: 0 5px 3px 5px;\n' + '          height: 20px;\n' + '      }\n' + '      #save,#filename,#load{\n' + '          float: right;\n' + '      }\n' + '      input[type="text"]{\n' + '          margin: 0;\n' + '          padding: 0;\n' + '          width: 45%;\n' + '          height:20px;\n' + '      }\n' + '   </style>\n' + '</head>\n' + '<body>\n' + '   <textarea name="state" id="state"></textarea>\n' + '</body>';

    var SaveDialogTemplate = '<button type="button" id="save">Save</button>\n' + '<input type="text" id="filename" value="ck-state.json"></input>';

    var LoadDialogTemplate = '<input type="file" id="load-disk"></button>' + '<button type="button" id="load">Load</button>';

    function createWindow() {
        var width = 320,
            height = 200;
        var window_ = window.open('', '', '\
        width=' + width + ',\
        height=' + height + ',\
        left=' + (window.screenX + window.innerWidth * 0.5 - width * 0.5) + ',\
        top=' + (window.screenY + window.innerHeight * 0.5 - height * 0.5) + ',\
        location=0,\
        titlebar=0,\
        resizable=0');
        window_.document.documentElement.innerHTML = DialogTemplate;
        return window_;
    }

    function save(data) {
        var window_ = createWindow();
        var document_ = window_.document;
        document_.body.innerHTML += SaveDialogTemplate;
        document_.getElementById('save').addEventListener('click', function () {
            //log & save in main window
            var str = document_.getElementById('state').value,
                blob = new Blob([str], { type: 'application:json' }),
                name = document_.getElementById('filename').value;
            var a = document.createElement('a');
            a.download = name;
            if (window.webkitURL) {
                a.href = window.webkitURL.createObjectURL(blob);
            } else {
                a.href = window.createObjectURL(blob);
                a.style.display = 'none';
                a.addEventListener('click', function () {
                    document_.body.removeChild(a);
                });
                document_.body.appendChild(a);
            }
            a.click();
        });
        document_.getElementById('state').innerText = JSON.stringify(data);
    }

    function load(callback) {
        var window_ = createWindow();
        var document_ = window_.document;
        document_.body.innerHTML += LoadDialogTemplate;
        var input = document_.getElementById('state');
        var btnLoad = document_.getElementById('load');
        btnLoad.disabled = true;

        function validateInput() {
            try {
                var obj = JSON.parse(input.value);
                if (obj && typeof obj === 'object' && obj !== null) {
                    btnLoad.disabled = false;
                }
            } catch (e) {
                btnLoad.disabled = true;
            }
        }

        input.addEventListener('input', function () {
            validateInput();
        });
        document_.getElementById('load').addEventListener('click', function () {
            var str = input.value;
            callback(JSON.parse(str).data);
            window_.close();
        });
        var loadFromDisk = document_.getElementById('load-disk');
        loadFromDisk.addEventListener('change', function () {
            var reader = new FileReader();
            reader.addEventListener('loadend', function (e) {
                input.value = e.target.result;
                validateInput();
            });
            reader.readAsText(loadFromDisk.files[0], 'utf-8');
        });
    }

    module.exports = {
        load: load,
        save: save
    };
});

var Style = createCommonjsModule(function (module) {
	var Style = {
		string: "#controlKit{position:absolute;top:0;left:0;width:100%;height:100%;-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;pointer-events:none}#controlKit .panel{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;pointer-events:auto;position:relative;z-index:1;-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;overflow:hidden;opacity:1;float:left;width:200px;border-radius:3px;-moz-border-radius:3px;box-shadow:0 2px 2px rgba(0,0,0,.25);margin:0;padding:0;background-color:#1a1a1a;font-family:Arial,sans-serif}#controlKit .panel .wrap{width:auto;height:auto;margin:0;padding:0;position:relative;overflow:hidden}#controlKit .panel ul{margin:0;padding:0;list-style:none}#controlKit .panel .color,#controlKit .panel input[type=text],#controlKit .panel textarea,#controlKit .picker input[type=text]{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;width:100%;height:25px;padding:0 0 0 8px;font-family:Arial,sans-serif;font-size:11px;color:#fff;text-shadow:1px 1px #000;outline:0;background:#222729;background-image:-o-linear-gradient(rgba(0,0,0,.075) 0,rgba(0,0,0,.125) 100%);background-image:linear-gradient(rgba(0,0,0,.075) 0,rgba(0,0,0,.125) 100%);border:none;box-shadow:0 0 0 1px #1f1f1f inset;border-radius:2px;-moz-border-radius:2px}#controlKit .panel .button,#controlKit .panel .select,#controlKit .panel .select-active,#controlKit .picker .button{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;cursor:pointer;width:100%;height:26px;margin:0;background-image:-o-linear-gradient(#404040 0,#3b3b3b 100%);background-image:linear-gradient(#404040 0,#3b3b3b 100%);border:none;outline:0;border-radius:2px;box-shadow:0 0 0 1px #1f1f1f inset,-1px 2px 0 0 #4a4a4a inset;font-family:Arial,sans-serif;color:#fff}#controlKit .panel textarea{padding:5px 8px 2px;overflow:hidden;resize:none;vertical-align:top;white-space:nowrap}#controlKit .panel .textarea-wrap{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;width:100%;padding:0;float:left;height:100%;overflow:hidden;border:none;border-radius:2px;-moz-border-radius:2px;background-color:#222729;box-shadow:0 0 1px 2px rgba(0,0,0,.0125) inset,0 0 1px 1px #111314 inset;background-image:-o-linear-gradient(rgba(0,0,0,.075) 0,rgba(0,0,0,.125) 100%);background-image:linear-gradient(rgba(0,0,0,.075) 0,rgba(0,0,0,.125) 100%)}#controlKit .panel .textarea-wrap textarea{border:none;border-radius:2px;-moz-border-radius:2px;box-shadow:none;background:0 0}#controlKit .panel .textarea-wrap .scrollBar{border:1px solid #101213;border-bottom-right-radius:2px;border-top-right-radius:2px;border-left:none;box-shadow:0 0 1px 2px rgba(0,0,0,.0125) inset,0 0 1px 1px #111314 inset}#controlKit .panel canvas{cursor:pointer;vertical-align:bottom;border:none;box-shadow:0 0 0 1px #1f1f1f inset;border-radius:2px;-moz-border-radius:2px}#controlKit .panel .canvas-wrap,#controlKit .panel .svg-wrap{margin:6px 0 0;position:relative;width:70%;float:right;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;border:none;border-radius:2px;-moz-border-radius:2px;background:#1e2224;background-image:-o-linear-gradient(transparent 0,rgba(0,0,0,.05) 100%);background-image:linear-gradient(transparent 0,rgba(0,0,0,.05) 100%)}#controlKit .panel .canvas-wrap svg,#controlKit .panel .svg-wrap svg{position:absolute;left:0;top:0;cursor:pointer;vertical-align:bottom;border:none;box-shadow:0 0 0 1px #1f1f1f inset;border-radius:2px;-moz-border-radius:2px}#controlKit .panel .button,#controlKit .picker .button{font-size:10px;font-weight:700;text-shadow:0 1px #000;text-transform:uppercase}#controlKit .panel .button:hover,#controlKit .picker .button:hover{background-image:-o-linear-gradient(#454545 0,#3b3b3b 100%);background-image:linear-gradient(#454545 0,#3b3b3b 100%)}#controlKit .panel .button:active,#controlKit .picker .button:active{background-image:-o-linear-gradient(#404040 0,#3b3b3b 100%);background-image:linear-gradient(#404040 0,#3b3b3b 100%)}#controlKit .panel .color-with-preset-wrap,#controlKit .panel .input-with-preset-wrap{width:100%;float:left}#controlKit .panel .color-with-preset-wrap .color,#controlKit .panel .input-with-preset-wrap input[type=text]{padding-right:25px;border-top-right-radius:2px;border-bottom-right-radius:2px;float:left}#controlKit .panel .button-preset,#controlKit .panel .button-preset-active{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:absolute;right:0;width:20px;height:25px;margin:0;cursor:pointer;float:right;border:none;border-top-right-radius:2px;border-bottom-right-radius:2px;box-shadow:0 0 0 1px #1f1f1f inset,-1px 2px 0 0 #4a4a4a inset;outline:0}#controlKit .panel .button-preset-active,#controlKit .panel .button-preset:hover{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAFCAYAAAB4ka1VAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAG5JREFUeNpi5ODiamRgYKhjwA4amVx8gxjmL1rC8P3rVxQ8b+ESBhffIAZmNR29A5evXWdiZGC019XSZGBgYGBYvmY9w7I16xoZGBgaWKBG1S9bs+4/AwNDPQMDA1ySgYGBgdEnPAbZzgY0mgEwAE9lJT1lrsffAAAAAElFTkSuQmCC) 50% 50% no-repeat,linear-gradient(#454545 0,#3b3b3b 100%)}#controlKit .panel .button-preset{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAFCAYAAAB4ka1VAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAG5JREFUeNpi5ODiamRgYKhjwA4amVx8gxjmL1rC8P3rVxQ8b+ESBhffIAZmNR29A5evXWdiZGC019XSZGBgYGBYvmY9w7I16xoZGBgaWKBG1S9bs+4/AwNDPQMDA1ySgYGBgdEnPAbZzgY0mgEwAE9lJT1lrsffAAAAAElFTkSuQmCC) 50% 50% no-repeat,linear-gradient(#404040 0,#3b3b3b 100%)}#controlKit .panel input[type=checkbox]{margin:6px 0 0}#controlKit .panel .select,#controlKit .panel .select-active{padding-left:10px;padding-right:20px;font-size:11px;text-align:left;text-shadow:1px 1px #000;cursor:pointer;overflow:hidden;white-space:nowrap;text-overflow:ellipsis}#controlKit .panel .select{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAYAAAB24g05AAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAMNJREFUeNqckjEKwjAUhj8l53AQ526BHKKLIhSlHkHxBkkuIFWPILQOQQd3V4VuXiguFlrFRPzhLXl833uB10uznCaP+q4BEqls83Y5HghFtOH1amkAit2+IwkmzXIGw5HeFFvfZFNs/WA40mmW470P1gf8LokJRCIV11vN9bb42C6RKvoDAdhX/RXxqO8G0F/6FjBBQSIV8+mE2XTcaVTuTOlO0Q36gCndyVbu/A5Hp7fvwLymaeBnuHNILQm/wgDPAQAPNIsHnO794QAAAABJRU5ErkJggg==) 100% 50% no-repeat,linear-gradient(#404040 0,#3b3b3b 100%)}#controlKit .panel .select-active,#controlKit .panel .select:hover{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAYAAAB24g05AAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAMNJREFUeNqckjEKwjAUhj8l53AQ526BHKKLIhSlHkHxBkkuIFWPILQOQQd3V4VuXiguFlrFRPzhLXl833uB10uznCaP+q4BEqls83Y5HghFtOH1amkAit2+IwkmzXIGw5HeFFvfZFNs/WA40mmW470P1gf8LokJRCIV11vN9bb42C6RKvoDAdhX/RXxqO8G0F/6FjBBQSIV8+mE2XTcaVTuTOlO0Q36gCndyVbu/A5Hp7fvwLymaeBnuHNILQm/wgDPAQAPNIsHnO794QAAAABJRU5ErkJggg==) 100% 50% no-repeat,linear-gradient(#454545 0,#3b3b3b 100%)}#controlKit .panel .slider-handle,#controlKit .panel .slider-slot,#controlKit .panel .slider-wrap,#controlKit .panel .wrap-slider{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}#controlKit .panel .wrap-slider{width:70%;padding:6px 0 0;float:right;height:100%}#controlKit .panel .wrap-slider input[type=text]{width:25%;text-align:center;padding:0;float:right}#controlKit .panel .slider-wrap{float:left;cursor:ew-resize;width:70%}#controlKit .panel .slider-slot{width:100%;height:25px;padding:3px;background-color:#1e2224;border:none;box-shadow:0 0 0 1px #1f1f1f inset;border-radius:2px;-moz-border-radius:2px}#controlKit .panel .slider-handle{position:relative;width:100%;height:100%;background:#b32435;background-image:-o-linear-gradient(transparent 0,rgba(0,0,0,.1) 100%);background-image:linear-gradient(transparent 0,rgba(0,0,0,.1) 100%);box-shadow:0 1px 0 0 #0f0f0f}#controlKit .panel .color{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;cursor:pointer;width:100%;height:25px;padding:0;border:none;background:#fff;box-shadow:0 0 0 1px #111314 inset;text-align:center;line-height:25px;border-radius:2px;-moz-border-radius:2px}#controlKit .panel .graph-slider-x-wrap,#controlKit .panel .graph-slider-y-wrap{position:absolute;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}#controlKit .panel .graph-slider-x-wrap{bottom:0;left:0;width:100%;padding:6px 20px 6px 6px}#controlKit .panel .graph-slider-y-wrap{top:0;right:0;height:100%;padding:6px 6px 20px}#controlKit .panel .graph-slider-x,#controlKit .panel .graph-slider-y{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;border-radius:2px;-moz-border-radius:2px;background:rgba(24,27,29,.5);border:1px solid #181b1d}#controlKit .panel .graph-slider-x{height:8px}#controlKit .panel .graph-slider-y{width:8px;height:100%}#controlKit .panel .graph-slider-x-handle,#controlKit .panel .graph-slider-y-handle{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;cursor:pointer;border:1px solid #181b1d;background:#303639}#controlKit .panel .graph-slider-x-handle{width:20px;height:100%;border-top:none;border-bottom:none}#controlKit .panel .graph-slider-y-handle{width:100%;height:20px;border-left:none;border-right:none}#controlKit .sub-group .wrap .wrap .wrap{width:25%!important;padding:0!important;float:left!important}#controlKit .sub-group .wrap .wrap .wrap .label{width:100%!important;padding:8px 0 0!important;color:#878787!important;text-align:center!important;text-transform:uppercase!important;font-weight:700!important;text-shadow:1px 1px #1a1a1a!important}#controlKit .sub-group .wrap .wrap .wrap input[type=text]{padding:0;text-align:center}#controlKit .options{pointer-events:auto;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;border:1px solid #1f1f1f;border-radius:2px;-moz-border-radius:2px;position:absolute;z-index:2147483638;left:0;top:0;width:auto;height:auto;box-shadow:0 1px 0 0 #4a4a4a inset;background-color:#454545;font-family:Arial,sans-serif;font-size:11px;color:#fff;text-shadow:1px 1px #000;overflow:hidden}#controlKit .options ul{width:100%;list-style:none;margin:0;padding:0}#controlKit .options ul li{margin:0;width:100%;height:25px;line-height:25px;padding:0 20px 0 10px;overflow:hidden;white-space:normal;text-overflow:ellipsis;cursor:pointer}#controlKit .options ul li:hover{background-color:#1f2325}#controlKit .options ul .li-selected{background-color:#292d30}#controlKit .options .color{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}#controlKit .options .color .li-selected,#controlKit .options .color li{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:0;height:25px;line-height:25px;text-align:center}#controlKit .options .color .li-selected:hover,#controlKit .options .color li:hover{background:0 0;font-weight:700}#controlKit .options .color .li-selected{font-weight:700}#controlKit .panel .label,#controlKit .picker .label{width:100%;float:left;font-size:11px;font-weight:700;text-shadow:0 1px #000;overflow:hidden;white-space:nowrap;text-overflow:ellipsis;cursor:default}#controlKit .panel .head,#controlKit .panel .panel-head-inactive,#controlKit .picker .head{height:30px;padding:0 10px;background:#1a1a1a;overflow:hidden}#controlKit .panel .head .wrap,#controlKit .panel .panel-head-inactive .wrap,#controlKit .picker .head .wrap{width:auto;height:auto;margin:0;padding:0;position:relative;overflow:hidden}#controlKit .panel .head .label,#controlKit .picker .head .label{cursor:pointer;line-height:30px;color:#65696b}#controlKit .panel .group-list .group .head{height:38px;padding:0 10px;border-top:1px solid #4f4f4f;border-bottom:1px solid #262626;background-image:-o-linear-gradient(#454545 0,#3b3b3b 100%);background-image:linear-gradient(#454545 0,#3b3b3b 100%);cursor:pointer}#controlKit .panel .group-list .group .head .label{font-size:12px;line-height:38px;color:#fff}#controlKit .panel .group-list .group .head:hover{border-top:1px solid #525252;background-image:-o-linear-gradient(#454545 0,#404040 100%);background-image:linear-gradient(#454545 0,#404040 100%)}#controlKit .panel .group-list .group li{height:35px;padding:0 10px}#controlKit .panel .group-list .group .sub-group-list .sub-group:last-of-type{border-bottom:none}#controlKit .panel .group-list .group .sub-group-list .sub-group{padding:0;height:auto;border-bottom:1px solid #242424}#controlKit .panel .group-list .group .sub-group-list .sub-group ul{overflow:hidden}#controlKit .panel .group-list .group .sub-group-list .sub-group ul li{background:#2e2e2e;border-bottom:1px solid #222729}#controlKit .panel .group-list .group .sub-group-list .sub-group ul li:last-of-type{border-bottom:none}#controlKit .panel .group-list .group .sub-group-list .sub-group:first-child{margin-top:0}#controlKit .panel .group-list .group .sub-group-list .sub-group .head,#controlKit .panel .group-list .group .sub-group-list .sub-group .head-inactive{cursor:pointer}#controlKit .panel .group-list .group .sub-group-list .sub-group .head{height:27px;padding:0 10px;border-top:none;border-bottom:1px solid #242424;background-image:none;background-color:#272727}#controlKit .panel .group-list .group .sub-group-list .sub-group .head:hover{background-image:none;background-color:#272727}#controlKit .panel .group-list .group .sub-group-list .sub-group .head-inactive{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;height:27px;padding:0 10px;box-shadow:0 1px 0 0 #404040 inset;background-image:-o-linear-gradient(#3b3b3b 0,#383838 100%);background-image:linear-gradient(#3b3b3b 0,#383838 100%)}#controlKit .panel .group-list .group .sub-group-list .sub-group .head-inactive:hover{box-shadow:0 1px 0 0 #474747 inset;background-image:none;background-image:-o-linear-gradient(#404040 0,#3b3b3b 100%);background-image:linear-gradient(#404040 0,#3b3b3b 100%)}#controlKit .panel .group-list .group .sub-group-list .sub-group .head .label,#controlKit .panel .group-list .group .sub-group-list .sub-group .head-inactive .label{margin:0;padding:0;line-height:27px;color:#fff;font-weight:700;font-size:11px;text-shadow:1px 1px #000;text-transform:capitalize}#controlKit .panel .group-list .group .sub-group-list .sub-group .head .wrap .label,#controlKit .panel .group-list .group .sub-group-list .sub-group .head-inactive .wrap .label{width:100%;font-weight:700;color:#fff;padding:0}#controlKit .panel .group-list .group .sub-group-list .sub-group .wrap .label{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;height:100%;width:30%;padding:12px 5px 0 0;float:left;font-size:11px;font-weight:400;color:#aeb5b8;text-shadow:1px 1px #000}#controlKit .panel .group-list .group .sub-group-list .sub-group .wrap .wrap{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;width:70%;padding:5px 0 0;float:right;height:100%}#controlKit .panel .group-list .group:last-child .scroll-buffer:nth-of-type(3),#controlKit .panel .group-list .group:last-child .sub-group-list{border-bottom:none}#controlKit .panel .scroll-wrap{position:relative;overflow:hidden}#controlKit .panel .scroll-buffer{width:100%;height:8px;border-top:1px solid #3b4447;border-bottom:1px solid #1e2224}#controlKit .panel .scrollBar{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;width:15px;height:100%;float:right;top:0;padding:0;margin:0;position:relative;background:#212628;background-image:linear-gradient(to right,#242424 0,#2e2e2e 100%)}#controlKit .panel .scrollBar .track{padding:0 3px 0 2px}#controlKit .panel .scrollBar .track .thumb{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;width:11px;position:absolute;cursor:pointer;background-color:#343434;border:1px solid #1b1f21;border-radius:10px;-moz-border-radius:10px;box-shadow:inset 0 1px 0 0 #434b50}#controlKit .panel .menu,#controlKit .panel .menu-active,#controlKit .picker .menu{float:right;padding:5px 0 0}#controlKit .panel .menu input[type=button],#controlKit .panel .menu-active input[type=button],#controlKit .picker .menu input[type=button]{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;cursor:pointer;height:20px;border:none;vertical-align:top;border-radius:2px;-moz-border-radius:2px;font-family:Arial,sans-serif;font-size:10px;font-weight:700;color:#aaa;text-shadow:0 -1px #000;text-transform:uppercase;box-shadow:0 0 0 1px #131313 inset,-1px 2px 0 0 #212527 inset;outline:0}#controlKit .panel .menu .button-menu-close,#controlKit .panel .menu .button-menu-hide,#controlKit .panel .menu .button-menu-show,#controlKit .panel .menu-active .button-menu-close,#controlKit .panel .menu-active .button-menu-hide,#controlKit .panel .menu-active .button-menu-show,#controlKit .picker .menu .button-menu-close,#controlKit .picker .menu .button-menu-hide,#controlKit .picker .menu .button-menu-show{width:20px;margin-left:4px}#controlKit .panel .menu .button-menu-hide,#controlKit .panel .menu-active .button-menu-hide,#controlKit .picker .menu .button-menu-hide{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAFCAYAAAB4ka1VAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAGRJREFUeNpidPUNYoCBU0cO1DMwMDCY2Tg0wsRYkCVlFZUboGy4ImZldU24pJySCgO/oBADAwODw/VL5xmk5RQOMr99/RIuCQPIiljMbBwYGBgYGH7//MmADCSlZRkkpWUZAAMAvTsgXBvOsq0AAAAASUVORK5CYII=) 50% 50% no-repeat,#1a1d1f}#controlKit .panel .menu .button-menu-hide:hover,#controlKit .panel .menu-active .button-menu-hide:hover,#controlKit .picker .menu .button-menu-hide:hover{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAFCAYAAAB4ka1VAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAGRJREFUeNpidPUNYoCBU0cO1DMwMDCY2Tg0wsRYkCVlFZUboGy4ImZldU24pJySCgO/oBADAwODw/VL5xmk5RQOMr99/RIuCQPIiljMbBwYGBgYGH7//MmADCSlZRkkpWUZAAMAvTsgXBvOsq0AAAAASUVORK5CYII=) 50% 50% no-repeat,#000;box-shadow:#fff 0,#000 100%}#controlKit .panel .menu .button-menu-show,#controlKit .panel .menu-active .button-menu-show,#controlKit .picker .menu .button-menu-show{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAFCAYAAAB4ka1VAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAFpJREFUeNpsjDEOgCAQBOc4eqNfoCB8wMrCnwk/82EHWEkwcatJZrKyrFsGLv5X/H6cqPc41Y9ptVLN0BDT3VsTETnFuVkWIGuICWBEvfchAfz0mqvZ4BeeAQDzViMzJy0RXgAAAABJRU5ErkJggg==) 50% 50% no-repeat,#1a1d1f}#controlKit .panel .menu .button-menu-show:hover,#controlKit .panel .menu-active .button-menu-show:hover,#controlKit .picker .menu .button-menu-show:hover{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAFCAYAAAB4ka1VAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAFpJREFUeNpsjDEOgCAQBOc4eqNfoCB8wMrCnwk/82EHWEkwcatJZrKyrFsGLv5X/H6cqPc41Y9ptVLN0BDT3VsTETnFuVkWIGuICWBEvfchAfz0mqvZ4BeeAQDzViMzJy0RXgAAAABJRU5ErkJggg==) 50% 50% no-repeat,#000;box-shadow:#fff 0,#000 100%}#controlKit .panel .menu .button-menu-close,#controlKit .panel .menu-active .button-menu-close,#controlKit .picker .menu .button-menu-close{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAJCAYAAAAPU20uAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAQ1JREFUeNpM0D9LAmEAx/HvPXeDTqeXpVeYYjpYGQ1hBQ7SnxfQ0pA1FEVbr6FeRgZuCb2EoOCgm26spoIgiKBQQaIUnuceW27wt36HD/wMO+ncAna1Vl9jbIHvtYANa2lltYJhuIHvXVVr9ZMoHpXmFw/tpCOtWCx+L0xzv1heOA58Lw68pqdnzlNpl1DKNws40GH4kJrKXAphNgZ/v2TzBZSUbaAhIrLZ/f66m8y4zBaK/PT7XaABICLzbDgcbOkwJFQKPdITge+1AQw76dy42dxufq5EqFQLeBdCXPR6HV6eHz+M9fr2Z8JxXCVlEziNyD3Tsq6VksosV5Y3tdYdYGfshqeR1jkDI/E/AO8rYRlwXBquAAAAAElFTkSuQmCC) 50% 50% no-repeat,#1a1d1f}#controlKit .panel .menu .button-menu-close:hover,#controlKit .panel .menu-active .button-menu-close:hover,#controlKit .picker .menu .button-menu-close:hover{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAJCAYAAAAPU20uAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAQ1JREFUeNpM0D9LAmEAx/HvPXeDTqeXpVeYYjpYGQ1hBQ7SnxfQ0pA1FEVbr6FeRgZuCb2EoOCgm26spoIgiKBQQaIUnuceW27wt36HD/wMO+ncAna1Vl9jbIHvtYANa2lltYJhuIHvXVVr9ZMoHpXmFw/tpCOtWCx+L0xzv1heOA58Lw68pqdnzlNpl1DKNws40GH4kJrKXAphNgZ/v2TzBZSUbaAhIrLZ/f66m8y4zBaK/PT7XaABICLzbDgcbOkwJFQKPdITge+1AQw76dy42dxufq5EqFQLeBdCXPR6HV6eHz+M9fr2Z8JxXCVlEziNyD3Tsq6VksosV5Y3tdYdYGfshqeR1jkDI/E/AO8rYRlwXBquAAAAAElFTkSuQmCC) 50% 50% no-repeat,#000;box-shadow:#fff 0,#000 100%}#controlKit .panel .menu .button-menu-undo,#controlKit .panel .menu-active .button-menu-undo{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAALCAYAAABLcGxfAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAYVJREFUeNpckD1IW1EYhp9z7rm3oqkhzZ/xD6tR1EpFKelghlBonVwKDpaWDnbq2lVF0MHBUbdCp5aCUigdnISgoUPAqWMlYsGlNtYK1Zhzzr1dVG7zbt/L97x87yceTz0lrHKp+BJYBHqurG/AfC5f+AwgwkC5VHybyrTPdvdmA9f1BEJQO//LYWWfk+OfS7l8YeEGKJeKr7ND99aT6QzWmHPgE+AAM47rcnR4wI/K/qS8Ts90dq+lMh1YY1aBFuAF8AyQVuvNrrt9xOKJjyIau/MOGJp49ORhrXZh9r7ubgPPc/nCr3A36TjG931HDY+OTyjP6w8AKR01MvagcFqtxoH/gLPT3wexRDKrIrdbd6Tj9AshcD0PQaTa3BI5oUFa13sIAiTwyrd2wWqNqV/uAR3AccOrPyRSbUrX63/Ulbfk+34FxJdyqdgELAO3gDgwPTBy/3pvRoWC3gMkUm3pSDT6RkqJcl3iyXQQWIs1ZgXYUo239g4M1sKz1fo7MAdsAPwbAL9hftvTlNkdAAAAAElFTkSuQmCC) 20% 50% no-repeat,#1a1d1f;padding:0 6px 1px 0;width:38px;vertical-align:top;text-align:end}#controlKit .panel .menu .button-menu-undo:hover,#controlKit .panel .menu-active .button-menu-undo:hover{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAALCAYAAABLcGxfAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAYVJREFUeNpckD1IW1EYhp9z7rm3oqkhzZ/xD6tR1EpFKelghlBonVwKDpaWDnbq2lVF0MHBUbdCp5aCUigdnISgoUPAqWMlYsGlNtYK1Zhzzr1dVG7zbt/L97x87yceTz0lrHKp+BJYBHqurG/AfC5f+AwgwkC5VHybyrTPdvdmA9f1BEJQO//LYWWfk+OfS7l8YeEGKJeKr7ND99aT6QzWmHPgE+AAM47rcnR4wI/K/qS8Ts90dq+lMh1YY1aBFuAF8AyQVuvNrrt9xOKJjyIau/MOGJp49ORhrXZh9r7ubgPPc/nCr3A36TjG931HDY+OTyjP6w8AKR01MvagcFqtxoH/gLPT3wexRDKrIrdbd6Tj9AshcD0PQaTa3BI5oUFa13sIAiTwyrd2wWqNqV/uAR3AccOrPyRSbUrX63/Ulbfk+34FxJdyqdgELAO3gDgwPTBy/3pvRoWC3gMkUm3pSDT6RkqJcl3iyXQQWIs1ZgXYUo239g4M1sKz1fo7MAdsAPwbAL9hftvTlNkdAAAAAElFTkSuQmCC) 20% 50% no-repeat,#000;box-shadow:#fff 0,#000 100%}#controlKit .panel .menu .button-menu-load,#controlKit .panel .menu-active .button-menu-load{margin-right:2px}#controlKit .panel .menu .button-menu-load,#controlKit .panel .menu .button-menu-save,#controlKit .panel .menu-active .button-menu-load,#controlKit .panel .menu-active .button-menu-save{background:#1a1d1f;font-size:9px!important}#controlKit .panel .menu .button-menu-load:hover,#controlKit .panel .menu .button-menu-save:hover,#controlKit .panel .menu-active .button-menu-load:hover,#controlKit .panel .menu-active .button-menu-save:hover{background:#000}#controlKit .panel .menu .wrap{display:none}#controlKit .panel .menu-active{width:100%;float:left}#controlKit .panel .menu-active .wrap{display:inline}#controlKit .panel .menu-active .button-menu-close,#controlKit .panel .menu-active .button-menu-hide,#controlKit .panel .menu-active .button-menu-show{float:right}#controlKit .panel .arrow-s-max{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAFCAYAAAB4ka1VAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAG5JREFUeNpi5ODiamRgYKhjwA4amVx8gxjmL1rC8P3rVxQ8b+ESBhffIAZmNR29A5evXWdiZGC019XSZGBgYGBYvmY9w7I16xoZGBgaWKBG1S9bs+4/AwNDPQMDA1ySgYGBgdEnPAbZzgY0mgEwAE9lJT1lrsffAAAAAElFTkSuQmCC) center no-repeat}#controlKit .panel .arrow-s-min{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAFCAYAAAB4ka1VAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAFpJREFUeNpsiiEOgDAMRf8SxNJzIYfB1PQkQ7RkZcfBYLnbUAsL4cn3Xkgs6NzXqQAwL+ve3TTGLWcDgKPWd0osiERa3FunuLdIpIkFiEQ2xu8UEosBUPxjzwATSjV/8qlMGAAAAABJRU5ErkJggg==) center no-repeat}#controlKit .panel .arrow-s-max,#controlKit .panel .arrow-s-min{width:100%;height:20px}#controlKit .panel .arrow-b-max{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAFCAYAAAB4ka1VAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAADJJREFUeNpsysENACAMAzE29+jhAxKlPSmveK2aszEIMiHI7UflbChJfx+3AQAA//8DAPLkSamHastxAAAAAElFTkSuQmCC) center no-repeat}#controlKit .panel .arrow-b-min{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAFCAYAAAB4ka1VAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAC9JREFUeNqEjDEOACAQgxh8OD/H2RhPkk40AAj0mKviS2U3Tien0iE3AAAA//8DAEd1NtICV4EuAAAAAElFTkSuQmCC) center no-repeat}#controlKit .panel .arrow-b-sub-max{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAGCAYAAAD68A/GAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAGJJREFUeNpi9AmPYUAGezavq2dgYGBw8Q1qRBZnQVdkae/cAGWjKGZW09FDUWTp4MIgq6DEwMDA4HBo1zYGJXXNg3CFyIpgAF0x86P7dxrQFWFTzOgTHtPAwMBQz4AfNAAGAN1CKPs4NDLvAAAAAElFTkSuQmCC) center no-repeat}#controlKit .panel .arrow-b-sub-min{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAGCAYAAAD68A/GAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAG9JREFUeNp8zrEOQDAAhOG/GESYBbtJvAKD1eKBRN+sL1NN57a7iSDipkvuG06kWSaBlf/IZJoXyqqhrOpPYc2ONZq47XoVvItADHlRfCEJbHHb9QAqeCdAjCe+I4ATPnDw7oEAktelzRp99ftwDACfsS0XAbz4PwAAAABJRU5ErkJggg==) center no-repeat}#controlKit .panel .arrow-b-max,#controlKit .panel .arrow-b-min,#controlKit .panel .arrow-b-sub-max,#controlKit .panel .arrow-b-sub-min{width:10px;height:100%;float:right}#controlKit .picker{pointer-events:auto;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;border-radius:3px;-moz-border-radius:3px;background-color:#3b3b3b;font-family:Arial,sans-serif;font-size:11px;color:#fff;text-shadow:1px 1px #000;overflow:hidden;position:absolute;z-index:2147483631;width:360px;-webkit-touch-callout:none;-webkit-user-select:none;-khtml-user-select:none;-moz-user-select:none;-ms-user-select:none;user-select:none;box-shadow:0 2px 2px rgba(0,0,0,.25)}#controlKit .picker canvas{vertical-align:bottom;cursor:pointer}#controlKit .picker .wrap{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:10px;float:left}#controlKit .picker .field-wrap{padding:3px}#controlKit .picker .slider-wrap{padding:3px 13px 3px 3px}#controlKit .picker .field-wrap,#controlKit .picker .input-wrap,#controlKit .picker .slider-wrap{height:auto;overflow:hidden;float:left}#controlKit .picker .input-wrap{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;border:1px solid #242424;border-radius:2px;-moz-border-radius:2px;width:140px;float:right;padding:5px 10px 1px 0}#controlKit .picker .input-field{width:50%;float:right;margin-bottom:4px}#controlKit .picker .input-field .label{padding:8px 0 0;color:#878787;text-align:center;text-transform:uppercase;font-weight:700;text-shadow:1px 1px #1a1a1a;width:40%}#controlKit .picker .input-field .wrap{padding:0;width:60%;height:auto;float:right}#controlKit .picker .controls-wrap{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;width:100%;height:auto;float:right;padding:9px 0 0}#controlKit .picker .controls-wrap input[type=button]{float:right;width:65px;margin:0 0 0 10px}#controlKit .picker .color-contrast{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;border:none;box-shadow:0 0 0 1px #1f1f1f inset;border-radius:2px;-moz-border-radius:2px;height:25px;padding:3px;width:80%;margin-bottom:4px;float:right}#controlKit .picker .color-contrast div{width:50%;height:100%;float:left}#controlKit .picker input[type=text]{padding:0;text-align:center;width:60%;float:right}#controlKit .picker .wrap .input-wrap:nth-of-type(3){border-bottom-left-radius:0;border-bottom-right-radius:0}#controlKit .picker .wrap .input-wrap:nth-of-type(4){border-top:none;border-top-left-radius:0;border-top-right-radius:0}#controlKit .picker .wrap .input-wrap:nth-of-type(4) .input-field{width:100%}#controlKit .picker .wrap .input-wrap:nth-of-type(4) .input-field .label{width:20%}#controlKit .picker .wrap .input-wrap:nth-of-type(4) input[type=text]{width:80%}#controlKit .picker .field-wrap,#controlKit .picker .slider-wrap{background:#1e2224;border:none;box-shadow:0 0 0 1px #1f1f1f inset;border-radius:2px;-moz-border-radius:2px;position:relative;margin-right:5px}#controlKit .picker .field-wrap .indicator,#controlKit .picker .slider-wrap .indicator{position:absolute;border:2px solid #fff;box-shadow:0 1px black,0 1px #000 inset;cursor:pointer}#controlKit .picker .field-wrap .indicator{width:8px;height:8px;left:50%;top:50%;border-radius:50%;-moz-border-radius:50%}#controlKit .picker .slider-wrap .indicator{width:14px;height:3px;border-radius:8px;-moz-border-radius:8px;left:1px;top:1px}#controlKit .picker .slider-wrap .indicator:after{content:'';width:0;height:0;border-top:4.5px solid transparent;border-bottom:4.5px solid transparent;border-right:4px solid #fff;float:right;position:absolute;top:-2px;left:19px}#controlKit .picker .slider-wrap .indicator:before{content:'';width:0;height:0;border-top:4.5px solid transparent;border-bottom:4.5px solid transparent;border-right:4px solid #000;float:right;position:absolute;top:-3px;left:19px}"
	};
	module.exports = Style;
});

var ControlKit$2 = createCommonjsModule(function (module) {
    var Node$$1 = Node,
        Panel$$1 = Panel,
        Options$$1 = Options,
        Picker$$1 = Picker;

    var CSS$$1 = CSS;

    var EventDispatcher$$1 = EventDispatcher,
        Event_ = Event$1,
        DocumentEvent$$1 = DocumentEvent,
        NodeEvent$$1 = NodeEvent,
        ComponentEvent$$1 = ComponentEvent,
        HistoryEvent$$1 = HistoryEvent,
        MenuEvent$$1 = MenuEvent;

    var History$$1 = History,
        State$$1 = State;

    var Mouse$$1 = Mouse;

    var ValuePlotter$$1 = ValuePlotter;
    var StringOutput = require$$2$12,
        NumberOutput$$1 = NumberOutput;

    var DEFAULT_HISTORY = false,
        DEFAULT_OPACITY = 1.0,
        DEFAULT_PANELS_CLOSABLE = false,
        DEFAULT_ENABLE = true,
        DEFAULT_LOAD_AND_SAVE = false;

    var DEFAULT_TRIGGER_SHORTCUT_CHAR = 'h';

    var initiated = false;

    /**
     * Initializes ControlKit.
     * @param {Object} [options] - ControlKit options
     * @param {Number} [options.opacity=1.0] - Overall opacity
     * @param {Boolean} [options.enable=true] - Initial ControlKit state, enabled / disabled
     * @param {Boolean} [options.useExternalStyle=false] - If true, an external style is used instead of the build-in one
     * @param {String} [options.styleString] - If true, an external style is used instead of the build-in one
     * @param {Boolean}[options.history=false] - (Experimental) Enables a value history for all components
     */
    function ControlKit(options) {
        if (initiated) {
            throw new Error('ControlKit is already initialized.');
        }
        options = options || {};
        options.history = options.history === undefined ? DEFAULT_HISTORY : options.history;
        options.loadAndSave = options.loadAndSave === undefined ? DEFAULT_LOAD_AND_SAVE : options.loadAndSave;
        options.opacity = options.opacity === undefined ? DEFAULT_OPACITY : options.opacity;
        options.panelsClosable = options.panelsClosable === undefined ? DEFAULT_PANELS_CLOSABLE : options.panelsClosable;
        options.useExternalStyle = options.useExternalStyle === undefined ? false : options.useExternalStyle;
        options.enable = options.enable === undefined ? DEFAULT_ENABLE : options.enable;

        EventDispatcher$$1.apply(this, arguments);

        var node = null;
        if (!options.parentDomElementId) {
            node = new Node$$1();
            document.body.appendChild(node.getElement());
        } else {
            node = Node$$1.getNodeById(options.parentDomElementId);
        }

        if (!options.useExternalStyle) {
            var style = document.createElement('style');
            style.type = 'text/css';
            var css = !options.style ? Style.string : options.styleString;
            if (style.stylesheet) {
                style.stylesheet.cssText = css;
            } else {
                style.appendChild(document.createTextNode(css));
            }
            (document.head || document.getElementsByTagName('head')[0]).appendChild(style);
        }

        node.setProperty('id', CSS$$1.ControlKit);

        this._node = node;
        this._panels = [];
        this._enabled = options.enable;
        this._historyEnabled = options.history;
        this._statesEnabled = options.loadAndSave;
        this._panelsClosable = options.panelsClosable;

        var history = History$$1.setup();

        if (!this._historyEnabled) {
            history.disable();
        } else {
            history.addEventListener(HistoryEvent$$1.STATE_PUSH, this, 'onHistoryStatePush');
            history.addEventListener(HistoryEvent$$1.STATE_POP, this, 'onHistoryStatePop');
        }

        Mouse$$1.setup();
        Picker$$1.setup(node);
        Options$$1.setup(node);

        var opacity = options.opacity;
        if (opacity != 1.0 && opacity != 0.0) {
            node.setStyleProperty('opacity', opacity);
        }

        this._canUpdate = true;

        var self = this;

        var interval,
            count = 0,
            countMax = 10;

        window.addEventListener(DocumentEvent$$1.WINDOW_RESIZE, function () {
            self._canUpdate = false;
            clearInterval(interval);
            interval = setInterval(function () {
                if (count >= countMax) {
                    count = 0;
                    self._canUpdate = true;
                    clearInterval(interval);
                }
                count++;
            }, 25);
        });

        this._shortcutEnable = DEFAULT_TRIGGER_SHORTCUT_CHAR;

        document.addEventListener('keydown', function (e) {
            if (!(e.ctrlKey && String.fromCharCode(e.which || e.keyCode).toLowerCase() == self._shortcutEnable)) {
                return;
            }
            self._enabled = !self._enabled;
            if (self._enabled) {
                self._enable();
            } else {
                self._disable();
            }
        });

        if (!this._enabled) {
            this._disable();
        }

        initiated = true;
    }
    ControlKit.prototype = Object.create(EventDispatcher$$1.prototype);
    ControlKit.prototype.constructor = ControlKit;

    /**
     * Adds a panel.
     * @param {Object} [params] - Panel options
     * @param {String} [params.label='Control Panel'] - The panel label
     * @param {Number} [params.width=300] - The width
     * @param {Number} [params.height] - Constrained panel height
     * @param {Number} [params.ratio=40] - The ratio of label (default:40%) and component (default:60%) width
     * @param {String} [params.align='right'] - Float 'left' or 'right', multiple panels get aligned next to each other
     * @param {Boolean} [params.fixed=true] - If false the panel can be moved
     * @param {Array} [params.position=[0,0]] - If unfixed, the panel panel position relative to alignment (eg. if 'left' 0 + position[0] or if 'right' window.innerHeight - position[0] - panelWidth)
     * @param {Number} [params.opacity=1.0] - The panel´s opacity
     * @param {String} [params.dock=false] - (Experimental) Indicates whether the panel should be docked to either the left or right window border (depending on params.align), docked panels height equal window height
      * @returns {Panel}
     */
    ControlKit.prototype.addPanel = function (params) {
        var panel = new Panel$$1(this, params);
        this._panels.push(panel);
        return panel;
    };

    /**
     * Updates all ControlKit components if the wat
     */
    ControlKit.prototype.update = function () {
        if (!this._enabled || !this._canUpdate) {
            return;
        }
        var i, j, k;
        var l, m, n;
        var panels = this._panels,
            panel,
            groups,
            components,
            component;

        i = -1;l = panels.length;
        while (++i < l) {
            panel = panels[i];

            if (panel.isDisabled()) {
                continue;
            }
            groups = panel.getGroups();
            j = -1;m = groups.length;

            while (++j < m) {
                components = groups[j].getComponents();
                k = -1;n = components.length;

                while (++k < n) {
                    component = components[k];
                    if (component.isDisabled()) {
                        continue;
                    }
                    if (component instanceof ValuePlotter$$1 || component instanceof StringOutput || component instanceof NumberOutput$$1) {
                        component.update();
                    }
                }
            }
        }
    };

    ControlKit.prototype.historyIsEnabled = function () {
        return this._historyEnabled;
    };

    ControlKit.prototype.statesAreEnabled = function () {
        return this._statesEnabled;
    };

    ControlKit.prototype.panelsAreClosable = function () {
        return this._panelsClosable;
    };

    ControlKit.prototype._enable = function () {
        var i = -1,
            p = this._panels,
            l = p.length;
        while (++i < l) {
            p[i].enable();
        }
        this._node.setStyleProperty('visibility', '');
    };

    ControlKit.prototype._disable = function () {
        var i = -1,
            p = this._panels,
            l = p.length;
        while (++i < l) {
            p[i].disable();
        }
        this._node.setStyleProperty('visibility', 'hidden');
    };

    /**
     * Enables and shows controlKit.
     */

    ControlKit.prototype.enable = function () {
        this._enable();
        this._enabled = true;
    };

    /**
     * Disable and hides controlKit.
     */

    ControlKit.prototype.disable = function () {
        this._disable();
        this._enabled = false;
    };

    /**
     * Specifies the key to be used with ctrl & char, to trigger ControlKits visibility.
     * @param char
     */

    ControlKit.prototype.setShortcutEnable = function (char) {
        this._shortcutEnable = char;
    };

    ControlKit.prototype.onHistoryStatePush = function () {
        this.dispatchEvent(new Event_(this, MenuEvent$$1.UPDATE_MENU, null));
    };

    ControlKit.prototype.onHistoryStatePop = function () {
        this.dispatchEvent(new Event_(this, ComponentEvent$$1.UPDATE_VALUE, { origin: null }));
        this.dispatchEvent(new Event_(this, MenuEvent$$1.UPDATE_MENU, null));
    };

    ControlKit.prototype.loadSettings = function (data) {
        var i = -1,
            l = data.length;
        var panels = this._panels;
        while (++i < l) {
            panels[i].setData(data[i]);
        }
    };

    ControlKit.prototype._loadState = function () {
        State$$1.load(this.loadSettings.bind(this));
    };

    ControlKit.prototype._saveState = function () {
        this.update(); //force sync
        var p = this._panels,
            i = -1,
            l = p.length;
        var data = new Array(l);
        while (++i < l) {
            data[i] = p[i].getData();
        }
        State$$1.save({ data: data });
    };

    /**
     * Returns the root element.
     * @returns {*}
     */

    ControlKit.prototype.getNode = function () {
        return this._node;
    };

    ControlKit.destroy = function () {
        Mouse$$1.get().destroy();
        Options$$1.get().destroy();
        Picker$$1.get().destroy();
        initiated = false;
    };

    module.exports = ControlKit;
});

var index$23 = createCommonjsModule(function (module) {
	var ControlKit = ControlKit$2;
	ControlKit.Canvas = Canvas;
	ControlKit.SVG = SVG;

	module.exports = ControlKit;
});

// ============= BUILD CORE STUFF =============
// renderer
const gl = createRenderer().setFullscreen().attachToScreen();

// basic camera
const camera = index$1({
    fov: 60.0,
    position: [0, 0, 10],
    near: 0.1,
    far: 10000,
    viewport: [0, 0, window.innerWidth, window.innerHeight]
});
//============ UI =============
window.globalvalues = {
    amplitude: {
        value: 0.0,
        range: [-1, 1]
    }
};
// build ui
var ui = new index$23();
//ui.addPanel()
// .addSlider(window.globalvalues.amplitude,'amplitude','range')


//============ Objects =============
var smoke = new SmokeParticleSystem$1(gl);
var speaker = new Speaker(gl);
var b = new Background(gl, background);

animate();
//============ ANIMATE =============


function animate() {
    gl.clearScreen();
    camera.viewport = [0, 0, window.innerWidth, window.innerHeight];
    camera.update();

    b.draw();
    //smoke.update();
    //smoke.draw(camera)
    speaker.draw(camera);

    requestAnimationFrame(animate);
}

})));
