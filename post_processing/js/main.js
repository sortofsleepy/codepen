(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory() :
	typeof define === 'function' && define.amd ? define(factory) :
	(factory());
}(this, (function () { 'use strict';

window.jraDebug = false;
window.toggleDebug = function () {
    window.jraDebug = !window.jraDebug;
};

/**
 * Logs an error message, only when window.jraDebug is set to true;
 * @param message
 */
function logError(message, renderImmediate) {
    var css = "background:red;color:white; padding:4px;";
    if (window.jraDebug) {
        console.log("%c " + message, css);
    }

    // if we're not in debug mode, we can choose to render this immediately
    if (renderImmediate) {
        console.log("%c " + message, css);
    }
}

/**
 * Checks the context to ensure it has the desired extension enabled
 * @param ctx {WebGLRenderingContext} the webgl context to check
 * @param extension {String} the name of the extension to look for
 */


/**
 * Logs a warning message, only when window.jraDebug is set to true
 * @param message
 */
function logWarn(message, renderImmediate) {
    var css = "background:yellow;color:red; padding:4px;";
    if (window.jraDebug) {
        console.log("%c " + message, css);
    }

    // if we're not in debug mode, we can choose to render this immediately
    if (renderImmediate) {
        console.log("%c " + message, css);
    }
}

/**
 * Logs a regular console.log call, only when window.jraDebug is set to true
 * @param message
 */

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var RendererFormat = function RendererFormat() {
    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : { width: window.innerWidth, height: window.innerHeight };

    this.width = options.width;
    this.height = options.height;
    this.viewportX = 0;
    this.viewportY = 0;
    this.clearColor = [0, 0, 0, 1];
};

RendererFormat.prototype = {
    /**
     * Appends the canvas to the DOM.
     * @param {node} el the element you want to append to. By default will append to body
     */
    attachToScreen: function attachToScreen() {
        var el = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : document.body;

        el.appendChild(this.canvas);
        return this;
    },


    /**
     * Enables an attribute to become instanced, provided that the GPU supports the extension.
     * TODO re-write when WebGL 2 comes along
     * @param attributeLoc the attribute location of the attribute you want to be instanced.
     * @param divisor The divisor setting for that attribute. It is 1 by default which should essentially turn on instancing.
     */
    enableInstancedAttribute: function enableInstancedAttribute(attributeLoc) {
        var divisor = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;


        if (this.hasOwnProperty("ANGLE_instanced_arrays")) {
            var ext = this.ANGLE_instanced_arrays;
            ext.vertexAttribDivisorANGLE(attributeLoc, divisor);
        } else {
            console.warn("Current GPU does not support the ANGLE_instance_arrays extension");
        }
    },


    /**
     * Disables an attribute to become instanced.
     * TODO re-write when WebGL 2 comes along
     * @param attributeLoc the attribute location of the attribute you want to be instanced.
     */
    disableInstancedAttribute: function disableInstancedAttribute(attributeLoc) {
        if (this.hasOwnProperty("ANGLE_instance_arrays")) {
            var ext = this.ANGLE_instanced_arrays;
            ext.vertexAttribDivisorANGLE(attributeLoc, 0);
        } else {
            console.warn("Current GPU does not support the ANGLE_instance_arrays extension");
        }
    },

    /**
     * Runs the drawArraysInstanced command of the context. If the context is
     * webgl 1, it attempts to try and use the extension, if webgl 2, it runs the
     * regular command.
     * @param mode A GLenum specifying the type primitive to render, ie GL_TRIANGLE, etc..:
     * @param first {Number} a number specifying the starting index in the array of vector points.
     * @param count {Number} a number specifying the number of vertices
     * @param primcount {Number} a number specifying the number of instances to draw
     */
    drawInstancedArrays: function drawInstancedArrays(mode, first, count, primcount) {
        if (!this.isWebGL2) {
            if (this.hasOwnProperty("ANGLE_instanced_arrays")) {
                this.ANGLE_instanced_arrays.drawArraysInstancedANGLE(mode, first, count, primcount);
            } else {
                console.error("Unable to draw instanced geometry - extension is not available");
            }
        } else {
            this.drawArraysInstanced(mode, first, count, primcount);
        }
    },


    /**
     * Drawing function to use for instanced items that have indices
     * @param mode {Number} the drawing mode, gl.TRIANGLES, etc..
     * @param numElements {Number} the number of element to draw(aka the number of indices)
     * @param numInstances {Number} the number of instances of the object to draw
     * @param type {Number} the data type of the index data, defaults to gl.UNSIGNED_SHORT
     * @param offset {Number} A GLintptr specifying an offset in the element array buffer. Must be a valid multiple of the size of the given type.
     */
    drawInstancedElements: function drawInstancedElements(mode, numElements, numInstances) {
        var _ref = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : {},
            _ref$type = _ref.type,
            type = _ref$type === undefined ? UNSIGNED_SHORT : _ref$type,
            _ref$offset = _ref.offset,
            offset = _ref$offset === undefined ? 0 : _ref$offset;

        if (!this.isWebGL2) {
            if (this.hasOwnProperty("ANGLE_instanced_arrays")) {
                this.ANGLE_instanced_arrays.drawElementsInstancedANGLE(mode, numElements, type, offset, numInstances);
            } else {
                console.error("Unable to draw instanced geometry - extension is not available");
            }
        } else {
            this.drawElementsInstanced(mode, numElements, type, offset, numInstances);
        }
    },


    /**
     * Sets the context to be fullscreen.
     * @param {function} customResizeCallback
     * @returns {RendererFormat}
     */
    setFullscreen: function setFullscreen() {
        var _this = this;

        var customResizeCallback = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

        var self = this;
        var gl = this;
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;

        //set the viewport size
        this.setViewport();

        if (customResizeCallback) {
            window.addEventListener("resize", customResizeCallback);
        } else {
            window.addEventListener("resize", function () {
                _this.canvas.width = window.innerWidth;
                _this.canvas.height = window.innerHeight;
                _this.setViewport();
            });
        }
        return this;
    },


    /**
     * Helper function for clearing the screen, clear with a clear color,
     * set the viewport and clear the depth and color buffer bits
     * @param {number} r the value for the red channel of the clear color.
     * @param {number} g the value for the green channel of the clear color.
     * @param {number} b the value for the blue channel of the clear color.
     * @param {number} a the value for the alpha channel
     */
    clearScreen: function clearScreen() {
        var r = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var g = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
        var b = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 0;
        var a = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 1;

        var gl = this;
        this.clearColor(r, g, b, a);
        gl.viewport(this.viewportX, this.viewportY, this.canvas.width, this.canvas.height);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        return this;
    },


    /**
     * Enable depth testing
     */
    enableDepth: function enableDepth() {
        this.gl.enable(this.gl.DEPTH_TEST);
        return this;
    },


    /**
     * Disables Depth testing
     */
    disableDepth: function disableDepth() {
        this.gl.disable(this.gl.DEPTH_TEST);
    },


    /**
     * Returns the maximum texture size that the current card
     * supports.
     */
    getMaxTextureSize: function getMaxTextureSize() {
        return this.gl.getParameter(this.gl.MAX_TEXTURE_SIZE);
    },


    /**
     * Sets the viewport for the context
     * @param {number} x the x coordinate for the viewport
     * @param {number} y the y coordinate for the viewport
     * @param {number} width the width for the viewport
     * @param {number} height the height for the viewport
     */
    setViewport: function setViewport() {
        var x = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
        var y = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
        var width = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : window.innerWidth;
        var height = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : window.innerHeight;

        var gl = this;
        gl.viewport(x, y, width, height);
    }
};

/**
 * Get list of all the extensions we want for this
 * @param gl a webgl context
 * @returns {{}}
 */
function getExtensions(gl) {
    var exts = {};

    // common extensions we might want
    var extensions = ["OES_texture_float", "OES_vertex_array_object", "ANGLE_instanced_arrays", "OES_texture_half_float", "OES_texture_float_linear", "OES_texture_half_float_linear", "WEBGL_color_buffer_float", "EXT_color_buffer_half_float", "WEBGL_draw_buffers"];

    extensions.forEach(function (name) {
        // try getting the extension
        var ext = gl.getExtension(name);

        // if debugging is active, show warning message for any missing extensions
        if (ext === null) {
            logWarn("Unable to get extension " + name + ", things might look weird or just plain fail");
        }
        exts[name] = ext;
    });

    return exts;
}

/**
 * Creates a WebGLRendering context
 * @param node an optional node to build the context from. If nothing is provided, we generate a canvas
 * @param options any options for the context
 * @returns {*} the resulting WebGLRenderingContext
 */
function createContext() {
    var node = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    var el = node !== null ? node : document.createElement("canvas");
    var isWebgl2 = false;
    var defaults = {
        alpha: true,
        antialias: true,
        depth: true
    };

    // override any defaults if set
    Object.assign(options, defaults);

    // the possible context flags, try for webgl 2
    var types = ["webgl2", "experimental-webgl2", "webgl", "experimental-webgl"];

    // loop through trying different context settings.
    var ctx = types.map(function (type) {
        var tCtx = el.getContext(type);
        if (tCtx !== null) {
            if (type === "webgl2" || type === "experimental-webgl2") {
                isWebgl2 = true;
            }
            return tCtx;
        }
    }).filter(function (val) {
        if (val !== undefined) {
            return val;
        }
    });

    // make sure to note that this is a webgl 2 context
    if (isWebgl2) {
        ctx[0]["isWebGL2"] = true;
    }
    // just return 1 context
    return ctx[0];
}

/**
 * Sets up some WebGL constant values on top of the
 * window object for ease of use so you don't have to always have a
 * context object handy.
 * @param gl {WebGLRenderingContext} a WebGLRenderingContext
 */
function setupConstants(gl) {
    var _constants;

    var constants = (_constants = {
        "FLOAT": gl.FLOAT,
        "UNSIGNED_BYTE": gl.UNSIGNED_BYTE,
        "UNSIGNED_SHORT": gl.UNSIGNED_SHORT,
        "ARRAY_BUFFER": gl.ARRAY_BUFFER,
        "ELEMENT_BUFFER": gl.ELEMENT_ARRAY_BUFFER,
        "RGBA": gl.RGBA,
        "RGB": gl.RGB,
        "TEXTURE_2D": gl.TEXTURE_2D,
        "STATIC_DRAW": gl.STATIC_DRAW,
        "DYNAMIC_DRAW": gl.DYNAMIC_DRAW,
        "TRIANGLES": gl.TRIANGLES,
        "TRIANGLE_STRIP": gl.TRIANGLE_STRIP,
        "POINTS": gl.POINTS
    }, _defineProperty(_constants, "UNSIGNED_SHORT", gl.UNSIGNED_SHORT), _defineProperty(_constants, "FRAMEBUFFER", gl.FRAMEBUFFER), _defineProperty(_constants, "COLOR_ATTACHMENT0", gl.COLOR_ATTACHMENT0), _defineProperty(_constants, "CLAMP_TO_EDGE", gl.CLAMP_TO_EDGE), _defineProperty(_constants, "LINEAR", gl.LINEAR), _defineProperty(_constants, "MAG_FILTER", gl.TEXTURE_MAG_FILTER), _defineProperty(_constants, "MIN_FILTER", gl.TEXTURE_MIN_FILTER), _defineProperty(_constants, "WRAP_S", gl.TEXTURE_WRAP_S), _defineProperty(_constants, "WRAP_T", gl.TEXTURE_WRAP_T), _defineProperty(_constants, "TEXTURE0", gl.TEXTURE0), _defineProperty(_constants, "TEXTURE1", gl.TEXTURE1), _defineProperty(_constants, "TEXTURE2", gl.TEXTURE2), _defineProperty(_constants, "PI", 3.14149), _defineProperty(_constants, "2_PI", 3.14149 * 3.14149), _constants);
    /**
     * WebGL 2 contexts directly support certain constants
     * that were previously only available via extensions.
     * Add those here.
     *
     * Your context must have a "isWebGL2" property in order for this to get
     * triggered.
     *
     * TODO at some point, should look and see if there might be native way to differentiate between ES 2.0 and 3.0 contexts
     */
    if (gl.hasOwnProperty('isWebGL2')) {

        if (gl.isWebGL2) {

            // add more color attachment constants
            constants["COLOR_ATTACHMENT1"] = gl.COLOR_ATTACHMENT1;
            constants["COLOR_ATTACHMENT2"] = gl.COLOR_ATTACHMENT2;
            constants["COLOR_ATTACHMENT3"] = gl.COLOR_ATTACHMENT3;
            constants["COLOR_ATTACHMENT4"] = gl.COLOR_ATTACHMENT4;
            constants["COLOR_ATTACHMENT5"] = gl.COLOR_ATTACHMENT5;
        }
    }

    if (!window.GL_CONSTANTS_SET) {
        for (var i in constants) {
            window[i] = constants[i];
        }
        window.GL_CONSTANTS_SET = true;
    }
}

/**
 * Builds the WebGLRendering context
 * @param canvas {DomElement} an optional canvas, if you'd rather use one already in the DOM
 * @param ctxOptions {Object} options for the context
 * @param getCommonExtensions {Bool} include the common extensions for doing neat things in WebGL 1
 */
function createRenderer() {
    var canvas = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var ctxOptions = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var getCommonExtensions = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

    var gl = createContext(canvas, ctxOptions);
    var format = new RendererFormat();
    var ext = null;

    if (getCommonExtensions) {
        ext = getExtensions(gl);
    }

    //setup constants
    setupConstants(gl);

    // assign some convenience functions onto the gl context
    var newProps = Object.assign(gl.__proto__, format.__proto__);
    gl.__proto__ = newProps;

    // loop through and assign extensions onto the context as well
    for (var i in ext) {
        gl[i] = ext[i];
    }

    return gl;
}

function ToObject(val) {
	if (val == null) {
		throw new TypeError('Object.assign cannot be called with null or undefined');
	}

	return Object(val);
}

var index$2 = Object.assign || function (target, source) {
	var from;
	var keys;
	var to = ToObject(target);

	for (var s = 1; s < arguments.length; s++) {
		from = arguments[s];
		keys = Object.keys(Object(from));

		for (var i = 0; i < keys.length; i++) {
			to[keys[i]] = from[keys[i]];
		}
	}

	return to;
};

var cross_1 = cross$1;

/**
 * Computes the cross product of two vec3's
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {vec3} out
 */
function cross$1(out, a, b) {
    var ax = a[0],
        ay = a[1],
        az = a[2],
        bx = b[0],
        by = b[1],
        bz = b[2];

    out[0] = ay * bz - az * by;
    out[1] = az * bx - ax * bz;
    out[2] = ax * by - ay * bx;
    return out;
}

var dot_1 = dot$1;

/**
 * Calculates the dot product of two vec3's
 *
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {Number} dot product of a and b
 */
function dot$1(a, b) {
  return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}

var subtract_1 = subtract;

/**
 * Subtracts vector b from vector a
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {vec3} out
 */
function subtract(out, a, b) {
  out[0] = a[0] - b[0];
  out[1] = a[1] - b[1];
  out[2] = a[2] - b[2];
  return out;
}

var cross = cross_1;
var dot = dot_1;
var sub = subtract_1;

var EPSILON = 0.000001;
var edge1 = [0, 0, 0];
var edge2 = [0, 0, 0];
var tvec = [0, 0, 0];
var pvec = [0, 0, 0];
var qvec = [0, 0, 0];

var index$6 = intersectTriangle;

function intersectTriangle(out, pt, dir, tri) {
    sub(edge1, tri[1], tri[0]);
    sub(edge2, tri[2], tri[0]);

    cross(pvec, dir, edge2);
    var det = dot(edge1, pvec);

    if (det < EPSILON) return null;
    sub(tvec, pt, tri[0]);
    var u = dot(tvec, pvec);
    if (u < 0 || u > det) return null;
    cross(qvec, tvec, edge1);
    var v = dot(dir, qvec);
    if (v < 0 || u + v > det) return null;

    var t = dot(edge2, qvec) / det;
    out[0] = pt[0] + t * dir[0];
    out[1] = pt[1] + t * dir[1];
    out[2] = pt[2] + t * dir[2];
    return out;
}

var add_1 = add$3;

/**
 * Adds two vec3's
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {vec3} out
 */
function add$3(out, a, b) {
  out[0] = a[0] + b[0];
  out[1] = a[1] + b[1];
  out[2] = a[2] + b[2];
  return out;
}

var scale_1 = scale$1;

/**
 * Scales a vec3 by a scalar number
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the vector to scale
 * @param {Number} b amount to scale the vector by
 * @returns {vec3} out
 */
function scale$1(out, a, b) {
  out[0] = a[0] * b;
  out[1] = a[1] * b;
  out[2] = a[2] * b;
  return out;
}

var copy_1 = copy$1;

/**
 * Copy the values from one vec3 to another
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the source vector
 * @returns {vec3} out
 */
function copy$1(out, a) {
  out[0] = a[0];
  out[1] = a[1];
  out[2] = a[2];
  return out;
}

var dot$2 = dot_1;
var add$2 = add_1;
var scale = scale_1;
var copy = copy_1;

var index$8 = intersectRayPlane$1;

var v0 = [0, 0, 0];

function intersectRayPlane$1(out, origin, direction, normal, dist) {
  var denom = dot$2(direction, normal);
  if (denom !== 0) {
    var t = -(dot$2(origin, normal) + dist) / denom;
    if (t < 0) {
      return null;
    }
    scale(v0, direction, t);
    return add$2(out, origin, v0);
  } else if (dot$2(normal, origin) + dist === 0) {
    return copy(out, origin);
  } else {
    return null;
  }
}

var squaredDistance_1 = squaredDistance;

/**
 * Calculates the squared euclidian distance between two vec3's
 *
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @returns {Number} squared distance between a and b
 */
function squaredDistance(a, b) {
    var x = b[0] - a[0],
        y = b[1] - a[1],
        z = b[2] - a[2];
    return x * x + y * y + z * z;
}

var scaleAndAdd_1 = scaleAndAdd$1;

/**
 * Adds two vec3's after scaling the second operand by a scalar value
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a the first operand
 * @param {vec3} b the second operand
 * @param {Number} scale the amount to scale b by before adding
 * @returns {vec3} out
 */
function scaleAndAdd$1(out, a, b, scale) {
  out[0] = a[0] + b[0] * scale;
  out[1] = a[1] + b[1] * scale;
  out[2] = a[2] + b[2] * scale;
  return out;
}

var squaredDist = squaredDistance_1;
var dot$3 = dot_1;
var sub$1 = subtract_1;
var scaleAndAdd = scaleAndAdd_1;
var scale$2 = scale_1;
var add$4 = add_1;

var tmp = [0, 0, 0];

var index$10 = intersectRaySphere$1;
function intersectRaySphere$1(out, origin, direction, center, radius) {
  sub$1(tmp, center, origin);
  var len = dot$3(direction, tmp);
  if (len < 0) {
    // sphere is behind ray
    return null;
  }

  scaleAndAdd(tmp, origin, direction, len);
  var dSq = squaredDist(center, tmp);
  var rSq = radius * radius;
  if (dSq > rSq) {
    return null;
  }

  scale$2(out, direction, len - Math.sqrt(rSq - dSq));
  return add$4(out, out, origin);
}

var index$12 = intersection;
var distance_1 = distance;

function intersection(out, ro, rd, aabb) {
  var d = distance(ro, rd, aabb);
  if (d === Infinity) {
    out = null;
  } else {
    out = out || [];
    for (var i = 0; i < ro.length; i++) {
      out[i] = ro[i] + rd[i] * d;
    }
  }

  return out;
}

function distance(ro, rd, aabb) {
  var dims = ro.length;
  var lo = -Infinity;
  var hi = +Infinity;

  for (var i = 0; i < dims; i++) {
    var dimLo = (aabb[0][i] - ro[i]) / rd[i];
    var dimHi = (aabb[1][i] - ro[i]) / rd[i];

    if (dimLo > dimHi) {
      var tmp = dimLo;
      dimLo = dimHi;
      dimHi = tmp;
    }

    if (dimHi < lo || dimLo > hi) {
      return Infinity;
    }

    if (dimLo > lo) lo = dimLo;
    if (dimHi < hi) hi = dimHi;
  }

  return lo > hi ? Infinity : lo;
}

index$12.distance = distance_1;

var intersectRayTriangle = index$6;
var intersectRayPlane = index$8;
var intersectRaySphere = index$10;
var intersectRayBox = index$12;
var copy3 = copy_1;

var tmpTriangle = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];

var tmp3 = [0, 0, 0];

var index$4 = Ray$1;
function Ray$1(origin, direction) {
  this.origin = origin || [0, 0, 0];
  this.direction = direction || [0, 0, -1];
}

Ray$1.prototype.set = function (origin, direction) {
  this.origin = origin;
  this.direction = direction;
};

Ray$1.prototype.copy = function (other) {
  copy3(this.origin, other.origin);
  copy3(this.direction, other.direction);
};

Ray$1.prototype.clone = function () {
  var other = new Ray$1();
  other.copy(this);
  return other;
};

Ray$1.prototype.intersectsSphere = function (center, radius) {
  return intersectRaySphere(tmp3, this.origin, this.direction, center, radius);
};

Ray$1.prototype.intersectsPlane = function (normal, distance) {
  return intersectRayPlane(tmp3, this.origin, this.direction, normal, distance);
};

Ray$1.prototype.intersectsTriangle = function (triangle) {
  return intersectRayTriangle(tmp3, this.origin, this.direction, triangle);
};

Ray$1.prototype.intersectsBox = function (aabb) {
  return intersectRayBox(tmp3, this.origin, this.direction, aabb);
};

Ray$1.prototype.intersectsTriangleCell = function (cell, positions) {
  var a = cell[0],
      b = cell[1],
      c = cell[2];
  tmpTriangle[0] = positions[a];
  tmpTriangle[1] = positions[b];
  tmpTriangle[2] = positions[c];
  return this.intersectsTriangle(tmpTriangle);
};

var transformMat4_1 = transformMat4$1;

/**
 * Transforms the vec4 with a mat4.
 *
 * @param {vec4} out the receiving vector
 * @param {vec4} a the vector to transform
 * @param {mat4} m matrix to transform with
 * @returns {vec4} out
 */
function transformMat4$1(out, a, m) {
  var x = a[0],
      y = a[1],
      z = a[2],
      w = a[3];
  out[0] = m[0] * x + m[4] * y + m[8] * z + m[12] * w;
  out[1] = m[1] * x + m[5] * y + m[9] * z + m[13] * w;
  out[2] = m[2] * x + m[6] * y + m[10] * z + m[14] * w;
  out[3] = m[3] * x + m[7] * y + m[11] * z + m[15] * w;
  return out;
}

var set_1 = set$1;

/**
 * Set the components of a vec4 to the given values
 *
 * @param {vec4} out the receiving vector
 * @param {Number} x X component
 * @param {Number} y Y component
 * @param {Number} z Z component
 * @param {Number} w W component
 * @returns {vec4} out
 */
function set$1(out, x, y, z, w) {
  out[0] = x;
  out[1] = y;
  out[2] = z;
  out[3] = w;
  return out;
}

var transformMat4 = transformMat4_1;
var set = set_1;

var NEAR_RANGE = 0;
var FAR_RANGE = 1;
var tmp4 = [0, 0, 0, 0];

var index$14 = cameraProject$1;
function cameraProject$1(out, vec, viewport, combinedProjView) {
  var vX = viewport[0],
      vY = viewport[1],
      vWidth = viewport[2],
      vHeight = viewport[3],
      n = NEAR_RANGE,
      f = FAR_RANGE;

  // convert: clip space -> NDC -> window coords
  // implicit 1.0 for w component
  set(tmp4, vec[0], vec[1], vec[2], 1.0);

  // transform into clip space
  transformMat4(tmp4, tmp4, combinedProjView);

  // now transform into NDC
  var w = tmp4[3];
  if (w !== 0) {
    // how to handle infinity here?
    tmp4[0] = tmp4[0] / w;
    tmp4[1] = tmp4[1] / w;
    tmp4[2] = tmp4[2] / w;
  }

  // and finally into window coordinates
  // the foruth component is (1/clip.w)
  // which is the same as gl_FragCoord.w
  out[0] = vX + vWidth / 2 * tmp4[0] + (0 + vWidth / 2);
  out[1] = vY + vHeight / 2 * tmp4[1] + (0 + vHeight / 2);
  out[2] = (f - n) / 2 * tmp4[2] + (f + n) / 2;
  out[3] = w === 0 ? 0 : 1 / w;
  return out;
}

var projectMat4 = project;

/**
 * Multiplies the input vec by the specified matrix, 
 * applying a W divide, and stores the result in out 
 * vector. This is useful for projection,
 * e.g. unprojecting a 2D point into 3D space.
 *
 * @method  prj
 * @param {vec3} out the output vector
 * @param {vec3} vec the input vector to project
 * @param {mat4} m the 4x4 matrix to multiply with 
 * @return {vec3} the out vector
 */
function project(out, vec, m) {
  var x = vec[0],
      y = vec[1],
      z = vec[2],
      a00 = m[0],
      a01 = m[1],
      a02 = m[2],
      a03 = m[3],
      a10 = m[4],
      a11 = m[5],
      a12 = m[6],
      a13 = m[7],
      a20 = m[8],
      a21 = m[9],
      a22 = m[10],
      a23 = m[11],
      a30 = m[12],
      a31 = m[13],
      a32 = m[14],
      a33 = m[15];

  var lw = 1 / (x * a03 + y * a13 + z * a23 + a33);

  out[0] = (x * a00 + y * a10 + z * a20 + a30) * lw;
  out[1] = (x * a01 + y * a11 + z * a21 + a31) * lw;
  out[2] = (x * a02 + y * a12 + z * a22 + a32) * lw;
  return out;
}

var transform = projectMat4;

var index$16 = unproject;

/**
 * Unproject a point from screen space to 3D space.
 * The point should have its x and y properties set to
 * 2D screen space, and the z either at 0 (near plane)
 * or 1 (far plane). The provided matrix is assumed to already
 * be combined, i.e. projection * view.
 *
 * After this operation, the out vector's [x, y, z] components will
 * represent the unprojected 3D coordinate.
 *
 * @param  {vec3} out               the output vector
 * @param  {vec3} vec               the 2D space vector to unproject
 * @param  {vec4} viewport          screen x, y, width and height in pixels
 * @param  {mat4} invProjectionView combined projection and view matrix
 * @return {vec3}                   the output vector
 */
function unproject(out, vec, viewport, invProjectionView) {
  var viewX = viewport[0],
      viewY = viewport[1],
      viewWidth = viewport[2],
      viewHeight = viewport[3];

  var x = vec[0],
      y = vec[1],
      z = vec[2];

  x = x - viewX;
  y = viewHeight - y - 1;
  y = y - viewY;

  out[0] = 2 * x / viewWidth - 1;
  out[1] = 2 * y / viewHeight - 1;
  out[2] = 2 * z - 1;
  return transform(out, out, invProjectionView);
}

var normalize_1 = normalize$1;

/**
 * Normalize a vec3
 *
 * @param {vec3} out the receiving vector
 * @param {vec3} a vector to normalize
 * @returns {vec3} out
 */
function normalize$1(out, a) {
    var x = a[0],
        y = a[1],
        z = a[2];
    var len = x * x + y * y + z * z;
    if (len > 0) {
        //TODO: evaluate use of glm_invsqrt here?
        len = 1 / Math.sqrt(len);
        out[0] = a[0] * len;
        out[1] = a[1] * len;
        out[2] = a[2] * len;
    }
    return out;
}

// could be modularized...
var cross$2 = cross_1;
var sub$2 = subtract_1;
var normalize = normalize_1;
var copy$2 = copy_1;
var dot$4 = dot_1;
var scale$3 = scale_1;

var tmp$1 = [0, 0, 0];
var epsilon = 0.000000001;

// modifies direction & up vectors in place
var cameraLookAt$1 = function cameraLookAt$1(direction, up, position, target) {
  sub$2(tmp$1, target, position);
  normalize(tmp$1, tmp$1);
  var isZero = tmp$1[0] === 0 && tmp$1[1] === 0 && tmp$1[2] === 0;
  if (!isZero) {
    var d = dot$4(tmp$1, up);
    if (Math.abs(d - 1) < epsilon) {
      // collinear
      scale$3(up, direction, -1);
    } else if (Math.abs(d + 1) < epsilon) {
      // collinear opposite
      copy$2(up, direction);
    }
    copy$2(direction, tmp$1);

    // normalize up vector
    cross$2(tmp$1, direction, up);
    normalize(tmp$1, tmp$1);

    cross$2(up, tmp$1, direction);
    normalize(up, up);
  }
};

var set_1$2 = set$3;

/**
 * Set the components of a vec3 to the given values
 *
 * @param {vec3} out the receiving vector
 * @param {Number} x X component
 * @param {Number} y Y component
 * @param {Number} z Z component
 * @returns {vec3} out
 */
function set$3(out, x, y, z) {
  out[0] = x;
  out[1] = y;
  out[2] = z;
  return out;
}

var unproject$1 = index$16;
var set$2 = set_1$2;
var sub$3 = subtract_1;
var normalize$2 = normalize_1;

var index$18 = createPickRay;
function createPickRay(origin, direction, point, viewport, invProjView) {
  set$2(origin, point[0], point[1], 0);
  set$2(direction, point[0], point[1], 1);
  unproject$1(origin, origin, viewport, invProjView);
  unproject$1(direction, direction, viewport, invProjView);
  sub$3(direction, direction, origin);
  normalize$2(direction, direction);
}

var multiply_1 = multiply;

/**
 * Multiplies two mat4's
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the first operand
 * @param {mat4} b the second operand
 * @returns {mat4} out
 */
function multiply(out, a, b) {
    var a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11],
        a30 = a[12],
        a31 = a[13],
        a32 = a[14],
        a33 = a[15];

    // Cache only the current line of the second matrix
    var b0 = b[0],
        b1 = b[1],
        b2 = b[2],
        b3 = b[3];
    out[0] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[1] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[2] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[3] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

    b0 = b[4];b1 = b[5];b2 = b[6];b3 = b[7];
    out[4] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[5] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[6] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[7] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

    b0 = b[8];b1 = b[9];b2 = b[10];b3 = b[11];
    out[8] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[9] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[10] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[11] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

    b0 = b[12];b1 = b[13];b2 = b[14];b3 = b[15];
    out[12] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[13] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[14] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[15] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;
    return out;
}

var invert_1 = invert;

/**
 * Inverts a mat4
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
function invert(out, a) {
    var a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11],
        a30 = a[12],
        a31 = a[13],
        a32 = a[14],
        a33 = a[15],
        b00 = a00 * a11 - a01 * a10,
        b01 = a00 * a12 - a02 * a10,
        b02 = a00 * a13 - a03 * a10,
        b03 = a01 * a12 - a02 * a11,
        b04 = a01 * a13 - a03 * a11,
        b05 = a02 * a13 - a03 * a12,
        b06 = a20 * a31 - a21 * a30,
        b07 = a20 * a32 - a22 * a30,
        b08 = a20 * a33 - a23 * a30,
        b09 = a21 * a32 - a22 * a31,
        b10 = a21 * a33 - a23 * a31,
        b11 = a22 * a33 - a23 * a32,


    // Calculate the determinant
    det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

    if (!det) {
        return null;
    }
    det = 1.0 / det;

    out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
    out[1] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
    out[2] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
    out[3] = (a22 * b04 - a21 * b05 - a23 * b03) * det;
    out[4] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
    out[5] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
    out[6] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
    out[7] = (a20 * b05 - a22 * b02 + a23 * b01) * det;
    out[8] = (a10 * b10 - a11 * b08 + a13 * b06) * det;
    out[9] = (a01 * b08 - a00 * b10 - a03 * b06) * det;
    out[10] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
    out[11] = (a21 * b02 - a20 * b04 - a23 * b00) * det;
    out[12] = (a11 * b07 - a10 * b09 - a12 * b06) * det;
    out[13] = (a00 * b09 - a01 * b07 + a02 * b06) * det;
    out[14] = (a31 * b01 - a30 * b03 - a32 * b00) * det;
    out[15] = (a20 * b03 - a21 * b01 + a22 * b00) * det;

    return out;
}

var identity_1 = identity;

/**
 * Set a mat4 to the identity matrix
 *
 * @param {mat4} out the receiving matrix
 * @returns {mat4} out
 */
function identity(out) {
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
}

var assign$1 = index$2;
var Ray = index$4;

var cameraProject = index$14;
var cameraUnproject = index$16;
var cameraLookAt = cameraLookAt$1;
var cameraPickRay = index$18;

var add$1 = add_1;
var multiply4x4 = multiply_1;
var invert4x4 = invert_1;
var identity4x4 = identity_1;
var setVec3$1 = set_1$2;

// this could also be useful for a orthographic camera
var cameraBase = function cameraBase(opt) {
  opt = opt || {};

  var camera = {
    projection: identity4x4([]),
    view: identity4x4([]),
    position: opt.position || [0, 0, 0],
    direction: opt.direction || [0, 0, -1],
    up: opt.up || [0, 1, 0],
    viewport: opt.viewport || [-1, -1, 1, 1],
    projView: identity4x4([]),
    invProjView: identity4x4([])
  };

  function update() {
    multiply4x4(camera.projView, camera.projection, camera.view);
    var valid = invert4x4(camera.invProjView, camera.projView);
    if (!valid) {
      throw new Error('camera projection * view is non-invertible');
    }
  }

  function lookAt(target) {
    cameraLookAt(camera.direction, camera.up, camera.position, target);
    return camera;
  }

  function identity() {
    setVec3$1(camera.position, 0, 0, 0);
    setVec3$1(camera.direction, 0, 0, -1);
    setVec3$1(camera.up, 0, 1, 0);
    identity4x4(camera.view);
    identity4x4(camera.projection);
    identity4x4(camera.projView);
    identity4x4(camera.invProjView);
    return camera;
  }

  function translate(vec) {
    add$1(camera.position, camera.position, vec);
    return camera;
  }

  function createPickingRay(mouse) {
    var ray = new Ray();
    cameraPickRay(ray.origin, ray.direction, mouse, camera.viewport, camera.invProjView);
    return ray;
  }

  function project(point) {
    return cameraProject([], point, camera.viewport, camera.projView);
  }

  function unproject(point) {
    return cameraUnproject([], point, camera.viewport, camera.invProjView);
  }

  return assign$1(camera, {
    translate: translate,
    identity: identity,
    lookAt: lookAt,
    createPickingRay: createPickingRay,
    update: update,
    project: project,
    unproject: unproject
  });
};

var index$20 = function index$20() {
    for (var i = 0; i < arguments.length; i++) {
        if (arguments[i] !== undefined) return arguments[i];
    }
};

var perspective_1 = perspective$1;

/**
 * Generates a perspective projection matrix with the given bounds
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {number} fovy Vertical field of view in radians
 * @param {number} aspect Aspect ratio. typically viewport width/height
 * @param {number} near Near bound of the frustum
 * @param {number} far Far bound of the frustum
 * @returns {mat4} out
 */
function perspective$1(out, fovy, aspect, near, far) {
    var f = 1.0 / Math.tan(fovy / 2),
        nf = 1 / (near - far);
    out[0] = f / aspect;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = f;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = (far + near) * nf;
    out[11] = -1;
    out[12] = 0;
    out[13] = 0;
    out[14] = 2 * far * near * nf;
    out[15] = 0;
    return out;
}

var identity$1 = identity_1;

var lookAt_1 = lookAt;

/**
 * Generates a look-at matrix with the given eye position, focal point, and up axis
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {vec3} eye Position of the viewer
 * @param {vec3} center Point the viewer is looking at
 * @param {vec3} up vec3 pointing up
 * @returns {mat4} out
 */
function lookAt(out, eye, center, up) {
    var x0,
        x1,
        x2,
        y0,
        y1,
        y2,
        z0,
        z1,
        z2,
        len,
        eyex = eye[0],
        eyey = eye[1],
        eyez = eye[2],
        upx = up[0],
        upy = up[1],
        upz = up[2],
        centerx = center[0],
        centery = center[1],
        centerz = center[2];

    if (Math.abs(eyex - centerx) < 0.000001 && Math.abs(eyey - centery) < 0.000001 && Math.abs(eyez - centerz) < 0.000001) {
        return identity$1(out);
    }

    z0 = eyex - centerx;
    z1 = eyey - centery;
    z2 = eyez - centerz;

    len = 1 / Math.sqrt(z0 * z0 + z1 * z1 + z2 * z2);
    z0 *= len;
    z1 *= len;
    z2 *= len;

    x0 = upy * z2 - upz * z1;
    x1 = upz * z0 - upx * z2;
    x2 = upx * z1 - upy * z0;
    len = Math.sqrt(x0 * x0 + x1 * x1 + x2 * x2);
    if (!len) {
        x0 = 0;
        x1 = 0;
        x2 = 0;
    } else {
        len = 1 / len;
        x0 *= len;
        x1 *= len;
        x2 *= len;
    }

    y0 = z1 * x2 - z2 * x1;
    y1 = z2 * x0 - z0 * x2;
    y2 = z0 * x1 - z1 * x0;

    len = Math.sqrt(y0 * y0 + y1 * y1 + y2 * y2);
    if (!len) {
        y0 = 0;
        y1 = 0;
        y2 = 0;
    } else {
        len = 1 / len;
        y0 *= len;
        y1 *= len;
        y2 *= len;
    }

    out[0] = x0;
    out[1] = y0;
    out[2] = z0;
    out[3] = 0;
    out[4] = x1;
    out[5] = y1;
    out[6] = z1;
    out[7] = 0;
    out[8] = x2;
    out[9] = y2;
    out[10] = z2;
    out[11] = 0;
    out[12] = -(x0 * eyex + x1 * eyey + x2 * eyez);
    out[13] = -(y0 * eyex + y1 * eyey + y2 * eyez);
    out[14] = -(z0 * eyex + z1 * eyey + z2 * eyez);
    out[15] = 1;

    return out;
}

var create = cameraBase;
var assign = index$2;
var defined = index$20;

var perspective = perspective_1;
var lookAt4x4 = lookAt_1;
var add = add_1;

var cameraPerspective = function cameraPerspective(opt) {
  opt = opt || {};

  var camera = create(opt);
  camera.fov = defined(opt.fov, Math.PI / 4);
  camera.near = defined(opt.near, 1);
  camera.far = defined(opt.far, 100);

  var center = [0, 0, 0];

  var updateCombined = camera.update;

  function update() {
    var aspect = camera.viewport[2] / camera.viewport[3];

    // build projection matrix
    perspective(camera.projection, camera.fov, aspect, Math.abs(camera.near), Math.abs(camera.far));

    // build view matrix
    add(center, camera.position, camera.direction);
    lookAt4x4(camera.view, camera.position, center, camera.up);

    // update projection * view and invert
    updateCombined();
    return camera;
  }

  // set it up initially from constructor options
  update();
  return assign(camera, {
    update: update
  });
};

var index$1 = cameraPerspective;

/**
 * A stand alone function for creating data based textures with TypedArrays.
 * Usable on it's own, but recommended that you use the {@link createTexture2d}
 * function
 * @param gl {WebGLRenderingContext} a WebGLRenderingContext
 * @param data {TypedArray} a TypedArray of data you want to write onto the texture
 * @param options {Object} a map of options for the texture creation
 * @returns {*}
 */
function createDataTexture(gl, data, options) {
    var texture = gl.createTexture();
    gl.bindTexture(TEXTURE_2D, texture);
    gl.texImage2D(TEXTURE_2D, 0, options.internalFormat, options.width, options.height, 0, options.format, options.type, data);

    // set min and mag filters
    gl.texParameteri(TEXTURE_2D, MAG_FILTER, options.magFilter);
    gl.texParameteri(TEXTURE_2D, MIN_FILTER, options.minFilter);

    //set wrapping
    gl.texParameteri(TEXTURE_2D, WRAP_S, options.wrapS);
    gl.texParameteri(TEXTURE_2D, WRAP_T, options.wrapT);

    // generate mipmaps if necessary
    if (options.generateMipMaps) {
        gl.generateMipmap(TEXTURE_2D);
    }

    gl.bindTexture(TEXTURE_2D, null);

    return texture;
}



/**
 * Create an image based texture. Usable on it's own, but recommended that you use the {@link createTexture2d}
 * function
 * @param gl {WebGLRenderingContext} a WebGLRenderingContext
 * @param image {Image} and image object
 * @param options {Object} a map of options for the texture creation
 * @returns {*}
 */
function createImageTexture(gl, image, options) {
    var texture = gl.createTexture();
    gl.bindTexture(TEXTURE_2D, texture);

    // set the image
    gl.texImage2D(TEXTURE_2D, 0, options.format, options.format, options.type, image);

    // set min and mag filters
    gl.texParameteri(TEXTURE_2D, MAG_FILTER, options.magFilter);
    gl.texParameteri(TEXTURE_2D, MIN_FILTER, options.minFilter);

    //set wrapping
    gl.texParameteri(TEXTURE_2D, WRAP_S, options.wrapS);
    gl.texParameteri(TEXTURE_2D, WRAP_T, options.wrapT);

    // generate mipmaps if necessary
    if (options.generateMipMaps) {
        gl.generateMipmap(TEXTURE_2D);
    }

    gl.bindTexture(TEXTURE_2D, null);

    return texture;
}

/**
 * Simple function for creating a 2D texture
 * @param gl a WebGLRendering context
 */
function createTexture2d(gl) {
    var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
        data = _ref.data,
        textureOptions = _ref.textureOptions,
        width = _ref.width,
        height = _ref.height;

    var texture = null;

    // NOTES
    // 1. in WebGL 1 , internalFormat and format ought to be the same value.
    // 2. UNSIGNED_BYTE corresponds to a Uint8Array, float corresponds to a Float32Array
    var defaults = {
        format: RGBA,
        internalFormat: RGBA,
        type: UNSIGNED_BYTE,
        wrapS: CLAMP_TO_EDGE,
        wrapT: CLAMP_TO_EDGE,
        minFilter: LINEAR,
        magFilter: LINEAR,
        generateMipMaps: false
    };

    if (textureOptions !== undefined) {
        Object.assign(defaults, textureOptions);
    }

    // if we have data, process it as such, otherwise generate a blank texture of random data
    if (data === undefined) {
        width = width || 128;
        height = height || 128;

        var _data = null;

        // if textureOptions isn't undefined, check to see if we've defined the "type" key.
        // if that is set to the floating point constant, make sure to use a Float32Array,
        // otherwise default to Uint8Array.
        // If the parameter isn't defined, default to Uint8Array
        if (textureOptions !== undefined) {
            if (textureOptions.hasOwnProperty('type')) {
                if (textureOptions.type === FLOAT) {
                    _data = new Float32Array(width * height * 4);
                } else {
                    _data = new Uint8Array(width * height * 4);
                }
            }
        } else {
            _data = new Uint8Array(width * height * 4);
        }

        for (var i = 0; i < width * height * 4; i += 4) {
            _data[i] = Math.random();
            _data[i + 1] = Math.random();
            _data[i + 2] = Math.random();
            _data[i + 3] = 1.0;
        }

        defaults["width"] = width;
        defaults["height"] = height;
        texture = createDataTexture(gl, _data, defaults);

        // if we have data
    } else {
        defaults["width"] = width || 128;
        defaults["height"] = height || 128;

        // if it's an image, build an image texture
        if (data instanceof Image) {
            texture = createImageTexture(gl, data, defaults);
        }

        // if it's a float 32 array we, build a data texture.
        if (data instanceof Float32Array) {
            if (defaults.type !== FLOAT) {
                defaults.type = FLOAT;
            }
            texture = createDataTexture(gl, data, defaults);
        }

        // if it's a float 32 array we, build a data texture.
        if (data instanceof Uint8Array) {
            if (defaults.type !== FLOAT) {
                defaults.type = FLOAT;
            }
            texture = createDataTexture(gl, data, defaults);
        }

        if (data instanceof Array) {
            if (defaults.type !== FLOAT) {
                defaults.type = FLOAT;
            }
            texture = createDataTexture(gl, new Float32Array(data), defaults);
        }
    }

    return {
        gl: gl,
        texture: texture,
        getTexture: function getTexture() {
            return this.texture;
        },
        bind: function bind() {
            var index = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

            var gl = this.gl;
            gl.activeTexture(TEXTURE0 + index);
            gl.bindTexture(TEXTURE_2D, this.texture);

            this.isBound = true;
        }
    };
}

/**
 * Creates a WebGL Framebuffer object.
 * @param gl {WebGLRenderingContext} a WebGlRenderingContext
 * @param width {Number} the width for the fbo
 * @param height {Number} the height for the number
 * @param floatingPoint {Bool} whether or not the FBO should store floating point information
 * @returns {{gl: *, drawTexture: *, fbo: *, bindFbo: bindFbo, unbindFbo: unbindFbo, bind: bind, unbind: unbind}}
 */
function createFBO(gl) {
    var _ref2 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
        width = _ref2.width,
        height = _ref2.height,
        _ref2$floatingPoint = _ref2.floatingPoint,
        floatingPoint = _ref2$floatingPoint === undefined ? true : _ref2$floatingPoint,
        _ref2$texture = _ref2.texture,
        texture = _ref2$texture === undefined ? null : _ref2$texture;

    width = width || 512;
    height = height || 512;
    floatingPoint = floatingPoint || false;

    var framebuffer = gl.createFramebuffer();
    var t = null;

    if (texture !== null) {
        t = texture;
    } else {
        // if floating point is true, we create a FBO with
        // a floating point texture(meaning, a texture that can store floating point numbers), otherwise, just create a regular fbo
        // with integer based information
        if (floatingPoint) {
            t = createTexture2d(gl, {
                width: width,
                height: height,
                textureOptions: {
                    type: FLOAT
                }
            });
        } else {
            t = createTexture2d(gl);
        }
    }
    // attach primary drawing texture.
    gl.bindFramebuffer(FRAMEBUFFER, framebuffer);
    gl.framebufferTexture2D(FRAMEBUFFER, COLOR_ATTACHMENT0, TEXTURE_2D, t.texture, 0);
    gl.bindFramebuffer(FRAMEBUFFER, null);

    return {
        gl: gl,
        drawTexture: t,
        fbo: framebuffer,
        /**
         * For binding the Fbo to draw onto it.
         * @deprecated
         */
        bindFbo: function bindFbo() {
            gl.bindFramebuffer(FRAMEBUFFER, framebuffer);
        },

        /**
         * Unbinds the previously bound FBO, returning drawing commands to
         * the main context Framebuffer.
         * @deprecated
         */
        unbindFbo: function unbindFbo() {
            gl.bindFramebuffer(FRAMEBUFFER, null);
        },

        /**
         * For binding the Fbo to draw onto it.
         */
        bind: function bind() {
            gl.bindFramebuffer(FRAMEBUFFER, framebuffer);
        },

        /**
         * Unbinds the previously bound FBO, returning drawing commands to
         * the main context Framebuffer.
         */
        unbind: function unbind() {
            gl.bindFramebuffer(FRAMEBUFFER, null);
        },

        /**
         * Binds the texture of the framebuffer
         * @param index the index to bind the texture to
         */
        bindTexture: function bindTexture() {
            var index = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;

            this.drawTexture.bind(index);
        },

        /**
         * Unbinds the framebuffer's texture
         */
        unbindTexture: function unbindTexture() {
            gl.bindTexture(gl.TEXTURE_2D, null);
        }
    };
}

var quadvert = "\nattribute vec3 position;\nvarying vec2 uv;\nconst vec2 scale = vec2(0.5,0.5);\nvoid main(){\n    uv = position.xy * scale + scale;\n    gl_Position = vec4(position,1.0);\n}";

var quadfrag = "precision highp float;\n#ifdef USE_TEXTURE\n    uniform sampler2D debugTex;\n#endif\nvarying vec2 uv;\nvoid main(){\n  #ifdef USE_TEXTURE\n     vec4 dat = texture2D(debugTex,uv);\n     gl_FragColor = dat;\n  #else\n     gl_FragColor = vec4(1.);\n  #endif\n}";

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/**
 * Compiles either a fragment or vertex shader
 * @param gl a webgl context
 * @param type the type of shader. Should be either gl.FRAGMENT_SHADER or gl.VERTEX_SHADER
 * @param source the source (as a string) for the shader
 * @returns {*} returns the compiled shader
 */
function compileShader(gl, type, source) {
    var shader = gl.createShader(type);

    gl.shaderSource(shader, source);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        console.error("Error in shader compilation - " + gl.getShaderInfoLog(shader));
        return false;
    } else {
        return shader;
    }
}

/**
 * The main function for creating a shader. Shader also manages figuring out
 * attribute and uniform location indices.
 *
 * @param gl a webgl context
 * @param vertex the source for the vertex shader
 * @param fragment the source for the fragment shader
 * @returns {*} returns the WebGLProgram compiled from the two shaders
 */
function makeShader(gl, vertex, fragment) {
    var vShader = compileShader(gl, gl.VERTEX_SHADER, vertex);
    var fShader = compileShader(gl, gl.FRAGMENT_SHADER, fragment);

    if (vShader !== false && fShader !== false) {
        var program = gl.createProgram();
        gl.attachShader(program, vShader);
        gl.attachShader(program, fShader);
        gl.linkProgram(program);

        if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
            logError("Could not initialize WebGLProgram");
            throw "Couldn't link shader program - " + gl.getProgramInfoLog(program);
            return false;
        } else {
            return program;
        }
    }
}
/**
 * A function to quickly setup a WebGL shader program.
 * Modeled a bit after thi.ng
 * @param gl the webgl context to use
 * @param spec a object containing the out line of what the shader would look like.
 * @returns {*} and JS object with the shader information along with some helpful functions
 */
function createShader() {
    var gl = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;
    var spec = arguments[1];

    var vs = null;
    var fs = null;
    var uniforms = {};
    var attributes = {};
    var precision = spec.precision !== undefined ? spec.precision : "highp";
    if (gl === null) {
        console.error("");
        return false;
    }

    if (!spec.hasOwnProperty("vertex") || !spec.hasOwnProperty("fragment")) {
        logError("spec does not contain vertex and/or fragment shader", true);
        return false;
    }

    // if either of the shader sources are arrays, run the compile shader function
    if (spec.vertex instanceof Array) {
        spec.vertex = compileShaderSource(spec.vertex);
    }

    if (spec.fragment instanceof Array) {
        spec.fragment = "precision " + precision + " float;" + compileShaderSource(spec.fragment);
    }

    // build the shader
    var shader = makeShader(gl, spec.vertex, spec.fragment);

    // set uniforms and their locations (plus default values if specified)
    if (spec.hasOwnProperty('uniforms')) {

        var uValues = spec.uniforms.map(function (value) {
            if (typeof value === 'string') {
                var loc = gl.getUniformLocation(shader, value);
                uniforms[value] = loc;
            } else if ((typeof value === "undefined" ? "undefined" : _typeof(value)) === 'object') {
                // TODO make sure to set default uniform value if present
                var _loc = gl.getUniformLocation(shader, value.name);
                uniforms[value.name] = _loc;
            }
        });
    }

    /**
     * Arranges all of the attribute data into neat containers
     * to allow for easy processing by a VAO.
     * Attributes should be specified as arrays
     */
    if (spec.hasOwnProperty('attributes')) {
        var attribs = spec.attributes.map(function (value) {

            attributes[value[0]] = {
                size: value[1],
                name: value[0]
            };

            // if a desired uniform location is set ,
            // make sure to reflect that in the information
            if (value[2] !== undefined) {
                attributes[value[0]].location = value[2];
            }
        });
    }

    return {
        gl: gl,
        program: shader,
        uniforms: uniforms,
        attributes: attributes,

        /**
         * Binds the shader for use. You can optionally pass in a object containing
         * the projection and view/modelView matrices and specify the specific uniform names
         * which default to projection and modelViewMatrix.
         * @param camera an object containing the projection and view/modelView matrices for the shader
         * @param proj the uniform name for the projection matrix
         * @param view the uniform name for the view/modelView matrix
         */
        bind: function bind() {
            var camera = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : null;

            var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
                _ref$proj = _ref.proj,
                proj = _ref$proj === undefined ? "projectionMatrix" : _ref$proj,
                _ref$view = _ref.view,
                view = _ref$view === undefined ? "modelViewMatrix" : _ref$view;

            this.gl.useProgram(this.program);
            if (camera !== null) {
                this.set4x4Uniform(proj, camera.projection);
                this.set4x4Uniform(view, camera.view);
            }
        },

        /**
         * Sets a matrix uniform for a 4x4 matrix
         * @deprecated prepare to remove and switch to something more descriptive for a 4x4 matrix
         * @param name the name of the uniform whose value you want to set.
         */
        setMatrixUniform: function setMatrixUniform(name, value) {
            this.gl.uniformMatrix4fv(this.uniforms[name], false, value);
        },


        /**
         * Sets a mat4 uniform in a shader
         * @param name the name of the uniform
         * @param value the value for the uniform
         */
        set4x4Uniform: function set4x4Uniform(name, value) {
            this.gl.uniformMatrix4fv(this.uniforms[name], false, value);
        },


        /**
         * Sets a mat3 uniform in a shader
         * @param name  the name of the uniform
         * @param value the value of the uniform
         */
        set3x3Uniform: function set3x3Uniform(name, value) {
            this.gl.uniformMatrix3fv(this.uniforms[name], false, value);
        },

        /**
         * Sets the uniform value for a texture. Optionally
         * @param value
         */
        setTextureUniform: function setTextureUniform(name, value) {
            this.gl.uniform1i(this.uniforms[name], value);
        },


        /**
         * Returns the uniform location of a shader's uniform
         * @param name  the name of the location you want
         * @returns {*}
         */
        getUniform: function getUniform(name) {
            return this.uniforms[name];
        },


        /**
         * sets a vec2 uniform
         * @param name
         * @param value
         */
        setVec2: function setVec2(name, v1, v2) {
            this.gl.uniform2f(this.uniforms[name], v1, v2);
        },


        /**
         * Sets a vec3 uniform
         * @param name
         * @param value
         */
        setVec3: function setVec3(name, value) {
            this.gl.uniform3fv(this.uniforms[name], value);
        },


        /**
         * Sends a uniform to the currently bound shader. Attempts to derrive
         * the correct uniform function to use
         * @param name {String} name of the uniform
         * @param value {*} the value to send to the uniform
         */
        uniform: function uniform(name, value) {

            /**
             *  "if" statement to properly figure out what uniform function to use.
             *  Assumes all matrix and vector values are in the forms of an Array object.
             *  Currently no great way to differentiate between integers and floating point values
             *  when it comes to non array values.
             *
             *  Currently works with
             *  - 4x4 matrices
             *  - 3x3 matrices
             *  - vec2 arrays represented by a array with just two values
             */
            if (value.length !== undefined && value.length === 16) {
                this.set4x4Uniform(name, value);
            } else if (value.length !== undefined && value.length === 3) {
                this.gl.uniform3fv(this.uniforms[name], value);
            } else if (value.length !== undefined && value.length === 2) {
                this.setVec2(name, value);
            } else if (value.length !== undefined && value.length === 3) {
                this.setVec3(name, value);
            } else {
                this.gl.uniform1f(this.uniforms[name], value);
            }
        }
    };
}

/**
 * Allows you to compile multiple shader sources into one file.
 * Keep in mind this does not distinguish between vertex and fragment and will not
 * insert things like precision specifiers and/or extensions
 * @param sources
 */
function compileShaderSource() {
    for (var _len = arguments.length, sources = Array(_len), _key = 0; _key < _len; _key++) {
        sources[_key] = arguments[_key];
    }

    if (sources[0] instanceof Array) {
        var s = sources[0].map(function (source) {
            return source + "\n";
        });
        return s.join("");
    } else {
        var s = sources.map(function (source) {
            return source + "\n";
        });
        return s.join("");
    }
}

/**
 * Creates a VertexAttributeObject aka VAO
 * @param gl a webgl context
 * @param useNative flag for whether or not to use native VAOs (which uses an extension for now)
 */
function createVAO(gl) {
    var useNative = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

    var vao = null;
    var ext = null;
    // TODO support cards that don't have this extension later
    if (useNative) {
        if (gl.hasOwnProperty('OES_vertex_array_object')) {
            ext = gl['OES_vertex_array_object'];
            vao = ext.createVertexArrayOES();
        } else {
            ext = gl.getExtension('OES_vertex_array_object');
            vao = ext.createVertexArrayOES();
        }
    }

    return {
        gl: gl,
        vao: vao,
        ext: ext,
        attributes: {},

        /**
         * Helper function to allow an attribute to become instanced.
         * For the time being until WebGL 2 is standardized, this is currently enabled as an
         * extension.
         * @param attribute {String} the name of the attribute to make instanced.
         * @returns {boolean} false if unable to utilize ANGLE_instanced_arrays.
         */
        makeInstancedAttribute: function makeInstancedAttribute(attribute) {
            var ext = null;
            if (gl.hasOwnProperty('ANGLE_instanced_arrays')) {
                ext = gl.ANGLE_instanced_arrays;
            } else {
                try {
                    ext = gl.getExtension('ANGLE_instanced_arrays');
                } catch (e) {
                    console.error("cannot utilize instanced attributes on this GPU");
                    return false;
                }
            }

            ext.vertexAttribDivisorANGLE(this.getAttribute(attribute), 1);
        },

        /**
         * Sets an attribute's location
         * @param shader {WebGLProgram} a WebGl shader program to associate with the attribute location
         * @param name {String} the name of the attribute
         * @param index {Number} an optional index. If null, will utilize the automatically assigned location
         * @returns {number} returns the location for the attribute
         */
        setAttributeLocation: function setAttributeLocation(shader, name) {
            var index = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

            var loc = 0;
            var gl = this.gl;

            // if we don't assign an index, get the automatically generated one
            if (index === null || index === undefined) {
                loc = gl.getAttribLocation(shader, name);
            } else {
                loc = gl.bindAttribLocation(shader, index, name);
            }
            return loc;
        },


        /**
         * Enable all of the attributes on a shader onto the VAO.
         * This will automatically set the attribute location to the order in which the
         * attribute was set in the shader settings, but will override that decision if the location index is
         * set in the attribute.
         *
         * @param shader a plane JS object that contains 3 things
         * 1. A WebGLProgram on the key "shader"
         * 2. an array at the key "attributes" that contains the name of all of the attributes we're looking for
         * as well as the size of each attribute.
         */
        enableAttributes: function enableAttributes(shader) {
            var gl = this.gl;
            var attribs = shader.attributes;
            for (var a in attribs) {
                var attrib = attribs[a];
                var attribLoc = this.attributes.length;

                // if the attribute has a location parameter, use that to set the attribute location,
                // otherwise use the next index in the attributes array
                if (attrib.hasOwnProperty('location')) {
                    attribLoc = attrib.location;
                }

                this.addAttribute(shader, attrib.name, attrib.size, attribLoc);
            }
            return this;
        },


        /**
         * Adds an attribute for the VAO to keep track of
         * @param shader {WebGLProgram} the shader that the attribute is a part of. Takes a WebGLProgram but also accepts a plain object created by the
         * {@link createShader} function
         * @param name {String} the name of the attribute to add/enable
         * @param size {Number} optional - the number of items that compose the attribute. For example, for something like, position, you might have xyz components, thus, 3 would be the size
         * @param location {Number} optional - the number to use as the attribute location. If it's not specified, will simply use it's index in the attributes object
         * @param setData {Boolean} optional - flag for whether or not to immediately run setData on the attribute. TODO enable by default
         * @param dataOptions {Object} optional - any options you might want to add when calling setData like an offset or stride value for the data
         * @returns {addAttribute}
         */
        addAttribute: function addAttribute(shader, name) {
            var _ref = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
                _ref$size = _ref.size,
                size = _ref$size === undefined ? 3 : _ref$size,
                location = _ref.location,
                _ref$setData = _ref.setData,
                setData = _ref$setData === undefined ? false : _ref$setData,
                _ref$dataOptions = _ref.dataOptions,
                dataOptions = _ref$dataOptions === undefined ? {} : _ref$dataOptions;

            var attribLoc = this.attributes.length;
            var webglProg = null;

            if (shader instanceof WebGLProgram) {
                webglProg = shader;
            } else {
                webglProg = shader.program;
            }

            // if location is undefined, just set attribute location
            // to be the next index in the attribute set.
            if (location === undefined) {
                attribLoc = location;
            }

            var attribLocation = this.setAttributeLocation(webglProg, name, attribLoc);
            this.attributes[name] = {
                loc: attribLocation,
                enabled: true,
                size: size
            };
            //enable the attribute
            this.enableAttribute(name);

            // if we want to just go ahead and set the data , run that
            this.setData(name, dataOptions);
            return this;
        },


        /**
         * Returns the location of the specified attribute
         * @param name {String} the name of the attribute.
         * @returns {*|number}
         */
        getAttribute: function getAttribute(name) {
            return this.attributes[name].loc;
        },


        /**
         * Enables a vertex attribute
         * @param name {String} the name of the attribute you want to enable
         */
        enableAttribute: function enableAttribute(name) {
            // enable vertex attribute at the location
            this.gl.enableVertexAttribArray(this.attributes[name].loc);
        },


        /**
         * Disables a vertex attribute
         * @param name {String} the name of the vertex attribute to disable
         */
        disableAttribute: function disableAttribute(name) {
            this.gl.disableVertexAttribArray(this.attributes[name].loc);
        },


        /**
         * Shorthand for calling gl.vertexAttribPointer. Essentially sets the data into the vao for the
         * currently bound buffer.
         * @param name {String} the name of the attribute to pass data to in the shader
         * @param options {Object} options for utilizing that information
         */
        setData: function setData(name) {
            var _ref2 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
                options = _ref2.options;

            var loc = this.attributes[name].loc;
            var size = this.attributes[name].size;

            var pointerOptions = {
                type: gl.FLOAT,
                normalized: gl.FALSE,
                stride: 0,
                offset: 0
            };

            if (options !== undefined) {
                Object.assign(pointerOptions, options);
            }
            gl.vertexAttribPointer(loc, size, pointerOptions.type, pointerOptions.normalized, pointerOptions.stride, pointerOptions.offset);
        },
        /**
         * Shorthand for calling gl.vertexAttribPointer
         * @deprecated gonna start using setData instead so it's a little more clear as to what this is doing
         * @param name {String} the name of the attribute to pass data to in the shader
         * @param options {Object} options for utilizing that information
         */
        point: function point(name) {
            var _ref3 = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
                options = _ref3.options;

            var loc = this.attributes[name].loc;
            var size = this.attributes[name].size;

            var pointerOptions = {
                type: gl.FLOAT,
                normalized: gl.FALSE,
                stride: 0,
                offset: 0
            };

            if (options !== undefined) {
                Object.assign(pointerOptions, options);
            }
            gl.vertexAttribPointer(loc, size, pointerOptions.type, pointerOptions.normalized, pointerOptions.stride, pointerOptions.offset);
        },


        /**
         * Binds the vao
         */
        bind: function bind() {
            ext.bindVertexArrayOES(this.vao);
        },


        /**
         * Unbinds the vao
         */
        unbind: function unbind() {
            ext.bindVertexArrayOES(null);
        }
    };
}

/**
 * Simple function to create a VBO aka buffer
 * @param gl a WebGLRendering context
 * @param data the information for the buffer. If it's a regular array, it'll be turned into a TypedArray
 * @param bufferType the type of buffer it is. By default, it's an ARRAY_BUFFER
 * @param usage the usage for the buffer. by default it's STATIC_DRAW
 */
function createVBO(gl) {
    var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
        _ref$data = _ref.data,
        data = _ref$data === undefined ? null : _ref$data,
        _ref$indexed = _ref.indexed,
        indexed = _ref$indexed === undefined ? false : _ref$indexed,
        usage = _ref.usage;

    var buffer = null;

    // set the buffer type
    var bufferType = "ARRAY_BUFFER";
    if (indexed === true) {
        bufferType = "ELEMENT_ARRAY_BUFFER";
    }
    var name = bufferType;
    bufferType = gl[bufferType];

    // set the usage
    usage = usage || "STATIC_DRAW";
    usage = gl[usage];
    buffer = gl.createBuffer();

    var obj = {
        gl: gl,
        buffer: buffer,
        bufferTypeName: name,
        type: bufferType,
        usage: usage,

        raw: function raw() {
            return this.buffer;
        },

        /**
         * Updates the buffer with new information
         * @param data a array of some kind containing your new data
         */
        updateBuffer: function updateBuffer(data) {
            if (data instanceof Array) {
                if (this.bufferTypeName === "ARRAY_BUFFER") {
                    data = new Float32Array(data);
                } else {
                    data = new Uint16Array(data);
                }
            }
            this.bind();
            this.gl.bufferSubData(this.type, 0, data);
            this.unbind();
        },


        /**
         * Fills buffer with data
         * @param data
         */
        fill: function fill(data, shader, vao, name) {
            if (data instanceof Array) {
                if (this.bufferTypeName === "ARRAY_BUFFER") {
                    data = new Float32Array(data);
                } else {
                    data = new Uint16Array(data);
                }
            }
            this.gl.bufferData(this.type, data, usage);
        },

        /**
         * Sets data onto the vbo.
         * @param data the data for the vbo. Can either be a regular array or a typed array.
         * If a regular array is used, will determine buffer type based on the settings.
         */
        bufferData: function bufferData(data) {
            var updatedData = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

            if (data instanceof Array) {
                if (this.bufferTypeName === "ARRAY_BUFFER") {
                    data = new Float32Array(data);
                } else {
                    data = new Uint16Array(data);
                }
            }
            this.gl.bufferData(this.type, data, usage);
        },
        bind: function bind() {
            this.gl.bindBuffer(this.type, this.buffer);
        },
        unbind: function unbind() {
            this.gl.bindBuffer(this.type, null);
        }
    };

    if (data !== null) {
        obj.bind();
        obj.bufferData(data);
        obj.unbind();
    }

    return obj;
}

/**
 * Function used to build a multi-purpose full screen quad.
 * @param gl {WebGLRenderingContext}
 * @param withTexture {boolean} a boolean value indicating whether or not you're trying to create a rendering quad. If you are, pass in true and instead of a color
 * things will get set up to draw a texture instead.
 * @param fragmentShader {String} optional fragment shader, primarily used for ping-ponging data between textures.
 * @returns {{vao: ({gl, vao, ext, attributes, setAttributeLocation, enableAttributes, addAttribute, getAttribute, enableAttribute, disableAttribute, setData, point, bind, unbind}|*), shader: ({gl, program, uniforms, attributes, bind, setMatrixUniform, setTextureUniform, uniform}|*), buffer: *, hasTexture: boolean, gl: *, draw: draw}}
 */
function createQuad(gl) {
    var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
        _ref$withTexture = _ref.withTexture,
        withTexture = _ref$withTexture === undefined ? false : _ref$withTexture,
        _ref$fragmentShader = _ref.fragmentShader,
        fragmentShader = _ref$fragmentShader === undefined ? false : _ref$fragmentShader,
        _ref$uniformMap = _ref.uniformMap,
        uniformMap = _ref$uniformMap === undefined ? [] : _ref$uniformMap,
        _ref$vertexShader = _ref.vertexShader,
        vertexShader = _ref$vertexShader === undefined ? false : _ref$vertexShader;

    var frag = quadfrag;
    var vertex = quadvert;
    var uniforms = ['debugTex'];
    var vertices = [1.0, 1.0, 0.0, -1.0, 1.0, 0.0, 1.0, -1.0, 0.0, -1.0, -1.0, 0.0];

    var vao = createVAO(gl);
    vao.bind();

    // if uniform map is not empty, merge with default map
    if (uniformMap.length > 0) {
        uniformMap.forEach(function (obj) {
            uniforms.push(obj);
        });
    }

    // if we don't have our own fragment shader, make sure to inject
    // the texture define statement if we need to support a texture.
    if (!fragmentShader) {
        if (withTexture) {
            frag = "#define USE_TEXTURE\n" + quadfrag;
        }
    } else {
        frag = fragmentShader;
    }

    // if we aren't using the default vertex shader
    if (vertexShader) {
        vertex = vertexShader;
    }

    var shader = createShader(gl, {
        vertex: vertex,
        fragment: frag,
        attributes: [['position', 3]],
        uniforms: uniforms
    });
    // buffer data onto the buffer
    var buffer = createVBO(gl);

    // enable attributes
    buffer.bind();
    buffer.bufferData(vertices);
    vao.addAttribute(shader, 'position');
    vao.setData('position');
    buffer.unbind();
    vao.unbind();

    return {
        vao: vao,
        shader: shader,
        buffer: buffer,
        type: "quad",
        hasTexture: withTexture,
        gl: gl,
        drawWithCallback: function drawWithCallback(cb) {
            this.vao.bind();
            this.shader.bind();

            this.vao.enableAttribute('position');
            cb(shader);

            this.gl.drawArrays(TRIANGLE_STRIP, 0, 4);
            this.vao.disableAttribute('position');
            this.vao.unbind();
        },
        draw: function draw() {
            var textureUnit = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;


            this.vao.bind();
            this.shader.bind();

            this.vao.enableAttribute('position');

            // if we need to supply a texture, set the uniform.
            // this assumes texture has already been bound.
            if (this.hasTexture) {
                this.shader.setTextureUniform('debugTex', textureUnit);
            }

            this.gl.drawArrays(TRIANGLE_STRIP, 0, 4);
            this.vao.disableAttribute('position');
            this.vao.unbind();
        }
    };
}

/**
 * Takes a Quad object made with `createQuad` and draws it
 * @param quad
 */

var _createClass$1 = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck$1(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var FxPass = function () {
    function FxPass(gl, fragment) {
        var _ref = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {},
            _ref$uniformMap = _ref.uniformMap,
            uniformMap = _ref$uniformMap === undefined ? [] : _ref$uniformMap,
            _ref$width = _ref.width,
            width = _ref$width === undefined ? window.innerWidth : _ref$width,
            _ref$height = _ref.height,
            height = _ref$height === undefined ? window.innerHeight : _ref$height,
            inputTexture = _ref.inputTexture;

        _classCallCheck$1(this, FxPass);

        this.gl = gl;
        this.input = inputTexture;
        this.fbo = createFBO(gl, {
            width: width,
            height: height
        });
        this.drawQuad = createQuad(gl, {
            withTexture: true,
            fragmentShader: fragment,
            uniformMap: uniformMap
        });
    }

    _createClass$1(FxPass, [{
        key: 'runPass',
        value: function runPass() {

            this.fbo.bind();
            this.gl.clearScreen();
            this.input.bind();
            this.drawQuad.drawWithCallback(function (shader) {
                shader.setTextureUniform('tex0', 0);
            });
            this.fbo.unbind();
        }
    }, {
        key: 'getOutput',
        value: function getOutput() {
            return this.fbo;
        }
    }]);

    return FxPass;
}();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Composer = function () {
    function Composer(gl, input) {
        _classCallCheck(this, Composer);

        this.gl = gl;

        for (var _len = arguments.length, passes = Array(_len > 2 ? _len - 2 : 0), _key = 2; _key < _len; _key++) {
            passes[_key - 2] = arguments[_key];
        }

        this.passes = passes;
        this.input = input !== undefined ? input : function () {
            console.warn("Composer instance does not have an input texture");
        }();
        this._sortPasses();
    }

    /**
     * Adds a pass to the composer.
     * TODO add in functionality to specify insertion order.
     * @param pass the FxPass object to add.
     * @param index
     */


    _createClass(Composer, [{
        key: "addPass",
        value: function addPass(pass) {
            var index = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;

            this._sortPasses();
        }

        /**
         * Assigns input textures to all of the passes
         * @private
         */

    }, {
        key: "_sortPasses",
        value: function _sortPasses() {
            var passes = this.passes;
            var input = this.input;
            var index = 0;
            var finalSetup = [];
            passes.forEach(function (obj) {
                if (index === 0) {
                    obj.input = input;
                    finalSetup.push(obj);
                } else {
                    obj.input = passes[index - 1].fbo.drawTexture;
                    finalSetup.push(obj);
                }

                index++;
            });

            this.passes = finalSetup;
        }
    }, {
        key: "run",
        value: function run() {
            this.passes.forEach(function (obj) {
                obj.runPass();
            });
        }
    }, {
        key: "getOutput",
        value: function getOutput() {
            var passes = this.passes;
            return passes[passes.length - 1].getOutput();
        }
    }]);

    return Composer;
}();

var bloom$1 = "precision highp float;\nuniform float sample_offset;\nuniform sampler2D tex0;\nconst float attenuation = 0.0323;\nvarying vec2 uv;\nvoid main(){\n     vec3 sum = vec3( 0.0, 0.0, 0.0 );\n      vec4 texDat = texture2D( tex0, uv );\n     sum += texture2D( tex0, uv + -10.0 * sample_offset ).rgb * 0.009167927656011385;\n     sum += texture2D( tex0, uv +  -9.0 * sample_offset ).rgb * 0.014053461291849008;\n     sum += texture2D( tex0, uv +  -8.0 * sample_offset ).rgb * 0.020595286319257878;\n     sum += texture2D( tex0, uv +  -7.0 * sample_offset ).rgb * 0.028855245532226279;\n     sum += texture2D( tex0, uv +  -6.0 * sample_offset ).rgb * 0.038650411513543079;\n     sum += texture2D( tex0, uv +  -5.0 * sample_offset ).rgb * 0.049494378859311142;\n     sum += texture2D( tex0, uv +  -4.0 * sample_offset ).rgb * 0.060594058578763078;\n     sum += texture2D( tex0, uv +  -3.0 * sample_offset ).rgb * 0.070921288047096992;\n     sum += texture2D( tex0, uv +  -2.0 * sample_offset ).rgb * 0.079358891804948081;\n     sum += texture2D( tex0, uv +  -1.0 * sample_offset ).rgb * 0.084895951965930902;\n     sum += texture2D( tex0, uv +   0.0 * sample_offset ).rgb * 0.086826196862124602;\n     sum += texture2D( tex0, uv +  +1.0 * sample_offset ).rgb * 0.084895951965930902;\n     sum += texture2D( tex0, uv +  +2.0 * sample_offset ).rgb * 0.079358891804948081;\n     sum += texture2D( tex0, uv +  +3.0 * sample_offset ).rgb * 0.070921288047096992;\n     sum += texture2D( tex0, uv +  +4.0 * sample_offset ).rgb * 0.060594058578763078;\n     sum += texture2D( tex0, uv +  +5.0 * sample_offset ).rgb * 0.049494378859311142;\n     sum += texture2D( tex0, uv +  +6.0 * sample_offset ).rgb * 0.038650411513543079;\n     sum += texture2D( tex0, uv +  +7.0 * sample_offset ).rgb * 0.028855245532226279;\n     sum += texture2D( tex0, uv +  +8.0 * sample_offset ).rgb * 0.020595286319257878;\n     sum += texture2D( tex0, uv +  +9.0 * sample_offset ).rgb * 0.014053461291849008;\n     sum += texture2D( tex0, uv + +10.0 * sample_offset ).rgb * 0.009167927656011385;\n     gl_FragColor = vec4(attenuation * sum * 100.0,1.0);\n}";

var _createClass$2 = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck$2(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var BloomPass = function () {
    function BloomPass(gl) {
        var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
            _ref$width = _ref.width,
            width = _ref$width === undefined ? window.innerWidth : _ref$width,
            _ref$height = _ref.height,
            height = _ref$height === undefined ? window.innerHeight : _ref$height,
            inputTexture = _ref.inputTexture;

        _classCallCheck$2(this, BloomPass);

        this.gl = gl;
        this.input = inputTexture;
        this.fbo = createFBO(gl, {
            width: width,
            height: height
        });
        this.drawQuad = createQuad(gl, {
            withTexture: true,
            fragmentShader: bloom$1,
            uniformMap: ['sample_offset', 'tex0']
        });
    }

    _createClass$2(BloomPass, [{
        key: 'runPass',
        value: function runPass() {

            this.fbo.bind();
            this.gl.clearScreen();
            this.input.bind();
            this.drawQuad.drawWithCallback(function (shader) {

                shader.setTextureUniform('tex0', 0);
                shader.uniform('sample_offset', 1.0 / window.innerWidth, 0.0);
            });
            this.fbo.unbind();
        }
    }, {
        key: 'getOutput',
        value: function getOutput() {
            return this.fbo;
        }
    }]);

    return BloomPass;
}();

var fxaa = "precision highp float;\nuniform sampler2D tex0;\nuniform sampler2D tDisp;uniform float amount;\nuniform float angle;\nuniform float seed;\nuniform float seed_x;\nuniform float seed_y;\nuniform float distortion_x;\nuniform float distortion_y;\nuniform float col_s;\nuniform float byp;\nvarying vec2 uv;\nfloat rand(vec2 co){\n    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);\n}\nvoid main(){\n\tif(byp < 1.0){\n\tvec2 p = uv;\n    \tfloat xs = floor(gl_FragCoord.x / 0.5);\n    \tfloat ys = floor(gl_FragCoord.y / 0.5);\n    \tvec4 normal = texture2D (tDisp, p*seed*seed);\n    \tif(p.y < distortion_x + col_s && p.y > distortion_x - col_s * seed) {\n    \t    if(seed_x>0.){\n    \t    \tp.y = 1. - (p.y + distortion_y);\n    \t    }\n    \t    else {\n    \t    \tp.y = distortion_y;\n    \t    }\n    \t}\n    \tif(p.x<distortion_y+col_s && p.x>distortion_y-col_s*seed) {\n    \t\tif(seed_y>0.){\n    \t\t\tp.x=distortion_x;\n    \t\t}\n    \t\telse {\n    \t\t\tp.x = 1. - (p.x + distortion_x);\n    \t\t}\n    \t}\n    \tp.x+=normal.x*seed_x*(seed/5.);\n    \tp.y+=normal.y*seed_y*(seed/5.);\n    \tvec2 offset = amount * vec2( cos(angle), sin(angle));\n    \tvec4 cr = texture2D(tex0, p + offset);\n    \tvec4 cga = texture2D(tex0, p);\n    \tvec4 cb = texture2D(tex0, p - offset);\n    \tgl_FragColor = vec4(cr.r, cga.g, cb.b, cga.a);\n    \tvec4 snow = 200.*amount*vec4(rand(vec2(xs * seed,ys * seed*50.))*0.2);\n    \tgl_FragColor = gl_FragColor+ snow;\n\t}else{\n\t    gl_FragColor = texture2D(tex0, uv);\n\t}\n}";

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

/**
 * Flattens an nested array that is assumed to be nested with child arrays used in place of
 * an actual vector object. Note, this does not check for completeness and will automatically
 * only take the first 3 values of the child arrays
 * @param array the parent array
 * @returns {Array}
 */


/**
 * Does subtraction between two arrays. Assumes both arrays have 3 values each inside
 * @param array1 {Array} the array to subtract from
 * @param array2 {Array} the array to subtract
 * @returns {*[]}
 */


/**
 * Converts value to radians
 * @param deg a value in degrees
 */
function toRadians(deg) {
    return deg * Math.PI / 180;
}





/**
 * Creates an array with a range of values
 * @param from {Number} the value to start from
 * @param to {Number} the value end at.
 * @returns {Array}
 */


/**
 * Returns a random vec3(in the form of an array)
 * @returns {*[]}
 */


/**
 * Performs linear interpolation of a value
 * @param value the value to interpolate
 * @param min the minimum value
 * @param max the maximum value
 * @returns {number}
 */


/**
 * Returns a random float value between two numbers
 * @param min the minimum value
 * @param max the maximum value
 * @returns {number}
 */
function randFloat() {
    var min = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
    var max = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;

    return min + Math.random() * (max - min + 1);
}

/**
 * Returns a random integer value between two numbers
 * @param min the minimum value
 * @param max the maximum value
 * @returns {number}
 */
function randInt() {
    var min = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
    var max = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;

    return Math.floor(min + Math.random() * (max - min + 1));
}

/**
 * Very simple array cloning util.
 * Note - only works with arrays who have 3 elements
 * @param arrayToClone the array to clone
 * @returns {*[]} the new array
 */




/**
 * ensures that when using an array as a 3d vector, that it actually
 * contains only 3 components.
 * @param array the array to verify
 * @returns {*}
 */

var _createClass$3 = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck$3(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var GlitchPass = function (_FxPass) {
    _inherits(GlitchPass, _FxPass);

    function GlitchPass(gl) {
        _classCallCheck$3(this, GlitchPass);

        var _this = _possibleConstructorReturn(this, (GlitchPass.__proto__ || Object.getPrototypeOf(GlitchPass)).call(this, gl, fxaa, {
            uniformMap: ['seed', 'amount', 'angle', 'seed_x', 'seed_y', 'distortion_x', 'distortion_y', 'col_s', 'tDisp', 'byp']
        }));

        _this.uniforms = {
            seed: 0.02,
            amount: 0,
            angle: 0,
            seed_x: 0,
            seed_y: 0,
            distortion_x: 0,
            distortion_y: 0,
            col_s: 0,
            byp: 1.0
        };
        _this.tDisp = _this.generateHeightMap(64);
        _this.curF = 0;
        _this.generateTrigger();
        return _this;
    }

    _createClass$3(GlitchPass, [{
        key: 'generateTrigger',
        value: function generateTrigger() {
            this.randX = randInt(120, 240);
        }
    }, {
        key: 'generateHeightMap',
        value: function generateHeightMap(dt_size) {

            var data_arr = new Float32Array(dt_size * dt_size * 4);
            var length = dt_size * dt_size;

            for (var i = 0; i < length; i++) {

                var val = randFloat(0, 1);
                data_arr[i * 3 + 0] = val;
                data_arr[i * 3 + 1] = val;
                data_arr[i * 3 + 2] = val;
                data_arr[i * 3 + 3] = 1.0;
            }

            return createTexture2d(this.gl, {
                data: data_arr,
                width: 64,
                height: 64,
                textureOptions: {
                    type: FLOAT
                }
            });
        }
    }, {
        key: 'runPass',
        value: function runPass() {
            var uniforms = this.uniforms;

            this.fbo.bind();
            this.gl.clearScreen();

            if (Math.random() * 20 > 10.0) {
                uniforms['byp'] = 0.0;
                uniforms["seed"] = Math.random();
            }

            if (this.curF % this.randX == 0) {
                uniforms['amount'] = Math.random() / 2;
                uniforms['angle'] = randFloat(-Math.PI, Math.PI);
                uniforms['seed_x'] = randFloat(-1, 1);
                uniforms['seed_y'] = randFloat(-1, 1);
                uniforms['distortion_x'] = randFloat(0, 1);
                uniforms['distortion_y'] = randFloat(0, 1);
                //uniforms["seed"] = 0.0;
                this.curF = 0;
                this.generateTrigger();
            } else if (this.curF % this.randX < this.randX / 5) {
                uniforms['amount'] = Math.random() / 40;
                uniforms['angle'] = randFloat(-Math.PI, Math.PI);
                uniforms['distortion_x'] = randFloat(0, 1);
                uniforms['distortion_y'] = randFloat(0, 1);
                uniforms['seed_x'] = randFloat(-0.3, 0.3);
                uniforms['seed_y'] = randFloat(-0.3, 0.3);
                //uniforms["seed"] = Math.random();
            } else {
                uniforms['byp'] = 1.0;
            }

            // scene input binds to 0
            this.input.bind();

            // displacement texture binds to 1
            this.tDisp.bind(1);
            this.drawQuad.drawWithCallback(function (shader) {
                shader.setTextureUniform('tex0', 0);
                shader.setTextureUniform('tDisp', 1);
                for (var i in uniforms) {
                    shader.uniform(i, uniforms[i]);
                }
            });
            uniforms['byp'] = 1.0;
            uniforms["seed"] = 0.0;
            this.fbo.unbind();

            this.curF++;
        }
    }]);

    return GlitchPass;
}(FxPass);

/* Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV.
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE. */

/**
 * @class Common utilities
 * @name glMatrix
 */
var glMatrix = {};

// Configuration Constants
glMatrix.EPSILON = 0.000001;
glMatrix.ARRAY_TYPE = typeof Float32Array !== 'undefined' ? Float32Array : Array;
glMatrix.RANDOM = Math.random;
glMatrix.ENABLE_SIMD = false;

// Capability detection
glMatrix.SIMD_AVAILABLE = glMatrix.ARRAY_TYPE === window.Float32Array && 'SIMD' in window;
glMatrix.USE_SIMD = glMatrix.ENABLE_SIMD && glMatrix.SIMD_AVAILABLE;

/**
 * Sets the type of array used when creating new vectors and matrices
 *
 * @param {Type} type Array type, such as Float32Array or Array
 */
glMatrix.setMatrixArrayType = function (type) {
  glMatrix.ARRAY_TYPE = type;
};

var degree = Math.PI / 180;

/**
 * Convert Degree To Radian
 *
 * @param {Number} a Angle in Degrees
 */
glMatrix.toRadian = function (a) {
  return a * degree;
};

/**
 * Tests whether or not the arguments have approximately the same value, within an absolute
 * or relative tolerance of glMatrix.EPSILON (an absolute tolerance is used for values less
 * than or equal to 1.0, and a relative tolerance is used for larger values)
 *
 * @param {Number} a The first number to test.
 * @param {Number} b The second number to test.
 * @returns {Boolean} True if the numbers are approximately equal, false otherwise.
 */
glMatrix.equals = function (a, b) {
  return Math.abs(a - b) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a), Math.abs(b));
};

/* Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV.
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE. */

/**
 * @class 4x4 Matrix
 * @name mat4
 */
var mat4 = {
    scalar: {},
    SIMD: {}
};

/**
 * Creates a new identity mat4
 *
 * @returns {mat4} a new 4x4 matrix
 */
mat4.create = function () {
    var out = new glMatrix.ARRAY_TYPE(16);
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a new mat4 initialized with values from an existing matrix
 *
 * @param {mat4} a matrix to clone
 * @returns {mat4} a new 4x4 matrix
 */
mat4.clone = function (a) {
    var out = new glMatrix.ARRAY_TYPE(16);
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    out[9] = a[9];
    out[10] = a[10];
    out[11] = a[11];
    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
    return out;
};

/**
 * Copy the values from one mat4 to another
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.copy = function (out, a) {
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    out[9] = a[9];
    out[10] = a[10];
    out[11] = a[11];
    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
    return out;
};

/**
 * Create a new mat4 with the given values
 *
 * @param {Number} m00 Component in column 0, row 0 position (index 0)
 * @param {Number} m01 Component in column 0, row 1 position (index 1)
 * @param {Number} m02 Component in column 0, row 2 position (index 2)
 * @param {Number} m03 Component in column 0, row 3 position (index 3)
 * @param {Number} m10 Component in column 1, row 0 position (index 4)
 * @param {Number} m11 Component in column 1, row 1 position (index 5)
 * @param {Number} m12 Component in column 1, row 2 position (index 6)
 * @param {Number} m13 Component in column 1, row 3 position (index 7)
 * @param {Number} m20 Component in column 2, row 0 position (index 8)
 * @param {Number} m21 Component in column 2, row 1 position (index 9)
 * @param {Number} m22 Component in column 2, row 2 position (index 10)
 * @param {Number} m23 Component in column 2, row 3 position (index 11)
 * @param {Number} m30 Component in column 3, row 0 position (index 12)
 * @param {Number} m31 Component in column 3, row 1 position (index 13)
 * @param {Number} m32 Component in column 3, row 2 position (index 14)
 * @param {Number} m33 Component in column 3, row 3 position (index 15)
 * @returns {mat4} A new mat4
 */
mat4.fromValues = function (m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33) {
    var out = new glMatrix.ARRAY_TYPE(16);
    out[0] = m00;
    out[1] = m01;
    out[2] = m02;
    out[3] = m03;
    out[4] = m10;
    out[5] = m11;
    out[6] = m12;
    out[7] = m13;
    out[8] = m20;
    out[9] = m21;
    out[10] = m22;
    out[11] = m23;
    out[12] = m30;
    out[13] = m31;
    out[14] = m32;
    out[15] = m33;
    return out;
};

/**
 * Set the components of a mat4 to the given values
 *
 * @param {mat4} out the receiving matrix
 * @param {Number} m00 Component in column 0, row 0 position (index 0)
 * @param {Number} m01 Component in column 0, row 1 position (index 1)
 * @param {Number} m02 Component in column 0, row 2 position (index 2)
 * @param {Number} m03 Component in column 0, row 3 position (index 3)
 * @param {Number} m10 Component in column 1, row 0 position (index 4)
 * @param {Number} m11 Component in column 1, row 1 position (index 5)
 * @param {Number} m12 Component in column 1, row 2 position (index 6)
 * @param {Number} m13 Component in column 1, row 3 position (index 7)
 * @param {Number} m20 Component in column 2, row 0 position (index 8)
 * @param {Number} m21 Component in column 2, row 1 position (index 9)
 * @param {Number} m22 Component in column 2, row 2 position (index 10)
 * @param {Number} m23 Component in column 2, row 3 position (index 11)
 * @param {Number} m30 Component in column 3, row 0 position (index 12)
 * @param {Number} m31 Component in column 3, row 1 position (index 13)
 * @param {Number} m32 Component in column 3, row 2 position (index 14)
 * @param {Number} m33 Component in column 3, row 3 position (index 15)
 * @returns {mat4} out
 */
mat4.set = function (out, m00, m01, m02, m03, m10, m11, m12, m13, m20, m21, m22, m23, m30, m31, m32, m33) {
    out[0] = m00;
    out[1] = m01;
    out[2] = m02;
    out[3] = m03;
    out[4] = m10;
    out[5] = m11;
    out[6] = m12;
    out[7] = m13;
    out[8] = m20;
    out[9] = m21;
    out[10] = m22;
    out[11] = m23;
    out[12] = m30;
    out[13] = m31;
    out[14] = m32;
    out[15] = m33;
    return out;
};

/**
 * Set a mat4 to the identity matrix
 *
 * @param {mat4} out the receiving matrix
 * @returns {mat4} out
 */
mat4.identity = function (out) {
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Transpose the values of a mat4 not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.scalar.transpose = function (out, a) {
    // If we are transposing ourselves we can skip a few steps but have to cache some values
    if (out === a) {
        var a01 = a[1],
            a02 = a[2],
            a03 = a[3],
            a12 = a[6],
            a13 = a[7],
            a23 = a[11];

        out[1] = a[4];
        out[2] = a[8];
        out[3] = a[12];
        out[4] = a01;
        out[6] = a[9];
        out[7] = a[13];
        out[8] = a02;
        out[9] = a12;
        out[11] = a[14];
        out[12] = a03;
        out[13] = a13;
        out[14] = a23;
    } else {
        out[0] = a[0];
        out[1] = a[4];
        out[2] = a[8];
        out[3] = a[12];
        out[4] = a[1];
        out[5] = a[5];
        out[6] = a[9];
        out[7] = a[13];
        out[8] = a[2];
        out[9] = a[6];
        out[10] = a[10];
        out[11] = a[14];
        out[12] = a[3];
        out[13] = a[7];
        out[14] = a[11];
        out[15] = a[15];
    }

    return out;
};

/**
 * Transpose the values of a mat4 using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.SIMD.transpose = function (out, a) {
    var a0, a1, a2, a3, tmp01, tmp23, out0, out1, out2, out3;

    a0 = SIMD.Float32x4.load(a, 0);
    a1 = SIMD.Float32x4.load(a, 4);
    a2 = SIMD.Float32x4.load(a, 8);
    a3 = SIMD.Float32x4.load(a, 12);

    tmp01 = SIMD.Float32x4.shuffle(a0, a1, 0, 1, 4, 5);
    tmp23 = SIMD.Float32x4.shuffle(a2, a3, 0, 1, 4, 5);
    out0 = SIMD.Float32x4.shuffle(tmp01, tmp23, 0, 2, 4, 6);
    out1 = SIMD.Float32x4.shuffle(tmp01, tmp23, 1, 3, 5, 7);
    SIMD.Float32x4.store(out, 0, out0);
    SIMD.Float32x4.store(out, 4, out1);

    tmp01 = SIMD.Float32x4.shuffle(a0, a1, 2, 3, 6, 7);
    tmp23 = SIMD.Float32x4.shuffle(a2, a3, 2, 3, 6, 7);
    out2 = SIMD.Float32x4.shuffle(tmp01, tmp23, 0, 2, 4, 6);
    out3 = SIMD.Float32x4.shuffle(tmp01, tmp23, 1, 3, 5, 7);
    SIMD.Float32x4.store(out, 8, out2);
    SIMD.Float32x4.store(out, 12, out3);

    return out;
};

/**
 * Transpse a mat4 using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.transpose = glMatrix.USE_SIMD ? mat4.SIMD.transpose : mat4.scalar.transpose;

/**
 * Inverts a mat4 not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.scalar.invert = function (out, a) {
    var a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11],
        a30 = a[12],
        a31 = a[13],
        a32 = a[14],
        a33 = a[15],
        b00 = a00 * a11 - a01 * a10,
        b01 = a00 * a12 - a02 * a10,
        b02 = a00 * a13 - a03 * a10,
        b03 = a01 * a12 - a02 * a11,
        b04 = a01 * a13 - a03 * a11,
        b05 = a02 * a13 - a03 * a12,
        b06 = a20 * a31 - a21 * a30,
        b07 = a20 * a32 - a22 * a30,
        b08 = a20 * a33 - a23 * a30,
        b09 = a21 * a32 - a22 * a31,
        b10 = a21 * a33 - a23 * a31,
        b11 = a22 * a33 - a23 * a32,


    // Calculate the determinant
    det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

    if (!det) {
        return null;
    }
    det = 1.0 / det;

    out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
    out[1] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
    out[2] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
    out[3] = (a22 * b04 - a21 * b05 - a23 * b03) * det;
    out[4] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
    out[5] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
    out[6] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
    out[7] = (a20 * b05 - a22 * b02 + a23 * b01) * det;
    out[8] = (a10 * b10 - a11 * b08 + a13 * b06) * det;
    out[9] = (a01 * b08 - a00 * b10 - a03 * b06) * det;
    out[10] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
    out[11] = (a21 * b02 - a20 * b04 - a23 * b00) * det;
    out[12] = (a11 * b07 - a10 * b09 - a12 * b06) * det;
    out[13] = (a00 * b09 - a01 * b07 + a02 * b06) * det;
    out[14] = (a31 * b01 - a30 * b03 - a32 * b00) * det;
    out[15] = (a20 * b03 - a21 * b01 + a22 * b00) * det;

    return out;
};

/**
 * Inverts a mat4 using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.SIMD.invert = function (out, a) {
    var row0,
        row1,
        row2,
        row3,
        tmp1,
        minor0,
        minor1,
        minor2,
        minor3,
        det,
        a0 = SIMD.Float32x4.load(a, 0),
        a1 = SIMD.Float32x4.load(a, 4),
        a2 = SIMD.Float32x4.load(a, 8),
        a3 = SIMD.Float32x4.load(a, 12);

    // Compute matrix adjugate
    tmp1 = SIMD.Float32x4.shuffle(a0, a1, 0, 1, 4, 5);
    row1 = SIMD.Float32x4.shuffle(a2, a3, 0, 1, 4, 5);
    row0 = SIMD.Float32x4.shuffle(tmp1, row1, 0, 2, 4, 6);
    row1 = SIMD.Float32x4.shuffle(row1, tmp1, 1, 3, 5, 7);
    tmp1 = SIMD.Float32x4.shuffle(a0, a1, 2, 3, 6, 7);
    row3 = SIMD.Float32x4.shuffle(a2, a3, 2, 3, 6, 7);
    row2 = SIMD.Float32x4.shuffle(tmp1, row3, 0, 2, 4, 6);
    row3 = SIMD.Float32x4.shuffle(row3, tmp1, 1, 3, 5, 7);

    tmp1 = SIMD.Float32x4.mul(row2, row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor0 = SIMD.Float32x4.mul(row1, tmp1);
    minor1 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row1, tmp1), minor0);
    minor1 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor1);
    minor1 = SIMD.Float32x4.swizzle(minor1, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(row1, row2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor0 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor0);
    minor3 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(minor0, SIMD.Float32x4.mul(row3, tmp1));
    minor3 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor3);
    minor3 = SIMD.Float32x4.swizzle(minor3, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(row1, 2, 3, 0, 1), row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    row2 = SIMD.Float32x4.swizzle(row2, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row2, tmp1), minor0);
    minor2 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(minor0, SIMD.Float32x4.mul(row2, tmp1));
    minor2 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor2);
    minor2 = SIMD.Float32x4.swizzle(minor2, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(row0, row1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor2 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor2);
    minor3 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row2, tmp1), minor3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor2 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row3, tmp1), minor2);
    minor3 = SIMD.Float32x4.sub(minor3, SIMD.Float32x4.mul(row2, tmp1));

    tmp1 = SIMD.Float32x4.mul(row0, row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor1 = SIMD.Float32x4.sub(minor1, SIMD.Float32x4.mul(row2, tmp1));
    minor2 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row1, tmp1), minor2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor1 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row2, tmp1), minor1);
    minor2 = SIMD.Float32x4.sub(minor2, SIMD.Float32x4.mul(row1, tmp1));

    tmp1 = SIMD.Float32x4.mul(row0, row2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor1 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor1);
    minor3 = SIMD.Float32x4.sub(minor3, SIMD.Float32x4.mul(row1, tmp1));
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor1 = SIMD.Float32x4.sub(minor1, SIMD.Float32x4.mul(row3, tmp1));
    minor3 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row1, tmp1), minor3);

    // Compute matrix determinant
    det = SIMD.Float32x4.mul(row0, minor0);
    det = SIMD.Float32x4.add(SIMD.Float32x4.swizzle(det, 2, 3, 0, 1), det);
    det = SIMD.Float32x4.add(SIMD.Float32x4.swizzle(det, 1, 0, 3, 2), det);
    tmp1 = SIMD.Float32x4.reciprocalApproximation(det);
    det = SIMD.Float32x4.sub(SIMD.Float32x4.add(tmp1, tmp1), SIMD.Float32x4.mul(det, SIMD.Float32x4.mul(tmp1, tmp1)));
    det = SIMD.Float32x4.swizzle(det, 0, 0, 0, 0);
    if (!det) {
        return null;
    }

    // Compute matrix inverse
    SIMD.Float32x4.store(out, 0, SIMD.Float32x4.mul(det, minor0));
    SIMD.Float32x4.store(out, 4, SIMD.Float32x4.mul(det, minor1));
    SIMD.Float32x4.store(out, 8, SIMD.Float32x4.mul(det, minor2));
    SIMD.Float32x4.store(out, 12, SIMD.Float32x4.mul(det, minor3));
    return out;
};

/**
 * Inverts a mat4 using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.invert = glMatrix.USE_SIMD ? mat4.SIMD.invert : mat4.scalar.invert;

/**
 * Calculates the adjugate of a mat4 not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.scalar.adjoint = function (out, a) {
    var a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11],
        a30 = a[12],
        a31 = a[13],
        a32 = a[14],
        a33 = a[15];

    out[0] = a11 * (a22 * a33 - a23 * a32) - a21 * (a12 * a33 - a13 * a32) + a31 * (a12 * a23 - a13 * a22);
    out[1] = -(a01 * (a22 * a33 - a23 * a32) - a21 * (a02 * a33 - a03 * a32) + a31 * (a02 * a23 - a03 * a22));
    out[2] = a01 * (a12 * a33 - a13 * a32) - a11 * (a02 * a33 - a03 * a32) + a31 * (a02 * a13 - a03 * a12);
    out[3] = -(a01 * (a12 * a23 - a13 * a22) - a11 * (a02 * a23 - a03 * a22) + a21 * (a02 * a13 - a03 * a12));
    out[4] = -(a10 * (a22 * a33 - a23 * a32) - a20 * (a12 * a33 - a13 * a32) + a30 * (a12 * a23 - a13 * a22));
    out[5] = a00 * (a22 * a33 - a23 * a32) - a20 * (a02 * a33 - a03 * a32) + a30 * (a02 * a23 - a03 * a22);
    out[6] = -(a00 * (a12 * a33 - a13 * a32) - a10 * (a02 * a33 - a03 * a32) + a30 * (a02 * a13 - a03 * a12));
    out[7] = a00 * (a12 * a23 - a13 * a22) - a10 * (a02 * a23 - a03 * a22) + a20 * (a02 * a13 - a03 * a12);
    out[8] = a10 * (a21 * a33 - a23 * a31) - a20 * (a11 * a33 - a13 * a31) + a30 * (a11 * a23 - a13 * a21);
    out[9] = -(a00 * (a21 * a33 - a23 * a31) - a20 * (a01 * a33 - a03 * a31) + a30 * (a01 * a23 - a03 * a21));
    out[10] = a00 * (a11 * a33 - a13 * a31) - a10 * (a01 * a33 - a03 * a31) + a30 * (a01 * a13 - a03 * a11);
    out[11] = -(a00 * (a11 * a23 - a13 * a21) - a10 * (a01 * a23 - a03 * a21) + a20 * (a01 * a13 - a03 * a11));
    out[12] = -(a10 * (a21 * a32 - a22 * a31) - a20 * (a11 * a32 - a12 * a31) + a30 * (a11 * a22 - a12 * a21));
    out[13] = a00 * (a21 * a32 - a22 * a31) - a20 * (a01 * a32 - a02 * a31) + a30 * (a01 * a22 - a02 * a21);
    out[14] = -(a00 * (a11 * a32 - a12 * a31) - a10 * (a01 * a32 - a02 * a31) + a30 * (a01 * a12 - a02 * a11));
    out[15] = a00 * (a11 * a22 - a12 * a21) - a10 * (a01 * a22 - a02 * a21) + a20 * (a01 * a12 - a02 * a11);
    return out;
};

/**
 * Calculates the adjugate of a mat4 using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.SIMD.adjoint = function (out, a) {
    var a0, a1, a2, a3;
    var row0, row1, row2, row3;
    var tmp1;
    var minor0, minor1, minor2, minor3;

    a0 = SIMD.Float32x4.load(a, 0);
    a1 = SIMD.Float32x4.load(a, 4);
    a2 = SIMD.Float32x4.load(a, 8);
    a3 = SIMD.Float32x4.load(a, 12);

    // Transpose the source matrix.  Sort of.  Not a true transpose operation
    tmp1 = SIMD.Float32x4.shuffle(a0, a1, 0, 1, 4, 5);
    row1 = SIMD.Float32x4.shuffle(a2, a3, 0, 1, 4, 5);
    row0 = SIMD.Float32x4.shuffle(tmp1, row1, 0, 2, 4, 6);
    row1 = SIMD.Float32x4.shuffle(row1, tmp1, 1, 3, 5, 7);

    tmp1 = SIMD.Float32x4.shuffle(a0, a1, 2, 3, 6, 7);
    row3 = SIMD.Float32x4.shuffle(a2, a3, 2, 3, 6, 7);
    row2 = SIMD.Float32x4.shuffle(tmp1, row3, 0, 2, 4, 6);
    row3 = SIMD.Float32x4.shuffle(row3, tmp1, 1, 3, 5, 7);

    tmp1 = SIMD.Float32x4.mul(row2, row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor0 = SIMD.Float32x4.mul(row1, tmp1);
    minor1 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row1, tmp1), minor0);
    minor1 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor1);
    minor1 = SIMD.Float32x4.swizzle(minor1, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(row1, row2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor0 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor0);
    minor3 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(minor0, SIMD.Float32x4.mul(row3, tmp1));
    minor3 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor3);
    minor3 = SIMD.Float32x4.swizzle(minor3, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(row1, 2, 3, 0, 1), row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    row2 = SIMD.Float32x4.swizzle(row2, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row2, tmp1), minor0);
    minor2 = SIMD.Float32x4.mul(row0, tmp1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor0 = SIMD.Float32x4.sub(minor0, SIMD.Float32x4.mul(row2, tmp1));
    minor2 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row0, tmp1), minor2);
    minor2 = SIMD.Float32x4.swizzle(minor2, 2, 3, 0, 1);

    tmp1 = SIMD.Float32x4.mul(row0, row1);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor2 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor2);
    minor3 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row2, tmp1), minor3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor2 = SIMD.Float32x4.sub(SIMD.Float32x4.mul(row3, tmp1), minor2);
    minor3 = SIMD.Float32x4.sub(minor3, SIMD.Float32x4.mul(row2, tmp1));

    tmp1 = SIMD.Float32x4.mul(row0, row3);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor1 = SIMD.Float32x4.sub(minor1, SIMD.Float32x4.mul(row2, tmp1));
    minor2 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row1, tmp1), minor2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor1 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row2, tmp1), minor1);
    minor2 = SIMD.Float32x4.sub(minor2, SIMD.Float32x4.mul(row1, tmp1));

    tmp1 = SIMD.Float32x4.mul(row0, row2);
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 1, 0, 3, 2);
    minor1 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row3, tmp1), minor1);
    minor3 = SIMD.Float32x4.sub(minor3, SIMD.Float32x4.mul(row1, tmp1));
    tmp1 = SIMD.Float32x4.swizzle(tmp1, 2, 3, 0, 1);
    minor1 = SIMD.Float32x4.sub(minor1, SIMD.Float32x4.mul(row3, tmp1));
    minor3 = SIMD.Float32x4.add(SIMD.Float32x4.mul(row1, tmp1), minor3);

    SIMD.Float32x4.store(out, 0, minor0);
    SIMD.Float32x4.store(out, 4, minor1);
    SIMD.Float32x4.store(out, 8, minor2);
    SIMD.Float32x4.store(out, 12, minor3);
    return out;
};

/**
 * Calculates the adjugate of a mat4 using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the source matrix
 * @returns {mat4} out
 */
mat4.adjoint = glMatrix.USE_SIMD ? mat4.SIMD.adjoint : mat4.scalar.adjoint;

/**
 * Calculates the determinant of a mat4
 *
 * @param {mat4} a the source matrix
 * @returns {Number} determinant of a
 */
mat4.determinant = function (a) {
    var a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11],
        a30 = a[12],
        a31 = a[13],
        a32 = a[14],
        a33 = a[15],
        b00 = a00 * a11 - a01 * a10,
        b01 = a00 * a12 - a02 * a10,
        b02 = a00 * a13 - a03 * a10,
        b03 = a01 * a12 - a02 * a11,
        b04 = a01 * a13 - a03 * a11,
        b05 = a02 * a13 - a03 * a12,
        b06 = a20 * a31 - a21 * a30,
        b07 = a20 * a32 - a22 * a30,
        b08 = a20 * a33 - a23 * a30,
        b09 = a21 * a32 - a22 * a31,
        b10 = a21 * a33 - a23 * a31,
        b11 = a22 * a33 - a23 * a32;

    // Calculate the determinant
    return b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;
};

/**
 * Multiplies two mat4's explicitly using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the first operand, must be a Float32Array
 * @param {mat4} b the second operand, must be a Float32Array
 * @returns {mat4} out
 */
mat4.SIMD.multiply = function (out, a, b) {
    var a0 = SIMD.Float32x4.load(a, 0);
    var a1 = SIMD.Float32x4.load(a, 4);
    var a2 = SIMD.Float32x4.load(a, 8);
    var a3 = SIMD.Float32x4.load(a, 12);

    var b0 = SIMD.Float32x4.load(b, 0);
    var out0 = SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b0, 0, 0, 0, 0), a0), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b0, 1, 1, 1, 1), a1), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b0, 2, 2, 2, 2), a2), SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b0, 3, 3, 3, 3), a3))));
    SIMD.Float32x4.store(out, 0, out0);

    var b1 = SIMD.Float32x4.load(b, 4);
    var out1 = SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b1, 0, 0, 0, 0), a0), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b1, 1, 1, 1, 1), a1), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b1, 2, 2, 2, 2), a2), SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b1, 3, 3, 3, 3), a3))));
    SIMD.Float32x4.store(out, 4, out1);

    var b2 = SIMD.Float32x4.load(b, 8);
    var out2 = SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b2, 0, 0, 0, 0), a0), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b2, 1, 1, 1, 1), a1), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b2, 2, 2, 2, 2), a2), SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b2, 3, 3, 3, 3), a3))));
    SIMD.Float32x4.store(out, 8, out2);

    var b3 = SIMD.Float32x4.load(b, 12);
    var out3 = SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b3, 0, 0, 0, 0), a0), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b3, 1, 1, 1, 1), a1), SIMD.Float32x4.add(SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b3, 2, 2, 2, 2), a2), SIMD.Float32x4.mul(SIMD.Float32x4.swizzle(b3, 3, 3, 3, 3), a3))));
    SIMD.Float32x4.store(out, 12, out3);

    return out;
};

/**
 * Multiplies two mat4's explicitly not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the first operand
 * @param {mat4} b the second operand
 * @returns {mat4} out
 */
mat4.scalar.multiply = function (out, a, b) {
    var a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11],
        a30 = a[12],
        a31 = a[13],
        a32 = a[14],
        a33 = a[15];

    // Cache only the current line of the second matrix
    var b0 = b[0],
        b1 = b[1],
        b2 = b[2],
        b3 = b[3];
    out[0] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[1] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[2] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[3] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

    b0 = b[4];b1 = b[5];b2 = b[6];b3 = b[7];
    out[4] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[5] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[6] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[7] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

    b0 = b[8];b1 = b[9];b2 = b[10];b3 = b[11];
    out[8] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[9] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[10] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[11] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;

    b0 = b[12];b1 = b[13];b2 = b[14];b3 = b[15];
    out[12] = b0 * a00 + b1 * a10 + b2 * a20 + b3 * a30;
    out[13] = b0 * a01 + b1 * a11 + b2 * a21 + b3 * a31;
    out[14] = b0 * a02 + b1 * a12 + b2 * a22 + b3 * a32;
    out[15] = b0 * a03 + b1 * a13 + b2 * a23 + b3 * a33;
    return out;
};

/**
 * Multiplies two mat4's using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the first operand
 * @param {mat4} b the second operand
 * @returns {mat4} out
 */
mat4.multiply = glMatrix.USE_SIMD ? mat4.SIMD.multiply : mat4.scalar.multiply;

/**
 * Alias for {@link mat4.multiply}
 * @function
 */
mat4.mul = mat4.multiply;

/**
 * Translate a mat4 by the given vector not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to translate
 * @param {vec3} v vector to translate by
 * @returns {mat4} out
 */
mat4.scalar.translate = function (out, a, v) {
    var x = v[0],
        y = v[1],
        z = v[2],
        a00,
        a01,
        a02,
        a03,
        a10,
        a11,
        a12,
        a13,
        a20,
        a21,
        a22,
        a23;

    if (a === out) {
        out[12] = a[0] * x + a[4] * y + a[8] * z + a[12];
        out[13] = a[1] * x + a[5] * y + a[9] * z + a[13];
        out[14] = a[2] * x + a[6] * y + a[10] * z + a[14];
        out[15] = a[3] * x + a[7] * y + a[11] * z + a[15];
    } else {
        a00 = a[0];a01 = a[1];a02 = a[2];a03 = a[3];
        a10 = a[4];a11 = a[5];a12 = a[6];a13 = a[7];
        a20 = a[8];a21 = a[9];a22 = a[10];a23 = a[11];

        out[0] = a00;out[1] = a01;out[2] = a02;out[3] = a03;
        out[4] = a10;out[5] = a11;out[6] = a12;out[7] = a13;
        out[8] = a20;out[9] = a21;out[10] = a22;out[11] = a23;

        out[12] = a00 * x + a10 * y + a20 * z + a[12];
        out[13] = a01 * x + a11 * y + a21 * z + a[13];
        out[14] = a02 * x + a12 * y + a22 * z + a[14];
        out[15] = a03 * x + a13 * y + a23 * z + a[15];
    }

    return out;
};

/**
 * Translates a mat4 by the given vector using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to translate
 * @param {vec3} v vector to translate by
 * @returns {mat4} out
 */
mat4.SIMD.translate = function (out, a, v) {
    var a0 = SIMD.Float32x4.load(a, 0),
        a1 = SIMD.Float32x4.load(a, 4),
        a2 = SIMD.Float32x4.load(a, 8),
        a3 = SIMD.Float32x4.load(a, 12),
        vec = SIMD.Float32x4(v[0], v[1], v[2], 0);

    if (a !== out) {
        out[0] = a[0];out[1] = a[1];out[2] = a[2];out[3] = a[3];
        out[4] = a[4];out[5] = a[5];out[6] = a[6];out[7] = a[7];
        out[8] = a[8];out[9] = a[9];out[10] = a[10];out[11] = a[11];
    }

    a0 = SIMD.Float32x4.mul(a0, SIMD.Float32x4.swizzle(vec, 0, 0, 0, 0));
    a1 = SIMD.Float32x4.mul(a1, SIMD.Float32x4.swizzle(vec, 1, 1, 1, 1));
    a2 = SIMD.Float32x4.mul(a2, SIMD.Float32x4.swizzle(vec, 2, 2, 2, 2));

    var t0 = SIMD.Float32x4.add(a0, SIMD.Float32x4.add(a1, SIMD.Float32x4.add(a2, a3)));
    SIMD.Float32x4.store(out, 12, t0);

    return out;
};

/**
 * Translates a mat4 by the given vector using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to translate
 * @param {vec3} v vector to translate by
 * @returns {mat4} out
 */
mat4.translate = glMatrix.USE_SIMD ? mat4.SIMD.translate : mat4.scalar.translate;

/**
 * Scales the mat4 by the dimensions in the given vec3 not using vectorization
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to scale
 * @param {vec3} v the vec3 to scale the matrix by
 * @returns {mat4} out
 **/
mat4.scalar.scale = function (out, a, v) {
    var x = v[0],
        y = v[1],
        z = v[2];

    out[0] = a[0] * x;
    out[1] = a[1] * x;
    out[2] = a[2] * x;
    out[3] = a[3] * x;
    out[4] = a[4] * y;
    out[5] = a[5] * y;
    out[6] = a[6] * y;
    out[7] = a[7] * y;
    out[8] = a[8] * z;
    out[9] = a[9] * z;
    out[10] = a[10] * z;
    out[11] = a[11] * z;
    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
    return out;
};

/**
 * Scales the mat4 by the dimensions in the given vec3 using vectorization
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to scale
 * @param {vec3} v the vec3 to scale the matrix by
 * @returns {mat4} out
 **/
mat4.SIMD.scale = function (out, a, v) {
    var a0, a1, a2;
    var vec = SIMD.Float32x4(v[0], v[1], v[2], 0);

    a0 = SIMD.Float32x4.load(a, 0);
    SIMD.Float32x4.store(out, 0, SIMD.Float32x4.mul(a0, SIMD.Float32x4.swizzle(vec, 0, 0, 0, 0)));

    a1 = SIMD.Float32x4.load(a, 4);
    SIMD.Float32x4.store(out, 4, SIMD.Float32x4.mul(a1, SIMD.Float32x4.swizzle(vec, 1, 1, 1, 1)));

    a2 = SIMD.Float32x4.load(a, 8);
    SIMD.Float32x4.store(out, 8, SIMD.Float32x4.mul(a2, SIMD.Float32x4.swizzle(vec, 2, 2, 2, 2)));

    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
    return out;
};

/**
 * Scales the mat4 by the dimensions in the given vec3 using SIMD if available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to scale
 * @param {vec3} v the vec3 to scale the matrix by
 * @returns {mat4} out
 */
mat4.scale = glMatrix.USE_SIMD ? mat4.SIMD.scale : mat4.scalar.scale;

/**
 * Rotates a mat4 by the given angle around the given axis
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @param {vec3} axis the axis to rotate around
 * @returns {mat4} out
 */
mat4.rotate = function (out, a, rad, axis) {
    var x = axis[0],
        y = axis[1],
        z = axis[2],
        len = Math.sqrt(x * x + y * y + z * z),
        s,
        c,
        t,
        a00,
        a01,
        a02,
        a03,
        a10,
        a11,
        a12,
        a13,
        a20,
        a21,
        a22,
        a23,
        b00,
        b01,
        b02,
        b10,
        b11,
        b12,
        b20,
        b21,
        b22;

    if (Math.abs(len) < glMatrix.EPSILON) {
        return null;
    }

    len = 1 / len;
    x *= len;
    y *= len;
    z *= len;

    s = Math.sin(rad);
    c = Math.cos(rad);
    t = 1 - c;

    a00 = a[0];a01 = a[1];a02 = a[2];a03 = a[3];
    a10 = a[4];a11 = a[5];a12 = a[6];a13 = a[7];
    a20 = a[8];a21 = a[9];a22 = a[10];a23 = a[11];

    // Construct the elements of the rotation matrix
    b00 = x * x * t + c;b01 = y * x * t + z * s;b02 = z * x * t - y * s;
    b10 = x * y * t - z * s;b11 = y * y * t + c;b12 = z * y * t + x * s;
    b20 = x * z * t + y * s;b21 = y * z * t - x * s;b22 = z * z * t + c;

    // Perform rotation-specific matrix multiplication
    out[0] = a00 * b00 + a10 * b01 + a20 * b02;
    out[1] = a01 * b00 + a11 * b01 + a21 * b02;
    out[2] = a02 * b00 + a12 * b01 + a22 * b02;
    out[3] = a03 * b00 + a13 * b01 + a23 * b02;
    out[4] = a00 * b10 + a10 * b11 + a20 * b12;
    out[5] = a01 * b10 + a11 * b11 + a21 * b12;
    out[6] = a02 * b10 + a12 * b11 + a22 * b12;
    out[7] = a03 * b10 + a13 * b11 + a23 * b12;
    out[8] = a00 * b20 + a10 * b21 + a20 * b22;
    out[9] = a01 * b20 + a11 * b21 + a21 * b22;
    out[10] = a02 * b20 + a12 * b21 + a22 * b22;
    out[11] = a03 * b20 + a13 * b21 + a23 * b22;

    if (a !== out) {
        // If the source and destination differ, copy the unchanged last row
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }
    return out;
};

/**
 * Rotates a matrix by the given angle around the X axis not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.scalar.rotateX = function (out, a, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad),
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11];

    if (a !== out) {
        // If the source and destination differ, copy the unchanged rows
        out[0] = a[0];
        out[1] = a[1];
        out[2] = a[2];
        out[3] = a[3];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    out[4] = a10 * c + a20 * s;
    out[5] = a11 * c + a21 * s;
    out[6] = a12 * c + a22 * s;
    out[7] = a13 * c + a23 * s;
    out[8] = a20 * c - a10 * s;
    out[9] = a21 * c - a11 * s;
    out[10] = a22 * c - a12 * s;
    out[11] = a23 * c - a13 * s;
    return out;
};

/**
 * Rotates a matrix by the given angle around the X axis using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.SIMD.rotateX = function (out, a, rad) {
    var s = SIMD.Float32x4.splat(Math.sin(rad)),
        c = SIMD.Float32x4.splat(Math.cos(rad));

    if (a !== out) {
        // If the source and destination differ, copy the unchanged rows
        out[0] = a[0];
        out[1] = a[1];
        out[2] = a[2];
        out[3] = a[3];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    var a_1 = SIMD.Float32x4.load(a, 4);
    var a_2 = SIMD.Float32x4.load(a, 8);
    SIMD.Float32x4.store(out, 4, SIMD.Float32x4.add(SIMD.Float32x4.mul(a_1, c), SIMD.Float32x4.mul(a_2, s)));
    SIMD.Float32x4.store(out, 8, SIMD.Float32x4.sub(SIMD.Float32x4.mul(a_2, c), SIMD.Float32x4.mul(a_1, s)));
    return out;
};

/**
 * Rotates a matrix by the given angle around the X axis using SIMD if availabe and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.rotateX = glMatrix.USE_SIMD ? mat4.SIMD.rotateX : mat4.scalar.rotateX;

/**
 * Rotates a matrix by the given angle around the Y axis not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.scalar.rotateY = function (out, a, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad),
        a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a20 = a[8],
        a21 = a[9],
        a22 = a[10],
        a23 = a[11];

    if (a !== out) {
        // If the source and destination differ, copy the unchanged rows
        out[4] = a[4];
        out[5] = a[5];
        out[6] = a[6];
        out[7] = a[7];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    out[0] = a00 * c - a20 * s;
    out[1] = a01 * c - a21 * s;
    out[2] = a02 * c - a22 * s;
    out[3] = a03 * c - a23 * s;
    out[8] = a00 * s + a20 * c;
    out[9] = a01 * s + a21 * c;
    out[10] = a02 * s + a22 * c;
    out[11] = a03 * s + a23 * c;
    return out;
};

/**
 * Rotates a matrix by the given angle around the Y axis using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.SIMD.rotateY = function (out, a, rad) {
    var s = SIMD.Float32x4.splat(Math.sin(rad)),
        c = SIMD.Float32x4.splat(Math.cos(rad));

    if (a !== out) {
        // If the source and destination differ, copy the unchanged rows
        out[4] = a[4];
        out[5] = a[5];
        out[6] = a[6];
        out[7] = a[7];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    var a_0 = SIMD.Float32x4.load(a, 0);
    var a_2 = SIMD.Float32x4.load(a, 8);
    SIMD.Float32x4.store(out, 0, SIMD.Float32x4.sub(SIMD.Float32x4.mul(a_0, c), SIMD.Float32x4.mul(a_2, s)));
    SIMD.Float32x4.store(out, 8, SIMD.Float32x4.add(SIMD.Float32x4.mul(a_0, s), SIMD.Float32x4.mul(a_2, c)));
    return out;
};

/**
 * Rotates a matrix by the given angle around the Y axis if SIMD available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.rotateY = glMatrix.USE_SIMD ? mat4.SIMD.rotateY : mat4.scalar.rotateY;

/**
 * Rotates a matrix by the given angle around the Z axis not using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.scalar.rotateZ = function (out, a, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad),
        a00 = a[0],
        a01 = a[1],
        a02 = a[2],
        a03 = a[3],
        a10 = a[4],
        a11 = a[5],
        a12 = a[6],
        a13 = a[7];

    if (a !== out) {
        // If the source and destination differ, copy the unchanged last row
        out[8] = a[8];
        out[9] = a[9];
        out[10] = a[10];
        out[11] = a[11];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    out[0] = a00 * c + a10 * s;
    out[1] = a01 * c + a11 * s;
    out[2] = a02 * c + a12 * s;
    out[3] = a03 * c + a13 * s;
    out[4] = a10 * c - a00 * s;
    out[5] = a11 * c - a01 * s;
    out[6] = a12 * c - a02 * s;
    out[7] = a13 * c - a03 * s;
    return out;
};

/**
 * Rotates a matrix by the given angle around the Z axis using SIMD
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.SIMD.rotateZ = function (out, a, rad) {
    var s = SIMD.Float32x4.splat(Math.sin(rad)),
        c = SIMD.Float32x4.splat(Math.cos(rad));

    if (a !== out) {
        // If the source and destination differ, copy the unchanged last row
        out[8] = a[8];
        out[9] = a[9];
        out[10] = a[10];
        out[11] = a[11];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    var a_0 = SIMD.Float32x4.load(a, 0);
    var a_1 = SIMD.Float32x4.load(a, 4);
    SIMD.Float32x4.store(out, 0, SIMD.Float32x4.add(SIMD.Float32x4.mul(a_0, c), SIMD.Float32x4.mul(a_1, s)));
    SIMD.Float32x4.store(out, 4, SIMD.Float32x4.sub(SIMD.Float32x4.mul(a_1, c), SIMD.Float32x4.mul(a_0, s)));
    return out;
};

/**
 * Rotates a matrix by the given angle around the Z axis if SIMD available and enabled
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to rotate
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.rotateZ = glMatrix.USE_SIMD ? mat4.SIMD.rotateZ : mat4.scalar.rotateZ;

/**
 * Creates a matrix from a vector translation
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.translate(dest, dest, vec);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {vec3} v Translation vector
 * @returns {mat4} out
 */
mat4.fromTranslation = function (out, v) {
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = v[0];
    out[13] = v[1];
    out[14] = v[2];
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from a vector scaling
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.scale(dest, dest, vec);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {vec3} v Scaling vector
 * @returns {mat4} out
 */
mat4.fromScaling = function (out, v) {
    out[0] = v[0];
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = v[1];
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = v[2];
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from a given angle around a given axis
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.rotate(dest, dest, rad, axis);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {Number} rad the angle to rotate the matrix by
 * @param {vec3} axis the axis to rotate around
 * @returns {mat4} out
 */
mat4.fromRotation = function (out, rad, axis) {
    var x = axis[0],
        y = axis[1],
        z = axis[2],
        len = Math.sqrt(x * x + y * y + z * z),
        s,
        c,
        t;

    if (Math.abs(len) < glMatrix.EPSILON) {
        return null;
    }

    len = 1 / len;
    x *= len;
    y *= len;
    z *= len;

    s = Math.sin(rad);
    c = Math.cos(rad);
    t = 1 - c;

    // Perform rotation-specific matrix multiplication
    out[0] = x * x * t + c;
    out[1] = y * x * t + z * s;
    out[2] = z * x * t - y * s;
    out[3] = 0;
    out[4] = x * y * t - z * s;
    out[5] = y * y * t + c;
    out[6] = z * y * t + x * s;
    out[7] = 0;
    out[8] = x * z * t + y * s;
    out[9] = y * z * t - x * s;
    out[10] = z * z * t + c;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from the given angle around the X axis
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.rotateX(dest, dest, rad);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.fromXRotation = function (out, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad);

    // Perform axis-specific matrix multiplication
    out[0] = 1;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = c;
    out[6] = s;
    out[7] = 0;
    out[8] = 0;
    out[9] = -s;
    out[10] = c;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from the given angle around the Y axis
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.rotateY(dest, dest, rad);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.fromYRotation = function (out, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad);

    // Perform axis-specific matrix multiplication
    out[0] = c;
    out[1] = 0;
    out[2] = -s;
    out[3] = 0;
    out[4] = 0;
    out[5] = 1;
    out[6] = 0;
    out[7] = 0;
    out[8] = s;
    out[9] = 0;
    out[10] = c;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from the given angle around the Z axis
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.rotateZ(dest, dest, rad);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {Number} rad the angle to rotate the matrix by
 * @returns {mat4} out
 */
mat4.fromZRotation = function (out, rad) {
    var s = Math.sin(rad),
        c = Math.cos(rad);

    // Perform axis-specific matrix multiplication
    out[0] = c;
    out[1] = s;
    out[2] = 0;
    out[3] = 0;
    out[4] = -s;
    out[5] = c;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 1;
    out[11] = 0;
    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;
    return out;
};

/**
 * Creates a matrix from a quaternion rotation and vector translation
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.translate(dest, vec);
 *     var quatMat = mat4.create();
 *     quat4.toMat4(quat, quatMat);
 *     mat4.multiply(dest, quatMat);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {quat4} q Rotation quaternion
 * @param {vec3} v Translation vector
 * @returns {mat4} out
 */
mat4.fromRotationTranslation = function (out, q, v) {
    // Quaternion math
    var x = q[0],
        y = q[1],
        z = q[2],
        w = q[3],
        x2 = x + x,
        y2 = y + y,
        z2 = z + z,
        xx = x * x2,
        xy = x * y2,
        xz = x * z2,
        yy = y * y2,
        yz = y * z2,
        zz = z * z2,
        wx = w * x2,
        wy = w * y2,
        wz = w * z2;

    out[0] = 1 - (yy + zz);
    out[1] = xy + wz;
    out[2] = xz - wy;
    out[3] = 0;
    out[4] = xy - wz;
    out[5] = 1 - (xx + zz);
    out[6] = yz + wx;
    out[7] = 0;
    out[8] = xz + wy;
    out[9] = yz - wx;
    out[10] = 1 - (xx + yy);
    out[11] = 0;
    out[12] = v[0];
    out[13] = v[1];
    out[14] = v[2];
    out[15] = 1;

    return out;
};

/**
 * Returns the translation vector component of a transformation
 *  matrix. If a matrix is built with fromRotationTranslation,
 *  the returned vector will be the same as the translation vector
 *  originally supplied.
 * @param  {vec3} out Vector to receive translation component
 * @param  {mat4} mat Matrix to be decomposed (input)
 * @return {vec3} out
 */
mat4.getTranslation = function (out, mat) {
    out[0] = mat[12];
    out[1] = mat[13];
    out[2] = mat[14];

    return out;
};

/**
 * Returns a quaternion representing the rotational component
 *  of a transformation matrix. If a matrix is built with
 *  fromRotationTranslation, the returned quaternion will be the
 *  same as the quaternion originally supplied.
 * @param {quat} out Quaternion to receive the rotation component
 * @param {mat4} mat Matrix to be decomposed (input)
 * @return {quat} out
 */
mat4.getRotation = function (out, mat) {
    // Algorithm taken from http://www.euclideanspace.com/maths/geometry/rotations/conversions/matrixToQuaternion/index.htm
    var trace = mat[0] + mat[5] + mat[10];
    var S = 0;

    if (trace > 0) {
        S = Math.sqrt(trace + 1.0) * 2;
        out[3] = 0.25 * S;
        out[0] = (mat[6] - mat[9]) / S;
        out[1] = (mat[8] - mat[2]) / S;
        out[2] = (mat[1] - mat[4]) / S;
    } else if (mat[0] > mat[5] & mat[0] > mat[10]) {
        S = Math.sqrt(1.0 + mat[0] - mat[5] - mat[10]) * 2;
        out[3] = (mat[6] - mat[9]) / S;
        out[0] = 0.25 * S;
        out[1] = (mat[1] + mat[4]) / S;
        out[2] = (mat[8] + mat[2]) / S;
    } else if (mat[5] > mat[10]) {
        S = Math.sqrt(1.0 + mat[5] - mat[0] - mat[10]) * 2;
        out[3] = (mat[8] - mat[2]) / S;
        out[0] = (mat[1] + mat[4]) / S;
        out[1] = 0.25 * S;
        out[2] = (mat[6] + mat[9]) / S;
    } else {
        S = Math.sqrt(1.0 + mat[10] - mat[0] - mat[5]) * 2;
        out[3] = (mat[1] - mat[4]) / S;
        out[0] = (mat[8] + mat[2]) / S;
        out[1] = (mat[6] + mat[9]) / S;
        out[2] = 0.25 * S;
    }

    return out;
};

/**
 * Creates a matrix from a quaternion rotation, vector translation and vector scale
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.translate(dest, vec);
 *     var quatMat = mat4.create();
 *     quat4.toMat4(quat, quatMat);
 *     mat4.multiply(dest, quatMat);
 *     mat4.scale(dest, scale)
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {quat4} q Rotation quaternion
 * @param {vec3} v Translation vector
 * @param {vec3} s Scaling vector
 * @returns {mat4} out
 */
mat4.fromRotationTranslationScale = function (out, q, v, s) {
    // Quaternion math
    var x = q[0],
        y = q[1],
        z = q[2],
        w = q[3],
        x2 = x + x,
        y2 = y + y,
        z2 = z + z,
        xx = x * x2,
        xy = x * y2,
        xz = x * z2,
        yy = y * y2,
        yz = y * z2,
        zz = z * z2,
        wx = w * x2,
        wy = w * y2,
        wz = w * z2,
        sx = s[0],
        sy = s[1],
        sz = s[2];

    out[0] = (1 - (yy + zz)) * sx;
    out[1] = (xy + wz) * sx;
    out[2] = (xz - wy) * sx;
    out[3] = 0;
    out[4] = (xy - wz) * sy;
    out[5] = (1 - (xx + zz)) * sy;
    out[6] = (yz + wx) * sy;
    out[7] = 0;
    out[8] = (xz + wy) * sz;
    out[9] = (yz - wx) * sz;
    out[10] = (1 - (xx + yy)) * sz;
    out[11] = 0;
    out[12] = v[0];
    out[13] = v[1];
    out[14] = v[2];
    out[15] = 1;

    return out;
};

/**
 * Creates a matrix from a quaternion rotation, vector translation and vector scale, rotating and scaling around the given origin
 * This is equivalent to (but much faster than):
 *
 *     mat4.identity(dest);
 *     mat4.translate(dest, vec);
 *     mat4.translate(dest, origin);
 *     var quatMat = mat4.create();
 *     quat4.toMat4(quat, quatMat);
 *     mat4.multiply(dest, quatMat);
 *     mat4.scale(dest, scale)
 *     mat4.translate(dest, negativeOrigin);
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {quat4} q Rotation quaternion
 * @param {vec3} v Translation vector
 * @param {vec3} s Scaling vector
 * @param {vec3} o The origin vector around which to scale and rotate
 * @returns {mat4} out
 */
mat4.fromRotationTranslationScaleOrigin = function (out, q, v, s, o) {
    // Quaternion math
    var x = q[0],
        y = q[1],
        z = q[2],
        w = q[3],
        x2 = x + x,
        y2 = y + y,
        z2 = z + z,
        xx = x * x2,
        xy = x * y2,
        xz = x * z2,
        yy = y * y2,
        yz = y * z2,
        zz = z * z2,
        wx = w * x2,
        wy = w * y2,
        wz = w * z2,
        sx = s[0],
        sy = s[1],
        sz = s[2],
        ox = o[0],
        oy = o[1],
        oz = o[2];

    out[0] = (1 - (yy + zz)) * sx;
    out[1] = (xy + wz) * sx;
    out[2] = (xz - wy) * sx;
    out[3] = 0;
    out[4] = (xy - wz) * sy;
    out[5] = (1 - (xx + zz)) * sy;
    out[6] = (yz + wx) * sy;
    out[7] = 0;
    out[8] = (xz + wy) * sz;
    out[9] = (yz - wx) * sz;
    out[10] = (1 - (xx + yy)) * sz;
    out[11] = 0;
    out[12] = v[0] + ox - (out[0] * ox + out[4] * oy + out[8] * oz);
    out[13] = v[1] + oy - (out[1] * ox + out[5] * oy + out[9] * oz);
    out[14] = v[2] + oz - (out[2] * ox + out[6] * oy + out[10] * oz);
    out[15] = 1;

    return out;
};

/**
 * Calculates a 4x4 matrix from the given quaternion
 *
 * @param {mat4} out mat4 receiving operation result
 * @param {quat} q Quaternion to create matrix from
 *
 * @returns {mat4} out
 */
mat4.fromQuat = function (out, q) {
    var x = q[0],
        y = q[1],
        z = q[2],
        w = q[3],
        x2 = x + x,
        y2 = y + y,
        z2 = z + z,
        xx = x * x2,
        yx = y * x2,
        yy = y * y2,
        zx = z * x2,
        zy = z * y2,
        zz = z * z2,
        wx = w * x2,
        wy = w * y2,
        wz = w * z2;

    out[0] = 1 - yy - zz;
    out[1] = yx + wz;
    out[2] = zx - wy;
    out[3] = 0;

    out[4] = yx - wz;
    out[5] = 1 - xx - zz;
    out[6] = zy + wx;
    out[7] = 0;

    out[8] = zx + wy;
    out[9] = zy - wx;
    out[10] = 1 - xx - yy;
    out[11] = 0;

    out[12] = 0;
    out[13] = 0;
    out[14] = 0;
    out[15] = 1;

    return out;
};

/**
 * Generates a frustum matrix with the given bounds
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {Number} left Left bound of the frustum
 * @param {Number} right Right bound of the frustum
 * @param {Number} bottom Bottom bound of the frustum
 * @param {Number} top Top bound of the frustum
 * @param {Number} near Near bound of the frustum
 * @param {Number} far Far bound of the frustum
 * @returns {mat4} out
 */
mat4.frustum = function (out, left, right, bottom, top, near, far) {
    var rl = 1 / (right - left),
        tb = 1 / (top - bottom),
        nf = 1 / (near - far);
    out[0] = near * 2 * rl;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = near * 2 * tb;
    out[6] = 0;
    out[7] = 0;
    out[8] = (right + left) * rl;
    out[9] = (top + bottom) * tb;
    out[10] = (far + near) * nf;
    out[11] = -1;
    out[12] = 0;
    out[13] = 0;
    out[14] = far * near * 2 * nf;
    out[15] = 0;
    return out;
};

/**
 * Generates a perspective projection matrix with the given bounds
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {number} fovy Vertical field of view in radians
 * @param {number} aspect Aspect ratio. typically viewport width/height
 * @param {number} near Near bound of the frustum
 * @param {number} far Far bound of the frustum
 * @returns {mat4} out
 */
mat4.perspective = function (out, fovy, aspect, near, far) {
    var f = 1.0 / Math.tan(fovy / 2),
        nf = 1 / (near - far);
    out[0] = f / aspect;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = f;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = (far + near) * nf;
    out[11] = -1;
    out[12] = 0;
    out[13] = 0;
    out[14] = 2 * far * near * nf;
    out[15] = 0;
    return out;
};

/**
 * Generates a perspective projection matrix with the given field of view.
 * This is primarily useful for generating projection matrices to be used
 * with the still experiemental WebVR API.
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {Object} fov Object containing the following values: upDegrees, downDegrees, leftDegrees, rightDegrees
 * @param {number} near Near bound of the frustum
 * @param {number} far Far bound of the frustum
 * @returns {mat4} out
 */
mat4.perspectiveFromFieldOfView = function (out, fov, near, far) {
    var upTan = Math.tan(fov.upDegrees * Math.PI / 180.0),
        downTan = Math.tan(fov.downDegrees * Math.PI / 180.0),
        leftTan = Math.tan(fov.leftDegrees * Math.PI / 180.0),
        rightTan = Math.tan(fov.rightDegrees * Math.PI / 180.0),
        xScale = 2.0 / (leftTan + rightTan),
        yScale = 2.0 / (upTan + downTan);

    out[0] = xScale;
    out[1] = 0.0;
    out[2] = 0.0;
    out[3] = 0.0;
    out[4] = 0.0;
    out[5] = yScale;
    out[6] = 0.0;
    out[7] = 0.0;
    out[8] = -((leftTan - rightTan) * xScale * 0.5);
    out[9] = (upTan - downTan) * yScale * 0.5;
    out[10] = far / (near - far);
    out[11] = -1.0;
    out[12] = 0.0;
    out[13] = 0.0;
    out[14] = far * near / (near - far);
    out[15] = 0.0;
    return out;
};

/**
 * Generates a orthogonal projection matrix with the given bounds
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {number} left Left bound of the frustum
 * @param {number} right Right bound of the frustum
 * @param {number} bottom Bottom bound of the frustum
 * @param {number} top Top bound of the frustum
 * @param {number} near Near bound of the frustum
 * @param {number} far Far bound of the frustum
 * @returns {mat4} out
 */
mat4.ortho = function (out, left, right, bottom, top, near, far) {
    var lr = 1 / (left - right),
        bt = 1 / (bottom - top),
        nf = 1 / (near - far);
    out[0] = -2 * lr;
    out[1] = 0;
    out[2] = 0;
    out[3] = 0;
    out[4] = 0;
    out[5] = -2 * bt;
    out[6] = 0;
    out[7] = 0;
    out[8] = 0;
    out[9] = 0;
    out[10] = 2 * nf;
    out[11] = 0;
    out[12] = (left + right) * lr;
    out[13] = (top + bottom) * bt;
    out[14] = (far + near) * nf;
    out[15] = 1;
    return out;
};

/**
 * Generates a look-at matrix with the given eye position, focal point, and up axis
 *
 * @param {mat4} out mat4 frustum matrix will be written into
 * @param {vec3} eye Position of the viewer
 * @param {vec3} center Point the viewer is looking at
 * @param {vec3} up vec3 pointing up
 * @returns {mat4} out
 */
mat4.lookAt = function (out, eye, center, up) {
    var x0,
        x1,
        x2,
        y0,
        y1,
        y2,
        z0,
        z1,
        z2,
        len,
        eyex = eye[0],
        eyey = eye[1],
        eyez = eye[2],
        upx = up[0],
        upy = up[1],
        upz = up[2],
        centerx = center[0],
        centery = center[1],
        centerz = center[2];

    if (Math.abs(eyex - centerx) < glMatrix.EPSILON && Math.abs(eyey - centery) < glMatrix.EPSILON && Math.abs(eyez - centerz) < glMatrix.EPSILON) {
        return mat4.identity(out);
    }

    z0 = eyex - centerx;
    z1 = eyey - centery;
    z2 = eyez - centerz;

    len = 1 / Math.sqrt(z0 * z0 + z1 * z1 + z2 * z2);
    z0 *= len;
    z1 *= len;
    z2 *= len;

    x0 = upy * z2 - upz * z1;
    x1 = upz * z0 - upx * z2;
    x2 = upx * z1 - upy * z0;
    len = Math.sqrt(x0 * x0 + x1 * x1 + x2 * x2);
    if (!len) {
        x0 = 0;
        x1 = 0;
        x2 = 0;
    } else {
        len = 1 / len;
        x0 *= len;
        x1 *= len;
        x2 *= len;
    }

    y0 = z1 * x2 - z2 * x1;
    y1 = z2 * x0 - z0 * x2;
    y2 = z0 * x1 - z1 * x0;

    len = Math.sqrt(y0 * y0 + y1 * y1 + y2 * y2);
    if (!len) {
        y0 = 0;
        y1 = 0;
        y2 = 0;
    } else {
        len = 1 / len;
        y0 *= len;
        y1 *= len;
        y2 *= len;
    }

    out[0] = x0;
    out[1] = y0;
    out[2] = z0;
    out[3] = 0;
    out[4] = x1;
    out[5] = y1;
    out[6] = z1;
    out[7] = 0;
    out[8] = x2;
    out[9] = y2;
    out[10] = z2;
    out[11] = 0;
    out[12] = -(x0 * eyex + x1 * eyey + x2 * eyez);
    out[13] = -(y0 * eyex + y1 * eyey + y2 * eyez);
    out[14] = -(z0 * eyex + z1 * eyey + z2 * eyez);
    out[15] = 1;

    return out;
};

/**
 * Returns a string representation of a mat4
 *
 * @param {mat4} a matrix to represent as a string
 * @returns {String} string representation of the matrix
 */
mat4.str = function (a) {
    return 'mat4(' + a[0] + ', ' + a[1] + ', ' + a[2] + ', ' + a[3] + ', ' + a[4] + ', ' + a[5] + ', ' + a[6] + ', ' + a[7] + ', ' + a[8] + ', ' + a[9] + ', ' + a[10] + ', ' + a[11] + ', ' + a[12] + ', ' + a[13] + ', ' + a[14] + ', ' + a[15] + ')';
};

/**
 * Returns Frobenius norm of a mat4
 *
 * @param {mat4} a the matrix to calculate Frobenius norm of
 * @returns {Number} Frobenius norm
 */
mat4.frob = function (a) {
    return Math.sqrt(Math.pow(a[0], 2) + Math.pow(a[1], 2) + Math.pow(a[2], 2) + Math.pow(a[3], 2) + Math.pow(a[4], 2) + Math.pow(a[5], 2) + Math.pow(a[6], 2) + Math.pow(a[7], 2) + Math.pow(a[8], 2) + Math.pow(a[9], 2) + Math.pow(a[10], 2) + Math.pow(a[11], 2) + Math.pow(a[12], 2) + Math.pow(a[13], 2) + Math.pow(a[14], 2) + Math.pow(a[15], 2));
};

/**
 * Adds two mat4's
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the first operand
 * @param {mat4} b the second operand
 * @returns {mat4} out
 */
mat4.add = function (out, a, b) {
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    out[3] = a[3] + b[3];
    out[4] = a[4] + b[4];
    out[5] = a[5] + b[5];
    out[6] = a[6] + b[6];
    out[7] = a[7] + b[7];
    out[8] = a[8] + b[8];
    out[9] = a[9] + b[9];
    out[10] = a[10] + b[10];
    out[11] = a[11] + b[11];
    out[12] = a[12] + b[12];
    out[13] = a[13] + b[13];
    out[14] = a[14] + b[14];
    out[15] = a[15] + b[15];
    return out;
};

/**
 * Subtracts matrix b from matrix a
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the first operand
 * @param {mat4} b the second operand
 * @returns {mat4} out
 */
mat4.subtract = function (out, a, b) {
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
    out[3] = a[3] - b[3];
    out[4] = a[4] - b[4];
    out[5] = a[5] - b[5];
    out[6] = a[6] - b[6];
    out[7] = a[7] - b[7];
    out[8] = a[8] - b[8];
    out[9] = a[9] - b[9];
    out[10] = a[10] - b[10];
    out[11] = a[11] - b[11];
    out[12] = a[12] - b[12];
    out[13] = a[13] - b[13];
    out[14] = a[14] - b[14];
    out[15] = a[15] - b[15];
    return out;
};

/**
 * Alias for {@link mat4.subtract}
 * @function
 */
mat4.sub = mat4.subtract;

/**
 * Multiply each element of the matrix by a scalar.
 *
 * @param {mat4} out the receiving matrix
 * @param {mat4} a the matrix to scale
 * @param {Number} b amount to scale the matrix's elements by
 * @returns {mat4} out
 */
mat4.multiplyScalar = function (out, a, b) {
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    out[3] = a[3] * b;
    out[4] = a[4] * b;
    out[5] = a[5] * b;
    out[6] = a[6] * b;
    out[7] = a[7] * b;
    out[8] = a[8] * b;
    out[9] = a[9] * b;
    out[10] = a[10] * b;
    out[11] = a[11] * b;
    out[12] = a[12] * b;
    out[13] = a[13] * b;
    out[14] = a[14] * b;
    out[15] = a[15] * b;
    return out;
};

/**
 * Adds two mat4's after multiplying each element of the second operand by a scalar value.
 *
 * @param {mat4} out the receiving vector
 * @param {mat4} a the first operand
 * @param {mat4} b the second operand
 * @param {Number} scale the amount to scale b's elements by before adding
 * @returns {mat4} out
 */
mat4.multiplyScalarAndAdd = function (out, a, b, scale) {
    out[0] = a[0] + b[0] * scale;
    out[1] = a[1] + b[1] * scale;
    out[2] = a[2] + b[2] * scale;
    out[3] = a[3] + b[3] * scale;
    out[4] = a[4] + b[4] * scale;
    out[5] = a[5] + b[5] * scale;
    out[6] = a[6] + b[6] * scale;
    out[7] = a[7] + b[7] * scale;
    out[8] = a[8] + b[8] * scale;
    out[9] = a[9] + b[9] * scale;
    out[10] = a[10] + b[10] * scale;
    out[11] = a[11] + b[11] * scale;
    out[12] = a[12] + b[12] * scale;
    out[13] = a[13] + b[13] * scale;
    out[14] = a[14] + b[14] * scale;
    out[15] = a[15] + b[15] * scale;
    return out;
};

/**
 * Returns whether or not the matrices have exactly the same elements in the same position (when compared with ===)
 *
 * @param {mat4} a The first matrix.
 * @param {mat4} b The second matrix.
 * @returns {Boolean} True if the matrices are equal, false otherwise.
 */
mat4.exactEquals = function (a, b) {
    return a[0] === b[0] && a[1] === b[1] && a[2] === b[2] && a[3] === b[3] && a[4] === b[4] && a[5] === b[5] && a[6] === b[6] && a[7] === b[7] && a[8] === b[8] && a[9] === b[9] && a[10] === b[10] && a[11] === b[11] && a[12] === b[12] && a[13] === b[13] && a[14] === b[14] && a[15] === b[15];
};

/**
 * Returns whether or not the matrices have approximately the same elements in the same position.
 *
 * @param {mat4} a The first matrix.
 * @param {mat4} b The second matrix.
 * @returns {Boolean} True if the matrices are equal, false otherwise.
 */
mat4.equals = function (a, b) {
    var a0 = a[0],
        a1 = a[1],
        a2 = a[2],
        a3 = a[3],
        a4 = a[4],
        a5 = a[5],
        a6 = a[6],
        a7 = a[7],
        a8 = a[8],
        a9 = a[9],
        a10 = a[10],
        a11 = a[11],
        a12 = a[12],
        a13 = a[13],
        a14 = a[14],
        a15 = a[15];

    var b0 = b[0],
        b1 = b[1],
        b2 = b[2],
        b3 = b[3],
        b4 = b[4],
        b5 = b[5],
        b6 = b[6],
        b7 = b[7],
        b8 = b[8],
        b9 = b[9],
        b10 = b[10],
        b11 = b[11],
        b12 = b[12],
        b13 = b[13],
        b14 = b[14],
        b15 = b[15];

    return Math.abs(a0 - b0) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a0), Math.abs(b0)) && Math.abs(a1 - b1) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a1), Math.abs(b1)) && Math.abs(a2 - b2) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a2), Math.abs(b2)) && Math.abs(a3 - b3) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a3), Math.abs(b3)) && Math.abs(a4 - b4) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a4), Math.abs(b4)) && Math.abs(a5 - b5) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a5), Math.abs(b5)) && Math.abs(a6 - b6) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a6), Math.abs(b6)) && Math.abs(a7 - b7) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a7), Math.abs(b7)) && Math.abs(a8 - b8) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a8), Math.abs(b8)) && Math.abs(a9 - b9) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a9), Math.abs(b9)) && Math.abs(a10 - b10) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a10), Math.abs(b10)) && Math.abs(a11 - b11) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a11), Math.abs(b11)) && Math.abs(a12 - b12) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a12), Math.abs(b12)) && Math.abs(a13 - b13) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a13), Math.abs(b13)) && Math.abs(a14 - b14) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a14), Math.abs(b14)) && Math.abs(a15 - b15) <= glMatrix.EPSILON * Math.max(1.0, Math.abs(a15), Math.abs(b15));
};

var utils = "vec3 pal( in float t, in vec3 a, in vec3 b, in vec3 c, in vec3 d )\n{\n    return a + b*cos( 6.28318*(c*t+d) );\n}\n";

var vert = "uniform mat4 projectionMatrix;\nuniform mat4 viewMatrix;\nuniform mat4 modelMatrix;\nuniform float time;\nuniform float dx;\nuniform float waveAmplitude;\nattribute vec3 position;\nattribute vec3 offsets;\nvarying float vColor;\nvoid main(){\n   float tmp = time + dx;\n   float scale = sin(tmp + 10.0) * waveAmplitude;\n    vec4 model = modelMatrix * vec4(1.);\n    vec3 offsetPos = offsets + position;\n    vec4 pos = projectionMatrix * viewMatrix * modelMatrix * vec4(offsetPos,1.);\n    float osc = sin(tmp + 10.0 + pos.z) * waveAmplitude;\n    pos.y += osc * 2.0;\n    vColor = osc;\n    gl_Position = pos;\n}";

var frag = "uniform vec2 resolution;\nvarying float vColor;\nuniform float time;\nvoid main(){\n    vec2 p = gl_FragCoord.xy / resolution.xy;\n    p.xy *= sin(time);\n    vec3 col = pal( p.x, vec3(0.5,0.5,0.5),vec3(0.5,0.5,0.5),vec3(1.0,0.0,1.0),vec3(0.0,0.33,0.67) );\n    float f = fract(p.y * 7.0);\n    col += (1.0/255.0);\n    gl_FragColor = vec4(col,1.);\n}";

var _createClass$4 = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck$4(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// add shaders
var Field = function () {
    function Field(gl) {
        var _ref = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {},
            _ref$width = _ref.width,
            width = _ref$width === undefined ? window.innerWidth : _ref$width,
            _ref$height = _ref.height,
            height = _ref$height === undefined ? window.innerHeight : _ref$height,
            _ref$resolution = _ref.resolution,
            resolution = _ref$resolution === undefined ? 8 : _ref$resolution;

        _classCallCheck$4(this, Field);

        this.gl = gl;
        this.model = mat4.create();
        this.width = width;
        this.height = height;
        this.resolution = resolution;

        this.shader = createShader(gl, {
            vertex: vert,
            fragment: [utils, frag],
            uniforms: ['projectionMatrix', 'viewMatrix', 'modelMatrix', 'time', 'dx', 'waveAmplitude', 'resolution']
        });

        this._build();
        this.wavePeriod = 5.0;
        this.waveAmplitude = 10.0;
        this.dx = Math.PI * Math.PI / this.wavePeriod * resolution;
        this.theta = 0;
    }

    _createClass$4(Field, [{
        key: 'translate',
        value: function translate(x, y, z) {
            mat4.translate(this.model, this.model, [x, y, z]);
        }
    }, {
        key: 'rotate',
        value: function rotate(val) {
            mat4.rotateX(this.model, this.model, toRadians(val));
        }
    }, {
        key: 'turntable',
        value: function turntable(val) {

            mat4.rotateZ(this.model, this.model, toRadians(val));
        }
    }, {
        key: '_build',
        value: function _build() {
            var gl = this.gl;
            var positions = createVBO(gl);
            var vao = createVAO(gl);

            //========== BUILD THE BASE SHAPE =========
            var shapedata = [0.0, 1.0, 0.0, -1.0, -1.0, 0.0, 1.0, -1.0, 0.0];
            var triangle = createVBO(gl);
            vao.bind();
            triangle.bind();
            triangle.bufferData(shapedata);
            vao.addAttribute(this.shader, 'position');
            triangle.unbind();
            vao.unbind();

            // ========== SETUP POSITIONS ==============
            var posdata = [];
            var cols = this.width / this.resolution;
            var rows = this.height / this.resolution;
            for (var i = 0; i < cols; ++i) {
                for (var j = 0; j < rows; ++j) {
                    posdata.push(i * this.resolution, j * this.resolution, 0);
                }
            }

            // =========== SETUP ATTRIBUTES ============
            vao.bind();
            positions.bind();
            positions.bufferData(posdata);
            vao.addAttribute(this.shader, 'offsets');
            vao.makeInstancedAttribute('offsets');
            positions.unbind();
            vao.unbind();

            // total number of vertices to draw
            this.num = posdata.length / 3;

            this.vao = vao;
        }
    }, {
        key: 'draw',
        value: function draw(camera, time) {
            var gl = this.gl;
            this.theta += 0.01;
            gl.enable(gl.BLEND);
            gl.disable(gl.DEPTH_TEST);
            gl.blendFunc(gl.SRC_ALPHA, gl.ONE);
            // bind shader and uniforms
            this.shader.bind();
            this.shader.set4x4Uniform('projectionMatrix', camera.projection);
            this.shader.set4x4Uniform('viewMatrix', camera.view);
            this.shader.set4x4Uniform('modelMatrix', this.model);
            this.shader.uniform('time', this.theta);
            gl.uniform2fv(this.shader.uniforms['resolution'], [window.innerWidth, window.innerHeight]);

            this.shader.uniform('dx', this.dx);
            this.shader.uniform('waveAmplitude', this.waveAmplitude);

            this.vao.bind();
            gl.drawInstancedArrays(gl.TRIANGLES, 0, 3, this.num);
            this.vao.unbind();
        }
    }]);

    return Field;
}();

// build renderer
var gl = createRenderer();
gl.setFullscreen().attachToScreen();

// build camera
var camera = index$1({
    fov: Math.PI / 4,
    position: [0, 0, 300],
    near: 0.1,
    far: 10000,
    viewport: [0, 0, window.innerWidth, window.innerHeight]
});

var controls = createOrbitControls({
    element: gl.canvas,
    distanceBounds: [0.0, 300],
    distance: 0.0001
});
controls.enable();
//====== RENDERING =======
var fbo = createFBO(gl, {
    width: window.innerWidth,
    height: window.innerHeight
});
var drawQuad$$1 = createQuad(gl, {
    withTexture: true
});
var bloom = new BloomPass(gl);
var glitch = new GlitchPass(gl);
var composer = new Composer(gl, fbo.drawTexture, bloom, glitch);
//
//====== OBJECTS ===========
var f = new Field(gl);
var f2 = new Field(gl);
f.translate(-window.innerWidth / 2, -100, window.innerWidth / 4);
f2.translate(-window.innerWidth / 2, 100, window.innerWidth / 4);

f.rotate(-90);
f2.rotate(-90);

//======== ANIMATE ========
animate();
function animate() {
    var time = Date.now() * 0.0000001;

    // make sure viewport is reset
    var viewport = [0, 0, window.innerWidth, window.innerHeight];
    camera.viewport = viewport;
    camera.update();

    // update controls
    //controls.update()
    //controls.copyInto(camera.position, camera.direction, camera.up)

    // clear the screen
    gl.clearScreen();

    fbo.bind();
    gl.clearScreen();
    // draw the objects
    f.draw(camera, time);
    f2.draw(camera);
    fbo.unbind();

    //====== RUNNING IN POST-PROCESSING COMPOSER ===========
    composer.run();
    composer.getOutput().bindTexture();
    drawQuad$$1.draw();
    composer.getOutput().unbindTexture();
    //====== RUNNING WITH BLOOM BY ITSELF ===========
    //bloom.runPass();
    //bloom.getOutput().bindTexture();
    //drawQuad.draw()
    //bloom.getOutput().unbindTexture();


    //====== NO POST PROCESSING ===========
    //fbo.bindTexture();
    //drawQuad.draw()
    //fbo.unbindTexture();

    requestAnimationFrame(animate);
}

})));
